# textcat-legacy

Application to compose avalanche bulletins and translate to multiple languages

Technically, textcat-legacy is a Maven solution composed by these projects:

- bulletinedit-textcat (the parent project)
- bulletinedit-textcat-core
- bulletinedit-textcat-dbloader
- bulletinedit-textcat-editor
- bulletinedit-textcat-myBatisImpl

## Build

```bash
# Get the source
$ git clone https://gitlab.com/albina-euregio/textcat-legacy.git

# package bulletinedit-textcat
$ mvn package -Djdbc.url=jdbc:postgresql://localhost:5432/postgres -Djdbc.username=postgres -Djdbc.password=postgres

```

## Deployment

To deploy, rename the .war file from `bulletinedit-textcat-editor\target` to `textcat.war` and publish to Tomcat.

## Translation

This project uses Transifex for its translations: https://www.transifex.com/albina-euregio/textcat-legacy/dashboard/
