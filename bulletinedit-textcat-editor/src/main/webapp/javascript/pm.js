var PM = function () { // eslint-disable-line no-redeclare, no-unused-vars

	//------------------------------------------------------------------------------------------------
	// Catalog
	//------------------------------------------------------------------------------------------------

	function refreshPhrases() {
		var filter = $('#filterInput').val();
		var words = (filter == "" | filter == undefined ? [] : filter.split(" "));
		var regExps = new Array();

			//unique for highlight
			words = words.unique();
			var words2 = new Array();
			for (var i = 0; i < words.length; i++) {
				if (words[i].length > 3) {
					words2.push(words[i].replace("(", "").replace(")",""));
				}

			}
			words = words2
		
		for (i = 0; i < words.length; i++)
			regExps.push(new RegExp(words[i], 'i'));

		$('tr.sentence:has(:checked)').each(function () {
			$('.singleOption', $(this)).each(function () { highlightCell(this, words, regExps); });

			$('.phraseTable', $(this)).each(function () {
				var cellToSelect = null;
				var maxHitRate = 0;
				$('.phraseRow > td', $(this)).each(function () {
					var hitRate = highlightCell(this, words, regExps);

					if (maxHitRate < hitRate) {
						cellToSelect = this;
						maxHitRate = hitRate;
					} else if (maxHitRate == hitRate)
						cellToSelect = null;
				});
				if (maxHitRate > 0 && cellToSelect != null)
					onPhraseClick(cellToSelect, false);
			});

		});
		$('#accordion').accordion("refresh");

	}

	function highlightCell(cell, words, regExps) {
		var phrase = phraseArray[cell.getAttribute('id')];
		var filteredOptions = filterPhraseOptions(cell.getAttribute('id'), words, regExps);
		var text = "";
		
		//hide for regions
		if (cell.outerHTML.indexOf("visibility: hidden;") < 0)
			text = highlight(phrase, words, regExps, filteredOptions);

		cell.innerHTML = text;
		return text.length - phrase.length;
	}

	function filterPhraseOptions(phraseId, words, regExps) {
		if (words.length == 0) return [];
		var result = new Array();
		var options = getPhraseOptions(phraseId);
		for (var i = 0; i < options.length; i++) {
			var notFound = true;
			var phrases = optionArray[options[i]];
			if (phrases == undefined) continue;
			for (var j = 0; j < phrases.length && notFound; j++) {
				var txt = phraseArray[phrases[j]];
				for (var k = 0; k < regExps.length && notFound; k++) {
					var pos = txt.search(regExps[k]);
					if (pos != -1) {
						result.push(i);
						notFound = false;
					}
				}
			}
		}
		return result;
	}

	function highlight(text, words, regExps, filteredOptions) {
		var froms = new Array();
		var tos = new Array();
		for (var i = 0; i < words.length; i++) {
			var pos = text.search(regExps[i]);
			if (pos != -1) {
				froms.push(pos + 0);
				tos.push(pos + words[i].length);
			}
		}
		// highlight filtered subOptions
	
		var regexp = new RegExp("{.+?}", 'gi');
		var list;
		if ((list = text.match(regexp)) != null)
			for (i = 0; i < filteredOptions.length; i++) {
				pos = text.search(list[filteredOptions[i]]);
				if (pos != -1) {
					froms.push(pos + 0);
					tos.push(pos + list[i].length);
				}
			}
		// 
	
		PMUTIL.cleanRanges(froms, tos);
		return highlightRanges(text, froms, tos);
	}

	function highlightRanges(text, froms, tos) {
		if (froms.length == 0 || tos.length == 0 || froms.length != tos.length) return text;
		var result = "";
		var index = 0;
		for (var i = 0; i < froms.length; i++) {
			result = result.concat(text.substring(index, froms[i]) + "<span class='filter'>" + text.substring(froms[i], tos[i]) + "</span>");
			index = tos[i];
		}
		if (index < text.length)
			result = result.concat(text.substring(index));
		return result;
	}

	function getPhraseOptions(id) {
		var options = new Array();
		var regexp = new RegExp("{(.+?)}", 'gi');
		var list;
		var phraseId;
		if ((list = phraseArray[id].match(regexp)) != null)
			for (var i = 0; i < list.length; i++)
				if ((phraseId = getOptionId(list[i])) != null)
					options.push(phraseId);
		return options;
	}

	function getPhraseOptionIds(id) {
		var options = new Array();
		var regexp = new RegExp("{(.+?)}", 'gi');
		var list;
		if ((list = cleanedPhraseArray[id].match(regexp)) != null)
			for (var i = 0; i < list.length; i++)
				options.push(list[i].replace("#1}", "").replace("{", ""));

		return options;
	}

	function getOptionId(optionName) {
		for (var i = 0; i < optionNames.length; i++)
			if (optionNames[i] == optionName)
				return i;
		return optionName.replace(/^(\d+)(#\d*)+/g, '$1');
	}

	function getPhraseOption(id, subPhrases, raw, depth, nr) {
		var phrase = phraseArray[id];

		var subOptions = getPhraseOptions(id);

		var regexp = new RegExp("{.+?}", 'gi');
		var list = null;
		var listmade = new Array();
		var numOccurence = [];
		numOccurence = findDuplicates(subOptions)
		if (numOccurence.length > 0) {
			var seatchtxt = phrase.match(regexp)[subOptions.indexOf(numOccurence[0])];
			var nth = 0;
			phrase = phrase.replace(new RegExp(seatchtxt, 'g'), function (match) {
				nth++;
				return (nth === 2) ? seatchtxt.replace("}", "°}") : match;
			});
		}
		phraseArray[id] = phrase;
		var subOptionIds = getPhraseOptionIds(id);
		if ((list = phrase.match(regexp)) != null) {
			var item_No = -1;
			var item_Desc = "";
			var itemNorm = -1
			for (var ii = 0; ii < list.length; ii++) {
				var index2 = (depth + nr) * 10 + ii;

				if (!raw) {
					if (subOptions[ii].indexOf('_NO') > 0) {
						item_No = index2;
						item_Desc = subOptions[ii].replace('_NO', '');
						itemNorm = list.indexOf(item_Desc);
						break;
					}
				}
			}

			for (var i = 0; i < list.length; i++) {
				var text = null;
				var index = (depth + nr) * 10 + i;
				if (subPhrases[index] == undefined || subPhrases[index] == "") {
					text = list[i];
				} else {
					text = (raw ? " " : "{") + getPhraseOption(subPhrases[index], subPhrases, raw, depth + 1, i) + (raw ? " " : "}");
				}

				if (!raw) {
					if (subOptions[i].indexOf('_NO') > -1) {
						//sync item
						text = "<span class='subOption" + depth + "' onClick='PM.openPhraseOption(this," + index + "," + subOptionIds[i] + ",event," + itemNorm + ")'>" + text + "</span>";
					} else {
						if (subOptions[i].indexOf(item_Desc) > -1) {
							//sync item_no
							text = "<span class='subOption" + depth + "' onClick='PM.openPhraseOption(this," + index + "," + subOptionIds[i] + ",event," + item_No + ")'>" + text + "</span>";
						}
						else {
							text = "<span class='subOption" + depth + "' onClick='PM.openPhraseOption(this," + index + "," + subOptionIds[i] + ",event,-1)'>" + text + "</span>";
						}
					}
				}

				phrase = phrase.split(list[i]).join(text);

				if (text != list[i] & !listmade.includes(list[i]) > 0) {
					//no ripetition;
					// only selected	
					if (!listmade.includes(list[i]) > 0)
						listmade.push(list[i]);
				}
			}
		}
		return phrase;
	}

	function findDuplicates(data) {
		var result = [];
		data.forEach(function (element, index) {
			if (data.indexOf(element, index + 1) > -1) {
				if (result.indexOf(element) === -1) {
					result.push(element);
				}
			}
		});
		return result;
	}

	function openPhraseOption(element, index, optionId, evt, colindex) {
		closePhraseOption();
		$(element).addClass('subOptionSelected');
		var phrasesData = new Array();
		optionArray[optionId].forEach(function (item) {
			var style = "";
			var text = "";
			text = phraseArray[item];
			if (rgnArray[item] != ' ') {
				if (rgnArray[item].indexOf(srcRgn) == -1) {
					text = "";
					style = "display:none"
				}
				/*else
					phrasesData.push({ Id: item, Text: text, Rgn: style });
				}else*/
			}
			phrasesData.push({ Id: item, Text: text, Rgn: style, ItemNO: colindex });
		});
		$('#phraseOptionTemplate').tmpl({ Index: index, Phrases: phrasesData })
			.insertBefore($('tr.phraseRow:first', $(element).parents('.phraseTable')));
		if (evt)
			evt.stopPropagation();
		return false;
	}

	function onPhraseClick(cell, once) {
		closePhraseOption();
		$(cell.parentNode.parentNode.rows[1].cells[0])
			.html(getPhraseOption($(cell).attr('id'), [], false, 0, 0, -99))
			.attr({
				id: $(cell).attr('id'),
				class: 'selectedOption',
				subPhrases: []
			});

		//Clesius
		//if _NO must set also other header and viceversa, only in italian
		if (!once) {
			var optionData = new Array();
			var idSentence = cell.parentNode.parentNode.parentNode.parentNode.parentNode.id;
			sentenceArray[idSentence].forEach(function (item) { optionData.push(getOptionData(item, true, idSentence)); });

			var nomeDiv = (cell.parentNode.parentNode.parentNode.id);

			if (nomeDiv.indexOf("_NO") > 0) {

				var row_index = $(cell).parent().index();
				var col_index = $(cell).index();

				var nometable = replDiv(nomeDiv.replace("_NO", "")).replace("§", "\\§");
				var table2 = $("#" + nometable);
				cell = table2[0].rows[row_index].cells[col_index];

				onPhraseClick(cell, true);
			} else {
				nomeDiv = nomeDiv + "_NO";
				var result = $.grep(optionData, function (e) { return e.Id == nomeDiv; });
				if (result.length) {
					row_index = $(cell).parent().index();
					col_index = $(cell).index();
					nometable = result[0].Id.replace("§", "\\§");
					table2 = $("#" + nometable);
					cell = table2[0].rows[row_index].cells[col_index];

					onPhraseClick(cell, true);
				}
			}
		}
	}

	function closePhraseOption() {
		$('#phraseOptionId').remove();
		$('.subOption0, .subOption1').removeClass('subOptionSelected');
	}

	function onSubPhraseClick(element, itemNO, stopIt) {
		var index = $('#phraseOptionId').attr('index');
		var cell = $(element).parents('.phraseTable').find('.selectedOption');

		var subPhrases = cell.attr('subPhrases').split(",");
		subPhrases[index] = $(element).attr('id');
		// clear subsubPhrases
		if (index < 10) {
			var start = (Number(index) + 1) * 10;
			for (var i = start; i < start + 10; i++)
				delete subPhrases[i];
		}

		//check if twice
		//get pos of selected span
		var posParent = -99;
		i = 1;
		var spanSelected = $(element).parents('.phraseTable').find('.subOptionSelected');
		$('.subOption0').each(function () {
			if (spanSelected[0] == $(this)[0])
				posParent = i;
			if (spanSelected[0].innerText == $(this)[0].innerText)
				i += 1;
		});


		cell.attr('subPhrases', subPhrases)
			.html(getPhraseOption($(cell).attr('id'), subPhrases, false, 0, 0, posParent));

		// highlight the suboption
		if (index < 10)
			$('.subOption0:eq(' + index + ')', cell).addClass('subOptionSelected');
		else {
			i = Math.floor(index / 10) - 1;
			var r = index % 10;
			$('.subOption0:eq(' + i + ')', cell).find('.subOption1:eq(' + r + ')').addClass('subOptionSelected');
		}

		if (!stopIt)
			stopIt = false;
		//set also subOptionNO
		if (itemNO >= 0 && !stopIt) {
			var spanToSync = $(element).parents('.phraseTable').find('.subOption0')[itemNO];
			spanToSync.onclick();
			var table = $(".phraseOptionTable")[0];
			var elTosync = table.rows[element.parentNode.rowIndex].cells[0];
			elTosync.attributes[2].nodeValue = elTosync.attributes[2].nodeValue.replace(")", ",true)")
			elTosync.onclick(event);
		}

	}

	function getSentence(id, expanded) {
		var optionData = new Array();
		sentenceArray[id].forEach(function (item) { optionData.push(getOptionData(item, expanded, id)); });
		var data = { Title: sentenceNames[id], Id: id, Options: optionData, ContextPath: contextPath };
		return $('#sentenceTemplate').tmpl(data);
	}

	function getOptionData(option, expanded, idSent) {
		var phrases = optionArray[option];
		if (phrases == undefined || phrases.length == 0) return null;

		var data;

		//Clesius
		//Id: replDiv(optionNames[option]) --> for sync ita, never for singleOption
		var style = "";
		var text = "";
		if (phrases.length == 1) {
			if (rgnArray[phrases[0]] != ' ' && rgnArray[phrases[0]].indexOf(srcRgn) == -1)
				style = "display:none;"
			else
				text = phraseArray[phrases[0]];

			data = { Id: phrases[0], Class: 'singleOption', Title: optionHeaders[option], Text: text, Phrases: [], Rgn: style };

		} else {
			data = { Id: idSent + "_" + replDiv(optionNames[option]), Class: 'openOption', Title: optionHeaders[option], Text: t("pleaseSelect"), Phrases: [] };

			if (expanded)
				phrases.forEach(function (item) {
					style = "";
					if (rgnArray[item] != ' ' && rgnArray[item].indexOf(srcRgn) == -1)
						style = "display:none;"

					data.Phrases.push({ Id: item, Class: 'phrase', Rgn: style });
				})
		}
		return data;
	}

	function onSentenceClick(el) {
		var newSentence = getSentence(el.parentNode.parentNode.id, el.checked);
		var ids = el.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.firstElementChild.id;
		//chek if new sentence
		if (ids === "-1")
			newSentence["0"].cells["0"].childNodes[5].remove();
		$(el.parentNode.parentNode).replaceWith(newSentence);
		$('.phraseRow', newSentence).css('visibility', el.checked ? 'visible' : 'collapse');
		if (el.checked)
			$(':checkbox', newSentence).attr('checked', 'checked');
		else
			$(':checkbox', newSentence).removeAttr('checked');
		refreshPhrases();
	}
	
	function search(force,db) {
		setWarning('');
		$('#filterInput')[0].selectionDirection = "forward";

		function setStatusSentencesFound(n) {
			setStatus(n === 1 ? t('foundSentence', n) : t('foundSentences', n));
		}

		if ($('#filterInput').val().length > 1) {
			setStatus(t('pleaseWaitSearching'));
			$(".wait").show();
			
			//if entire sentence call new api
			if (db) {
				try {
					return $.ajax({
						type: 'GET',
						url: searchURL,
						data: {
							l: srcLang,
							dn: srcDomain,
							se: 'start',
							query: $('#filterInput').val()
						},
						dataType: "json",
						success: function (result) {
							$("#wait").show();
							// TODO use clean data structure
							if (result.startWithComma && result.startWithComma.length) {
								setWarning(t('warnSentencePrefix', result.startWithComma.join(', ')));	
							}
							if (result.sentences && result.sentences.length) {
								var def = convertDBID(result.sentences.join("."), true);
								filtered = def.split(".")
	
								buildSentences(filtered);
								expandOptions(filtered.length == 1);
								refreshPhrases();
								//Hide save button
								$("#sTable").find('tr').each(function () {
									if ($(this).find('td:eq(0)')[0].childNodes.length > 5)
										$(this).find('td:eq(0)')[0].childNodes[5].remove();
								});
								setStatusSentencesFound(filtered.length);
							} else {
								setStatusSentencesFound(0);
								$('#sTable').empty();
							}
							$("#wait").hide();
						}
					});

				} catch (e) {
					setStatusSentencesFound(length);
					$('#sTable').empty();
					$("#wait").hide();
					return '';
				}
			} else {
				filtered = getMatchingSentences($('#filterInput').val().split(" "),srcLang);

				//limit search result
				if (filtered.length > 50 && !force)
					filtered = [];

				buildSentences(filtered);
				expandOptions(filtered.length == 1);
				refreshPhrases();
				//Hide save button
				$("#sTable").find('tr').each(function () {
					if ($(this).find('td:eq(0)')[0].childNodes.length > 5)
						$(this).find('td:eq(0)')[0].childNodes[5].remove();
				});
				setStatusSentencesFound(filtered.length);
				$("#wait").hide();
			}
		}
		if ($('#filterInput').val().length == 0) {
			$('#sTable').empty();
		}
	}

	function getMatchingSentences(words) {
		var filtered = [];
		for (var i = 0; i < words.length; i++) {
			if (words[i].length == 0) continue;
			//if (stopWords.includes(words[i])) continue;
			var regExp = new RegExp(words[i], 'i');
			//var regExp = RegExp("\\b"+words[i]+"\\b",'i');
			var tmp = new Array();
			for (var j in indexArray)
				if (j.match(regExp))
					tmp = tmp.concat(indexArray[j]);

			//if (tmp.length>0)		
				filtered = (i == 0) ? PMUTIL.unique(tmp) : PMUTIL.intersection(filtered, PMUTIL.unique(tmp));
		}
		return filtered;
	}

	function buildSentences(list) {
		$('#sTable').empty();
		list.forEach(function (item) { getSentence(item, list.length == 1).appendTo('#sTable'); });
		//expandOptions(isExpanded);
	}

	function reloadCatalog() {
		// force reload
		sentenceArray = new Array();
		optionArray = new Array();
		phraseArray = new Array();
		rgnArray = new Array();

		cleanedPhraseArray = new Array();
		regExpArray = new Array();
		indexArray = new Array();

		sentenceNames = new Array();
		optionNames = new Array();
		optionHeaders = new Array();
		optionRgn = new Array();

		$.ajaxSetup({
			async: false
		});

		loadCatalog();

		$.ajaxSetup({
			async: true
		});

		// for init
		count = 4;
	}

	function loadCatalog() {
		var config = {dn: srcDomain, l: srcLang};
		jQuery.getJSON(sentenceURL, config, putSentences);
		jQuery.getJSON(optionURL, config, function (data) {
			putOptions(data);
			jQuery.getJSON(phraseURL, config, putPhrases);
		});
		jQuery.getJSON(indexURL, config, putIndexes);	
	}

	function previewSentence(dbID) {
		//always reload json file
		reloadCatalog();
		init();
		expand($("div.textcat.tch"));
		filtered = [];
		// convert dbid to position
		var def = convertDBID(dbID, true);
		filtered.push(def)
		buildSentences(filtered);
		expandOptions(true);
		refreshPhrases();

		//Hide all for preview
		$(".textcat")[0].childNodes[1].remove();
		$(".s_panel")[0].childNodes[0].remove();

		$("#pmdialogSearch")[0].childNodes[0].remove();
		$("#pmdialogSearch")[0].childNodes[0].remove();
		$("#pmdialogSearch")[0].childNodes[0].remove();
		$("#pmdialogSearch")[0].childNodes[0].remove();
		$("#pmdialogSearch")[0].childNodes[0].remove();
		$("#pmdialogSearch")[0].childNodes[0].remove();
		$("#pmdialogSearch")[0].childNodes[0].remove();
		$("#pmdialogSearch")[0].childNodes[0].remove();
		//Hide save button
		$("#sTable").find('tr').each(function () {
			if ($(this).find('td:eq(0)')[0].childNodes.length > 5) {
				$(this).find('td:eq(0)')[0].childNodes[5].remove();
				//	$(this).find('td:eq(0)')[0].childNodes[3].remove();
			}

		});

	}

	function expandOptions(flag) {
		$('.phraseRow').css('visibility', flag ? 'visible' : 'collapse');
		// requires jquery 1.6.2: $(':checkbox', $('tr.sentence')).prop("checked", flag);
		if (flag)
			$(':checkbox', $('tr.sentence')).attr('checked', 'checked');
		else
			$(':checkbox', $('tr.sentence')).removeAttr('checked');
	}

	function addSentenceClick(row, insert) {
		if ($('.openOption', row).length > 0) {
			alert(t('incompleteSentence'));
			return;
		}
		var errors = [];
		var options = [];
		$('.selectedOption, .singleOption', row).each(function () {
			var elements = [];
			var subPhrases = this.textContent.match(/{/g);
			var undefinedSubPhrases = subPhrases == null ? 0 : subPhrases.length;
			subPhrases = $(this).attr('subPhrases');
			if (!(subPhrases == '' || subPhrases == undefined)) {
				var list = subPhrases.split(',');
				for (var i = 0; i < list.length; i++)
					if (list[i] != '')
						undefinedSubPhrases--;
				for (i = 0; i < list.length && i < 10; i++) {
					var sub = list.slice((i + 1) * 10, (i + 1) * 10 + 9).filter(function (el) { return (el != ''); }).join(',');
					elements.push(list[i] + (sub == '' ? '' : '[' + sub + ']'));
				}
			}
			if (undefinedSubPhrases > 0) {
				errors.push(i('incompletePhrase', this.textContent));
			}
			options.push($(this).attr('id') + (elements.length == 0 ? '' : '[' + elements.filter(function (el) { return (el != ''); }).join(',') + ']'));
		});
		if (errors.length > 0) {
			alert(errors.join('\n'));
		} else {
			var def = $(row).attr('id') + '[' + options.join(',') + ']';
			//setStatus(convertDBID(def,false));
			if (insert) {
				appendSentence(null, def);
				updateValue(divPointer);
				$('#sTable').empty();
				$('#filterInput').val('');
				setStatus('');
				refreshPhrases();
				divTextcat = divPointer[0];

				if (($('input.textcat')[0].value.split(DELIM).length == 1 | (location.href.indexOf('preview') > -1)) & divTextcat.childNodes[0].tagName != "IMG") {
					_collapseButton().appendTo(divTextcat);
				}
			}
			else {
				if (location.href.indexOf('preview') > 0) {
					_collapseButton().appendTo(divTextcat);
				}
				replaceSentence(def);
				updateValue(divPointer);
			}

			//$('#send-message-2-albina').show();
		}
		$('#accordion').accordion({ active: false });
	}

	function setSentence(def) {

		var options = [];
		buildSentences((def == undefined || def == null) ? [] : [extract(def, options)]);

		var i = 0;
		$('.openOption, .singleOption').each(function () {
			var subOptions = [];
			var id = extract(options[i++], subOptions);
			subOptions = fakeSubOptions(subOptions);
			if ($(this).hasClass('openOption'))
				$(this)
					.html(getPhraseOption(id, subOptions, false, 0, 0))
					.attr({
						id: id,
						class: 'selectedOption',
						subPhrases: subOptions
					});
		});
		expandOptions(true);
		refreshPhrases();
	}

	function createSentence(def) {
		if (def == undefined || def == null) return '';
		var phrases = [];
		var options = [];

		try {
			extract(def, options);
			options.forEach(function (item) {
				var subOptions = [];
				var id = extract(item, subOptions);
				phrases.push(getPhraseOption(id, fakeSubOptions(subOptions), true, 0, 0, -99));
			});
			// trap <br>
			if (phrases[0]=="<br/>"){
				phrases[0] = "&lt;br&gt;";
			}
				
				
			return formatSentence(phrases.join(' '));
		} catch (e) {
			return $('<span class="empty">').text(t('invalidText'));
		}
	}

	function fakeSubOptions(subOptions) {
		var result = [];
		for (var i = 0; i < subOptions.length && i < 10; i++) {
			var subsubOptions = [];
			result[i] = extract(subOptions[i], subsubOptions);
			for (var j = 0; j < subsubOptions.length; j++)
				result[(i + 1) * 10 + j] = subsubOptions[j];
		}
		return result;
	}

	function convertDBID(def, from) {
		var result = [];
		var map = from ? DBIDMap : IDMap;
		var char = from ? '-' : '';
		def.split(DELIM).forEach(function (item) {
			item = item.replace(/^(\d+)/g, function (a, b) { return map.s[b + char]; });
			result.push(item.replace(/([^^\d])([\d]+)/g, function (a, b, c) { return b + map.p[c + char]; }));
		});
		return result.join(DELIM);
	}

	function extract(def, options) {
		if (def == null || def.length == 0) return null;
		// "98[1526[502,512],6801,6799]" => id="98" options=[ "1526[502,512]", "6801", "6799" ]
		// 13[5059,5062,5078[2596[6695],3049[6694],5748[6551,6565]],5081]
		var regexp = /^(\d+)(?:\[(.*)\])?$/g;
		var match = regexp.exec(def);
		options.length = 0;
		var depth = 0;
		if (match != null) {
			if (match[2] != undefined) {
				var letters = match[2].split('');
				for (var i = 0; i < letters.length; i++) {
					if (letters[i] == '[') depth++;
					if (letters[i] == ']') depth--;
					if ((depth == 0) && (letters[i] == ',')) letters[i] = ';';
				}
				letters.join('').split(';').forEach(function (item) { options.push(item); });
			}
		}
		return match[1];
	}

	function formatSentence(string) {
		if (string == null || string == undefined || string.length == 0) return string;
		// delete multiple spaces
		string = string.replace(/[ ]+/g, ' ');
		// delete spaces before comma and point
		string = string.replace(/[ ]+,/g, ',');
		string = string.replace(/[ ]+\./g, '.');
		// delete spaces at the beginning
		string = string.replace(/^[ ]+/g, '');
		// start with uppercase letter
		string = string[0].toUpperCase() + string.slice(1);
		return string;
	}

	//------------------------------------------------------------------------------------------------
	// Catalog/Editor - Joker-Sentence
	//------------------------------------------------------------------------------------------------

	var isJokerSentenceFormOpen = false;

	function openJokerSentenceForm() {
		if (!isJokerSentenceFormOpen) {

			isJokerSentenceFormOpen = true;
			$('#jokerSentenceFormTemplate').tmpl({ ContextPath: contextPath }).appendTo($('#dialog_jokerSentenceArea'));
			//$('#jokerSentenceForm_de').val($('#filterInput').val());
			$('#jokerSentenceForm_de').focus();
			//$('#sTable').remove();
		}
	}

	function closeJokerSentenceForm() {
		if (isJokerSentenceFormOpen) {
			$('#dialog_jokerSentenceArea').empty();
			isJokerSentenceFormOpen = false;
		}
	}

	function saveJokerSentence() {
		var bValid = true;
		var jokerde = $('#jokerSentenceForm_de');
		var jokeren = $('#jokerSentenceForm_en');
		var jokerfr = $('#jokerSentenceForm_fr');
		var jokerit = $('#jokerSentenceForm_it');
		var allFields = $([]).add(jokerde).add(jokeren).add(jokerfr).add(jokerit)

		allFields.removeClass("ui-state-error");

		bValid = bValid && checkValue(jokerde, "deutschen");
		bValid = bValid && checkValue(jokeren, "englischen");
		bValid = bValid && checkValue(jokerfr, "französischen");
		bValid = bValid && checkValue(jokerit, "italienischen");

		if (bValid) {
			var js = new Object();
			js.de = jokerde.val();
			js.en = jokeren.val();
			js.fr = jokerfr.val();
			js.it = jokerit.val();

			sendSentence(js);
			closeJokerSentenceForm();
		}
	}

	var wait = 0;
	function sendSentence(jokerSentence) {
		$.post(
			saveJokerURL,
			{
				jokerde: jokerSentence.de,
				jokeren: jokerSentence.en,
				jokerfr: jokerSentence.fr,
				jokerit: jokerSentence.it,
				l: srcLang,
				dn: srcDomain,
			},
			function (dbid) {
				reloadCatalog();
				displayJoker(dbid);
			}
		);
	}

	function displayJoker(dbid) {
		if (wait == 0) {
			var ids = new Array();
			var id = DBIDMap.s[dbid + "-"];
			console.info("Id: %d, dbId: %d", id, dbid);
			ids.push(id);

			buildSentences(ids);
			//Hide save button
			$("#sTable").find('tr').each(function () {
				if ($(this).find('td:eq(0)')[0].childNodes.length > 5)
					$(this).find('td:eq(0)')[0].childNodes[5].remove();
			});
			// for init
			count = 4;
		}
	}

	function updateTips(t) {
		var tips = $('#jokerSentenceForm_validateTips');
		tips.text(t).addClass("ui-state-highlight");
		setTimeout(function () {
			tips.removeClass("ui-state-highlight", 1500);
		}, 500);
	}

	function checkValue(o, n) {
		if (o.val().length == 0) {
			o.addClass("ui-state-error");
			updateTips(t("peaseEnterSentence", n));
			return false;
		} else {
			return true;
		}
	}

	// Check if entered chars are within ISO-8859-15 charset
	function validateCharsetISO8859(s) {
		if (s.value.length) {
			var lastChar = s.value.charCodeAt(s.value.length - 1)
			if (!((lastChar >= 32 && lastChar <= 126) || (
				lastChar >= 160 && lastChar <= 255))) {
				s.value = s.value.substring(0, s.value.length - 1);
				alert(t('invalidCharacter'));
			}
		}
	}

	//------------------------------------------------------------------------------------------------
	// Editor
	//------------------------------------------------------------------------------------------------

	var divPointer = null;
	var divTextcat = null;
	var DELIM = '.';
	function setStatus(text) { document.getElementById('status').innerHTML = text; }
	function setWarning(text) { document.getElementById('warning').innerHTML = text; }

	$ = jQuery;

	// function getValue(element) { return $('input', element).attr("value"); }
	// function setValue(element, value) { $('input', element).attr("value", value); }

	function newAccordion() {
		$('#filterInput').val('');
		if ($('#pmdialog').length == 0)
			initAccordion();

		divPointer = $('.textcat');
		var divAccordion = $('<div class="s_panel">');
		var h3 = $('<h3>')
			.text(t('search'))
			.attr('id', -1)
		h3.appendTo(divAccordion);

		$("<div id='pmdiag'></div>").appendTo(divAccordion);
		divAccordion.appendTo($('#accordion', divPointer));
		var acc = $("#accordion h3")[0];
		if (acc)
			jQuery("#pmdialog").detach().appendTo(acc.nextSibling);

		$('#accordion').accordion("refresh");
		$('#accordion').accordion({ active: 0 });
		$('#accordion').accordion("refresh");

	}

	function openAccordion(ref, def) {
		$('#filterInput').val('');
		jQuery("#pmdialog").remove();

		if (def != -1 && def.indexOf('undefined') == -1) {
			if ($('#pmdialog').length == 0)
				initAccordion();
			divPointer = ref;
			setSentence(def);
			adjustTable();
			$('#accordion').accordion("refresh");
			var active = $("#accordion").accordion("option", "active");
			var acc = $("#accordion h3")[active];
			if (acc) {
				jQuery("#pmdialog").detach().appendTo(acc.nextSibling);
				var sentTable = acc.nextSibling.childNodes["0"].childNodes[1];
				sentTable.childNodes["0"].childNodes["0"].childNodes[1].childNodes[3].remove();
			}

		}
		$('#accordion').accordion("refresh");
	}

	function adjustTable() {
		var tableToAlign;
		$('.optionTable tr td table').each(function (i, el) {
			if (el.id.indexOf("_NO") > 0)
				tableToAlign = "#" + el.id;
		});
		$(tableToAlign + ' tr').each(function (i) {
			var maxHeight = Math.max($(tableToAlign + ' tr').eq(i)["0"].clientHeight, $(tableToAlign.replace("_NO", "") + ' tr').eq(i)["0"].clientHeight);
			$(tableToAlign + ' tr').eq(i).height(maxHeight);
			$(tableToAlign.replace("_NO", "") + ' tr').eq(i).height(maxHeight);
		});
	}

	function createText(def) {
		var sentences = [];
		try {
			convertDBID(def, true).split(DELIM).forEach(function (item) {
				if (item != '')
					sentences.push(createSentence(item));
			});
		} catch (e) {
			return $('<span class="empty">').text(t('invalidText'));
		}

		if (sentences.length == 0) {
			return $('<span class="empty">').text(t('noText'));
		}
		return sentences.join(' ');
	}

	//Clesius 
	function translateAll(el, def) {
    var languages = ["de", "it", "en", "fr", "ca"];
    $.ajax({
      async: false,
      url: translateURL,
      data: {
        def: def,
        l: languages.join(),
        "input-lang": srcLang == "it" ? "it" : undefined,
        dn: srcDomain,
      },
      dataType: "json",
      success: function (data) {
        if (typeof data.texts !== "object") {
          alert(t("invalidTranslation"));
        } else if (!data.texts.de) {
          alert(t("noText"));
        } else {
          languages.forEach(function (l) {
            $("#tradTable td#" + l)["0"].innerText = data.texts[l];
          });
        }
      },
      error: function () {
        alert(t("invalidTranslation"));
      },
    });
  }

	function def2De(inputDef, toLang, callback) {
		try {
			var def = inputDef.textDef
			return $.ajax({
				async: false,
				type: 'GET',
				url: translateDefURL,
				data: {
					def: def,
					l: toLang,
					dn: srcDomain,
				},
				success: callback
			});

		} catch (e) {
			return '999999999';
		}
	}

	function removeSentenceClick(uirem, panel) {
		var draggable = $("#" + panel)[0].childNodes[0];
		draggable.remove();
		/*draggable = $("#" + panel + "> #" + id.replace(/(:|\.|\[|\]|,|=|@)/g, "\\$1")),
			draggable.remove();*/
	}

	function replaceSentence(value) {
		var index = $("#accordion").accordion("option", "active");
		$("#accordion h3").eq(index)
			.html(createSentence(value) + '<button class="ui-button ui-widget" onclick="PM.removeSentenceClick(this)" style="float:right;margin-right: 2px;"><span class="ui-icon ui-icon-trash"></span></button>')
			.attr('id', value);
		$('#accordion').accordion({ active: false });
	}

	function appendSentence(div, value, preceding) {
		if (div == null)
			div = divPointer;

		var divAccordion = $('<div class="s_panel">');
		divAccordion.uniqueId();

		var h3 = $('<h3>')
			.html(createSentence(value) + '<button class="ui-button ui-widget" onclick="PM.removeSentenceClick(this,\'' + divAccordion.attr("id") + '\')" style="float:right;margin-right: 2px;"><span class="ui-icon ui-icon-trash"></span></button>')
			.attr('id', value)

		h3.appendTo(divAccordion);
		$("<div id='pmdiag'></div>").appendTo(divAccordion);

		if (preceding == null)
			divAccordion.appendTo($('#accordion', div));
		else
			divAccordion.insertAfter(preceding);

		//$('li').removeClass('selected');

		$('<h3>').draggable();
		$('<h3>').droppable();
		$('#send-message-2-albina').hide();
		//	$('#dialog').dialog('close');
	}

	function expandMe() {
		expand(this);
	}

	function _collapseButton() {
		return $('<button class="ui-button ui-widget"></button>')
			.css('font-size', '15pt')
			.append('<span class="ui-icon ui-icon-check"></span>')
			.append(t('confirmation'))
			.attr('onclick', 'PM.collapse(this.parentNode)')
	}

	function expand(div) {
		$(div).removeClass('tch');
		_collapseButton().appendTo(div);
		$("br", div).remove();
		$('<br><br>').appendTo(div);
		$("p", div).remove();

		$("table", div).remove();

		//init search
		var list = $("<div id='searchDiv'>").appendTo(div);
		$('#filterInput').val('');
		divPointer = $('.textcat');
		var divAccordion = $('<div class="s_panel">');
		$('<h1>')
			.attr('id', -1)
			.attr('class', 'ui-accordion-header ui-helper-reset ui-state-default ui-accordion-header-active ui-state-active ui-corner-top ui-accordion-icons')
			.appendTo(divAccordion);
		$("<div id='pmdiag'></div>").appendTo(divAccordion);
		divAccordion.appendTo($('#searchDiv', divPointer));
		initSearch();
		var acc = $("#searchDiv h1")[0];
		if (acc)
			jQuery("#pmdialogSearch").detach().appendTo(acc.nextSibling);

		// finish search

		list = $("<div id='accordion'>").appendTo(div);
		//new def
		var def = "";

		if ($('input', div).attr('value').length > 5) {
			// if ($('#filterInput').val().length > 5) {
			def = convertDBID($('input', div).attr('value'), true);
			def.split(DELIM).forEach(function (item) {
				if (item.length > 0) {
					appendSentence(div, item);
				}
			});
		}
		$(function () {
			$('.trash').droppable(
				{
					tolerance: 'pointer',
					drop: function (event, ui) {
						$(ui.draggable).remove();
					}
				});
			$(list).sortable({
				helper: "clone",
				connectWith: '.sortable',
				stop: function () {
					updateValue(this.parentNode);
				},
				//For albina integration
				receive: function () {
					updateValue(this.parentNode);
				}
			}).disableSelection();

		});
		$(div).unbind();

		$('#accordion').accordion({
			collapsible: true,
			active: false,
			height: 'fill',
			header: 'h3'
		}).sortable({
			items: '.s_panel'
		});

		$("#accordion").accordion();

		$("#accordion").accordion({
			activate: function (event, ui) {
				if (ui.newHeader.length) {
					$('#accordion').sortable('disable');
					openAccordion($(this.parentNode), ui.newHeader.attr('id'));
					$('#accordion').accordion("refresh");
				} else {
					$('#accordion').sortable('enable');
					if (ui.oldHeader.attr('id') != -1)
						$('#pmdialog').remove();
					$('#accordion').accordion("refresh");
				}
			}
		});
		if (def.split(DELIM).length == 1)
			$('#accordion').accordion({ active: 0 });

		// remove transaltion button
		if (def == "")
			if (typeof div[0] !== "undefined")
				div[0].childNodes[1].remove();

		$('#filterInput').focus();
		$("#entireSentence").text(t("entireSentence"));
	}

	function expandPreview(div, dbID) {
		$(div).removeClass('tch');
		_collapseButton().appendTo(divTextcat);
		$("br", div).remove();
		$('<br><br>').appendTo(div);
		$("p", div).remove();

		$("table", div).remove();

		//init search
		var list = $("<div id='searchDiv'>").appendTo(div);
		$('#filterInput').val('');
		divPointer = $('.textcat');
		var divAccordion = $('<div class="s_panel">');
		var h1 = $('<h1>')
			.attr('id', -1)
			.attr('class', 'ui-accordion-header ui-helper-reset ui-state-default ui-accordion-header-active ui-state-active ui-corner-top ui-accordion-icons')
			.html('Suchen...')

		h1.appendTo(divAccordion);

		$("<div id='pmdiag'></div>").appendTo(divAccordion);

		divAccordion.appendTo($('#searchDiv', divPointer));
		initSearch();
		var acc = $("#searchDiv h1")[0];
		if (acc)
			jQuery("#pmdialogSearch").detach().appendTo(acc.nextSibling);
		// finish search

		list = $("<div id='accordion'>").appendTo(div);
		//new def
		var def = "";
		def = convertDBID(dbID, true);
		def.split(DELIM).forEach(function (item) {
			if (item.length > 0) {
				appendSentence(div, item);
			}
		});

		$(function () {
			$('.trash').droppable(
				{
					tolerance: 'pointer',
					drop: function (event, ui) {
						$(ui.draggable).remove();
					}
				});

			$(list).sortable({
				helper: "clone",
				connectWith: '.sortable',
				stop: function () {
					updateValue(this.parentNode);
				},
				//For albina integration
				receive: function () {
					updateValue(this.parentNode);
				}
			}).disableSelection();

		});
		$(div).unbind();

		$('#accordion').accordion({
			collapsible: true,
			active: false,
			height: 'fill',
			header: 'h3'
		}).sortable({
			items: '.s_panel'
		});

		$("#accordion").accordion("option", "icons", { "header": "ui-icon-plus", "activeHeader": "ui-icon-minus" });

		$("#accordion").accordion({
			activate: function (event, ui) {
				if (ui.newHeader.length) {
					$('#accordion').sortable('disable');
					openAccordion($(this.parentNode), ui.newHeader.attr('id'));
					$('#accordion').accordion("refresh");
				} else {
					$('#accordion').sortable('enable');
					if (ui.oldHeader.attr('id') != -1)
						$('#pmdialog').remove();
					$('#accordion').accordion("refresh");
				}
			}
		});
		if (def.split(DELIM).length == 1)
			$('#accordion').accordion({ active: 0 });

		if (def == "")
			div[0].childNodes[1].remove();

		$('#filterInput').focus();
	}

	function collapse(el) {
		$('#wait').show("fast", function () {
			var def = updateValue(el);
			$(el).addClass('tch');
			//switch button on preview of copedit
			if (location.href.indexOf('preview') == -1)
				$('#accordion', el).remove();
			if (location.href.indexOf('preview') > 0) {
				$('#tradTable').remove();
				$('#composedText').remove();
			}

			$('img', el).remove();
			//Clesius
			$('<table id="tradTable">', el).remove();
			$('<p id="composedText">').append(createText(def)).appendTo(el);
			$('<table id="tradTable">').appendTo(el);
			$('#tradTable').append('<tr><th>[de]</th><td id="de" class="textcatTrad">Warte auf laufende Übersetzung ....</td></tr>');
			$('#tradTable').append('<tr><th>[it]</th><td id="it" class="textcatTrad">Attendere traduzione in corso....</td></tr>');
			$('#tradTable').append('<tr><th>[en]</th><td id="en" class="textcatTrad">Wait for translation in progress ....</td></tr>');
			$('#tradTable').append('<tr><th>[fr]</th><td id="fr" class="textcatTrad">Attendez la traduction en cours ....</td></tr>');
			$('#tradTable').append('<tr><th>[ca]</th><td id="ca" class="textcatTrad">....</td></tr>');
			$(el).unbind();

			//$(el).dblclick(expandMe);
			$('#composedText').hammer().on("tap", function () { expand($(el)); });

			//for async call
			translateAll(el, def);
			$('#wait').hide(0);
			if ($('#composedText')[0].textContent != '' && $('#composedText')[0].textContent) {
				//make Json to send to albina
				var outputText = {
					textDef: $('input.textcat')[0].value,
					textField: $('#textField')[0].value,
					textDe: $('#tradTable #de')["0"].textContent,
					textIt: $('#tradTable #it')["0"].textContent,
					textEn: $('#tradTable #en')["0"].textContent,
					textFr: $('#tradTable #fr')["0"].textContent,
					textCa: $('#tradTable #ca')["0"].textContent,
				};
			}
			else {
				outputText = {
					textDef: '',
					textField: '',
					textDe: '',
					textIt: '',
					textEn: '',
					textFr: '',
					textCa: '',
				};
			}
			var pmData = JSON.stringify(outputText);
			if (window.parent.opener != null)
				window.parent.opener.postMessage(pmData, "*");
			else
				parent.postMessage(pmData, "*");
		});
	}

	function collapseSimple(el) {
		updateValue(el);
		$(el).addClass('tch');
		$('ul', el).remove();
		$('img', el).remove();
		//Clesius
		$('<table id="tradTable">', el).remove();
		$(el).unbind();
	}

	function updateValue(el) {
		var values = [];
		$('h3', el).each(function () {
			values.push(convertDBID($(this).attr('id'), false));
		});
		var def = values.join(DELIM);
		$('input.textcat', el).attr('value', def);
		return def;
	}

	function initAccordion() {
		$('#accordionTemplate').tmpl({ ContextPath: contextPath }).appendTo($('input.textcat').first());
	}

	function initSearch() {
		$('#searchTemplate').tmpl({ ContextPath: contextPath }).appendTo($('input.textcat').first());
	}

	/*
	function initDialog(autoOpen) {
		$('#dialogTemplate').tmpl({ ContextPath: contextPath }).appendTo($('input.textcat').first());
		$('#dialog').dialog({
			title: t('textcat'),
			class: 'dialog',
			autoOpen: autoOpen,
			width: '90%',
			height: '900',
			close: function (event, ui) {
				$('li').removeClass('selected');
				closeJokerSentenceForm();
			}
		});
	}
	*/

	function readTemplates() {
		$.get(contextPath + 'templates/_templates.albina.html', function (templates) {
			$('body').append(templates);
			//initDialog(false);
		}, 'html');
	}

	function initFields() {
		// Init input fields
		$('input.textcat').each(function () {
			if ($(this).get(0).type != 'hidden') {
				$(this).wrap('<div class="textcat tch"/>');
				//$(this.parentNode).dblclick(expandMe);
				$(this.parentNode).hammer().on("tap", expandMe);
				$(this).get(0).type = 'hidden';
				$('<p>').append(createText(this.value)).appendTo(this.parentNode);
			}
		});

		// Init text fields
		$('input.textcatRO').each(function () {
			if ($(this).get(0).type != 'hidden') {
				$(this).wrap('<div class="textcatRO"/>');
				$(this).get(0).type = 'hidden';
				var p = $('<p>').appendTo(this.parentNode);
				this.value.split(DELIM).forEach(function (item) {
					p.append($('<li>').html(createText(item)).attr('id', convertDBID(item, true)));
				});
			}
		});
	}

	var count = 0;
	function initAfterLoad() {
		if (count++ == 3) {
			readTemplates();
			initFields();
			console.info('Texteditor initialized');
		}
	}
	function init() {
		if (count == 4) {
			initFields();
			console.log('Fields refreshed');
		} else
			console.log('Refresh ignored');
	}

	function replDiv(s) {
		s = s.replace(/ /g, "");
		s = s.replace("(", "");
		s = s.replace(")", "");
		s = s.replace("{", "");
		s = s.replace("}", "");
		s = s.replace(",", "_");
		return s;
	}

	//------------------------------------------------------------------------------------------------
	// Initialisation
	//------------------------------------------------------------------------------------------------

	var param = new URLSearchParams(location.search.substring(1));
	var srcLang = param.get("l") || "de";
	var srcRgn = param.get("r") || "";
	var srcDomain = param.get("dn") || param.get("d") || "PRODUCTION";

	var i18nMessages = {};
	["en", "de", "it"].forEach(function (lang) {
		jQuery.ajax({
			url: "./i18n/" + lang + ".json",
			dataType: 'json',
			async: false,
			success: function (data) {
				i18nMessages[lang] = data;
			}
		})
	})
	/**
	 * Returns the translation for the given message key.
	 * @param {string} key the message key
	 * @param {*} argument an argument to substitue $1 in the message for
	 */
	function t(key, argument) {
		var messages = i18nMessages[srcLang] || i18nMessages.en;
		var message = messages[key] || i18nMessages.en[key] || key;
		return argument !== undefined ? message.replace(/\$1/g, argument) : message;
	}

	var contextPath = getEnvValue(window.contextPath, './');
	var contextApiPath = getEnvValue(window.contextApiPath, './api/v2');
	var translateURL = getEnvValue(window.sentenceURL, contextApiPath + "/translate");
	var sentenceURL = getEnvValue(window.sentenceURL, contextApiPath + "/catalog/sentences");
	var optionURL = getEnvValue(window.optionURL, contextApiPath + "/catalog/options");
	var phraseURL = getEnvValue(window.phraseURL, contextApiPath + "/catalog/phrases");
	var indexURL = getEnvValue(window.indexURL, contextApiPath + "/catalog/indexfull");
	var searchURL = getEnvValue(window.searchURL, contextApiPath + "/search");
	var translateDefURL = getEnvValue(window.sentenceURL, contextApiPath + "/translatedef");
	var saveJokerURL = getEnvValue(window.saveJokerURL, contextApiPath + "/joker");

	function getEnvValue(envValue, defaultValue) {
		return envValue == undefined ? defaultValue : envValue;
	}

	var sentenceArray = new Array();
	var optionArray = new Array();
	var phraseArray = new Array();
	var rgnArray = new Array();

	var cleanedPhraseArray = new Array();
	var regExpArray = new Array();
	var indexArray = new Array();

	var sentenceNames = new Array();
	var optionNames = new Array();
	var optionHeaders = new Array();
	var optionRgn = new Array();

	var IDMap = new Object();
	var DBIDMap = new Object();

	var filtered = new Array();

	function setId(idm, dbidm, i, item) {
		idm.push(item.dbid);
		dbidm[item.dbid + '-'] = i;
	}

	function putSentences(data) {
		IDMap.s = new Array();
		DBIDMap.s = new Array();
		$.each(data.items, function (i, item) {
			sentenceArray.push(item.options.split(";"));
			if (srcLang == "it")
				sentenceNames.push(item.header);
			else
				sentenceNames.push(item.name);
			setId(IDMap.s, DBIDMap.s, i, item);
		});
		console.info("Init: %d sentences loaded", sentenceArray.length);
		initAfterLoad();
	}
	function putOptions(data) {
		IDMap.o = new Array();
		DBIDMap.o = new Array();
		$.each(data.items, function (i, item) {

			optionArray.push(item.phrases.split(";"));
			optionNames.push(item.name);
			optionHeaders.push(item.header);
			optionRgn.push(item.rgn);
			setId(IDMap.o, DBIDMap.o, i, item);
		});
		console.info("Init: %d options loaded", optionArray.length);
		initAfterLoad();
	}
	function putPhrases(data) {
		IDMap.p = new Array();
		DBIDMap.p = new Array();
		var regexp = new RegExp("{(\\d+)(#\\d+)}", 'gi');
		$.each(data.items, function (i, item) {
			phraseArray.push(item.value.replace(regexp, function (match, id) { return optionHeaders[id]; }));
			rgnArray.push(item.rgn);
			var phrase = item.value;
			// leading spaces has to be removed in the metadata!!
			phrase = phrase.replace(/^[\s]+/, '');

			cleanedPhraseArray.push(phrase);

			// trailing blanks have to be removed in the metadata!!
			//phrase = phrase.replace(/\s+$/,''); => makes it slow, but why
			phrase = phrase.replace(/\.\s$/, '');
			// the dot at the end has to be removed in the metadata!!
			phrase = phrase.replace(/\.$/, '');

			var regExpStr = "^" + phrase.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
			regExpArray.push(new RegExp(regExpStr, 'i'));

			setId(IDMap.p, DBIDMap.p, i, item);

		});
		console.info("Init: %d phrases loaded", phraseArray.length);
		console.info("Init: %d regexp phrases created", regExpArray.length);
		initAfterLoad();
	}
	function putIndexes(data) {
		$.each(data.items, function (i, item) {
			indexArray[item.word] = item.sentences.split(";").map(Number);
		});
		console.info("Init: %d indexes loaded", indexArray.length);
		initAfterLoad();
	}
	
	loadCatalog();

	return {
		cut: function () {
			if ($('li:hover').length > 0) {
				console.log('Cut sentence');
				$('li:hover').remove();
			}
			if ($('p:hover').length > 0) {
				console.log('Cut block');
				var parent = $('p:hover').parent();
				$('input', parent).attr('value', '');
				$('p', parent).html(createText(''));
			}
		},

		copy: function () {
			if ($('li:hover').length > 0) {
				var value = $('li:hover').attr('id');
				console.log('Copy sentence ' + value);
				localStorage.setItem("clipboard", value);
			} else
				if ($('p:hover').length > 0) {
					var parent = $('p:hover').parent();
					value = $('input', parent).attr('value');
					console.log('Copy block ' + value);
					localStorage.setItem("clipboard", convertDBID(value, true));
				}
		},

		paste: function () {
			var value = localStorage.getItem("clipboard");
			var sentences = value.split(".");
			if ($('.textcat ul li:hover').length > 0) {
				var parent = $('li:hover').parent().parent();
				sentences.forEach(function (item) {
					console.log('Append sentence ' + item);
					appendSentence(parent, item, $('li:hover'));
				});
				updateValue(parent);
			} else if ($('.textcat ul:hover').length > 0) {
				sentences.forEach(function (item) {
					console.log('Paste sentence ' + item);
					appendSentence($('ul:hover').parent(), item);
				});
				updateValue($('ul:hover').parent());
			} else if ($('.textcat p:hover').length > 0) {
				console.log('Paste block ' + value);
				parent = $('p:hover').parent();
				var def = convertDBID(value, false);
				$('input', parent).attr('value', def);
				$('p', parent).html(createText(def));
			}
		},

		init: function () { init() },
		//load:					function()			{ initialLoad()				},
		search: function (a, b) { search(a, b) },
		collapse: function (a) { collapse(a) },
		collapseSimple: function (a) { collapseSimple(a) },

		//openDialog: function (a, b) { openDialog(a, b) },
		addSentenceClick: function (a, b) { addSentenceClick(a, b) },
		onSentenceClick: function (a) { onSentenceClick(a) },
		previewSentence: function (a) { previewSentence(a) },
		newAccordion: function () { newAccordion() },
		openAccordion: function (a, b) { openAccordion(a, b) },
		onPhraseClick: function (a, b) { onPhraseClick(a, b) },
		onSubPhraseClick: function (a, b, c) { onSubPhraseClick(a, b, c) },
		openPhraseOption: function (a, b, c, d, e) { openPhraseOption(a, b, c, d, e) },
		removeSentenceClick: function (a, b) { removeSentenceClick(a, b) },

		openJokerSentenceForm: function () { openJokerSentenceForm() },
		closeJokerSentenceForm: function () { closeJokerSentenceForm() },
		saveJokerSentence: function () { saveJokerSentence() },
		validateCharsetISO8859: function (s) { validateCharsetISO8859(s) },

		translateAll: function (a, b) { translateAll(a, b) },
		expand: function (a) { expand(a) },
		def2De: function (a, b, c) { def2De(a, b, c) },
		expandPreview: function (a, b) { expandPreview(a, b) },
	}

}();

//$(document).ready(PM.load);
$(document).ajaxError(function (e, xhr, settings) {
	alert('error in: ' + settings.url + ' \n' + 'error:\n' + xhr.responseText);
});

//Clesius

var $loading = $('#wait').hide();
$(document)
  .ajaxStart(function () {
    $loading.show();
  })
  .ajaxStop(function () {
    $loading.hide();
  });

Array.prototype.unique = function() {
	return this.filter(function (value, index, self) { 
		return self.indexOf(value) === index;
	});
}
