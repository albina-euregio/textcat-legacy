// Define a grammar called Hello
// 11[5452,2079,2656[7314[5754],6771[5753],4059[2275,4881]],4769]
grammar TextCat;
SENTENCE_SEPARATOR: '.' ;
PHRASE_SEPARATOR: ',' ;
SET_OPEN: '[' ;
SET_CLOSE: ']' ;
INDEX_NUMBER: [1-9][0-9]* ;
WS: (' ' | '\t') -> skip ;
NEWLINE: '\r'? '\n' -> skip ;

multisentence
    : sentence (SENTENCE_SEPARATOR sentence)*
    ;
sentence
    :sentenceId SET_OPEN sentenceContent SET_CLOSE
    ;
sentenceId
    :INDEX_NUMBER
    ;
sentenceContent
    : sentencePhrase (PHRASE_SEPARATOR sentencePhrase)*
    ;
sentencePhrase
    : INDEX_NUMBER option?
    ;
option
    : SET_OPEN optionPhrase(PHRASE_SEPARATOR optionPhrase)* SET_CLOSE
    ;
optionPhrase
    : INDEX_NUMBER option?
    ;
