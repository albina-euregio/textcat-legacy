package it.clesius.albina.model;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.*;

public class Sentence {
    private Long id;
    private List<SentenceContent> sentenceContent;

    //in italian often there is an extra sentence_module than germanan (punkt)
    // we need a list of this phrase_id
    public List<String> punti = new ArrayList<String>();
    public List<String>  getPunti() {
        return punti;
    }
    public void setPunti(String punto) {
        this.punti.add(punto);
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public Sentence punti(List<String> punti) {
        this.punti = punti;
        return this;
    }


    public Sentence id(Long id) {
        this.id = id;
        return this;
    }

    public List<SentenceContent> getSentenceContent() {
        return sentenceContent;
    }

    public void setSentenceContent(List<SentenceContent> sentenceContent) {
        this.sentenceContent = sentenceContent;
    }

    public Sentence sentenceContent(List<SentenceContent> content) {
        this.sentenceContent = content;
        return this;
    }


    @Override
    public String toString() {
        return String.format("%d[%s]",id, sentenceContent.stream().filter(x->x!=null).map(x->x.toString()).collect(joining(",")));
    }
}
