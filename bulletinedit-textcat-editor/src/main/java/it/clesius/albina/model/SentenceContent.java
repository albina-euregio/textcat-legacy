package it.clesius.albina.model;

/**
 * phrase or option not both
 */
public class SentenceContent {
    private SentencePhrase phrase;

    public SentencePhrase getPhrase() {
        return phrase;
    }
    public void setPhrase(SentencePhrase phrase) {
        this.phrase = phrase;
    }
    public SentenceContent phrase(SentencePhrase phrase) {
        this.phrase = phrase;
        return this;
    }

    @Override
    public String toString() {
        return phrase.toString();
    }

}
