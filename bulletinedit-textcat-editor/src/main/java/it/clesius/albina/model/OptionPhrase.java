package it.clesius.albina.model;

public class OptionPhrase {
    private Long id;
    private Option option;
    private String header;
    private Long sortPosition;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OptionPhrase id(Long id) {
        this.id = id;
        return this;
    }

    public Option getOption() {
        return option;
    }

    public void setOption(Option option) {
        this.option = option;
    }

    public OptionPhrase option(Option option) {
        this.option = option;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb=new StringBuilder();
        sb.append(String.format("%d", id));
        if (option!=null){
            sb.append(option.toString());
        }
        return sb.toString();
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }
    public OptionPhrase header(String header) {
        this.header = header;
        return this;
    }


    public Long getSortPosition() {
        return sortPosition;
    }

    public void setSortPosition(Long sortPosition) {
        this.sortPosition = sortPosition;
    }
    public OptionPhrase sortPosition(Long sortPosition) {
        this.sortPosition = sortPosition;
        return this;
    }
}
