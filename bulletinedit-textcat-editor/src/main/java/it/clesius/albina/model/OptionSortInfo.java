package it.clesius.albina.model;

public class OptionSortInfo {
    private Long position;
    private String value;

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }
    public OptionSortInfo position(Long position) {
        this.position = position;
        return this;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public OptionSortInfo value(String value) {
        this.value = value;
        return this;
    }
}
