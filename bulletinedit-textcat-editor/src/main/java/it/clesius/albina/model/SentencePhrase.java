package it.clesius.albina.model;

public class SentencePhrase {
    private Long id;
    private Option option;
    private Long sortPosition;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SentencePhrase id(Long id) {
        this.id = id;
        return this;
    }

    public Long getSortPosition() {
        return sortPosition;
    }

    public void setSortPosition(Long sortPosition) {
        this.sortPosition = sortPosition;
    }

    public SentencePhrase sortPosition(Long itemNo) {
        this.sortPosition = itemNo;
        return this;
    }

    public Option getOption() {
        return option;
    }

    public void setOption(Option option) {
        this.option = option;
    }

    public SentencePhrase option(Option option) {
        this.option = option;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb=new StringBuilder();
        sb.append(String.format("%d", id));
        if (option!=null){
            sb.append(String.format("[%s]", option.toString()));
        }
        return sb.toString();
    }
}
