package it.clesius.albina.model;

import java.util.List;

import static java.util.stream.Collectors.joining;

public class Option{
    private List<OptionPhrase> optionPhrase;
    private boolean rootLevel;
    private List<String> targetSortInfo;


    public List<OptionPhrase> getOptionPhrase() {
        return this.optionPhrase;
    }

    public void setOptionPhrase(List<OptionPhrase> optionPhrase) {
        this.optionPhrase = optionPhrase;
    }

    public Option optionPhrase(List<OptionPhrase> phrase) {
        this.optionPhrase = phrase;
        return this;
    }

    @Override
    public String toString() {
        if (optionPhrase==null)
            return "";
        String template=isRootLevel()?"%s":"[%s]";
        return String.format(template,optionPhrase
                .stream()
                .map(OptionPhrase::toString)
                .collect(joining(",")));
    }

    public boolean isRootLevel() {
        return rootLevel;
    }

    public void setRootLevel(boolean rootLevel) {
        this.rootLevel = rootLevel;
    }

    public List<String> getTargetSortInfo() {
        return targetSortInfo;
    }

    public void setTargetSortInfo(List<String> targetSortInfo) {
        this.targetSortInfo = targetSortInfo;
    }

    public Option targetSortInfo(List<String> targetSortInfo) {
        this.targetSortInfo = targetSortInfo;
        return this;
    }

}
