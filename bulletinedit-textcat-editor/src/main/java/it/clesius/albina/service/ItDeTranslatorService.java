package it.clesius.albina.service;

import ch.slf.lwp.bulletinedit.textcat.BoTextImpl;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages;
import ch.slf.lwp.bulletinedit.textcat.dao.DaoSentence;
import it.clesius.albina.jooq.Keys;
import it.clesius.albina.jooq.tables.PhraseOption;
import it.clesius.albina.jooq.tables.SentenceModule;
import it.clesius.albina.model.*;
import it.clesius.albina.parser.TextCatVisitorImpl;
import org.jooq.*;
import org.jooq.conf.RenderNameStyle;
import org.jooq.lambda.Unchecked;
import org.jooq.lambda.tuple.Tuple4;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static it.clesius.albina.jooq.Tables.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.jooq.impl.DSL.*;
import static org.jooq.lambda.Seq.seq;
import static org.jooq.lambda.tuple.Tuple.tuple;

public class ItDeTranslatorService {
    @Autowired
    DataSource dataSource;
    @Autowired
    DaoSentence daoSentence;

    private final static Pattern SUB_PHRASE_OPTION_CONTROL_PATTERN = Pattern.compile("(?:\\((-{0,2})\\))?\\s*\\{(.+?)(?:#(.+?))?\\}\\s*(?:\\((-?)\\))?");

    public TextCatLanguages.Language guessLanguage(String stringSentence) {
        TextCatVisitorImpl sc = new TextCatVisitorImpl();
        List<Sentence> sentences = sc.parse(stringSentence);
        int id = sentences.iterator().next().getId().intValue();
        final ch.slf.lwp.bulletinedit.textcat.model.Sentence sentence = daoSentence.getSentence(id);
        return sentence.getLanguage();
    }

    /**
     * Translate italian sentence on input into german sentence
     * @param stringSentence
     * @return
     * @throws SQLException
     */
    public String getGermanSentence(String stringSentence) {
        TextCatVisitorImpl sc=new TextCatVisitorImpl();
        String outLang = "de";
        List<it.clesius.albina.model.Sentence> sentences=sc.parse(stringSentence);
        return sentences
                .stream()
                .map(Unchecked.function(sentence->visitSentence(sentence, outLang)))
                .map(x->x.toString())
                .collect(joining("."));

    }

    private Sentence visitSentence(Sentence sentenceIt, String outLang) throws SQLException {
        Long sentenceItId = sentenceIt.getId();
        Long sentenceDeId = findMatchingSentenceId(sentenceIt.getId(), outLang);
        return new Sentence()
                .id(sentenceDeId)
                .sentenceContent(visitSentenceContent(sentenceIt.getSentenceContent(),sentenceItId,sentenceDeId,outLang)
                );
    }

    private List<SentenceContent> visitSentenceContent(List<SentenceContent> sentenceContent, Long sentenceItId, Long sentenceDeId, String outLang) {
        return sentenceContent
                .stream()
                .map(Unchecked.function(sentenceContentItem -> {
                    if (sentenceContentItem.getPhrase() != null) {
                        SentencePhrase phrase = visitSentencePhrase(sentenceContentItem.getPhrase(), sentenceItId, sentenceDeId, outLang);
                        if (phrase!=null)
                        return new SentenceContent()
                                .phrase(phrase);
                    }
                    return null;
                }))
                .filter(Objects::nonNull)
                .filter(x->x.getPhrase().getId()!=null)
                .sorted(Comparator.comparingLong(x->x.getPhrase().getSortPosition()))
                .collect(toList());
    }

    private SentencePhrase visitSentencePhrase(SentencePhrase phrase, Long sentenceItId, Long sentenceDeId, String outLang) throws SQLException {
        Long phraseItId = phrase.getId();
        Tuple4<Long, Long, String, String> phraseDeTuple=findMatchingPhraseId(sentenceItId, sentenceDeId, phraseItId);
        if (phraseDeTuple==null)
            return null;

        Option childOption=visitOption(
                phrase.getOption(),
                sentenceItId,
                sentenceDeId,
                outLang,
                phraseDeTuple.v4, // VALUE IT
                phraseDeTuple.v3  //VALUE DE
             );
        if (childOption!=null){
            childOption.setRootLevel(true);
        }
        return new SentencePhrase()
                .id(Optional.ofNullable(phraseDeTuple).map(x -> x.v1).orElse(null))
                .sortPosition(Optional.ofNullable(phraseDeTuple).map(x -> x.v2).orElse(null))
                .option(childOption);
    }



    private Option visitOption(Option optionIt, Long sentenceItId, Long sentenceDeId, String outLang, String optionDefinitionIt, String optionDefinitionDe) {
        if (optionIt==null)
            return null;
        // Add sort information
        return new Option()
                .optionPhrase(
                        seq(optionIt.getOptionPhrase())
                            .zipWithIndex()
                            .stream()
                             // translate here
                            .map(Unchecked.function(optionPhrase -> visitOptionPhrase(optionPhrase.v1, sentenceItId, sentenceDeId, outLang, optionDefinitionIt, optionDefinitionDe, optionPhrase.v2)))
                            .filter(optionPhrase->optionPhrase.getSortPosition()!=-1)
                                // PER MANTENERE L'ORDINE NEINUOVI PARIMERITO STESSO DEFINIZIONE PIU' VOLTE
                            .sorted(Comparator.comparingLong(OptionPhrase::getSortPosition))
                            .collect(toList())
                        );
    }

    private OptionPhrase visitOptionPhrase(OptionPhrase phrase, Long sentenceItId, Long sentenceDeId, String outLang, String optionDefinitionIt, String optionDefinitionDe, Long optionIdxIt) throws SQLException {
        Long phraseItId = phrase.getId();
        Tuple4<Long, String, String, String> phraseDeTuple=findOptionId(phraseItId, outLang);
        Long id=Optional.ofNullable(phraseDeTuple).map(x -> x.v1).orElse(null);
        String header=Optional.ofNullable(phraseDeTuple).map(x -> x.v2).orElse(null);
        String optionValueDe=Optional.ofNullable(phraseDeTuple).map(x -> x.v3).orElse(null);
        String optionValueIt=Optional.ofNullable(phraseDeTuple).map(x -> x.v4).orElse(null);
        Long sortOrder=new Long(translatedOptionIdx(optionDefinitionIt, optionDefinitionDe, optionIdxIt));

        return new OptionPhrase()
                .id(id)
                .header(header)
                .option(visitOption(phrase.getOption(),sentenceItId, sentenceDeId, outLang, optionValueIt, optionValueDe))
                .sortPosition(sortOrder)
                ;
    }

    private List<String> tokenizeOptions(String phrase)  {
        List<String> fragments = new ArrayList<>();
        String pattern = "(\\{[^{|^}]*\\})";
        // Create a Pattern object
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(phrase);
        while (m.find()) {
            fragments.add(m.group());
        }
        return fragments;
    }

    private Integer translatedOptionIdx(String phraseIt, String phraseDe, Long optionIdxIt)  {
        List<String> tokensDe=tokenizeOptions(phraseDe);
        List<String> tokensIt=tokenizeOptions(phraseIt);
        String searchKey=tokensIt.get(optionIdxIt.intValue());
        return tokensDe.indexOf(searchKey);
    }


    private Long findMatchingSentenceId(Long sentenceIdIn, String langOut) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {

            DSLContext create = using(conn, SQLDialect.DEFAULT);

            create.configuration().settings().setRenderCatalog(false);
            create.configuration().settings().setRenderSchema(false);
            create.configuration().settings().setRenderNameStyle(RenderNameStyle.LOWER);

            CommonTableExpression<Record4<Long, String, String, String>> sentenceInQuery =
                    name("SENTENCE_IN")
                            .as(
                                    select(
                                            SENTENCE.SENTENCE_ID,
                                            SENTENCE.NAME,
                                            SENTENCE.LANGUAGE,
                                            SENTENCE.STRUCTURE
                                    )
                                            .from(SENTENCE)
                                            .where(SENTENCE.SENTENCE_ID.eq(sentenceIdIn))
                            );
            it.clesius.albina.jooq.tables.Sentence sentenceOutTable = SENTENCE.as("SENTENCE_OUT");
            SelectOnConditionStep<Record4<Long, String, String, String>> cte =
                    create
                            .with(sentenceInQuery)
                            .select(
                                    sentenceOutTable.SENTENCE_ID,
                                    sentenceOutTable.NAME,
                                    sentenceOutTable.LANGUAGE,
                                    sentenceOutTable.STRUCTURE
                            )
                            .from(sentenceInQuery)
                            .innerJoin(sentenceOutTable).on(
                            sentenceOutTable.NAME.eq(sentenceInQuery.field(SENTENCE.NAME.getName(), String.class))
                                    .and(sentenceOutTable.LANGUAGE.eq(langOut)));

            Record4<Long, String, String, String> fecthed = cte.fetchOne();

            return fecthed.getValue(sentenceOutTable.SENTENCE_ID);
        }
    }

    private Tuple4<Long, Long, String, String> findMatchingPhraseId(Long sentenceItId, Long sentenceDeId, Long phraseId) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            DSLContext create = using(conn, SQLDialect.DEFAULT);
            create.configuration().settings().setRenderCatalog(false);
            create.configuration().settings().setRenderSchema(false);
            create.configuration().settings().setRenderNameStyle(RenderNameStyle.LOWER);


            it.clesius.albina.jooq.tables.Sentence sentence1=SENTENCE.as("sentence_1");
            SentenceModule sentenceModule1=SENTENCE_MODULE.as("sentence_module_1");
            PhraseOption phraseOption1=PHRASE_OPTION.as("phrase_option_1");
            Field<Long> moduleNoIt=field(SENTENCE_MODULE.MODULE_NO).as("module_no_de");

            SelectConditionStep<Record12<String, String, String, Long, Long, String, String, Long, Long, String, Long, Long>> query1 = create
                    .select(
                            SENTENCE.NAME,
                            SENTENCE.LANGUAGE,
                            SENTENCE.STRUCTURE,
                            SENTENCE_MODULE.PHRASE_OPTION_ID,
                            SENTENCE_MODULE.MODULE_NO,
                            PHRASE_OPTION.NAME.as("PHNAME"),
                            PHRASE_OPTION.HEADER,
                            PHRASE_OPTION_ITEM.ITEM_NO,
                            PHRASE.PHRASE_ID,
                            PHRASE.VALUE,
                            PHRASE.ITEM_PART_NO,
                            moduleNoIt
                    )
                    .from(SENTENCE)
                    .innerJoin(SENTENCE_MODULE).onKey(Keys.FK_SENTENCE)
                    .innerJoin(PHRASE_OPTION).onKey(Keys.FK_PHRASE_OPTION)
                    .innerJoin(PHRASE_OPTION_ITEM).onKey(Keys.FK_COMPONENT)
                    .innerJoin(PHRASE).onKey(Keys.FK_SUBCOMPONENT)
                    .leftJoin(
                            select(sentenceModule1.MODULE_NO.as(moduleNoIt),phraseOption1.NAME)
                                    .from(sentence1)
                                    .innerJoin(sentenceModule1).on(sentence1.SENTENCE_ID.eq(sentenceModule1.SENTENCE_ID))
                                    .innerJoin(phraseOption1).on(sentenceModule1.PHRASE_OPTION_ID.eq(phraseOption1.PHRASE_OPTION_ID))
                                    .where(sentence1.SENTENCE_ID.eq(sentenceDeId))
                                    .asTable("innerTable")
                    ).on(PHRASE_OPTION.NAME.eq("innerTable."+phraseOption1.NAME.getName()))
                    .where(PHRASE.PHRASE_ID.eq(phraseId)
                           // .and(PHRASE.VALUE.isNotNull())
                            .and(SENTENCE.SENTENCE_ID.eq(sentenceItId))
                    );
            Record12<String, String, String, Long, Long, String, String, Long, Long, String, Long, Long> q1Resultset= query1.fetchOne();
            if (q1Resultset==null)
                return null;
            Long moduleNo=q1Resultset.getValue(SENTENCE_MODULE.MODULE_NO);
            Long itemNo=q1Resultset.getValue(PHRASE_OPTION_ITEM.ITEM_NO);
            Long itemPartNo=q1Resultset.getValue(PHRASE.ITEM_PART_NO);
            String phraseValueIt = q1Resultset.getValue(PHRASE.VALUE);

            SelectConditionStep<Record11<String, String, String, Long, Long, String, String, Long, Long, String, Long>> query2 = create
                    .select(
                            SENTENCE.NAME,
                            SENTENCE.LANGUAGE,
                            SENTENCE.STRUCTURE,
                            SENTENCE_MODULE.PHRASE_OPTION_ID,
                            SENTENCE_MODULE.MODULE_NO,
                            PHRASE_OPTION.NAME.as("PHNAME"),
                            PHRASE_OPTION.HEADER,
                            PHRASE_OPTION_ITEM.ITEM_NO,
                            PHRASE.PHRASE_ID,
                            PHRASE.VALUE,
                            PHRASE.ITEM_PART_NO
                    )
                    .from(SENTENCE)
                    .innerJoin(SENTENCE_MODULE).onKey(Keys.FK_SENTENCE)
                    .innerJoin(PHRASE_OPTION).onKey(Keys.FK_PHRASE_OPTION)
                    .innerJoin(PHRASE_OPTION_ITEM).onKey(Keys.FK_COMPONENT)
                    .innerJoin(PHRASE).onKey(Keys.FK_SUBCOMPONENT)
                    .where(
                            SENTENCE_MODULE.MODULE_NO.eq(moduleNo)
                                    .and(SENTENCE.SENTENCE_ID.eq(sentenceDeId))
                                    .and(PHRASE_OPTION_ITEM.ITEM_NO.eq(itemNo))
                                    .and(PHRASE.ITEM_PART_NO.eq(itemPartNo))
                    );
            Record11<String, String, String, Long, Long, String, String, Long, Long, String, Long> row=query2.fetchOne();
            if (row==null)
                return null;
            Long phraseDeId = row.getValue(PHRASE.PHRASE_ID);
            Long moduleNoDe = row.getValue(SENTENCE_MODULE.MODULE_NO);
            String phraseValueDe = row.getValue(PHRASE.VALUE);
            return tuple(phraseDeId,moduleNoDe,phraseValueDe,phraseValueIt);


        }
    }

    private Tuple4<Long, String, String, String> findOptionId(Long phraseId, String language) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            DSLContext create = using(conn, SQLDialect.DEFAULT);

            create.configuration().settings().setRenderCatalog(false);
            create.configuration().settings().setRenderSchema(false);
            create.configuration().settings().setRenderNameStyle(RenderNameStyle.LOWER);

            SelectConditionStep<Record4<String, Long, String, Long>> query1 = create
                    .select(
                            PHRASE_OPTION.NAME,
                            PHRASE_OPTION_ITEM.ITEM_NO,
                            PHRASE.VALUE,
                            PHRASE.ITEM_PART_NO
                    )
                    .from(PHRASE_OPTION)
                    .innerJoin(PHRASE_OPTION_ITEM).onKey(Keys.FK_COMPONENT)
                    .innerJoin(PHRASE).onKey(Keys.FK_SUBCOMPONENT)
                    .where(PHRASE.PHRASE_ID.eq(phraseId));
            Record4<String, Long, String, Long> q1Resultset= query1.fetchOne();
            if (q1Resultset==null){
                return null;
            }
            String optionName=q1Resultset.getValue(PHRASE_OPTION.NAME);
            Long itemNo=q1Resultset.getValue(PHRASE_OPTION_ITEM.ITEM_NO);
            String optionValueIt=q1Resultset.getValue(PHRASE.VALUE);

            SelectConditionStep<Record3<String, Long, String>> query2 = create
                    .select(
                            PHRASE_OPTION.HEADER,
                            PHRASE.PHRASE_ID,
                            PHRASE.VALUE
                    )
                    .from(PHRASE_OPTION)
                    .innerJoin(PHRASE_OPTION_ITEM).onKey(Keys.FK_COMPONENT)
                    .innerJoin(PHRASE).onKey(Keys.FK_SUBCOMPONENT)
                    .where(PHRASE_OPTION.NAME.eq(optionName))
                        .and(PHRASE_OPTION_ITEM.ITEM_NO.eq(itemNo))
                        .and(PHRASE_OPTION.LANGUAGE.eq(language));

            Record3<String, Long, String> row=query2.fetchOne();
            Long optiondId=row.getValue(PHRASE.PHRASE_ID);
            String header=row.getValue(PHRASE_OPTION.HEADER);
            String optionValueDe=row.getValue(PHRASE.VALUE);
            return tuple(optiondId,header,optionValueDe, optionValueIt);
        }
    }

}