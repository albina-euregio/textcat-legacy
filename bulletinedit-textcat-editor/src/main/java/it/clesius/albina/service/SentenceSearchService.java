package it.clesius.albina.service;

import it.clesius.albina.jooq.Keys;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.SelectConditionStep;
import org.jooq.conf.RenderNameStyle;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static it.clesius.albina.jooq.Tables.PHRASE;
import static it.clesius.albina.jooq.Tables.PHRASE_OPTION;
import static it.clesius.albina.jooq.Tables.PHRASE_OPTION_ITEM;
import static it.clesius.albina.jooq.Tables.SENTENCE;
import static it.clesius.albina.jooq.Tables.SENTENCE_MODULE;
import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.inline;
import static org.jooq.impl.DSL.using;

public class SentenceSearchService {
    @Autowired
    DataSource dataSource;

    public TreeMap<String, Object> getSentences(String sentence, String lang, String se) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            DSLContext create = using(conn, SQLDialect.POSTGRES);

            create.configuration().settings().setRenderCatalog(false);
            create.configuration().settings().setRenderSchema(false);
            create.configuration().settings().setRenderNameStyle(RenderNameStyle.LOWER);

            String[] array = sentence.split(" ");
            String tosearch = array[0];
            List<String> startWithComma = new ArrayList<>();

            String tmpStopWords = new String();
            switch (lang) {
                case "de":
                    //    tmpStopWords = "a,ab,aber,ach,acht,achte,achten,achter,achtes,ag,alle,allein,allem,allen,aller,allerdings,alles,allgemeinen,als,also,am,an,ander,andere,anderem,anderen,anderer,anderes,anderm,andern,anderr,anders,au,auch,auf,aus,ausser,ausserdem,außer,außerdem,b,bald,bei,beide,beiden,beim,beispiel,bekannt,bereits,besonders,besser,besten,bin,bis,bisher,bist,c,d,d.h,da,dabei,dadurch,dafür,dagegen,daher,dahin,dahinter,damals,damit,danach,daneben,dank,dann,daran,darauf,daraus,darf,darfst,darin,darum,darunter,darüber,das,dasein,daselbst,dass,dasselbe,davon,davor,dazu,dazwischen,daß,dein,deine,deinem,deinen,deiner,deines,dem,dementsprechend,demgegenüber,demgemäss,demgemäß,demselben,demzufolge,den,denen,denn,denselben,der,deren,derer,derjenige,derjenigen,dermassen,dermaßen,derselbe,derselben,des,deshalb,desselben,dessen,deswegen,dich,die,diejenige,diejenigen,dies,diese,dieselbe,dieselben,diesem,diesen,dieser,dieses,dir,doch,dort,drei,drin,dritte,dritten,dritter,drittes,du,durch,durchaus,durfte,durften,dürfen,dürft,e,eben,ebenso,ehrlich,ei,ei,,eigen,eigene,eigenen,eigener,eigenes,ein,einander,eine,einem,einen,einer,eines,einig,einige,einigem,einigen,einiger,einiges,einmal,eins,elf,en,ende,endlich,entweder,er,ernst,erst,erste,ersten,erster,erstes,es,etwa,etwas,euch,euer,eure,eurem,euren,eurer,eures,f,folgende,früher,fünf,fünfte,fünften,fünfter,fünftes,für,g,gab,ganz,ganze,ganzen,ganzer,ganzes,gar,gedurft,gegen,gegenüber,gehabt,gehen,geht,gekannt,gekonnt,gemacht,gemocht,gemusst,genug,gerade,gern,gesagt,geschweige,gewesen,gewollt,geworden,gibt,ging,gleich,gott,gross,grosse,grossen,grosser,grosses,groß,große,großen,großer,großes,gut,gute,guter,gutes,h,hab,habe,haben,habt,hast,hat,hatte,hatten,hattest,hattet,heisst,her,heute,hier,hin,hinter,hoch,hätte,hätten,i,ich,ihm,ihn,ihnen,ihr,ihre,ihrem,ihren,ihrer,ihres,im,immer,in,indem,infolgedessen,ins,irgend,ist,j,ja,jahr,jahre,jahren,je,jede,jedem,jeden,jeder,jedermann,jedermanns,jedes,jedoch,jemand,jemandem,jemanden,jene,jenem,jenen,jener,jenes,jetzt,k,kam,kann,kannst,kaum,kein,keine,keinem,keinen,keiner,keines,kleine,kleinen,kleiner,kleines,kommen,kommt,konnte,konnten,kurz,können,könnt,könnte,l,lang,lange,leicht,leide,lieber,los,m,machen,macht,machte,mag,magst,mahn,mal,man,manche,manchem,manchen,mancher,manches,mann,mehr,mein,meine,meinem,meinen,meiner,meines,mensch,menschen,mich,mir,mit,mittel,mochte,mochten,morgen,muss,musst,musste,mussten,muß,mußt,möchte,mögen,möglich,mögt,müssen,müsst,müßt,n,na,nach,nachdem,nahm,natürlich,neben,nein,neue,neuen,neun,neunte,neunten,neunter,neuntes,nicht,nichts,nie,niemand,niemandem,niemanden,noch,nun,nur,o,ob,oben,oder,offen,oft,ohne,ordnung,p,q,r,recht,rechte,rechten,rechter,rechtes,richtig,rund,s,sa,sache,sagt,sagte,sah,satt,schlecht,schluss,schon,sechs,sechste,sechsten,sechster,sechstes,sehr,sei,seid,seien,sein,seine,seinem,seinen,seiner,seines,seit,seitdem,selbst,sich,sie,sieben,siebente,siebenten,siebenter,siebentes,sind,so,solang,solche,solchem,solchen,solcher,solches,soll,sollen,sollst,sollt,sollte,sollten,sondern,sonst,soweit,sowie,später,startseite,statt,steht,suche,t,tag,tage,tagen,tat,teil,tel,tritt,trotzdem,tun,u,uhr,um,und,und?,uns,unse,unsem,unsen,unser,unsere,unserer,unses,unter,v,vergangenen,viel,viele,vielem,vielen,vielleicht,vier,vierte,vierten,vierter,viertes,vom,von,vor,w,wahr?,wann,war,waren,warst,wart,warum,was,weg,wegen,weil,weit,weiter,weitere,weiteren,weiteres,welche,welchem,welchen,welcher,welches,wem,wen,wenig,wenige,weniger,weniges,wenigstens,wenn,wer,werde,werden,werdet,weshalb,wessen,wie,wieder,wieso,will,willst,wir,wird,wirklich,wirst,wissen,wo,woher,wohin,wohl,wollen,wollt,wollte,wollten,worden,wurde,wurden,während,währenddem,währenddessen,wäre,würde,würden,x,y,z,z.b,zehn,zehnte,zehnten,zehnter,zehntes,zeit,zu,zuerst,zugleich,zum,zunächst,zur,zurück,zusammen,zwanzig,zwar,zwei,zweite,zweiten,zweiter,zweites,zwischen,zwölf,über,überhaupt,übrigens";
                    tmpStopWords = "aber,alle,allem,allen,aller,alles,als,also,am,an,ander,andere,anderem,anderen,anderer,anderes,anderm,andern,anders,auch,auf,aus,bei,bin,bis,bist,da,damit,dann,das,dass,dasselbe,dazu,daß,dein,deine,deinem,deinen,deiner,deines,dem,demselben,den,denn,denselben,der,derer,derselbe,derselben,des,desselben,dessen,dich,die,dies,diese,dieselbe,dieselben,diesem,diesen,dieser,dieses,dir,doch,dort,du,durch,ein,eine,einem,einen,einer,eines,einig,einige,einigem,einigen,einiger,einiges,einmal,er,es,etwas,euch,euer,eure,eurem,euren,eurer,eures,für,gegen,gewesen,hab,habe,haben,hat,hatte,hatten,hier,hin,hinter,ich,ihm,ihn,ihnen,ihr,ihre,ihrem,ihren,ihrer,ihres,im,in,indem,ins,ist,jede,jedem,jeden,jeder,jedes,jene,jenem,jenen,jener,jenes,jetzt,kann,kein,keine,keinem,keinen,keiner,keines,können,könnte,machen,man,manche,manchem,manchen,mancher,manches,mein,meine,meinem,meinen,meiner,meines,mich,mir,mit,muss,musste,nach,nicht,nichts,noch,nun,nur,ob,oder,ohne,sehr,sein,seine,seinem,seinen,seiner,seines,selbst,sich,sie,sind,so,solche,solchem,solchen,solcher,solches,soll,sollte,sondern,sonst,um,und,uns,unser,unsere,unserem,unseren,unserer,unseres,unter,viel,vom,von,vor,war,waren,warst,was,weg,weil,weiter,welche,welchem,welchen,welcher,welches,wenn,werde,werden,wie,wieder,will,wir,wird,wirst,wo,wollen,wollte,während,würde,würden,zu,zum,zur,zwar,zwischen,über";
                    break;
                case "it":
                    //   tmpStopWords = "a,abbastanza,abbia,abbiamo,abbiano,abbiate,accidenti,ad,adesso,affinche,agl,agli,ahime,ahimã¨,ahimè,ai,al,alcuna,alcuni,alcuno,all,alla,alle,allo,allora,altre,altri,altrimenti,altro,altrove,altrui,anche,ancora,anni,anno,ansa,anticipo,assai,attesa,attraverso,avanti,avemmo,avendo,avente,aver,avere,averlo,avesse,avessero,avessi,avessimo,aveste,avesti,avete,aveva,avevamo,avevano,avevate,avevi,avevo,avrai,avranno,avrebbe,avrebbero,avrei,avremmo,avremo,avreste,avresti,avrete,avrà,avrò,avuta,avute,avuti,avuto,basta,ben,bene,benissimo,berlusconi,brava,bravo,buono,c,casa,caso,cento,certa,certe,certi,certo,che,chi,chicchessia,chiunque,ci,ciascuna,ciascuno,cima,cinque,cio,cioe,cioã¨,cioè,circa,citta,città,cittã,ciã²,ciò,co,codesta,codesti,codesto,cogli,coi,col,colei,coll,coloro,colui,come,cominci,comprare,comunque,con,concernente,conciliarsi,conclusione,consecutivi,consecutivo,consiglio,contro,cortesia,cos,cosa,cosi,cosã¬,così,cui,d,da,dagl,dagli,dai,dal,dall,dalla,dalle,dallo,dappertutto,davanti,degl,degli,dei,del,dell,della,delle,dello,dentro,detto,deve,devo,di,dice,dietro,dire,dirimpetto,diventa,diventare,diventato,dopo,doppio,dov,dove,dovra,dovrà,dovrã,dovunque,due,dunque,durante,e,ebbe,ebbero,ebbi,ecc,ecco,ed,effettivamente,egli,ella,entrambi,eppure,era,erano,eravamo,eravate,eri,ero,esempio,esse,essendo,esser,essere,essi,ex,fa,faccia,facciamo,facciano,facciate,faccio,facemmo,facendo,facesse,facessero,facessi,facessimo,faceste,facesti,faceva,facevamo,facevano,facevate,facevi,facevo,fai,fanno,farai,faranno,fare,farebbe,farebbero,farei,faremmo,faremo,fareste,faresti,farete,farà,farò,fatto,favore,fece,fecero,feci,fin,finalmente,finche,fine,fino,forse,forza,fosse,fossero,fossi,fossimo,foste,fosti,fra,frattempo,fu,fui,fummo,fuori,furono,futuro,generale,gente,gia,giacche,giorni,giorno,giu,già,giã,gli,gliela,gliele,glieli,glielo,gliene,governo,grande,grazie,gruppo,ha,haha,hai,hanno,ho,i,ie,ieri,il,improvviso,in,inc,indietro,infatti,inoltre,insieme,intanto,intorno,invece,io,l,la,lasciato,lato,lavoro,le,lei,li,lo,lontano,loro,lui,lungo,luogo,là,lã,ma,macche,magari,maggior,mai,male,malgrado,malissimo,mancanza,marche,me,medesimo,mediante,meglio,meno,mentre,mesi,mezzo,mi,mia,mie,miei,mila,miliardi,milioni,minimi,ministro,mio,modo,molta,molti,moltissimo,molto,momento,mondo,mosto,nazionale,ne,negl,negli,nei,nel,nell,nella,nelle,nello,nemmeno,neppure,nessun,nessuna,nessuno,niente,no,noi,nome,non,nondimeno,nonostante,nonsia,nostra,nostre,nostri,nostro,novanta,nove,nulla,nuovi,nuovo,o,od,oggi,ogni,ognuna,ognuno,oltre,oppure,ora,ore,osi,ossia,ottanta,otto,paese,parecchi,parecchie,parecchio,parte,partendo,peccato,peggio,per,perche,perchã¨,perchè,perché,percio,perciã²,perciò,perfino,pero,persino,persone,perã²,però,piedi,pieno,piglia,piu,piuttosto,piã¹,più,po,pochissimo,poco,poi,poiche,possa,possedere,posteriore,posto,potrebbe,preferibilmente,presa,press,prima,primo,principalmente,probabilmente,promesso,proprio,puo,pure,purtroppo,puã²,può,qua,qualche,qualcosa,qualcuna,qualcuno,quale,quali,qualunque,quando,quanta,quante,quanti,quanto,quantunque,quarto,quasi,quattro,quel,quella,quelle,quelli,quello,quest,questa,queste,questi,questo,qui,quindi,quinto,realmente,recente,recentemente,registrazione,relativo,riecco,rispetto,salvo,sara,sarai,saranno,sarebbe,sarebbero,sarei,saremmo,saremo,sareste,saresti,sarete,sarà,sarã,sarò,scola,scopo,scorso,se,secondo,seguente,seguito,sei,sembra,sembrare,sembrato,sembrava,sembri,sempre,senza,sette,si,sia,siamo,siano,siate,siete,sig,solito,solo,soltanto,sono,sopra,soprattutto,sotto,spesso,srl,sta,stai,stando,stanno,starai,staranno,starebbe,starebbero,starei,staremmo,staremo,stareste,staresti,starete,starà,starò,stata,state,stati,stato,stava,stavamo,stavano,stavate,stavi,stavo,stemmo,stessa,stesse,stessero,stessi,stessimo,stesso,steste,stesti,stette,stettero,stetti,stia,stiamo,stiano,stiate,sto,su,sua,subito,successivamente,successivo,sue,sugl,sugli,sui,sul,sull,sulla,sulle,sullo,suo,suoi,tale,tali,talvolta,tanto,te,tempo,terzo,th,ti,titolo,torino,tra,tranne,tre,trenta,triplo,troppo,trovato,tu,tua,tue,tuo,tuoi,tutta,tuttavia,tutte,tutti,tutto,uguali,ulteriore,ultimo,un,una,uno,uomo,va,vai,vale,vari,varia,varie,vario,verso,vi,via,vicino,visto,vita,voi,volta,volte,vostra,vostre,vostri,vostro,ã¨,è";
                    tmpStopWords = "a,abbia,abbiamo,abbiano,abbiate,ad,adesso,agl,agli,ai,al,all,alla,alle,allo,allora,altre,altri,altro,anche,ancora,avemmo,avendo,avere,avesse,avessero,avessi,avessimo,aveste,avesti,avete,aveva,avevamo,avevano,avevate,avevi,avevo,avrai,avranno,avrebbe,avrebbero,avrei,avremmo,avremo,avreste,avresti,avrete,avrà,avrò,avuta,avute,avuti,avuto,c,che,chi,ci,coi,col,come,con,contro,cui,da,dagl,dagli,dai,dal,dall,dalla,dalle,dallo,degl,degli,dei,del,dell,della,delle,dello,dentro,di,dov,dove,e,ebbe,ebbero,ebbi,ecco,ed,era,erano,eravamo,eravate,eri,ero,essendo,faccia,facciamo,facciano,facciate,faccio,facemmo,facendo,facesse,facessero,facessi,facessimo,faceste,facesti,faceva,facevamo,facevano,facevate,facevi,facevo,fai,fanno,farai,faranno,fare,farebbe,farebbero,farei,faremmo,faremo,fareste,faresti,farete,farà,farò,fece,fecero,feci,fino,fosse,fossero,fossi,fossimo,foste,fosti,fra,fu,fui,fummo,furono,giù,gli,ha,hai,hanno,ho,i,il,in,io,l,la,le,lei,li,lo,loro,lui,ma,me,mi,mia,mie,miei,mio,ne,negl,negli,nei,nel,nell,nella,nelle,nello,no,noi,non,nostra,nostre,nostri,nostro,o,per,perché,però,più,pochi,poco,qua,quale,quanta,quante,quanti,quanto,quasi,quella,quelle,quelli,quello,questa,queste,questi,questo,qui,quindi,sarai,saranno,sarebbe,sarebbero,sarei,saremmo,saremo,sareste,saresti,sarete,sarà,sarò,se,sei,senza,si,sia,siamo,siano,siate,siete,sono,sopra,sotto,sta,stai,stando,stanno,starai,staranno,stare,starebbe,starebbero,starei,staremmo,staremo,stareste,staresti,starete,starà,starò,stava,stavamo,stavano,stavate,stavi,stavo,stemmo,stesse,stessero,stessi,stessimo,stesso,steste,stesti,stette,stettero,stetti,stia,stiamo,stiano,stiate,sto,su,sua,sue,sugl,sugli,sui,sul,sull,sulla,sulle,sullo,suo,suoi,te,ti,tra,tu,tua,tue,tuo,tuoi,tutti,tutto,un,una,uno,vai,vi,voi,vostra,vostre,vostri,vostro,è";
                    break;
                default:
                    tmpStopWords = "";
                    break;
            }

            List<String> stopWords = new ArrayList<String>(Arrays.asList(tmpStopWords.split(",")));

            Map<Long, Integer> bestResult = new HashMap<>();
            Result<Record1<Long>> qnResultset = null;

            //SEARCH the word
            for (int i = 0; i < array.length; i++) {
                tosearch = array[i];
                if (i > 1) {
                    if (tosearch.substring(tosearch.length() - 1).equals(",")) {

                        // search if the is a sentence that start with ,     only in first module
                        String tosearch2 = "";
                        tosearch2 = ", " + array[i + 1];
                        SelectConditionStep<Record1<Long>> query2 = create
                                .select(
                                        SENTENCE.SENTENCE_ID
                                )
                                .distinctOn(field("SENTENCE_ID"))
                                .from(SENTENCE)
                                .innerJoin(SENTENCE_MODULE).onKey(Keys.FK_SENTENCE)
                                .innerJoin(PHRASE_OPTION).onKey(Keys.FK_PHRASE_OPTION)
                                .innerJoin(PHRASE_OPTION_ITEM).onKey(Keys.FK_COMPONENT)
                                .innerJoin(PHRASE).onKey(Keys.FK_SUBCOMPONENT)
                                .where(PHRASE.VALUE.likeIgnoreCase(tosearch2 + "%"))
                                .and(SENTENCE.LANGUAGE.eq(lang))
                                .and(SENTENCE.DOMAIN_ID.eq(1L)) //fixed for texcat
                                .and(SENTENCE_MODULE.MODULE_NO.eq(SENTENCE.STRUCTURE.substring(1, 1).cast(Long.class)));
                        qnResultset = query2.fetch();
                        if (qnResultset.size() > 0) {
                            startWithComma.add(tosearch2);
                        }
                    }
                }

                tosearch = tosearch.replace(",", "");
                tosearch = tosearch.replace(";", "");
                tosearch = tosearch.replace(".", "");


                if (i < 2 && se.equals("start")) {

                    // search only in first and second module
                    SelectConditionStep<Record1<Long>> query2 = create
                            .select(
                                    SENTENCE.SENTENCE_ID
                            )
                            .distinctOn(field("SENTENCE_ID"))
                            .from(SENTENCE)
                            .innerJoin(SENTENCE_MODULE).onKey(Keys.FK_SENTENCE)
                            .innerJoin(PHRASE_OPTION).onKey(Keys.FK_PHRASE_OPTION)
                            .innerJoin(PHRASE_OPTION_ITEM).onKey(Keys.FK_COMPONENT)
                            .innerJoin(PHRASE).onKey(Keys.FK_SUBCOMPONENT)
                            .where(PHRASE.VALUE.likeIgnoreCase(tosearch + "%"))
                            .and(SENTENCE.LANGUAGE.eq(lang))
                            .and(SENTENCE.DOMAIN_ID.eq(1L)) //fixed for texcat
                            .and(SENTENCE_MODULE.MODULE_NO.eq(SENTENCE.STRUCTURE.substring(1, 1).cast(Long.class))
                                    .or(SENTENCE_MODULE.MODULE_NO.eq(SENTENCE.STRUCTURE.substring(5, 1).cast(Long.class))));
                    qnResultset = query2.fetch();

                    SelectConditionStep<Record1<String>> subphrases = create
                            .select(PHRASE_OPTION.NAME)
                            .distinctOn(field("NAME"))
                            .from(PHRASE)
                            .innerJoin(PHRASE_OPTION_ITEM).on(PHRASE_OPTION_ITEM.PHRASE_OPTION_ITEM_ID.eq(PHRASE.PHRASE_OPTION_ITEM_ID))
                            .innerJoin(PHRASE_OPTION).on(PHRASE_OPTION.PHRASE_OPTION_ID.eq(PHRASE_OPTION_ITEM.PHRASE_OPTION_ID))
                            .leftJoin(SENTENCE_MODULE).on(SENTENCE_MODULE.PHRASE_OPTION_ID.eq(PHRASE_OPTION.PHRASE_OPTION_ID))
                            .where(PHRASE.VALUE.likeIgnoreCase(tosearch + "%"))
                            .and(PHRASE_OPTION.LANGUAGE.eq(lang))
                            .and(PHRASE_OPTION.DOMAIN_ID.eq(1L))
                            //.and(SENTENCE_MODULE.SENTENCE_ID.isNull())
                            ;

                    SelectConditionStep<Record1<Long>> query3 = create
                            .select(
                                    SENTENCE.SENTENCE_ID
                            )
                            .distinctOn(field("SENTENCE_ID"))
                            .from(subphrases)

                            .innerJoin(PHRASE_OPTION).on(PHRASE_OPTION.NAME.eq((Field<String>) subphrases.field(0)))
                            .innerJoin(PHRASE).on(PHRASE.VALUE.likeIgnoreCase(concat(inline("%"), PHRASE_OPTION.NAME, inline("%"))))
                            .innerJoin(PHRASE_OPTION_ITEM).on(PHRASE_OPTION_ITEM.PHRASE_OPTION_ITEM_ID.eq(PHRASE.PHRASE_OPTION_ITEM_ID))
                            .innerJoin(SENTENCE_MODULE).on(SENTENCE_MODULE.PHRASE_OPTION_ID.eq(PHRASE_OPTION_ITEM.PHRASE_OPTION_ID))

                            //.innerJoin(SENTENCE_MODULE).on(SENTENCE_MODULE.PHRASE_OPTION_ID.eq(PHRASE_OPTION.PHRASE_OPTION_ID))
                            .innerJoin(SENTENCE).on(SENTENCE.SENTENCE_ID.eq(SENTENCE_MODULE.SENTENCE_ID))

                            //.innerJoin(SENTENCE_MODULE).on(SENTENCE_MODULE.PHRASE_OPTION_ID.eq((Field<Long>) subphrases.field(0)))
                            //.innerJoin(SENTENCE).on(SENTENCE.SENTENCE_ID.eq(SENTENCE_MODULE.SENTENCE_ID))
                            .where(SENTENCE.LANGUAGE.eq(lang))
                            .and(SENTENCE.DOMAIN_ID.eq(1L))
                            .and(SENTENCE_MODULE.MODULE_NO.eq(SENTENCE.STRUCTURE.substring(1, 1).cast(Long.class))
                                    .or(SENTENCE_MODULE.MODULE_NO.eq(SENTENCE.STRUCTURE.substring(5, 1).cast(Long.class))));

                    qnResultset.addAll(query3.fetch());


                } else {
                    if (!stopWords.contains(tosearch)) {
                        SelectConditionStep<Record1<Long>> query2 = null;
                        if (bestResult.size() > 3 && se.equals("start")) {
                            query2 = create
                                    .select(
                                            SENTENCE.SENTENCE_ID
                                    )
                                    .distinctOn(field("SENTENCE_ID"))
                                    .from(SENTENCE)
                                    .innerJoin(SENTENCE_MODULE).onKey(Keys.FK_SENTENCE)
                                    .innerJoin(PHRASE_OPTION).onKey(Keys.FK_PHRASE_OPTION)
                                    .innerJoin(PHRASE_OPTION_ITEM).onKey(Keys.FK_COMPONENT)
                                    .innerJoin(PHRASE).onKey(Keys.FK_SUBCOMPONENT)
                                    .where(PHRASE.VALUE.likeIgnoreCase("%" + tosearch + "%"))
                                    .and(SENTENCE.LANGUAGE.eq(lang))
                                    .and(SENTENCE.DOMAIN_ID.eq(1L)) //fixed for texcat
                                    .and(SENTENCE.SENTENCE_ID.in(bestResult.keySet()))
                                    .and(SENTENCE_MODULE.MODULE_NO.ne(SENTENCE.STRUCTURE.substring(1, 1).cast(Long.class)))
                                    .and(SENTENCE_MODULE.MODULE_NO.ne(SENTENCE.STRUCTURE.substring(5, 1).cast(Long.class)))
                            ;
                        } else {
                            if (se.equals("start")) {
                                query2 = create
                                        .select(
                                                SENTENCE.SENTENCE_ID
                                        )
                                        .distinctOn(field("SENTENCE_ID"))
                                        .from(SENTENCE)
                                        .innerJoin(SENTENCE_MODULE).onKey(Keys.FK_SENTENCE)
                                        .innerJoin(PHRASE_OPTION).onKey(Keys.FK_PHRASE_OPTION)
                                        .innerJoin(PHRASE_OPTION_ITEM).onKey(Keys.FK_COMPONENT)
                                        .innerJoin(PHRASE).onKey(Keys.FK_SUBCOMPONENT)
                                        .where(PHRASE.VALUE.likeIgnoreCase("%" + tosearch + "%"))
                                        .and(SENTENCE.LANGUAGE.eq(lang))
                                        .and(SENTENCE.DOMAIN_ID.eq(1L)) //fixed for texcat
                                        .and(SENTENCE_MODULE.MODULE_NO.ne(SENTENCE.STRUCTURE.substring(1, 1).cast(Long.class)))
                                        .and(SENTENCE_MODULE.MODULE_NO.ne(SENTENCE.STRUCTURE.substring(5, 1).cast(Long.class)))
                                ;
                            } else {
                                query2 = create
                                        .select(
                                                SENTENCE.SENTENCE_ID
                                        )
                                        .distinctOn(field("SENTENCE_ID"))
                                        .from(SENTENCE)
                                        .innerJoin(SENTENCE_MODULE).onKey(Keys.FK_SENTENCE)
                                        .innerJoin(PHRASE_OPTION).onKey(Keys.FK_PHRASE_OPTION)
                                        .innerJoin(PHRASE_OPTION_ITEM).onKey(Keys.FK_COMPONENT)
                                        .innerJoin(PHRASE).onKey(Keys.FK_SUBCOMPONENT)
                                        .where(PHRASE.VALUE.likeIgnoreCase("%" + tosearch + "%"))
                                        .and(SENTENCE.LANGUAGE.eq(lang))
                                        .and(SENTENCE.DOMAIN_ID.eq(1L)) //fixed for texcat
                                ;
                            }

                        }
                        qnResultset = query2.fetch();

                        SelectConditionStep<Record1<Long>> subphrases = create
                                .select(PHRASE_OPTION.PHRASE_OPTION_ID)
                                .distinctOn(field("PHRASE_OPTION_ID"))
                                .from(PHRASE)
                                .innerJoin(PHRASE_OPTION_ITEM).on(PHRASE_OPTION_ITEM.PHRASE_OPTION_ITEM_ID.eq(PHRASE.PHRASE_OPTION_ITEM_ID))
                                .innerJoin(PHRASE_OPTION).on(PHRASE_OPTION.PHRASE_OPTION_ID.eq(PHRASE_OPTION_ITEM.PHRASE_OPTION_ID))
                                .leftJoin(SENTENCE_MODULE).on(SENTENCE_MODULE.PHRASE_OPTION_ID.eq(PHRASE_OPTION.PHRASE_OPTION_ID))
                                .where(PHRASE.VALUE.likeIgnoreCase("%" + tosearch + "%"))
                                .and(PHRASE_OPTION.LANGUAGE.eq(lang))
                                .and(PHRASE_OPTION.DOMAIN_ID.eq(1L))
                                //.and(SENTENCE_MODULE.SENTENCE_ID.isNull())
                                ;
                        SelectConditionStep<Record1<Long>> query3 = null;

                        if (bestResult.size() > 3) {
                            query3 = create
                                    .select(
                                            SENTENCE.SENTENCE_ID
                                    )
                                    .distinctOn(field("SENTENCE_ID"))
                                    .from(subphrases)
                                    .innerJoin(SENTENCE_MODULE).on(SENTENCE_MODULE.PHRASE_OPTION_ID.eq((Field<Long>) subphrases.field(0)))
                                    .innerJoin(SENTENCE).on(SENTENCE.SENTENCE_ID.eq(SENTENCE_MODULE.SENTENCE_ID))
                                    .where(SENTENCE.LANGUAGE.eq(lang))
                                    .and(SENTENCE.DOMAIN_ID.eq(1L))
                                    .and(SENTENCE.SENTENCE_ID.in(bestResult.keySet()))
                            ;
                        } else {
                            query3 = create
                                    .select(
                                            SENTENCE.SENTENCE_ID
                                    )
                                    .distinctOn(field("SENTENCE_ID"))
                                    .from(subphrases)
                                    .innerJoin(SENTENCE_MODULE).on(SENTENCE_MODULE.PHRASE_OPTION_ID.eq((Field<Long>) subphrases.field(0)))
                                    .innerJoin(SENTENCE).on(SENTENCE.SENTENCE_ID.eq(SENTENCE_MODULE.SENTENCE_ID))
                                    .where(SENTENCE.LANGUAGE.eq(lang))
                                    .and(SENTENCE.DOMAIN_ID.eq(1L))
                            ;
                        }
                        qnResultset.addAll(query3.fetch());
                    }
                }
                if (qnResultset != null) {
                    if (qnResultset.isNotEmpty()) {
                        for (Record result : qnResultset) {
                            Integer punkt = 0;

                            if (i < 2 && se.equals("start")) {
                                punkt = 2;
                            } else {
                                punkt = 1;
                            }

                            Long id = result.getValue(SENTENCE.SENTENCE_ID);
                            if (bestResult.containsKey(id)) {
                                bestResult.put(id, bestResult.get(id) + punkt);
                            } else {
                                bestResult.put(id, punkt);
                            }
                        }
                    }

                }

            }

            //check number of word searched
            long numWords = 1 + Arrays.stream(array).skip(1).filter(s -> !stopWords.contains(s)).count();
            //sort descending and filter on 50 % of number of word
            List<Long> list = bestResult
                    .entrySet()
                    .stream()
                    .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                    .filter(x -> x.getValue() > numWords / 2)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());
            final TreeMap<String, Object> body = new TreeMap<>();
            body.put("startWithComma", startWithComma);
            body.put("sentences", list);
            return body;
        }
    }
}
