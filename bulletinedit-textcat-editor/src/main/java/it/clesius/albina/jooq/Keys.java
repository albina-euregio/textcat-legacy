/*
 * This file is generated by jOOQ.
*/
package it.clesius.albina.jooq;


import it.clesius.albina.jooq.tables.*;
import it.clesius.albina.jooq.tables.records.*;
import org.jooq.ForeignKey;
import org.jooq.UniqueKey;
import org.jooq.impl.AbstractKeys;

import javax.annotation.Generated;


/**
 * A class modelling foreign key relationships between tables of the <code>ALBINA</code> 
 * schema
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.2"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Keys {

    // -------------------------------------------------------------------------
    // IDENTITY definitions
    // -------------------------------------------------------------------------


    // -------------------------------------------------------------------------
    // UNIQUE and PRIMARY KEY definitions
    // -------------------------------------------------------------------------

    public static final UniqueKey<DomainRecord> PK_DOMAIN = UniqueKeys0.PK_DOMAIN;
    public static final UniqueKey<KeywordRecord> PK_KEYWORD = UniqueKeys0.PK_KEYWORD;
    public static final UniqueKey<PhraseRecord> PK_PHRASE = UniqueKeys0.PK_PHRASE;
    public static final UniqueKey<PhraseOptionRecord> PK_PHRASE_OPTION = UniqueKeys0.PK_PHRASE_OPTION;
    public static final UniqueKey<PhraseOptionRecord> UC_PHRASE_OPTION_NAME_BV = UniqueKeys0.UC_PHRASE_OPTION_NAME_BV;
    public static final UniqueKey<PhraseOptionRecord> UC_PHRASE_OPTION_NAME_V = UniqueKeys0.UC_PHRASE_OPTION_NAME_V;
    public static final UniqueKey<PhraseOptionItemRecord> PK_PHRASE_OPTION_ITEM = UniqueKeys0.PK_PHRASE_OPTION_ITEM;
    public static final UniqueKey<PhraseSearchRecord> PHRASE_SEARCH_PK = UniqueKeys0.PHRASE_SEARCH_PK;
    public static final UniqueKey<PhraseSearchSentencesRecord> PHRASE_SEARC_SENTENCES_PK = UniqueKeys0.PHRASE_SEARC_SENTENCES_PK;
    public static final UniqueKey<SentenceRecord> PK_SENTENCES = UniqueKeys0.PK_SENTENCES;
    public static final UniqueKey<SentenceRecord> UC_SENTENCES_NAME_BV = UniqueKeys0.UC_SENTENCES_NAME_BV;
    public static final UniqueKey<SentenceRecord> UC_SENTENCES_NAME_V = UniqueKeys0.UC_SENTENCES_NAME_V;
    public static final UniqueKey<Sentence_KeywordRecord> PK_SENTENCES__KEYWORDS = UniqueKeys0.PK_SENTENCES__KEYWORDS;
    public static final UniqueKey<SentenceModuleRecord> PK_SENTENCE_MODULE = UniqueKeys0.PK_SENTENCE_MODULE;
    public static final UniqueKey<Sentence_TopicRecord> PK_SENTENCES__TOPICS = UniqueKeys0.PK_SENTENCES__TOPICS;
    public static final UniqueKey<TopicRecord> PK_TOPIC = UniqueKeys0.PK_TOPIC;

    // -------------------------------------------------------------------------
    // FOREIGN KEY definitions
    // -------------------------------------------------------------------------

    public static final ForeignKey<PhraseRecord, PhraseOptionItemRecord> FK_SUBCOMPONENT = ForeignKeys0.FK_SUBCOMPONENT;
    public static final ForeignKey<PhraseOptionRecord, DomainRecord> FK_DOMAIN_PHRASE_OPTION = ForeignKeys0.FK_DOMAIN_PHRASE_OPTION;
    public static final ForeignKey<PhraseOptionItemRecord, PhraseOptionRecord> FK_COMPONENT = ForeignKeys0.FK_COMPONENT;
    public static final ForeignKey<SentenceRecord, DomainRecord> FK_DOMAIN_SENTENCE = ForeignKeys0.FK_DOMAIN_SENTENCE;
    public static final ForeignKey<Sentence_KeywordRecord, SentenceRecord> FK_SENTENCE_SENTENCE_KEYWORD = ForeignKeys0.FK_SENTENCE_SENTENCE_KEYWORD;
    public static final ForeignKey<Sentence_KeywordRecord, KeywordRecord> FK_KEYWORD_SENTENCE_KEYWORD = ForeignKeys0.FK_KEYWORD_SENTENCE_KEYWORD;
    public static final ForeignKey<SentenceModuleRecord, SentenceRecord> FK_SENTENCE = ForeignKeys0.FK_SENTENCE;
    public static final ForeignKey<SentenceModuleRecord, PhraseOptionRecord> FK_PHRASE_OPTION = ForeignKeys0.FK_PHRASE_OPTION;
    public static final ForeignKey<Sentence_TopicRecord, SentenceRecord> FK_SENTENCE_SENTENCE_TOPIC = ForeignKeys0.FK_SENTENCE_SENTENCE_TOPIC;
    public static final ForeignKey<Sentence_TopicRecord, TopicRecord> FK_TOPIC_SENTENCE_TOPIC = ForeignKeys0.FK_TOPIC_SENTENCE_TOPIC;

    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class UniqueKeys0 extends AbstractKeys {
        public static final UniqueKey<DomainRecord> PK_DOMAIN = createUniqueKey(Domain.DOMAIN, "pk_domain", Domain.DOMAIN.DOMAIN_ID);
        public static final UniqueKey<KeywordRecord> PK_KEYWORD = createUniqueKey(Keyword.KEYWORD, "pk_keyword", Keyword.KEYWORD.KEYWORD_ID);
        public static final UniqueKey<PhraseRecord> PK_PHRASE = createUniqueKey(Phrase.PHRASE, "pk_phrase", Phrase.PHRASE.PHRASE_ID);
        public static final UniqueKey<PhraseOptionRecord> PK_PHRASE_OPTION = createUniqueKey(PhraseOption.PHRASE_OPTION, "pk_phrase_option", PhraseOption.PHRASE_OPTION.PHRASE_OPTION_ID);
        public static final UniqueKey<PhraseOptionRecord> UC_PHRASE_OPTION_NAME_BV = createUniqueKey(PhraseOption.PHRASE_OPTION, "uc_phrase_option_name_bv", PhraseOption.PHRASE_OPTION.NAME, PhraseOption.PHRASE_OPTION.LANGUAGE, PhraseOption.PHRASE_OPTION.DOMAIN_ID, PhraseOption.PHRASE_OPTION.BASIS_VERSION);
        public static final UniqueKey<PhraseOptionRecord> UC_PHRASE_OPTION_NAME_V = createUniqueKey(PhraseOption.PHRASE_OPTION, "uc_phrase_option_name_v", PhraseOption.PHRASE_OPTION.NAME, PhraseOption.PHRASE_OPTION.LANGUAGE, PhraseOption.PHRASE_OPTION.DOMAIN_ID, PhraseOption.PHRASE_OPTION.VERSION);
        public static final UniqueKey<PhraseOptionItemRecord> PK_PHRASE_OPTION_ITEM = createUniqueKey(PhraseOptionItem.PHRASE_OPTION_ITEM, "pk_phrase_option_item", PhraseOptionItem.PHRASE_OPTION_ITEM.PHRASE_OPTION_ITEM_ID);
        public static final UniqueKey<PhraseSearchRecord> PHRASE_SEARCH_PK = createUniqueKey(PhraseSearch.PHRASE_SEARCH, "phrase_search_pk", PhraseSearch.PHRASE_SEARCH.PHRASEDECODED_ID);
        public static final UniqueKey<PhraseSearchSentencesRecord> PHRASE_SEARC_SENTENCES_PK = createUniqueKey(PhraseSearchSentences.PHRASE_SEARCH_SENTENCES, "phrase_searc_sentences_pk", PhraseSearchSentences.PHRASE_SEARCH_SENTENCES.PHRASEDECODED_ID, PhraseSearchSentences.PHRASE_SEARCH_SENTENCES.SENTENCE_ID, PhraseSearchSentences.PHRASE_SEARCH_SENTENCES.MODULE_NO);
        public static final UniqueKey<SentenceRecord> PK_SENTENCES = createUniqueKey(Sentence.SENTENCE, "pk_sentences", Sentence.SENTENCE.SENTENCE_ID);
        public static final UniqueKey<SentenceRecord> UC_SENTENCES_NAME_BV = createUniqueKey(Sentence.SENTENCE, "uc_sentences_name_bv", Sentence.SENTENCE.NAME, Sentence.SENTENCE.LANGUAGE, Sentence.SENTENCE.DOMAIN_ID, Sentence.SENTENCE.BASIS_VERSION);
        public static final UniqueKey<SentenceRecord> UC_SENTENCES_NAME_V = createUniqueKey(Sentence.SENTENCE, "uc_sentences_name_v", Sentence.SENTENCE.NAME, Sentence.SENTENCE.LANGUAGE, Sentence.SENTENCE.DOMAIN_ID, Sentence.SENTENCE.VERSION);
        public static final UniqueKey<Sentence_KeywordRecord> PK_SENTENCES__KEYWORDS = createUniqueKey(Sentence_Keyword.SENTENCE__KEYWORD, "pk_sentences__keywords", Sentence_Keyword.SENTENCE__KEYWORD.SENTENCE_ID, Sentence_Keyword.SENTENCE__KEYWORD.KEYWORD_ID);
        public static final UniqueKey<SentenceModuleRecord> PK_SENTENCE_MODULE = createUniqueKey(SentenceModule.SENTENCE_MODULE, "pk_sentence_module", SentenceModule.SENTENCE_MODULE.SENTENCE_ID, SentenceModule.SENTENCE_MODULE.PHRASE_OPTION_ID);
        public static final UniqueKey<Sentence_TopicRecord> PK_SENTENCES__TOPICS = createUniqueKey(Sentence_Topic.SENTENCE__TOPIC, "pk_sentences__topics", Sentence_Topic.SENTENCE__TOPIC.SENTENCE_ID, Sentence_Topic.SENTENCE__TOPIC.TOPIC_ID);
        public static final UniqueKey<TopicRecord> PK_TOPIC = createUniqueKey(Topic.TOPIC, "pk_topic", Topic.TOPIC.TOPIC_ID);
    }

    private static class ForeignKeys0 extends AbstractKeys {
        public static final ForeignKey<PhraseRecord, PhraseOptionItemRecord> FK_SUBCOMPONENT = createForeignKey(Keys.PK_PHRASE_OPTION_ITEM, Phrase.PHRASE, "fk_subcomponent", Phrase.PHRASE.PHRASE_OPTION_ITEM_ID);
        public static final ForeignKey<PhraseOptionRecord, DomainRecord> FK_DOMAIN_PHRASE_OPTION = createForeignKey(Keys.PK_DOMAIN, PhraseOption.PHRASE_OPTION, "fk_domain_phrase_option", PhraseOption.PHRASE_OPTION.DOMAIN_ID);
        public static final ForeignKey<PhraseOptionItemRecord, PhraseOptionRecord> FK_COMPONENT = createForeignKey(Keys.PK_PHRASE_OPTION, PhraseOptionItem.PHRASE_OPTION_ITEM, "fk_component", PhraseOptionItem.PHRASE_OPTION_ITEM.PHRASE_OPTION_ID);
        public static final ForeignKey<SentenceRecord, DomainRecord> FK_DOMAIN_SENTENCE = createForeignKey(Keys.PK_DOMAIN, Sentence.SENTENCE, "fk_domain_sentence", Sentence.SENTENCE.DOMAIN_ID);
        public static final ForeignKey<Sentence_KeywordRecord, SentenceRecord> FK_SENTENCE_SENTENCE_KEYWORD = createForeignKey(Keys.PK_SENTENCES, Sentence_Keyword.SENTENCE__KEYWORD, "fk_sentence_sentence_keyword", Sentence_Keyword.SENTENCE__KEYWORD.SENTENCE_ID);
        public static final ForeignKey<Sentence_KeywordRecord, KeywordRecord> FK_KEYWORD_SENTENCE_KEYWORD = createForeignKey(Keys.PK_KEYWORD, Sentence_Keyword.SENTENCE__KEYWORD, "fk_keyword_sentence_keyword", Sentence_Keyword.SENTENCE__KEYWORD.KEYWORD_ID);
        public static final ForeignKey<SentenceModuleRecord, SentenceRecord> FK_SENTENCE = createForeignKey(Keys.PK_SENTENCES, SentenceModule.SENTENCE_MODULE, "fk_sentence", SentenceModule.SENTENCE_MODULE.SENTENCE_ID);
        public static final ForeignKey<SentenceModuleRecord, PhraseOptionRecord> FK_PHRASE_OPTION = createForeignKey(Keys.PK_PHRASE_OPTION, SentenceModule.SENTENCE_MODULE, "fk_phrase_option", SentenceModule.SENTENCE_MODULE.PHRASE_OPTION_ID);
        public static final ForeignKey<Sentence_TopicRecord, SentenceRecord> FK_SENTENCE_SENTENCE_TOPIC = createForeignKey(Keys.PK_SENTENCES, Sentence_Topic.SENTENCE__TOPIC, "fk_sentence_sentence_topic", Sentence_Topic.SENTENCE__TOPIC.SENTENCE_ID);
        public static final ForeignKey<Sentence_TopicRecord, TopicRecord> FK_TOPIC_SENTENCE_TOPIC = createForeignKey(Keys.PK_TOPIC, Sentence_Topic.SENTENCE__TOPIC, "fk_topic_sentence_topic", Sentence_Topic.SENTENCE__TOPIC.TOPIC_ID);
    }
}
