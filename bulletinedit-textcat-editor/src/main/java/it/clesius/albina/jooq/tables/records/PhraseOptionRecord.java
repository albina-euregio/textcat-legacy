/*
 * This file is generated by jOOQ.
*/
package it.clesius.albina.jooq.tables.records;


import it.clesius.albina.jooq.tables.PhraseOption;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record11;
import org.jooq.Row11;
import org.jooq.impl.UpdatableRecordImpl;

import javax.annotation.Generated;
import java.sql.Date;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.2"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class PhraseOptionRecord extends UpdatableRecordImpl<PhraseOptionRecord> implements Record11<Long, String, String, String, String, String, String, String, Long, Date, Date> {

    private static final long serialVersionUID = 929316182;

    /**
     * Setter for <code>ALBINA.PHRASE_OPTION.PHRASE_OPTION_ID</code>.
     */
    public void setPhraseOptionId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>ALBINA.PHRASE_OPTION.PHRASE_OPTION_ID</code>.
     */
    public Long getPhraseOptionId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>ALBINA.PHRASE_OPTION.NAME</code>.
     */
    public void setName(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>ALBINA.PHRASE_OPTION.NAME</code>.
     */
    public String getName() {
        return (String) get(1);
    }

    /**
     * Setter for <code>ALBINA.PHRASE_OPTION.LANGUAGE</code>.
     */
    public void setLanguage(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>ALBINA.PHRASE_OPTION.LANGUAGE</code>.
     */
    public String getLanguage() {
        return (String) get(2);
    }

    /**
     * Setter for <code>ALBINA.PHRASE_OPTION.HEADER</code>.
     */
    public void setHeader(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>ALBINA.PHRASE_OPTION.HEADER</code>.
     */
    public String getHeader() {
        return (String) get(3);
    }

    /**
     * Setter for <code>ALBINA.PHRASE_OPTION.VERSION</code>.
     */
    public void setVersion(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>ALBINA.PHRASE_OPTION.VERSION</code>.
     */
    public String getVersion() {
        return (String) get(4);
    }

    /**
     * Setter for <code>ALBINA.PHRASE_OPTION.BASIS_VERSION</code>.
     */
    public void setBasisVersion(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>ALBINA.PHRASE_OPTION.BASIS_VERSION</code>.
     */
    public String getBasisVersion() {
        return (String) get(5);
    }

    /**
     * Setter for <code>ALBINA.PHRASE_OPTION.REMARK</code>.
     */
    public void setRemark(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>ALBINA.PHRASE_OPTION.REMARK</code>.
     */
    public String getRemark() {
        return (String) get(6);
    }

    /**
     * Setter for <code>ALBINA.PHRASE_OPTION.DELETED</code>.
     */
    public void setDeleted(String value) {
        set(7, value);
    }

    /**
     * Getter for <code>ALBINA.PHRASE_OPTION.DELETED</code>.
     */
    public String getDeleted() {
        return (String) get(7);
    }

    /**
     * Setter for <code>ALBINA.PHRASE_OPTION.DOMAIN_ID</code>.
     */
    public void setDomainId(Long value) {
        set(8, value);
    }

    /**
     * Getter for <code>ALBINA.PHRASE_OPTION.DOMAIN_ID</code>.
     */
    public Long getDomainId() {
        return (Long) get(8);
    }

    /**
     * Setter for <code>ALBINA.PHRASE_OPTION.DATENEW</code>.
     */
    public void setDatenew(Date value) {
        set(9, value);
    }

    /**
     * Getter for <code>ALBINA.PHRASE_OPTION.DATENEW</code>.
     */
    public Date getDatenew() {
        return (Date) get(9);
    }

    /**
     * Setter for <code>ALBINA.PHRASE_OPTION.DATELAST</code>.
     */
    public void setDatelast(Date value) {
        set(10, value);
    }

    /**
     * Getter for <code>ALBINA.PHRASE_OPTION.DATELAST</code>.
     */
    public Date getDatelast() {
        return (Date) get(10);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record11 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row11<Long, String, String, String, String, String, String, String, Long, Date, Date> fieldsRow() {
        return (Row11) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row11<Long, String, String, String, String, String, String, String, Long, Date, Date> valuesRow() {
        return (Row11) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field1() {
        return PhraseOption.PHRASE_OPTION.PHRASE_OPTION_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return PhraseOption.PHRASE_OPTION.NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return PhraseOption.PHRASE_OPTION.LANGUAGE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return PhraseOption.PHRASE_OPTION.HEADER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field5() {
        return PhraseOption.PHRASE_OPTION.VERSION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field6() {
        return PhraseOption.PHRASE_OPTION.BASIS_VERSION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field7() {
        return PhraseOption.PHRASE_OPTION.REMARK;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field8() {
        return PhraseOption.PHRASE_OPTION.DELETED;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field9() {
        return PhraseOption.PHRASE_OPTION.DOMAIN_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Date> field10() {
        return PhraseOption.PHRASE_OPTION.DATENEW;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Date> field11() {
        return PhraseOption.PHRASE_OPTION.DATELAST;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value1() {
        return getPhraseOptionId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getLanguage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getHeader();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value5() {
        return getVersion();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value6() {
        return getBasisVersion();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value7() {
        return getRemark();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value8() {
        return getDeleted();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value9() {
        return getDomainId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date value10() {
        return getDatenew();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date value11() {
        return getDatelast();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PhraseOptionRecord value1(Long value) {
        setPhraseOptionId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PhraseOptionRecord value2(String value) {
        setName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PhraseOptionRecord value3(String value) {
        setLanguage(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PhraseOptionRecord value4(String value) {
        setHeader(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PhraseOptionRecord value5(String value) {
        setVersion(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PhraseOptionRecord value6(String value) {
        setBasisVersion(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PhraseOptionRecord value7(String value) {
        setRemark(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PhraseOptionRecord value8(String value) {
        setDeleted(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PhraseOptionRecord value9(Long value) {
        setDomainId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PhraseOptionRecord value10(Date value) {
        setDatenew(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PhraseOptionRecord value11(Date value) {
        setDatelast(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PhraseOptionRecord values(Long value1, String value2, String value3, String value4, String value5, String value6, String value7, String value8, Long value9, Date value10, Date value11) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        value11(value11);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached PhraseOptionRecord
     */
    public PhraseOptionRecord() {
        super(PhraseOption.PHRASE_OPTION);
    }

    /**
     * Create a detached, initialised PhraseOptionRecord
     */
    public PhraseOptionRecord(Long phraseOptionId, String name, String language, String header, String version, String basisVersion, String remark, String deleted, Long domainId, Date datenew, Date datelast) {
        super(PhraseOption.PHRASE_OPTION);

        set(0, phraseOptionId);
        set(1, name);
        set(2, language);
        set(3, header);
        set(4, version);
        set(5, basisVersion);
        set(6, remark);
        set(7, deleted);
        set(8, domainId);
        set(9, datenew);
        set(10, datelast);
    }
}
