/*
 * This file is generated by jOOQ.
*/
package it.clesius.albina.jooq.tables;


import it.clesius.albina.jooq.Keys;
import it.clesius.albina.jooq.Albina;
import it.clesius.albina.jooq.tables.records.PhraseRecord;
import org.jooq.*;
import org.jooq.impl.TableImpl;

import javax.annotation.Generated;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.2"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Phrase extends TableImpl<PhraseRecord> {

    private static final long serialVersionUID = 29344570;

    /**
     * The reference instance of <code>ALBINA.PHRASE</code>
     */
    public static final Phrase PHRASE = new Phrase();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<PhraseRecord> getRecordType() {
        return PhraseRecord.class;
    }

    /**
     * The column <code>ALBINA.PHRASE.PHRASE_ID</code>.
     */
    public final TableField<PhraseRecord, Long> PHRASE_ID = createField("phrase_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>ALBINA.PHRASE.VALUE</code>.
     */
    public final TableField<PhraseRecord, String> VALUE = createField("value", org.jooq.impl.SQLDataType.VARCHAR.length(500), this, "");

    /**
     * The column <code>ALBINA.PHRASE.SPACE_BEFORE</code>.
     */
    public final TableField<PhraseRecord, String> SPACE_BEFORE = createField("space_before", org.jooq.impl.SQLDataType.CHAR.length(1).nullable(false), this, "");

    /**
     * The column <code>ALBINA.PHRASE.SPACE_AFTER</code>.
     */
    public final TableField<PhraseRecord, String> SPACE_AFTER = createField("space_after", org.jooq.impl.SQLDataType.CHAR.length(1).nullable(false), this, "");

    /**
     * The column <code>ALBINA.PHRASE.REMOVE_PUNCTUATION_BEFORE</code>.
     */
    public final TableField<PhraseRecord, String> REMOVE_PUNCTUATION_BEFORE = createField("remove_punctuation_before", org.jooq.impl.SQLDataType.CHAR.length(1).nullable(false), this, "");

    /**
     * The column <code>ALBINA.PHRASE.ITEM_PART_NO</code>.
     */
    public final TableField<PhraseRecord, Long> ITEM_PART_NO = createField("item_part_no", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>ALBINA.PHRASE.INCORRECT</code>.
     */
    public final TableField<PhraseRecord, String> INCORRECT = createField("incorrect", org.jooq.impl.SQLDataType.CHAR.length(1).nullable(false), this, "");

    /**
     * The column <code>ALBINA.PHRASE.PHRASE_OPTION_ITEM_ID</code>.
     */
    public final TableField<PhraseRecord, Long> PHRASE_OPTION_ITEM_ID = createField("phrase_option_item_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>ALBINA.PHRASE.DATENEW</code>.
     */
    public final TableField<PhraseRecord, Date> DATENEW = createField("datenew", org.jooq.impl.SQLDataType.DATE.nullable(false).defaultValue(org.jooq.impl.DSL.field("sysdate ", org.jooq.impl.SQLDataType.DATE)), this, "");

    /**
     * The column <code>ALBINA.PHRASE.DATELAST</code>.
     */
    public final TableField<PhraseRecord, Date> DATELAST = createField("datelast", org.jooq.impl.SQLDataType.DATE.nullable(false).defaultValue(org.jooq.impl.DSL.field("sysdate ", org.jooq.impl.SQLDataType.DATE)), this, "");

    /**
     * Create a <code>ALBINA.PHRASE</code> table reference
     */
    public Phrase() {
        this("phrase", null);
    }

    /**
     * Create an aliased <code>ALBINA.PHRASE</code> table reference
     */
    public Phrase(String alias) {
        this(alias, PHRASE);
    }

    private Phrase(String alias, Table<PhraseRecord> aliased) {
        this(alias, aliased, null);
    }

    private Phrase(String alias, Table<PhraseRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Albina.ALBINA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<PhraseRecord> getPrimaryKey() {
        return Keys.PK_PHRASE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<PhraseRecord>> getKeys() {
        return Arrays.<UniqueKey<PhraseRecord>>asList(Keys.PK_PHRASE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<PhraseRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<PhraseRecord, ?>>asList(Keys.FK_SUBCOMPONENT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Phrase as(String alias) {
        return new Phrase(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Phrase rename(String name) {
        return new Phrase(name, null);
    }
}
