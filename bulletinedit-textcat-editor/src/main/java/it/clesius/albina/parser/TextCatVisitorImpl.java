package it.clesius.albina.parser;

import it.clesius.albina.model.*;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CodePointCharStream;
import org.antlr.v4.runtime.CommonTokenStream;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

//http://jakubdziworski.github.io/java/2016/04/01/antlr_visitor_vs_listener.html
public class TextCatVisitorImpl{

    /**
     * Parse given input and return Object result of visit
     * @param inputText
     * @return
     */
    public List<Sentence> parse(String inputText) {
        CodePointCharStream charStream = CharStreams.fromString(inputText);
        TextCatLexer lexer = new TextCatLexer(charStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        TextCatParser parser = new TextCatParser(tokens);
        MultiSentenceVisitor visitor = new MultiSentenceVisitor();
        return visitor.visitMultisentence(parser.multisentence());
    }

    private static class MultiSentenceVisitor extends TextCatBaseVisitor<List<Sentence>> {
        @Override
        public List<Sentence> visitMultisentence(TextCatParser.MultisentenceContext ctx) {
            return ctx.sentence()
                    .stream()
                    .map(sentenceCtx->{
                        SentenceVisitor sentence1Visitor=new SentenceVisitor();
                        Sentence sentence=sentence1Visitor.visitSentence(sentenceCtx);
                        return sentence;
                    })
                    .collect(toList());
        }
    }

    private static class SentenceVisitor extends TextCatBaseVisitor<Sentence> {
        @Override
        public Sentence visitSentence(TextCatParser.SentenceContext ctx) {
            SentenceIdVisitor sentenceIdVisitor=new SentenceIdVisitor();
            Long sentenceId=sentenceIdVisitor.visitSentenceId(ctx.sentenceId());
            SentenceContentVisitor sentenceContentVisitor=new SentenceContentVisitor();
            List<SentenceContent> content=sentenceContentVisitor.visitSentenceContent(ctx.sentenceContent());
            return new Sentence()
                    .id(sentenceId)
                    .sentenceContent(content);
        }
    }

    private static class SentenceIdVisitor extends TextCatBaseVisitor<Long> {
        public Long visitSentenceId(TextCatParser.SentenceIdContext ctx) {
            return Long.parseLong(ctx.getText());
        }
    }

    private static class SentenceContentVisitor extends TextCatBaseVisitor<List<SentenceContent>> {
        @Override
        public List<SentenceContent> visitSentenceContent(TextCatParser.SentenceContentContext ctx) {
            return ctx.children
                    .stream()
                    .map(child -> {
                        if (child instanceof TextCatParser.SentencePhraseContext) {
                            TextCatParser.SentencePhraseContext childCtx = (TextCatParser.SentencePhraseContext) child;
                            SentencePhraseVisitor phraseVisitor = new SentencePhraseVisitor();
                            SentencePhrase phrase = phraseVisitor.visitSentencePhrase(childCtx);
                            return new SentenceContent()
                                    .phrase(phrase)
                                    ;
                        }
                        //Just Ignore other tokens
                        return null;
                    })
                    .filter(Objects::nonNull).collect(java.util.stream.Collectors.toList());
        }
    }

    private static class SentencePhraseVisitor extends TextCatBaseVisitor<SentencePhrase> {
        @Override
        public SentencePhrase visitSentencePhrase(TextCatParser.SentencePhraseContext ctx) {
            Long phraseId=Long.parseLong(ctx.INDEX_NUMBER().getText());
            OptionVisitor optionVisitor=new OptionVisitor();
            Option option=optionVisitor.visitOption(ctx.option());
            return new SentencePhrase()
                    .id(phraseId)
                    .option(option);
        }
    }

    private static class OptionVisitor extends TextCatBaseVisitor<Option> {
        @Override
        public Option visitOption(TextCatParser.OptionContext ctx) {
            if (ctx==null)
                return null;
            OptionPhraseVisitor optionPhraseVisitor = new OptionPhraseVisitor();
            List<OptionPhrase> optionPhraseList=ctx.optionPhrase()
                    .stream()
                    .map(optionPhraseVisitor::visitOptionPhrase)
                    .collect(toList());

            return new Option().optionPhrase(optionPhraseList);
        }
    }

    private static class OptionPhraseVisitor extends TextCatBaseVisitor<OptionPhrase> {

        @Override
        public OptionPhrase visitOptionPhrase(TextCatParser.OptionPhraseContext ctx) {
            if (ctx==null)
                return null;
            Long phraseId=Long.parseLong(ctx.INDEX_NUMBER().getText());
            OptionVisitor optionVisitor=new OptionVisitor();
            Option option=optionVisitor.visitOption(ctx.option());
            return new OptionPhrase()
                    .id(phraseId)
                    .option(option);
        }
    }

}
