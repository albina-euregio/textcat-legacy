package eu.albina.textcat;

import ch.slf.lwp.bulletinedit.textcat.*;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;
import ch.slf.lwp.bulletinedit.textcat.dbloader.ttn.TTNImport;
import ch.slf.lwp.bulletinedit.textcat.json.Json;
import ch.slf.lwp.bulletinedit.textcat.json.JsonTextCat;
import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.JokerSentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;
import it.clesius.albina.service.ItDeTranslatorService;
import it.clesius.albina.service.DeItTranslatorService;
import it.clesius.albina.service.SentenceSearchService;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.*;

@EnableWebMvc
@RequestMapping(value = {"/api-old", "/v2"})
@CrossOrigin
public class LegacyAppController {

    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private ItDeTranslatorService itDeTranslatorService;
    @Autowired
    private DeItTranslatorService deItTranslatorService;
    @Autowired
    private SentenceSearchService sentenceSearchService;

    @Autowired
    private BoText boText;

    /**
     * the primary parameter, to decide which method is requested.
     * <p>
     * for possible values.
     */
    private static final String PARAM_TYPE = "type";

    /**
     * the requested text language. Only used if the type value is
     * {@value }
     */
    private static final String PARAM_LANGUAGE = "l";

    /**
     * the text definition. Only used if the type value is {@value }
     */
    private static final String PARAM_TEXT_DEFINITION = "def";


    /**
     * the joker sentence language parameter prefix.
     * The suffix is the iso2 language code.
     */
    private static final String PARAM_PREFIX_JOKER_SENTENCE = "joker";

    /**
     * none official parameter to change the used domain.
     */
    private static final String PARAM_DOMAIN_NAME = "dn";

    private final Logger log = LogManager.getLogger(this.getClass());

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<?> getRoot() throws TextCatException {
        // Just a simple status object, no detail test
        final JsonTextCat jsonTextCatalog = boText.getJsonTextCatalog(Language.de, DomainName.PRODUCTION);
        return ResponseEntity.ok(new Object() {
            @SuppressWarnings("unused")
            public final String servlet = "available";
            @SuppressWarnings("unused")
            public final String bo = boText != null ? "available" : "null";
            @SuppressWarnings("unused")
            public final String catalog = jsonTextCatalog != null ? "available" : "null";
            @SuppressWarnings("unused")
            public final String base_version = jsonTextCatalog != null ? jsonTextCatalog.getBasis_version().toString() : "null";
            @SuppressWarnings("unused")
            public final String initMsg = jsonTextCatalog != null ? jsonTextCatalog.getInitMsg() : "null";
        });
    }

    // importzip for copedit
    @RequestMapping(value = "/importzip", method = RequestMethod.GET)
    public ResponseEntity<String> getImportZip(@RequestParam(value = "lang", required = false) List<Language> languages,
                                               @RequestParam(value = "domain", required = false) String domain) {
        if (languages == null || languages.isEmpty()) {
            languages = Arrays.asList(Language.de, Language.it, Language.en, Language.fr);
        }
        StringBuilder p = new StringBuilder("-bv 1.0 ");
        for (Language language : languages) {
            String file = Paths.get("/mnt/importextcat/").resolve(language + ".zip").toString();
            p.append("-f ").append(file).append(",").append(language).append(",1.0 ");
        }
        p.append("-prebv 1.0 -r");
        if (domain != null && !domain.equals("")) {
            p.append(" -d ").append(domain);
        }
        log.info("Running /importzip on [{}]", p.toString());
        String[] arg = p.toString().split(" ");
        String result = TTNImport.go(arg);
        return ResponseEntity.ok(result);
    }

    // recodedomain for copedit
    @RequestMapping(value = "/recodedomain", method = RequestMethod.GET)
    public ResponseEntity<String> recodeDomain(@RequestParam(value = "domainFrom", required = true) Integer domainFrom,
                                               @RequestParam(value = "domainTo", required = true) Integer domainTo) throws TextCatException {

        BoAdministrationImpl boAdministration = applicationContext.getBean(BoAdministrationImpl.class);
        DomainName domainName;
        Version basisVersion = Version.create("1.0");
        switch (domainTo) {
            case 1:
                domainName = DomainName.PRODUCTION;
                break;
            default:
                domainName = DomainName.STAGING;
                break;
        }
        //boAdministration.deleteDomain(basisVersion, domainName);
        String result = boAdministration.recodeDomain(domainFrom, domainTo);
        return ResponseEntity.ok(result);
    }


    // reloadtextcat for copedit
    @RequestMapping(value = "/reloadtextcat", method = RequestMethod.GET)
    public ResponseEntity<String> reloadtextcat(@RequestParam(value = "lang", required = true) String lang,
                                                @RequestParam(value = "domain", required = true) String domain) {
        return ResponseEntity.ok("OK");
    }

    @RequestMapping(value = "/catalog/sentences", method = RequestMethod.GET)
    public ResponseEntity<Json> sentences(
            @RequestParam(value = PARAM_DOMAIN_NAME, defaultValue = "PRODUCTION") DomainName domainName,
            @RequestParam(PARAM_LANGUAGE) Language langValue) throws TextCatException {
        final Json json = boText.getJsonTextCatalog(langValue, domainName).getJsonSentences();
        return ResponseEntity.ok(json);
    }

    @RequestMapping(value = "/catalog/options", method = RequestMethod.GET)
    public ResponseEntity<Json> options(
            @RequestParam(value = PARAM_DOMAIN_NAME, defaultValue = "PRODUCTION") DomainName domainName,
            @RequestParam(PARAM_LANGUAGE) Language langValue) throws TextCatException {
        final Json json = boText.getJsonTextCatalog(langValue, domainName).getJsonOptions();
        return ResponseEntity.ok(json);
    }

    @RequestMapping(value = "/catalog/phrases", method = RequestMethod.GET)
    public ResponseEntity<Json> phrases(
            @RequestParam(value = PARAM_DOMAIN_NAME, defaultValue = "PRODUCTION") DomainName domainName,
            @RequestParam(PARAM_LANGUAGE) Language langValue) throws TextCatException {
        final Json json = boText.getJsonTextCatalog(langValue, domainName).getJsonPhrases();
        return ResponseEntity.ok(json);
    }

    @RequestMapping(value = "/catalog/index", method = RequestMethod.GET)
    public ResponseEntity<Json> index(
            @RequestParam(value = PARAM_DOMAIN_NAME, defaultValue = "PRODUCTION") DomainName domainName,
            @RequestParam(PARAM_LANGUAGE) Language langValue) throws TextCatException {
        final Json json = boText.getJsonTextCatalog(langValue, domainName).getJsonWord2SentenceIndx();
        return ResponseEntity.ok(json);
    }

    @RequestMapping(value = "/catalog/indexfull", method = RequestMethod.GET)
    public ResponseEntity<Json> indexfull(
            @RequestParam(value = PARAM_DOMAIN_NAME, defaultValue = "PRODUCTION") DomainName domainName,
            @RequestParam(PARAM_LANGUAGE) Language langValue) throws TextCatException {
        final Json json = boText.getJsonTextCatalog(langValue, domainName).getFullJsonWord2SentenceIndx();
        return ResponseEntity.ok(json);
    }

    @RequestMapping(value = "/joker", method = RequestMethod.POST)
    protected ResponseEntity<Integer> addJoker(
            @RequestParam(value = PARAM_DOMAIN_NAME, defaultValue = "PRODUCTION") DomainName domainName,
            @RequestParam(value = PARAM_LANGUAGE) Language srcLang,
            HttpServletRequest req) throws TextCatException {
        JokerSentence js = new JokerSentence();
        js.setDomainName(domainName);
        for (Language language : Language.values()) {
            js.setValue(req.getParameter(PARAM_PREFIX_JOKER_SENTENCE + language.toString()), language);
        }
        Map<Language, Sentence> sentences = boText.addJokerSentence(js, domainName);
        int id = sentences.get(srcLang).getId();
        return ResponseEntity.ok(id);
    }

    @RequestMapping(value = "/translate", method = RequestMethod.GET)
    public ResponseEntity<Translation> translate(
            @RequestParam(value = PARAM_DOMAIN_NAME, defaultValue = "PRODUCTION") DomainName domainName,
            @RequestParam(value = PARAM_TEXT_DEFINITION) String textDefinition,
            @RequestParam(value = PARAM_LANGUAGE) List<Language> language,
            @RequestParam(value = "input-lang", required = false) Language inputLang) throws TextCatException {
        try {
            if (Language.it.equals(inputLang)) {
                textDefinition = itDeTranslatorService.getGermanSentence(textDefinition);
            }
            if (language.size() != 1) {
                EnumMap<Language, String> texts = new EnumMap<>(Language.class);
                for (Language l : language) {
                    texts.put(l, boText.buildText(textDefinition, l, domainName));
                }
                return ResponseEntity.ok(new Translation(null, texts, textDefinition));
            } else {
                final String text = boText.buildText(textDefinition, language.get(0), domainName);
                return ResponseEntity.ok(new Translation(text, null, textDefinition));
            }
        } catch (TextCatException e) {
            log.error("Create text with language " + language + ",definition " + textDefinition + " and domain " + domainName + " failed.", e);
            throw e;
        }
    }

    static class Translation {
        public final String text;
        public final Map<Language, String> texts;
        public final String def;

        Translation(String text, Map<Language, String> texts, String def) {
            this.text = text;
            this.texts = texts;
            this.def = def;
        }
    }

    @RequestMapping(value = "/translatedef", method = RequestMethod.GET)
    protected ResponseEntity<String> translateDef(
            @RequestParam(value = PARAM_TEXT_DEFINITION) String sentence,
            @RequestParam(value = PARAM_DOMAIN_NAME, defaultValue = "PRODUCTION") String domain,
            @RequestParam(value = PARAM_LANGUAGE) String tolang
    ) throws SQLException {
        final String srclang = itDeTranslatorService.guessLanguage(sentence).name();
        log.info("Converting sentence from {} to {}", srclang, tolang);
        final String result;
        if (Objects.equals(srclang, tolang)) {
            result = sentence;
        } else if ("it".equals(srclang) & "de".equals(tolang)) {
            result = itDeTranslatorService.getGermanSentence(sentence);
        } else {
            result = deItTranslatorService.getItalianSentence(sentence);
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<Object> search(
            @RequestParam("query") String sentence,
            @RequestParam(value = PARAM_LANGUAGE) String lang,
            @RequestParam(value = PARAM_DOMAIN_NAME, defaultValue = "PRODUCTION") String domain,
            @RequestParam("se") String se
    ) throws TextCatException, SQLException {
        try {
            final TreeMap<String, Object> body = sentenceSearchService.getSentences(sentence, lang, se);
            return ResponseEntity.ok(body);
        } catch (Exception e) {
            log.error("Create Json failed", e);
//            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return ResponseEntity.badRequest().contentType(MediaType.TEXT_PLAIN).body("Create Json failed -- " + e.getMessage());
        }
    }

}
