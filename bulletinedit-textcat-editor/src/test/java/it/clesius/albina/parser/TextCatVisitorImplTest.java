package it.clesius.albina.parser;

import it.clesius.albina.model.Sentence;
import junit.framework.TestCase;

import java.util.List;

public class TextCatVisitorImplTest extends TestCase {

    public void testParser() {
        String input = "11[5452,2079,2656[7314[5754],6771[5753],4059[2275,4881]],4769]";
        List<Sentence> sentences = new TextCatVisitorImpl().parse(input);
        assertEquals("[11[5452,2079,2656[[7314[5754],6771[5753],4059[2275,4881]]],4769]]", sentences.toString());
    }
}
