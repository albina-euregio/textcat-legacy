package ch.slf.lwp.bulletinedit.textcat.text;

import org.jmock.Expectations;
import org.jmock.integration.junit3.MockObjectTestCase;

import ch.slf.lwp.bulletinedit.textcat.text.TextFragment;
import ch.slf.lwp.bulletinedit.textcat.text.TextStringBuilder;

public class TextStringBuilderTest extends MockObjectTestCase {
	
	private TextStringBuilder tb;

	protected void setUp() throws Exception {
		super.setUp();
		tb = new TextStringBuilder();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		tb = null;
	}

	/**
	 * Add to simple sentences
	 */
	public void testAppendSentenceSimple() {
		
		final TextFragment e1 = mock(TextFragment.class, "e1");		
		final TextFragment e2 = mock(TextFragment.class, "e2");
		TextFragment[] sentence1 = {e1,e2};
		
		// init mocks
		checking(new Expectations(){{
			atLeast(1).of(e1).getText(); will(returnValue("Im Gotthardgebiet sowie allgemein in Kamm- und Passlagen sind die Gefahrenstellen häufiger."));
			allowing(e1).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e1).isSpaceAfter(); will(returnValue(true));
			allowing(e1).isSpaceBefore(); will(returnValue(true));
			
			atLeast(1).of(e2).getText(); will(returnValue("Eine vorsichtige Routenwahl ist wichtig."));
			allowing(e2).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e2).isSpaceAfter(); will(returnValue(true));
			allowing(e2).isSpaceBefore(); will(returnValue(true));
				
		}});
		
		tb.appendSentence(sentence1);
		
		String expected = "Im Gotthardgebiet sowie allgemein in Kamm- und Passlagen sind die Gefahrenstellen häufiger. Eine vorsichtige Routenwahl ist wichtig.";
		String actual = tb.toString();
		
		assertEquals(expected, actual);
	}
	
	public void testAppendSentenceToUpperCaseOnStart() {
		
		final TextFragment e1 = mock(TextFragment.class, "e1");		
		final TextFragment e2 = mock(TextFragment.class, "e2");
		TextFragment[] sentence1 = {e1,e2};
		
		// init mocks
		checking(new Expectations(){{
			atLeast(1).of(e1).getText(); will(returnValue(""));
			allowing(e1).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e1).isSpaceAfter(); will(returnValue(true));
			allowing(e1).isSpaceBefore(); will(returnValue(true));
			
			atLeast(1).of(e2).getText(); will(returnValue("eine vorsichtige Routenwahl ist wichtig."));
			allowing(e2).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e2).isSpaceAfter(); will(returnValue(true));
			allowing(e2).isSpaceBefore(); will(returnValue(true));
				
		}});
		
		tb.appendSentence(sentence1);
		
		String expected = "Eine vorsichtige Routenwahl ist wichtig.";
		String actual = tb.toString();
		
		assertEquals(expected, actual);
	}
	
	public void testAppendSentenceToUpperCaseInsideText() {

		final TextFragment e1 = mock(TextFragment.class, "e1");		
		TextFragment[] sentence1 = {e1};

		final TextFragment e3 = mock(TextFragment.class, "e3");		
		final TextFragment e4 = mock(TextFragment.class, "e4");
		TextFragment[] sentence2 = {e3,e4};
		
		// init mocks
		checking(new Expectations(){{
			atLeast(1).of(e1).getText(); will(returnValue("Im Gotthardgebiet sowie allgemein in Kamm- und Passlagen sind die Gefahrenstellen häufiger."));
			allowing(e1).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e1).isSpaceAfter(); will(returnValue(true));
			allowing(e1).isSpaceBefore(); will(returnValue(true));

			atLeast(1).of(e3).getText(); will(returnValue(""));
			allowing(e3).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e3).isSpaceAfter(); will(returnValue(true));
			allowing(e3).isSpaceBefore(); will(returnValue(true));
			
			atLeast(1).of(e4).getText(); will(returnValue("eine vorsichtige Routenwahl ist wichtig."));
			allowing(e4).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e4).isSpaceAfter(); will(returnValue(true));
			allowing(e4).isSpaceBefore(); will(returnValue(true));
				
		}});
		
		tb.appendSentence(sentence1);
		tb.appendSentence(sentence2);
		
		String expected = "Im Gotthardgebiet sowie allgemein in Kamm- und Passlagen sind die Gefahrenstellen häufiger. Eine vorsichtige Routenwahl ist wichtig.";
		String actual = tb.toString();
		
		assertEquals(expected, actual);
	}


	public void testAppendSentenceRemovePunctuationMark() {
		
		final TextFragment e1 = mock(TextFragment.class, "e1");		
		final TextFragment e2 = mock(TextFragment.class, "e2");
		TextFragment[] sentence1 = {e1,e2};
		
		// init mocks
		checking(new Expectations(){{
			atLeast(1).of(e1).getText(); will(returnValue("Im Gotthardgebiet sowie allgemein in Kamm- und Passlagen sind die Gefahrenstellen häufiger."));
			allowing(e1).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e1).isSpaceAfter(); will(returnValue(true));
			allowing(e1).isSpaceBefore(); will(returnValue(true));
			
			atLeast(1).of(e2).getText(); will(returnValue(", eine vorsichtige Routenwahl ist wichtig."));
			allowing(e2).isRemovePunctuationBefore(); will(returnValue(true));
			allowing(e2).isSpaceAfter(); will(returnValue(true));
			allowing(e2).isSpaceBefore(); will(returnValue(false));
				
		}});
		
		tb.appendSentence(sentence1);
		
		String expected = "Im Gotthardgebiet sowie allgemein in Kamm- und Passlagen sind die Gefahrenstellen häufiger, eine vorsichtige Routenwahl ist wichtig.";
		String actual = tb.toString();
		
		assertEquals(expected, actual);
	}

	public void testAppendSentenceNoneWhiteSpace() {
		final TextFragment e1 = mock(TextFragment.class, "e1");		
		final TextFragment e2 = mock(TextFragment.class, "e2");
		final TextFragment e3 = mock(TextFragment.class, "e3");		
		final TextFragment e4 = mock(TextFragment.class, "e4");

		TextFragment[] sentence1 = {e1,e2,e3,e4};
		
		// init mocks
		checking(new Expectations(){{
			atLeast(1).of(e1).getText(); will(returnValue("Le danger"));
			allowing(e1).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e1).isSpaceAfter(); will(returnValue(true));
			allowing(e1).isSpaceBefore(); will(returnValue(true));
			
			atLeast(1).of(e2).getText(); will(returnValue("n'"));
			allowing(e2).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e2).isSpaceAfter(); will(returnValue(false));
			allowing(e2).isSpaceBefore(); will(returnValue(true));

			atLeast(1).of(e3).getText(); will(returnValue("augmentera"));
			allowing(e3).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e3).isSpaceAfter(); will(returnValue(true));
			allowing(e3).isSpaceBefore(); will(returnValue(true));

			atLeast(1).of(e4).getText(); will(returnValue("pas."));
			allowing(e4).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e4).isSpaceAfter(); will(returnValue(true));
			allowing(e4).isSpaceBefore(); will(returnValue(true));

		}});
		
		tb.appendSentence(sentence1);

		String expected = "Le danger n'augmentera pas.";
		String actual = tb.toString();
		
		assertEquals(expected, actual);
	}

	public void testAppendSentenceEmptyElementInside() {
		final TextFragment e1 = mock(TextFragment.class, "e1");		
		final TextFragment e2 = mock(TextFragment.class, "e2");
		final TextFragment e3 = mock(TextFragment.class, "e3");		
		final TextFragment e4 = mock(TextFragment.class, "e4");

		TextFragment[] sentence1 = {e1,e2,e3,e4};
		
		// init mocks
		checking(new Expectations(){{
			atLeast(1).of(e1).getText(); will(returnValue("Le danger"));
			allowing(e1).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e1).isSpaceAfter(); will(returnValue(true));
			allowing(e1).isSpaceBefore(); will(returnValue(true));
			
			atLeast(1).of(e2).getText(); will(returnValue(""));
			allowing(e2).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e2).isSpaceAfter(); will(returnValue(true));
			allowing(e2).isSpaceBefore(); will(returnValue(true));

			atLeast(1).of(e3).getText(); will(returnValue("augmentera"));
			allowing(e3).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e3).isSpaceAfter(); will(returnValue(true));
			allowing(e3).isSpaceBefore(); will(returnValue(true));

			atLeast(1).of(e4).getText(); will(returnValue("pas."));
			allowing(e4).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e4).isSpaceAfter(); will(returnValue(true));
			allowing(e4).isSpaceBefore(); will(returnValue(true));

		}});
		
		tb.appendSentence(sentence1);

		String expected = "Le danger augmentera pas.";
		String actual = tb.toString();
		
		assertEquals(expected, actual);
	}
	
	public void testAppendSentenceToUpperCaseOnStartFirstPhrase() {
		
		final TextFragment e1 = mock(TextFragment.class, "e1");
		TextFragment[] sentence1 = {e1};
		
		// init mocks
		checking(new Expectations(){{
			
			atLeast(1).of(e1).getText(); will(returnValue("eine vorsichtige Routenwahl ist wichtig."));
			allowing(e1).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e1).isSpaceAfter(); will(returnValue(true));
			allowing(e1).isSpaceBefore(); will(returnValue(true));
				
		}});
		
		tb.appendSentence(sentence1);
		
		String expected = "Eine vorsichtige Routenwahl ist wichtig.";
		String actual = tb.toString();
		
		assertEquals(expected, actual);
	}
	
	public void testAppendSentenceToUpperCaseInsideTextFirstPhrase() {

		final TextFragment e1 = mock(TextFragment.class, "e1");		
		TextFragment[] sentence1 = {e1};

		final TextFragment e4 = mock(TextFragment.class, "e4");
		TextFragment[] sentence2 = {e4};
		
		// init mocks
		checking(new Expectations(){{
			atLeast(1).of(e1).getText(); will(returnValue("Im Gotthardgebiet sowie allgemein in Kamm- und Passlagen sind die Gefahrenstellen häufiger."));
			allowing(e1).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e1).isSpaceAfter(); will(returnValue(true));
			allowing(e1).isSpaceBefore(); will(returnValue(true));

			atLeast(1).of(e4).getText(); will(returnValue("eine vorsichtige Routenwahl ist wichtig."));
			allowing(e4).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e4).isSpaceAfter(); will(returnValue(true));
			allowing(e4).isSpaceBefore(); will(returnValue(true));
				
		}});
		
		tb.appendSentence(sentence1);
		tb.appendSentence(sentence2);
		
		String expected = "Im Gotthardgebiet sowie allgemein in Kamm- und Passlagen sind die Gefahrenstellen häufiger. Eine vorsichtige Routenwahl ist wichtig.";
		String actual = tb.toString();
		
		assertEquals(expected, actual);
	}

	public void testAppendSentenceEmptyElementInsideNoSpaceAfter() {
		final TextFragment e1 = mock(TextFragment.class, "e1");		
		final TextFragment e2 = mock(TextFragment.class, "e2");
		final TextFragment e3 = mock(TextFragment.class, "e3");		
		final TextFragment e4 = mock(TextFragment.class, "e4");

		TextFragment[] sentence1 = {e1,e2,e3,e4};
		
		// init mocks
		checking(new Expectations(){{
			atLeast(1).of(e1).getText(); will(returnValue("Le de"));
			allowing(e1).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e1).isSpaceAfter(); will(returnValue(false));
			allowing(e1).isSpaceBefore(); will(returnValue(true));
			
			atLeast(1).of(e2).getText(); will(returnValue(""));
			allowing(e2).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e2).isSpaceAfter(); will(returnValue(true));
			allowing(e2).isSpaceBefore(); will(returnValue(true));

			atLeast(1).of(e3).getText(); will(returnValue("i"));
			allowing(e3).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e3).isSpaceAfter(); will(returnValue(true));
			allowing(e3).isSpaceBefore(); will(returnValue(true));

			atLeast(1).of(e4).getText(); will(returnValue("pas."));
			allowing(e4).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e4).isSpaceAfter(); will(returnValue(true));
			allowing(e4).isSpaceBefore(); will(returnValue(true));

		}});
		
		tb.appendSentence(sentence1);

		String expected = "Le dei pas.";
		String actual = tb.toString();
		
		assertEquals(expected, actual);
	}

	public void testToUpperCaseAfterLineBreak() {
		final TextFragment e1 = mock(TextFragment.class, "e1");		
		final TextFragment e2 = mock(TextFragment.class, "e2");
		final TextFragment e3 = mock(TextFragment.class, "e3");		

		TextFragment[] sentence1 = {e1};
		TextFragment[] sentence2 = {e2};
		TextFragment[] sentence3 = {e3};
		
		// init mocks
		checking(new Expectations(){{
			atLeast(1).of(e1).getText(); will(returnValue("Teils störanfällige Triebschneeansammlungen."));
			allowing(e1).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e1).isSpaceAfter(); will(returnValue(true));
			allowing(e1).isSpaceBefore(); will(returnValue(true));
			
			atLeast(1).of(e2).getText(); will(returnValue("<br/>"));
			allowing(e2).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e2).isSpaceAfter(); will(returnValue(true));
			allowing(e2).isSpaceBefore(); will(returnValue(true));

			atLeast(1).of(e3).getText(); will(returnValue("die frischen Triebschneeansammlungen"));
			allowing(e3).isRemovePunctuationBefore(); will(returnValue(false));
			allowing(e3).isSpaceAfter(); will(returnValue(true));
			allowing(e3).isSpaceBefore(); will(returnValue(true));

		}});
		
		tb.appendSentence(sentence1);
		tb.appendSentence(sentence2);
		tb.appendSentence(sentence3);

		String expected = "Teils störanfällige Triebschneeansammlungen. <br/> Die frischen Triebschneeansammlungen";
		String actual = tb.toString();
		
		assertEquals(expected, actual);
	}

}
