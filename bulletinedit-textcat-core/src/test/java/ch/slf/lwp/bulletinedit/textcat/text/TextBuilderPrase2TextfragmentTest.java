package ch.slf.lwp.bulletinedit.textcat.text;

import java.util.HashMap;
import java.util.List;

import org.jmock.Expectations;
import org.jmock.integration.junit3.MockObjectTestCase;

import ch.slf.lwp.bulletinedit.textcat.Constants;
import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;
import ch.slf.lwp.bulletinedit.textcat.TextCatException;
import ch.slf.lwp.bulletinedit.textcat.dao.DaoPhraseOption;
import ch.slf.lwp.bulletinedit.textcat.dao.DaoSentence;
import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.util.SubPhraseOptionName;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;
import ch.slf.lwp.bulletinedit.textcat.text.SentenceConfiguration.ModuleConfiguration;
import ch.slf.lwp.bulletinedit.textcat.text.SentenceConfiguration.PhraseOptionConfiguration;
import ch.slf.lwp.bulletinedit.textcat.text.SentenceConfiguration.SubPhraseOptionConfiguration;

public class TextBuilderPrase2TextfragmentTest extends MockObjectTestCase {

	private TextBuilder textBuilder;
	private DaoSentence mockDaoSentence;
	private DaoPhraseOption mockDaoPhraseOption;

	protected void setUp() throws Exception {
		super.setUp();
		mockDaoSentence = mock(DaoSentence.class, "daoSentence");
		mockDaoPhraseOption = mock(DaoPhraseOption.class, "daoPhraseOption");
		textBuilder = new TextBuilder(mockDaoSentence, mockDaoPhraseOption);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		mockDaoSentence = null;
		mockDaoPhraseOption = null;
		textBuilder = null;
	}

	public void testPhraseWithoutSubPhrase() {
		final Phrase phrase = mock(Phrase.class, "phrase");

		checking(new Expectations() {
			{
				oneOf(phrase).getValue();
				will(returnValue("schnee"));
				oneOf(phrase).isSpaceAfter();
				will(returnValue(false));
				oneOf(phrase).isSpaceBefore();
				will(returnValue(false));
				oneOf(phrase).isRemovePunctuationBefore();
				will(returnValue(true));
				allowing(phrase).getId();
				will(returnValue(1));
			}
		});

		SentenceConfiguration sc = new SentenceConfiguration("test", new Version(1, 0));
		sc.addModuleConfiguration(1, 1);
		PhraseOptionConfiguration phraseOptionConfiguration = sc.getModuleConfiguration(1);

		try {
			List<? extends TextFragment> fragments = textBuilder.phrase2Textfragments(phrase, phraseOptionConfiguration, Language.de, DomainName.PRODUCTION, new HashMap<String, PhraseOption>());
			assertEquals("Textfragment count: ", 1, fragments.size());
			assertTextFragment("f1", fragments.get(0), "schnee", true, false, false);
		} catch (TextCatException e) {
			fail(e.getMessage());
		}

	}

	public void testPhraseWithOneSubPhrase() {
		final Phrase phrase = mock(Phrase.class, "phrase");
		final PhraseOption phraseOption = mock(PhraseOption.class, "phraseOption");
		final Phrase subPhrase = mock(Phrase.class, "subPhrase");

		checking(new Expectations() {
			{
				// first phrase
				oneOf(phrase).getValue();
				will(returnValue("schnee{art_und_weise}"));
				oneOf(phrase).isSpaceAfter();
				will(returnValue(false));
				oneOf(phrase).isSpaceBefore();
				will(returnValue(false));
				oneOf(phrase).isRemovePunctuationBefore();
				will(returnValue(true));
				allowing(phrase).getId();
				will(returnValue(1));
				// daoPhraseOption
				oneOf(mockDaoPhraseOption).getPhraseOption("art_und_weise", Language.de, new Version(1, 0), DomainName.PRODUCTION);
				will(returnValue(phraseOption));
				// phrase option
				oneOf(phraseOption).getPhrase(Constants.FIRST_ITEM_NO, Constants.FIRST_ITEM_PART_NO);
				will(returnValue(subPhrase));
				// sub phrase
				oneOf(subPhrase).getValue();
				will(returnValue("fall"));
				oneOf(subPhrase).isSpaceAfter();
				will(returnValue(false));
				oneOf(subPhrase).isSpaceBefore();
				will(returnValue(false));
				oneOf(subPhrase).isRemovePunctuationBefore();
				will(returnValue(true));
				allowing(subPhrase).getId();
				will(returnValue(2));
			}
		});

		SentenceConfiguration sc = new SentenceConfiguration("test", new Version(1, 0));
		ModuleConfiguration mc = sc.addModuleConfiguration(1, Constants.FIRST_ITEM_NO);
		mc.addSubPhraseOptionConfiguration(new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("art_und_weise"), Constants.FIRST_ITEM_NO);
		PhraseOptionConfiguration phraseOptionConfiguration = sc.getModuleConfiguration(1);

		try {
			List<? extends TextFragment> fragments = textBuilder.phrase2Textfragments(phrase, phraseOptionConfiguration, Language.de, DomainName.PRODUCTION, new HashMap<String, PhraseOption>());
			assertEquals("Textfragment count: ", 2, fragments.size());
			assertTextFragment("f1", fragments.get(0), "schnee", true, false, true);
			assertTextFragment("f2", fragments.get(1), "fall", true, false, false);
		} catch (TextCatException e) {
			fail(e.getMessage());
		}

	}

	public void testPhraseWithTwoSubPhrase() {
		final Phrase phrase = mock(Phrase.class, "phrase");
		final PhraseOption phraseOption = mock(PhraseOption.class, "phraseOption");
		final Phrase subPhrase1 = mock(Phrase.class, "subPhrase1");
		final Phrase subPhrase2 = mock(Phrase.class, "subPhrase2");

		checking(new Expectations() {
			{
				// first phrase
				oneOf(phrase).getValue();
				will(returnValue("schnee (-) {art_und_weise#1} {art_und_weise#2}morgen"));
				oneOf(phrase).isSpaceAfter();
				will(returnValue(false));
				oneOf(phrase).isSpaceBefore();
				will(returnValue(false));
				oneOf(phrase).isRemovePunctuationBefore();
				will(returnValue(true));
				allowing(phrase).getId();
				will(returnValue(1));
				// daoPhraseOption
				oneOf(mockDaoPhraseOption).getPhraseOption("art_und_weise", Language.de, new Version(1, 0), DomainName.PRODUCTION);
				will(returnValue(phraseOption));
				// phrase option
				oneOf(phraseOption).getPhrase(Constants.FIRST_ITEM_NO, Constants.FIRST_ITEM_PART_NO);
				will(returnValue(subPhrase1));
				oneOf(phraseOption).getPhrase(Constants.FIRST_ITEM_NO + 1, Constants.FIRST_ITEM_PART_NO);
				will(returnValue(subPhrase2));
				// sub phrase 1
				oneOf(subPhrase1).getValue();
				will(returnValue("fall"));
				oneOf(subPhrase1).isSpaceAfter();
				will(returnValue(true));
				oneOf(subPhrase1).isSpaceBefore();
				will(returnValue(true));
				oneOf(subPhrase1).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(subPhrase1).getId();
				will(returnValue(2));
				// sub phrase 2
				oneOf(subPhrase2).getValue();
				will(returnValue("dauer"));
				oneOf(subPhrase2).isSpaceAfter();
				will(returnValue(true));
				oneOf(subPhrase2).isSpaceBefore();
				will(returnValue(true));
				oneOf(subPhrase2).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(subPhrase2).getId();
				will(returnValue(3));
			}
		});

		SentenceConfiguration sc = new SentenceConfiguration("test", new Version(1, 0));
		ModuleConfiguration mc = sc.addModuleConfiguration(1, Constants.FIRST_ITEM_NO);
		mc.addSubPhraseOptionConfiguration(new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("art_und_weise").setPosNo(1), Constants.FIRST_ITEM_NO);
		mc.addSubPhraseOptionConfiguration(new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("art_und_weise").setPosNo(2), Constants.FIRST_ITEM_NO + 1);
		PhraseOptionConfiguration phraseOptionConfiguration = sc.getModuleConfiguration(1);

		try {
			List<? extends TextFragment> fragments = textBuilder.phrase2Textfragments(phrase, phraseOptionConfiguration, Language.de, DomainName.PRODUCTION, new HashMap<String, PhraseOption>());
			assertEquals("Textfragment count: ", 4, fragments.size());
			assertTextFragment("f1", fragments.get(0), "schnee", true, false, false);
			assertTextFragment("f2", fragments.get(1), "fall", false, false, true);
			assertTextFragment("f3", fragments.get(2), "dauer", false, true, true);
			assertTextFragment("f4", fragments.get(3), "morgen", false, true, false);
		} catch (TextCatException e) {
			fail(e.getMessage());
		}

	}

	public void testPhraseWithSubSubPhrase() {
		final Phrase phrase = mock(Phrase.class, "phrase");
		final PhraseOption phraseOptionExpo = mock(PhraseOption.class, "phraseOptionExpo");
		final Phrase subPhrase = mock(Phrase.class, "subPhrase");

		final PhraseOption phraseOptionUndExpo = mock(PhraseOption.class, "phraseOptionUndExpo");
		final Phrase subsubPhrase = mock(Phrase.class, "subsubPhrase");

		checking(new Expectations() {
			{
				// first phrase
				oneOf(phrase).getValue();
				will(returnValue("an {Expo}(-)hängen"));
				oneOf(phrase).isSpaceAfter();
				will(returnValue(true));
				oneOf(phrase).isSpaceBefore();
				will(returnValue(true));
				oneOf(phrase).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(phrase).getId();
				will(returnValue(1));

				// daoPhraseOption
				oneOf(mockDaoPhraseOption).getPhraseOption("Expo", Language.de, new Version(1, 0), DomainName.PRODUCTION);
				will(returnValue(phraseOptionExpo));
				oneOf(mockDaoPhraseOption).getPhraseOption("und_Expo", Language.de, new Version(1, 0), DomainName.PRODUCTION);
				will(returnValue(phraseOptionUndExpo));

				// phrase option Expo
				oneOf(phraseOptionExpo).getPhrase(Constants.FIRST_ITEM_NO, Constants.FIRST_ITEM_PART_NO);
				will(returnValue(subPhrase));

				// phrase option und_Expo
				oneOf(phraseOptionUndExpo).getPhrase(Constants.FIRST_ITEM_NO, Constants.FIRST_ITEM_PART_NO);
				will(returnValue(subsubPhrase));

				// subPhrase
				oneOf(subPhrase).getValue();
				will(returnValue("Nord{und_Expo}"));
				oneOf(subPhrase).isSpaceAfter();
				will(returnValue(true));
				oneOf(subPhrase).isSpaceBefore();
				will(returnValue(true));
				oneOf(subPhrase).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(subPhrase).getId();
				will(returnValue(2));

				// subsubPhrase
				oneOf(subsubPhrase).getValue();
				will(returnValue("- und Süd"));
				oneOf(subsubPhrase).isSpaceAfter();
				will(returnValue(true));
				oneOf(subsubPhrase).isSpaceBefore();
				will(returnValue(false));
				oneOf(subsubPhrase).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(subsubPhrase).getId();
				will(returnValue(2));
			}
		});

		SentenceConfiguration sc = new SentenceConfiguration("test", new Version(1, 0));
		ModuleConfiguration mc = sc.addModuleConfiguration(1, Constants.FIRST_ITEM_NO);
		SubPhraseOptionConfiguration spoc = mc.addSubPhraseOptionConfiguration(new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("Expo").setPosNo(1), Constants.FIRST_ITEM_NO);
		spoc.addSubPhraseOptionConfiguration(new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("und_Expo").setPosNo(1), Constants.FIRST_ITEM_NO);

		PhraseOptionConfiguration phraseOptionConfiguration = sc.getModuleConfiguration(1);

		try {
			List<? extends TextFragment> fragments = textBuilder.phrase2Textfragments(phrase, phraseOptionConfiguration, Language.de, DomainName.PRODUCTION, new HashMap<String, PhraseOption>());
			assertEquals("Textfragment count: ", 4, fragments.size());
			assertTextFragment("f1", fragments.get(0), "an", false, true, true);
			assertTextFragment("f2", fragments.get(1), "Nord", false, true, true);
			assertTextFragment("f3", fragments.get(2), "- und Süd", false, false, false);
			assertTextFragment("f4", fragments.get(3), "hängen", false, false, true);
		} catch (TextCatException e) {
			fail(e.getMessage());
		}

	}

	public void testSplitSubPhraseOption() {
		final Phrase phrase = mock(Phrase.class, "phrase");
		final PhraseOption phraseOption = mock(PhraseOption.class, "phraseOption");
		final Phrase subPhrase1 = mock(Phrase.class, "subPhrase1");
		final Phrase subPhrase2 = mock(Phrase.class, "subPhrase2");

		checking(new Expectations() {
			{
				// first phrase
				oneOf(phrase).getValue();
				will(returnValue("{viele_NO#1} banane {viele#1} apfel"));
				oneOf(phrase).isSpaceAfter();
				will(returnValue(false));
				oneOf(phrase).isSpaceBefore();
				will(returnValue(false));
				oneOf(phrase).isRemovePunctuationBefore();
				will(returnValue(true));
				allowing(phrase).getId();
				will(returnValue(1));
				// daoPhraseOption
				oneOf(mockDaoPhraseOption).getPhraseOption("viele", Language.fr, new Version(1, 0), DomainName.PRODUCTION);
				will(returnValue(phraseOption));
				// phrase option
				oneOf(phraseOption).getPhrase(Constants.FIRST_ITEM_NO, Constants.FIRST_ITEM_PART_NO);
				will(returnValue(subPhrase1));
				oneOf(phraseOption).getPhrase(Constants.FIRST_ITEM_NO, Constants.FIRST_ITEM_PART_NO + 1);
				will(returnValue(subPhrase2));
				// sub phrase, first item part
				oneOf(subPhrase1).getValue();
				will(returnValue("sehr viele"));
				oneOf(subPhrase1).isSpaceAfter();
				will(returnValue(true));
				oneOf(subPhrase1).isSpaceBefore();
				will(returnValue(true));
				oneOf(subPhrase1).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(subPhrase1).getId();
				will(returnValue(2));
				// sub phrase, second item part
				oneOf(subPhrase2).getValue();
				will(returnValue("noch mehr"));
				oneOf(subPhrase2).isSpaceAfter();
				will(returnValue(true));
				oneOf(subPhrase2).isSpaceBefore();
				will(returnValue(true));
				oneOf(subPhrase2).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(subPhrase2).getId();
				will(returnValue(3));
			}
		});

		SentenceConfiguration sc = new SentenceConfiguration("test", new Version(1, 0));
		ModuleConfiguration mc = sc.addModuleConfiguration(1, Constants.FIRST_ITEM_NO);
		mc.addSubPhraseOptionConfiguration(new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("viele"), Constants.FIRST_ITEM_NO);
		PhraseOptionConfiguration phraseOptionConfiguration = sc.getModuleConfiguration(1);

		try {
			List<? extends TextFragment> fragments = textBuilder.phrase2Textfragments(phrase, phraseOptionConfiguration, Language.fr, DomainName.PRODUCTION, new HashMap<String, PhraseOption>());
			assertEquals("Textfragment count: ", 4, fragments.size());
			assertTextFragment("f1", fragments.get(0), "noch mehr", true, false, true);
			assertTextFragment("f2", fragments.get(1), "banane", false, true, true);
			assertTextFragment("f3", fragments.get(2), "sehr viele", false, true, true);
			assertTextFragment("f4", fragments.get(3), "apfel", false, true, false);
		} catch (TextCatException e) {
			fail(e.getMessage());
		}

	}

	private void assertTextFragment(String name, TextFragment f, String eText, boolean eRemovePunctuationBefore, boolean eSpaceBefore, boolean eSpaceAfter) {
		assertEquals(name + " text", eText, f.getText());
		assertEquals(name + " remove punctuation before", eRemovePunctuationBefore, f.isRemovePunctuationBefore());
		assertEquals(name + " space before", eSpaceBefore, f.isSpaceBefore());
		assertEquals(name + " space after", eSpaceAfter, f.isSpaceAfter());
	}
	
	public void testPhraseWithControllSignBeforeText_A() {
		final Phrase phrase = mock(Phrase.class, "phrase");
		final PhraseOption phraseOption1 = mock(PhraseOption.class, "phraseOption1_an_steilen");
		final Phrase subPhrase1_1 = mock(Phrase.class, "subPhrase1_1");

		final PhraseOption phraseOption2 = mock(PhraseOption.class, "phraseOption2_Expo");
		final Phrase subPhrase2_1 = mock(Phrase.class, "subPhrase2_1");

		final PhraseOption phraseOption3 = mock(PhraseOption.class, "phraseOption3_und_Expo");
		final Phrase subPhrase3_1 = mock(Phrase.class, "subPhrase3_1");

		final PhraseOption phraseOption4 = mock(PhraseOption.class, "phraseOption4_oberhalb_von_Höhe_optional");
		final Phrase subPhrase4_1 = mock(Phrase.class, "subPhrase4_1");

		checking(new Expectations() {
			{
				// first phrase
				oneOf(phrase).getValue();
				will(returnValue("sowie {an_steilen} {Expo} {und_Expo} (-)hängen {oberhalb_von_Höhe_optional}"));
				oneOf(phrase).isSpaceAfter();
				will(returnValue(true));
				oneOf(phrase).isSpaceBefore();
				will(returnValue(true));
				oneOf(phrase).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(phrase).getId();
				will(returnValue(1));
				// daoPhraseOption
				oneOf(mockDaoPhraseOption).getPhraseOption("an_steilen", Language.de, new Version(1, 0), DomainName.PRODUCTION);
				will(returnValue(phraseOption1));

				oneOf(mockDaoPhraseOption).getPhraseOption("Expo", Language.de, new Version(1, 0), DomainName.PRODUCTION);
				will(returnValue(phraseOption2));

				oneOf(mockDaoPhraseOption).getPhraseOption("und_Expo", Language.de, new Version(1, 0), DomainName.PRODUCTION);
				will(returnValue(phraseOption3));

				oneOf(mockDaoPhraseOption).getPhraseOption("oberhalb_von_Höhe_optional", Language.de, new Version(1, 0), DomainName.PRODUCTION);
				will(returnValue(phraseOption4));

				// phrase option 1
				oneOf(phraseOption1).getPhrase(Constants.FIRST_ITEM_NO, Constants.FIRST_ITEM_PART_NO);
				will(returnValue(subPhrase1_1));
				// sub phrase 1
				oneOf(subPhrase1_1).getValue();
				will(returnValue("an"));
				oneOf(subPhrase1_1).isSpaceAfter();
				will(returnValue(true));
				oneOf(subPhrase1_1).isSpaceBefore();
				will(returnValue(true));
				oneOf(subPhrase1_1).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(subPhrase1_1).getId();
				will(returnValue(2));
				
				// phrase option 2
				oneOf(phraseOption2).getPhrase(Constants.FIRST_ITEM_NO, Constants.FIRST_ITEM_PART_NO);
				will(returnValue(subPhrase2_1));
				// sub phrase 1
				oneOf(subPhrase2_1).getValue();
				will(returnValue("Nord"));
				oneOf(subPhrase2_1).isSpaceAfter();
				will(returnValue(true));
				oneOf(subPhrase2_1).isSpaceBefore();
				will(returnValue(true));
				oneOf(subPhrase2_1).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(subPhrase2_1).getId();
				will(returnValue(3));

				// phrase option 3
				oneOf(phraseOption3).getPhrase(Constants.FIRST_ITEM_NO, Constants.FIRST_ITEM_PART_NO);
				will(returnValue(subPhrase3_1));
				// sub phrase 1
				oneOf(subPhrase3_1).getValue();
				will(returnValue("- und Süd"));
				oneOf(subPhrase3_1).isSpaceAfter();
				will(returnValue(true));
				oneOf(subPhrase3_1).isSpaceBefore();
				will(returnValue(false));
				oneOf(subPhrase3_1).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(subPhrase3_1).getId();
				will(returnValue(4));

				// phrase option 4
				oneOf(phraseOption4).getPhrase(Constants.FIRST_ITEM_NO, Constants.FIRST_ITEM_PART_NO);
				will(returnValue(subPhrase4_1));
				// sub phrase 1
				oneOf(subPhrase4_1).getValue();
				will(returnValue("in allen Höhenlagen"));
				oneOf(subPhrase4_1).isSpaceAfter();
				will(returnValue(true));
				oneOf(subPhrase4_1).isSpaceBefore();
				will(returnValue(true));
				oneOf(subPhrase4_1).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(subPhrase4_1).getId();
				will(returnValue(5));
			}
		});

		SentenceConfiguration sc = new SentenceConfiguration("test", new Version(1, 0));
		ModuleConfiguration mc = sc.addModuleConfiguration(1, Constants.FIRST_ITEM_NO);
		mc.addSubPhraseOptionConfiguration(new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("an_steilen").setPosNo(1), Constants.FIRST_ITEM_NO);
		mc.addSubPhraseOptionConfiguration(new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("Expo").setPosNo(1), Constants.FIRST_ITEM_NO);
		mc.addSubPhraseOptionConfiguration(new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("und_Expo").setPosNo(1), Constants.FIRST_ITEM_NO);
		mc.addSubPhraseOptionConfiguration(new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("oberhalb_von_Höhe_optional").setPosNo(1), Constants.FIRST_ITEM_NO);
		PhraseOptionConfiguration phraseOptionConfiguration = sc.getModuleConfiguration(1);

		try {
			List<? extends TextFragment> fragments = textBuilder.phrase2Textfragments(phrase, phraseOptionConfiguration, Language.de, DomainName.PRODUCTION, new HashMap<String, PhraseOption>());
			assertEquals("Textfragment count: ", 6, fragments.size());
			assertTextFragment("f1", fragments.get(0), "sowie", false, true, true);
			assertTextFragment("f2", fragments.get(1), "an", false, true, true);
			assertTextFragment("f3", fragments.get(2), "Nord", false, true, true);
			assertTextFragment("f4", fragments.get(3), "- und Süd", false, false, false);
			assertTextFragment("f5", fragments.get(4), "hängen", false, false, true);
			assertTextFragment("f6", fragments.get(5), "in allen Höhenlagen", false, true, true);
		} catch (TextCatException e) {
			fail(e.getMessage());
		}

	}

	public void testPhraseWithControllSignBeforeText_B() {
		final Phrase phrase = mock(Phrase.class, "phrase");
		final PhraseOption phraseOption1 = mock(PhraseOption.class, "phraseOption1_an_steilen");
		final Phrase subPhrase1_1 = mock(Phrase.class, "subPhrase1_1");

		final PhraseOption phraseOption2 = mock(PhraseOption.class, "phraseOption2_Expo");
		final Phrase subPhrase2_1 = mock(Phrase.class, "subPhrase2_1");

		final PhraseOption phraseOption3 = mock(PhraseOption.class, "phraseOption3_und_Expo");
		final Phrase subPhrase3_1 = mock(Phrase.class, "subPhrase3_1");

		final PhraseOption phraseOption4 = mock(PhraseOption.class, "phraseOption4_oberhalb_von_Höhe_optional");
		final Phrase subPhrase4_1 = mock(Phrase.class, "subPhrase4_1");

		checking(new Expectations() {
			{
				// first phrase
				oneOf(phrase).getValue();
				will(returnValue("sowie {an_steilen} {Expo} {und_Expo} (-)hängen {oberhalb_von_Höhe_optional}"));
				oneOf(phrase).isSpaceAfter();
				will(returnValue(true));
				oneOf(phrase).isSpaceBefore();
				will(returnValue(true));
				oneOf(phrase).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(phrase).getId();
				will(returnValue(1));
				// daoPhraseOption
				oneOf(mockDaoPhraseOption).getPhraseOption("an_steilen", Language.de, new Version(1, 0), DomainName.PRODUCTION);
				will(returnValue(phraseOption1));

				oneOf(mockDaoPhraseOption).getPhraseOption("Expo", Language.de, new Version(1, 0), DomainName.PRODUCTION);
				will(returnValue(phraseOption2));

				oneOf(mockDaoPhraseOption).getPhraseOption("und_Expo", Language.de, new Version(1, 0), DomainName.PRODUCTION);
				will(returnValue(phraseOption3));

				oneOf(mockDaoPhraseOption).getPhraseOption("oberhalb_von_Höhe_optional", Language.de, new Version(1, 0), DomainName.PRODUCTION);
				will(returnValue(phraseOption4));

				// phrase option 1
				oneOf(phraseOption1).getPhrase(Constants.FIRST_ITEM_NO, Constants.FIRST_ITEM_PART_NO);
				will(returnValue(subPhrase1_1));
				// sub phrase 1
				oneOf(subPhrase1_1).getValue();
				will(returnValue("an"));
				oneOf(subPhrase1_1).isSpaceAfter();
				will(returnValue(true));
				oneOf(subPhrase1_1).isSpaceBefore();
				will(returnValue(true));
				oneOf(subPhrase1_1).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(subPhrase1_1).getId();
				will(returnValue(2));
				
				// phrase option 2
				oneOf(phraseOption2).getPhrase(Constants.FIRST_ITEM_NO, Constants.FIRST_ITEM_PART_NO);
				will(returnValue(subPhrase2_1));
				// sub phrase 1
				oneOf(subPhrase2_1).getValue();
				will(returnValue("Nord"));
				oneOf(subPhrase2_1).isSpaceAfter();
				will(returnValue(true));
				oneOf(subPhrase2_1).isSpaceBefore();
				will(returnValue(true));
				oneOf(subPhrase2_1).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(subPhrase2_1).getId();
				will(returnValue(3));

				// phrase option 3
				oneOf(phraseOption3).getPhrase(Constants.FIRST_ITEM_NO, Constants.FIRST_ITEM_PART_NO);
				will(returnValue(subPhrase3_1));
				// sub phrase 1
				oneOf(subPhrase3_1).getValue();
				will(returnValue(""));
				allowing(subPhrase3_1).isSpaceAfter();
				will(returnValue(true));
				allowing(subPhrase3_1).isSpaceBefore();
				will(returnValue(true));
				allowing(subPhrase3_1).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(subPhrase3_1).getId();
				will(returnValue(4));

				// phrase option 4
				oneOf(phraseOption4).getPhrase(Constants.FIRST_ITEM_NO, Constants.FIRST_ITEM_PART_NO);
				will(returnValue(subPhrase4_1));
				// sub phrase 1
				oneOf(subPhrase4_1).getValue();
				will(returnValue("in allen Höhenlagen"));
				oneOf(subPhrase4_1).isSpaceAfter();
				will(returnValue(true));
				oneOf(subPhrase4_1).isSpaceBefore();
				will(returnValue(true));
				oneOf(subPhrase4_1).isRemovePunctuationBefore();
				will(returnValue(false));
				allowing(subPhrase4_1).getId();
				will(returnValue(5));
			}
		});

		SentenceConfiguration sc = new SentenceConfiguration("test", new Version(1, 0));
		ModuleConfiguration mc = sc.addModuleConfiguration(1, Constants.FIRST_ITEM_NO);
		mc.addSubPhraseOptionConfiguration(new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("an_steilen").setPosNo(1), Constants.FIRST_ITEM_NO);
		mc.addSubPhraseOptionConfiguration(new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("Expo").setPosNo(1), Constants.FIRST_ITEM_NO);
		mc.addSubPhraseOptionConfiguration(new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("und_Expo").setPosNo(1), Constants.FIRST_ITEM_NO);
		mc.addSubPhraseOptionConfiguration(new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("oberhalb_von_Höhe_optional").setPosNo(1), Constants.FIRST_ITEM_NO);
		PhraseOptionConfiguration phraseOptionConfiguration = sc.getModuleConfiguration(1);

		try {
			List<? extends TextFragment> fragments = textBuilder.phrase2Textfragments(phrase, phraseOptionConfiguration, Language.de, DomainName.PRODUCTION, new HashMap<String, PhraseOption>());
			assertEquals("Textfragment count: ", 5, fragments.size());
			assertTextFragment("f1", fragments.get(0), "sowie", false, true, true);
			assertTextFragment("f2", fragments.get(1), "an", false, true, true);
			assertTextFragment("f3", fragments.get(2), "Nord", false, true, true);
			assertTextFragment("f5", fragments.get(3), "hängen", false, false, true);
			assertTextFragment("f6", fragments.get(4), "in allen Höhenlagen", false, true, true);
		} catch (TextCatException e) {
			fail(e.getMessage());
		}

	}

}
