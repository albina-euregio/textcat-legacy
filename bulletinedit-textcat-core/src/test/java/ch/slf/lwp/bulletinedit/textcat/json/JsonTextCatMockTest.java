package ch.slf.lwp.bulletinedit.textcat.json;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.jmock.Expectations;
import org.jmock.integration.junit3.MockObjectTestCase;

import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;
import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructure;
import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructureModule;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

public class JsonTextCatMockTest extends MockObjectTestCase {
	
	JsonTextCat textCat;

	protected void setUp() throws Exception {
		super.setUp();
		textCat =  new JsonTextCat();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		textCat = null;
	}

	public void testTextCatOutput() {
		// init mocks
		final Sentence s = mock(Sentence.class, "s");
		final PhraseOption po1 = mock(PhraseOption.class, "op1");
		final PhraseOptionItem poi1 = mock(PhraseOptionItem.class, "poi1");
		final Phrase p1 = mock(Phrase.class, "p1");
		
		final PhraseOption po2 = mock(PhraseOption.class, "op2");
		final PhraseOptionItem poi2 = mock(PhraseOptionItem.class, "poi2");
		final Phrase p2 = mock(Phrase.class, "p2");
		
		SentenceStructure structure = SentenceStructure.parse("1.1");
		SentenceStructure.StructureElement e = structure.iterator().next();
		
		final SentenceStructureModule ssm = new SentenceStructureModule(po1, e);

		checking(new Expectations(){{
			atLeast(1).of(s).getName();will(returnValue("Schnee1"));
			atLeast(1).of(s).getId();will(returnValue(12));
			atLeast(1).of(s).getHeader();will(returnValue("Schnee1"));
			atLeast(1).of(s).getLanguage();will(returnValue(Language.de));
			atLeast(1).of(s).getKeywords();will(returnValue(getAsList("key1", "key2")));
			atLeast(1).of(s).getTopics();will(returnValue(getAsList("topic1", "topic2")));
			atLeast(1).of(s).isJokerSentence();will(returnValue(false));
			allowing(s).moduleIterator();will(returnIterator(ssm));
			allowing(s).getBasisVersion();will(returnValue(new Version(1,0)));
			
			atLeast(1).of(po1).getId();will(returnValue(34));
			atLeast(1).of(po1).getHeader();will(returnValue("HeaderPo1"));
			atLeast(1).of(po1).getName();will(returnValue("NamePo1"));
			atLeast(1).of(po1).phraseOptionItemIterator();will(returnIterator(poi1));
			atLeast(1).of(po1).phraseIterator(1);will(returnIterator(p1));
			
			atLeast(1).of(poi1).getPhrase(1);will(returnValue(p1));
			
			atLeast(1).of(p1).getId();will(returnValue(12));
			atLeast(1).of(p1).getRgn();will(returnValue("Region42"));
			atLeast(1).of(p1).getValue();will(returnValue("test{NamePo2}end"));
			
			// sub 
			atLeast(1).of(po2).getId();will(returnValue(234));
			atLeast(1).of(po2).getHeader();will(returnValue("HeaderPo2"));
			atLeast(1).of(po2).getName();will(returnValue("NamePo2"));
			atLeast(1).of(po2).phraseOptionItemIterator();will(returnIterator(poi2));
			atLeast(1).of(po2).phraseIterator(1);will(returnIterator(p2));
			
			atLeast(1).of(poi2).getPhrase(1);will(returnValue(p2));
			
			atLeast(1).of(p2).getId();will(returnValue(213));
			atLeast(1).of(p2).getRgn();will(returnValue("Region43"));
			atLeast(1).of(p2).getValue();will(returnValue("valueOf2"));

			
		}});
		
		JsonIndexConverter converter = new JsonIndexConverter();
		converter.getSentenceId(s);
		converter.getOptionId(po1);
		converter.getOptionId(po2);
		converter.getPhraseId(p2);
		converter.getPhraseId(p1);
		
		try {
			textCat.setJsonIndexConverter(converter);
			textCat.init(getAsList(s), getAsList(po1, po2));

			CatalogJson jsonS = (CatalogJson) textCat.getJsonSentences();
			CatalogJson jsonO = (CatalogJson) textCat.getJsonOptions();
			CatalogJson jsonP = (CatalogJson) textCat.getJsonPhrases();

			// verify sentence;
			Item[] sItems = jsonS.getItems();
			assertEquals("Sentence Size", 1, sItems.length);
			SentenceItem si = (SentenceItem) sItems[0];
			assertEquals("id", 0, si.getId());
			assertEquals("dbid", 12, si.getDbid());
			assertEquals("Name", "Schnee1", si.getName());
			assertEquals("options", "0", si.getOptions());

			// verify options;
			Item[] oItems = jsonO.getItems();
			assertEquals("Option Size", 2, oItems.length);
			OptionItem oi = (OptionItem) oItems[0];
			assertEquals("o1 id", 0, oi.getId());
			assertEquals("o1 dbId", 34, oi.getDbid());
			assertEquals("o1 Name", "NamePo1", oi.getName());
			assertEquals("o1 Phrases", "1", oi.getPhrases());

			oi = (OptionItem) oItems[1];
			assertEquals("o2 id", 1, oi.getId());
			assertEquals("o2 dbId", 234, oi.getDbid());
			assertEquals("o2 Name", "NamePo2", oi.getName());
			assertEquals("o2 Phrases", "0", oi.getPhrases());

			// verify Phrases
			// items are sorted by their id, therefore phrase2 is the first
			// phrase!
			Item[] pItems = jsonP.getItems();
			assertEquals("Phrases Size", 2, pItems.length);
			PhraseItem pi = (PhraseItem) pItems[0];
			assertEquals("p2 id", 0, pi.getId());
			assertEquals("p2 dbId", 213, pi.getDbid());
			assertEquals("p2 Value", "valueOf2", pi.getValue());

			pi = (PhraseItem) pItems[1];
			assertEquals("p1 id", 1, pi.getId());
			assertEquals("p1 dbId", 12, pi.getDbid());
			assertEquals("p1 Value", "test{1#1}end", pi.getValue());

			/*
			 * verify Object attribute names and getter Names. This names are
			 * required to create the JSON object attribute names
			 */
			// sentence fields
			List<String> fields = getAllFieldNames(si.getClass());
			List<String> methods = getMethodNames(si.getClass());
			assertTrue("s field: id", fields.contains("id"));
			assertTrue("s field: dbid", fields.contains("dbid"));
			assertTrue("s field: name", fields.contains("name"));
			assertTrue("s field: options", fields.contains("options"));

			assertTrue("s method: getId", methods.contains("getId"));
			assertTrue("s method: getDbid", methods.contains("getDbid"));
			assertTrue("s method: getName", methods.contains("getName"));
			assertTrue("s method: getOptions", methods.contains("getOptions"));

			// options fields
			fields = getAllFieldNames(oi.getClass());
			methods = getMethodNames(oi.getClass());
			assertTrue("o field: id", fields.contains("id"));
			assertTrue("o field: dbid", fields.contains("dbid"));
			assertTrue("o field: name", fields.contains("name"));
			assertTrue("o field: phrases", fields.contains("phrases"));

			assertTrue("o method: getId", methods.contains("getId"));
			assertTrue("o method: getDbid", methods.contains("getDbid"));
			assertTrue("o method: getName", methods.contains("getName"));
			assertTrue("o method: getPhrases", methods.contains("getPhrases"));

			// phrase fields
			fields = getAllFieldNames(pi.getClass());
			methods = getMethodNames(pi.getClass());
			assertTrue("p field: id", fields.contains("id"));
			assertTrue("p field: dbid", fields.contains("dbid"));
			assertTrue("p field: value", fields.contains("value"));

			assertTrue("p method: getId", methods.contains("getId"));
			assertTrue("p method: getDbid", methods.contains("getDbid"));
			assertTrue("p method: getValue", methods.contains("getValue"));

		} catch (Exception e1) {
			e1.printStackTrace();
			fail(e1.getMessage());
		}
	}
	
	public void testJsonSentenceFlag() {
		// init mocks
		final Sentence s = mock(Sentence.class, "s");
		final PhraseOption po1 = mock(PhraseOption.class, "op1");
		final PhraseOptionItem poi1 = mock(PhraseOptionItem.class, "poi1");
		final Phrase p1 = mock(Phrase.class, "p1");

		SentenceStructure structure = SentenceStructure.parse("1.1");
		SentenceStructure.StructureElement e = structure.iterator().next();
		
		final SentenceStructureModule ssm = new SentenceStructureModule(po1, e);
		
		checking(new Expectations(){{
			atLeast(1).of(s).getName();will(returnValue("Schnee1"));
			atLeast(1).of(s).getHeader();will(returnValue("Schnee1"));
			atLeast(1).of(s).getLanguage();will(returnValue(Language.de));
			atLeast(1).of(s).getId();will(returnValue(12));
			atLeast(1).of(s).getKeywords();will(returnValue(getAsList("key1", "key2")));
			atLeast(1).of(s).getTopics();will(returnValue(getAsList("topic1", "topic2")));
			atLeast(1).of(s).isJokerSentence();will(returnValue(true));
			allowing(s).moduleIterator();will(returnIterator(ssm));
			allowing(s).getBasisVersion();will(returnValue(new Version(1,0)));
			
			atLeast(1).of(po1).getId();will(returnValue(34));
			atLeast(1).of(po1).getHeader();will(returnValue("HeaderPo1"));
			atLeast(1).of(po1).getName();will(returnValue("NamePo1"));
			atLeast(1).of(po1).phraseOptionItemIterator();will(returnIterator(poi1));
			atLeast(1).of(po1).phraseIterator(1);will(returnIterator(p1));
			
			atLeast(1).of(poi1).getPhrase(1);will(returnValue(p1));
			
			atLeast(1).of(p1).getId();will(returnValue(12));
			atLeast(1).of(p1).getValue();will(returnValue("test end"));
			atLeast(1).of(p1).getRgn();will(returnValue("region42"));
		}});

		JsonIndexConverter converter = new JsonIndexConverter();
		converter.getSentenceId(s);
		converter.getOptionId(po1);
		converter.getPhraseId(p1);
		
		try {
			textCat.setJsonIndexConverter(converter);
			textCat.init(getAsList(s), getAsList(po1));

			CatalogJson jsonS = (CatalogJson) textCat.getJsonSentences();
			
			Item[] sItems = jsonS.getItems();
			assertEquals("Sentence Size", 1, sItems.length);
			SentenceItem si = (SentenceItem) sItems[0];
			assertEquals("id", 0, si.getId());
			assertEquals("dbid", 12, si.getDbid());
			assertEquals("Name", "Schnee1", si.getName());
			assertEquals("options", "0", si.getOptions());
			assertEquals("joker", true, si.isJoker());
			
			/*
			 * verify Object attribute names and getter Names. This names are
			 * required to create the JSON object attribute names
			 */
			// sentence fields
			List<String> fields = getAllFieldNames(si.getClass());
			List<String> methods = getMethodNames(si.getClass());
			assertTrue("s field: id", fields.contains("id"));
			assertTrue("s field: dbid", fields.contains("dbid"));
			assertTrue("s field: name", fields.contains("name"));
			assertTrue("s field: options", fields.contains("options"));
			assertTrue("s field: joker", fields.contains("joker"));

			assertTrue("s method: getId", methods.contains("getId"));
			assertTrue("s method: getDbid", methods.contains("getDbid"));
			assertTrue("s method: getName", methods.contains("getName"));
			assertTrue("s method: getOptions", methods.contains("getOptions"));
			assertTrue("s method: isJoker", methods.contains("isJoker"));
		} catch (Exception e1) {
			e1.printStackTrace();
			fail(e1.getMessage());
		}

	}
	
	private final <T> List<T> getAsList(T... ts) {
		ArrayList<T> l = new ArrayList<T>(ts.length);

		for (T t : ts) {
			l.add(t);
		}
		return l;
	}

	private List<String> getAllFieldNames(Class<?> clazz) {

		List<String> f = new ArrayList<String>();

		if (clazz.getSuperclass() != null) {
			Class<?> superClazz = clazz.getSuperclass();

			if (superClazz.getName().equals(Object.class.getName()) == false) {
				f.addAll(getAllFieldNames(superClazz));
			}
		}

		for (Field field : clazz.getDeclaredFields()) {
			f.add(field.getName());
		}

		return f;
	}

	private List<String> getMethodNames(Class<?> clazz) {

		List<String> m = new ArrayList<String>();

		for (Method methode : clazz.getMethods()) {
			m.add(methode.getName());
		}

		return m;
	}

}
