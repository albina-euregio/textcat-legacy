package ch.slf.lwp.bulletinedit.textcat.text;

import ch.slf.lwp.bulletinedit.textcat.text.TextDefinition;
import junit.framework.TestCase;

public class TextDefinitionTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testAsString() {
		String expected = "10[11[12,13],14[15,16]]";

		TextDefinition textdefinition = new TextDefinition();
		TextDefinition.Sentence s;

		s = textdefinition.new Sentence();
		s.setSentenceId(10);
		s.addPhrase(createTextelement(textdefinition, 11, 12, 13));
		s.addPhrase(createTextelement(textdefinition, 14, 15, 16));
		textdefinition.addSentence(s);

		assertEquals(expected, textdefinition.toString());
	}

	public void testAsString2Sentences() {
		String expected = "10[11].12[13]";

		TextDefinition textdefinition = new TextDefinition();
		TextDefinition.Sentence s;

		s = textdefinition.new Sentence();
		s.setSentenceId(10);
		s.addPhrase(createTextelement(textdefinition, 11));
		textdefinition.addSentence(s);

		s = textdefinition.new Sentence();
		s.setSentenceId(12);
		s.addPhrase(createTextelement(textdefinition, 13));
		textdefinition.addSentence(s);

		assertEquals(expected, textdefinition.toString());
	}

	private TextDefinition.Phrase createTextelement(TextDefinition textdefinition, int phraseId, int... subPhraseIds) {
		TextDefinition.Phrase textDefinitionPhrase = textdefinition.new Phrase();
		textDefinitionPhrase.setPhraseId(phraseId);

		for (int i = 0; i < subPhraseIds.length; i++) {
			TextDefinition.Phrase se = textdefinition.new Phrase();
			se.setPhraseId(subPhraseIds[i]);
			textDefinitionPhrase.addPhrase(se);
		}

		return textDefinitionPhrase;
	}

}
