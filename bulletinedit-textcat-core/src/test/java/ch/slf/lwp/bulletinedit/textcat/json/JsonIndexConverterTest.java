package ch.slf.lwp.bulletinedit.textcat.json;

import org.jmock.Expectations;
import org.jmock.integration.junit3.MockObjectTestCase;

import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;

public class JsonIndexConverterTest extends MockObjectTestCase {

	JsonIndexConverter converter;

	protected void setUp() throws Exception {
		super.setUp();
		converter = new JsonIndexConverter();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		converter = null;
	}

	public void testGetSentenceId() {
		final Sentence s0 = mock(Sentence.class, "s1");
		final Sentence s1 = mock(Sentence.class, "s2");
		final Sentence s2 = mock(Sentence.class, "s3");

		// init mocks
		checking(new Expectations() {
			{
				allowing(s0).getId();
				will(returnValue(23));
				allowing(s1).getId();
				will(returnValue(56));
				allowing(s2).getId();
				will(returnValue(12));
			}
		});

		// ids staring with zero ...
		assertEquals("0 - first call", 0, converter.getSentenceId(s0));
		assertEquals("1 - first call", 1, converter.getSentenceId(s1));
		assertEquals("2 - first call", 2, converter.getSentenceId(s2));

		// during the second call the id should be the same as before
		assertEquals("1 - second call", 1, converter.getSentenceId(s1));
		assertEquals("2 - second call", 2, converter.getSentenceId(s2));
		assertEquals("0 - second call", 0, converter.getSentenceId(s0));

	}

	public void testGetOptionId() {
		final PhraseOption o0 = mock(PhraseOption.class, "p1");
		final PhraseOption o1 = mock(PhraseOption.class, "p2");
		final PhraseOption o2 = mock(PhraseOption.class, "p3");

		// init mocks
		checking(new Expectations() {
			{
				allowing(o0).getId();will(returnValue(63));
				allowing(o1).getId();will(returnValue(356));
				allowing(o2).getId();will(returnValue(2312));
			}
		});

		// ids staring with zero ...
		assertEquals("0 - first call", 0, converter.getOptionId(o0));
		assertEquals("1 - first call", 1, converter.getOptionId(o1));
		assertEquals("2 - first call", 2, converter.getOptionId(o2));

		// during the second call the id should be the same as before
		assertEquals("1 - second call", 1, converter.getOptionId(o1));
		assertEquals("2 - second call", 2, converter.getOptionId(o2));
		assertEquals("0 - second call", 0, converter.getOptionId(o0));
	}

	public void testGetPhraseId() {
		final Phrase p0 = mock(Phrase.class, "o1");
		final Phrase p1 = mock(Phrase.class, "o2");
		final Phrase p2 = mock(Phrase.class, "o3");

		// init mocks
		checking(new Expectations() {
			{
				allowing(p0).getId();will(returnValue(234));
				allowing(p1).getId();will(returnValue(5546));
				allowing(p2).getId();will(returnValue(1290));
			}
		});

		// ids staring with zero ...
		assertEquals("0 - first call", 0, converter.getPhraseId(p0));
		assertEquals("1 - first call", 1, converter.getPhraseId(p1));
		assertEquals("2 - first call", 2, converter.getPhraseId(p2));

		// during the second call the id should be the same as before
		assertEquals("1 - second call", 1, converter.getPhraseId(p1));
		assertEquals("2 - second call", 2, converter.getPhraseId(p2));
		assertEquals("0 - second call", 0, converter.getPhraseId(p0));
	}

	public void testMixedIdRequest() {
		final Sentence s0 = mock(Sentence.class, "s1");
		final Sentence s1 = mock(Sentence.class, "s2");
		final PhraseOption o0 = mock(PhraseOption.class, "o1");
		final PhraseOption o1 = mock(PhraseOption.class, "o2");
		final Phrase p0 = mock(Phrase.class, "p1");
		final Phrase p1 = mock(Phrase.class, "p2");

		// init mocks
		checking(new Expectations() {
			{
				allowing(s0).getId();will(returnValue(23));
				allowing(s1).getId();will(returnValue(56));

				allowing(o0).getId();will(returnValue(63));
				allowing(o1).getId();will(returnValue(356));

				allowing(p0).getId();will(returnValue(234));
				allowing(p1).getId();will(returnValue(5546));
			}
		});

		// ids staring with zero ...
		assertEquals("s0 - first call", 0, converter.getSentenceId(s0));
		assertEquals("o0 - first call", 0, converter.getOptionId(o0));
		assertEquals("p0 - first call", 0, converter.getPhraseId(p0));
		assertEquals("p1 - first call", 1, converter.getPhraseId(p1));
		assertEquals("o1 - first call", 1, converter.getOptionId(o1));
		assertEquals("s1 - first call", 1, converter.getSentenceId(s1));

		assertEquals("o1 - second call", 1, converter.getOptionId(o1));
		assertEquals("s0 - second call", 0, converter.getSentenceId(s0));
		assertEquals("o0 - second call", 0, converter.getOptionId(o0));
		assertEquals("p1 - second call", 1, converter.getPhraseId(p1));
		assertEquals("p0 - second call", 0, converter.getPhraseId(p0));
		assertEquals("s1 - second call", 1, converter.getSentenceId(s1));

	}

}
