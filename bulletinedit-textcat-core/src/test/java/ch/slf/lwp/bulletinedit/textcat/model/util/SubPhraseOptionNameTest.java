package ch.slf.lwp.bulletinedit.textcat.model.util;

import ch.slf.lwp.bulletinedit.textcat.Constants;
import junit.framework.TestCase;

public class SubPhraseOptionNameTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testHashCode() {
		SubPhraseOptionName spon1;
		SubPhraseOptionName spon2;

		spon1 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("banane").setPosNo(2);
		spon2 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("banane").setPosNo(2);
		assertEquals(spon1.hashCode(), spon2.hashCode());

		spon1 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("banane").setPosNo(1).setItemPartNo(2);
		spon2 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("banane").setPosNo(1).setItemPartNo(2);
		assertEquals(spon1.hashCode(), spon2.hashCode());
	}

	public void testEqualsObject() {
		SubPhraseOptionName spon1;
		SubPhraseOptionName spon2;

		spon1 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("banane").setPosNo(2);
		spon2 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("banane").setPosNo(2);
		assertTrue(spon1.equals(spon2));
		assertTrue(spon2.equals(spon1));

		spon1 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("banane").setPosNo(1).setItemPartNo(2);
		spon2 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("banane").setPosNo(1).setItemPartNo(2);
		assertTrue(spon1.equals(spon2));
		assertTrue(spon2.equals(spon1));

		spon1 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("banane").setPosNo(1).setItemPartNo(1);
		spon2 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("banane").setPosNo(1).setItemPartNo(2);
		assertFalse(spon1.equals(spon2));
		assertFalse(spon2.equals(spon1));

		spon1 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("banane").setPosNo(1).setItemPartNo(1);
		spon2 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("banane").setPosNo(2).setItemPartNo(1);
		assertFalse(spon1.equals(spon2));
		assertFalse(spon2.equals(spon1));

		spon1 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("bananen").setPosNo(1).setItemPartNo(1);
		spon2 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("banane").setPosNo(1).setItemPartNo(1);
		assertFalse(spon1.equals(spon2));
		assertFalse(spon2.equals(spon1));
	}

	public void testSetPhraseOptionNameWithItemPartNo() {
		String phraseOptionName = "banane";
		SubPhraseOptionName spon = null;
		
		spon = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo(phraseOptionName);
		assertEquals("Name 1", phraseOptionName,  spon.getPhraseOptionName());
		assertEquals("ItemPartNo 1", Constants.FIRST_ITEM_PART_NO, spon.getItemPartNo());
		
		spon = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo(phraseOptionName + SubPhraseOptionName.SUP_PHRASEOPTION_SECOND_ITEMPARTNO_SUFFIX);
		assertEquals("Name 1", phraseOptionName,  spon.getPhraseOptionName());
		assertEquals("ItemPartNo 1", Constants.FIRST_ITEM_PART_NO + 1, spon.getItemPartNo());
		
	}

}
