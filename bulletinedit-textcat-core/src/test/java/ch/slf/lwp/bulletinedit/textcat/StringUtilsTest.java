package ch.slf.lwp.bulletinedit.textcat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.slf.lwp.bulletinedit.textcat.model.util.SubPhraseOptionName;

import junit.framework.TestCase;

public class StringUtilsTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testInsertSubPhraseOptionValue() {

		String expected = "das ist value e1 ein Test value e2 Phrase value {option3}";
		String phraseValue = "das ist {option1} ein Test {option2#3} Phrase value {option3}";

		SubPhraseOptionName optionName1 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("option1");
		SubPhraseOptionName optionName2 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("option2").setPosNo(3);

		HashMap<SubPhraseOptionName, String> replacements = new HashMap<SubPhraseOptionName, String>();
		replacements.put(optionName1, "value e1");
		replacements.put(optionName2, "value e2");

		String actual = StringUtils.insertSubPhraseOptionValue(phraseValue, replacements);
		assertNotNull("String required", actual);
		assertEquals(expected, actual);
	}

	public void testGetSubPhraseOptionNames() {
		SubPhraseOptionName e1 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("option1");
		SubPhraseOptionName e2 = new SubPhraseOptionName().setPhraseOptionNameWithItemPartNo("option2").setPosNo(3);

		List<SubPhraseOptionName> actuals = StringUtils.getSubPhraseOptionNames("das ist {option1} ein Test {option2#3} Phrase value");

		assertNotNull("Expect a list", actuals);
		assertEquals("Count", 2, actuals.size());

		SubPhraseOptionName a1 = actuals.get(0);
		assertEquals("equals 1: Option 1", e1, a1);
		assertEquals("phrase option no 1", e1.getPhraseOptionName(), a1.getPhraseOptionName());
		assertEquals("pos no", e1.getPosNo(), a1.getPosNo());

		SubPhraseOptionName a2 = actuals.get(1);
		assertEquals("equals 1: Option 1", e2, a2);
		assertEquals("phrase option no 1", e2.getPhraseOptionName(), a2.getPhraseOptionName());
		assertEquals("pos no", e2.getPosNo(), a2.getPosNo());
	}

	public void testReplacePhraseOptionNames() {
		Map<String, String> replacements = new HashMap<String, String>();
		replacements.put("parent§Himmelsrichtung", "1");
		replacements.put("Himmelsrichtung", "2");

		String phraseValue;
		String actual;
		String expected;

		phraseValue = "und überall an {parent§Himmelsrichtung#1}- und {parent§Himmelsrichtung#2}hängen";
		expected = "und überall an {1#1}- und {1#2}hängen";
		actual = StringUtils.replacePhraseOptionNames(phraseValue, replacements);
		assertEquals("Test1", expected, actual);

		phraseValue = "und überall an {parent§Himmelsrichtung}- und Dächern hängen";
		expected = "und überall an {1#1}- und Dächern hängen";
		actual = StringUtils.replacePhraseOptionNames(phraseValue, replacements);
		assertEquals("Test2", expected, actual);

		phraseValue = "und überall an {parent§Himmelsrichtung#1}- und {Himmelsrichtung#3}hängen";
		expected = "und überall an {1#1}- und {2#3}hängen";
		actual = StringUtils.replacePhraseOptionNames(phraseValue, replacements);
		assertEquals("Test3", expected, actual);

		phraseValue = "und überall an {parent§Himmelsrichtung}- und {Himmelsrichtung}hängen";
		expected = "und überall an {1#1}- und {2#1}hängen";
		actual = StringUtils.replacePhraseOptionNames(phraseValue, replacements);
		assertEquals("Test4", expected, actual);

		phraseValue = "und überall an {parent§Himmelsrichtung}- und {parent@1}hängen";
		expected = "und überall an {1#1}- und {parent@1}hängen";
		actual = StringUtils.replacePhraseOptionNames(phraseValue, replacements);
		assertEquals("Test5", expected, actual);

		phraseValue = "In diesen Gebieten";
		expected = "In diesen Gebieten";
		actual = StringUtils.replacePhraseOptionNames(phraseValue, replacements);
		assertEquals("Test6", expected, actual);
	}
	
	public void testExpandShortSubPhraseOptionName() throws Exception {
		String parentName = "parent";
		String phraseValue;
		String expected;
		String actual;

		// Test 1
		phraseValue = "und überall an {Himmelsrichtung}- und {Himmelsrichtung}hängen";
		expected = "und überall an {Himmelsrichtung}- und {Himmelsrichtung}hängen";
		actual = StringUtils.expandShortSubPhraseOptionName(phraseValue,parentName);
		assertEquals("Multible usage sub option", expected, actual);

		// Test 2
		phraseValue = "und überall an {Himmelsrichtung}- und {§Himmelsrichtung}hängen";
		expected = "und überall an {Himmelsrichtung}- und {parent§Himmelsrichtung}hängen";
		actual = StringUtils.expandShortSubPhraseOptionName(phraseValue,parentName);
		assertEquals("Mixed sub option", expected, actual);

		// Test 3
		phraseValue = "In diesen Gebieten";
		expected = "In diesen Gebieten";
		actual = StringUtils.expandShortSubPhraseOptionName(phraseValue,parentName);
		assertEquals("none sub option", expected, actual);

		// Test 4
		phraseValue = "und überall an {§Himmelsrichtung}- und {§Himmelsrichtung}hängen";
		expected = "und überall an {parent§Himmelsrichtung}- und {parent§Himmelsrichtung}hängen";
		actual = StringUtils.expandShortSubPhraseOptionName(phraseValue,parentName);
		assertEquals("None multible usage sub option", expected, actual);

		// Test 5
		parentName = "parent@1";
		phraseValue = "und überall an {§Himmelsrichtung}- und {§Himmelsrichtung}hängen";
		expected = "und überall an {parent§Himmelsrichtung}- und {parent§Himmelsrichtung}hängen";
		actual = StringUtils.expandShortSubPhraseOptionName(phraseValue,parentName);
		assertEquals("None multible usage sub option", expected, actual);

		// Test 6
		phraseValue = "und überall an {Himmelsrichtung#1}- und {Himmelsrichtung#2}hängen";
		expected = "und überall an {Himmelsrichtung#1}- und {Himmelsrichtung#2}hängen";
		actual = StringUtils.expandShortSubPhraseOptionName(phraseValue,parentName);
		assertEquals("Pos number", expected, actual);
	}
	
	public void testCreateUniqueSubPhraseOptionName() {
		String parentName = "affe";
		String subPhraseoptionSeq = "banane";
		String actual = null;

		// null test
		try {
			actual = StringUtils.createUniqueSubPhraseOptionName(null, null);
			fail("Exception expected");
		} catch (Throwable e) {
			assertEquals("Expected exception", IllegalArgumentException.class, e.getClass());
		}

		actual = StringUtils.createUniqueSubPhraseOptionName(parentName, subPhraseoptionSeq);
		assertEquals("Multible usage sub phrase", subPhraseoptionSeq, actual);

		actual = StringUtils.createUniqueSubPhraseOptionName(parentName, "§" + subPhraseoptionSeq);
		String expected = parentName + "§" + subPhraseoptionSeq;
		assertEquals("sub phrase without parent", expected, actual);

		expected = parentName + "§" + subPhraseoptionSeq;
		actual = StringUtils.createUniqueSubPhraseOptionName(parentName, expected);
		assertEquals("sub phrase without parent", expected, actual);

		expected = parentName + "§" + subPhraseoptionSeq;
		String h = StringUtils.createUniquePhraseOptionNo(parentName, 1);
		actual = StringUtils.createUniqueSubPhraseOptionName(h, "§" + subPhraseoptionSeq);
		assertEquals("sub phrase option inside a single usage phrase option", expected, actual);
	}


}
