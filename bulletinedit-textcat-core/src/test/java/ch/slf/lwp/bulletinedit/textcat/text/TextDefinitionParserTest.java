package ch.slf.lwp.bulletinedit.textcat.text;

import java.text.ParseException;
import java.util.List;

import ch.slf.lwp.bulletinedit.textcat.text.TextDefinition;
import ch.slf.lwp.bulletinedit.textcat.text.TextDefinitionParser;

import junit.framework.TestCase;

/**
 * @author gw
 */
public class TextDefinitionParserTest extends TestCase {

	TextDefinitionParser parser = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		parser = new TextDefinitionParser();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		parser = null;
	}

	/**
	 * Test method for
	 * {@link ch.slf.lwp.bulletinedit.textcat.text.TextDefinitionParser#parse(java.lang.String)}
	 * .
	 */
	public void testParseSimple() {
		String textDef = "10[11]";

		try {
			TextDefinition text = parser.parse(textDef);

			assertNotNull("A text", text);

			List<TextDefinition.Sentence> sentences = text.getSentences();
			assertEquals("sentence size", 1, sentences.size());

			TextDefinition.Sentence s = sentences.get(0);
			assertEquals("sentence id", 10, s.getSentenceId());

			List<TextDefinition.Phrase> textDefinitionPhrases = s.getPhrases();
			;
			assertEquals("textelement size", 1, textDefinitionPhrases.size());

			TextDefinition.Phrase textDefinitionPhrase = textDefinitionPhrases.get(0);
			assertEquals("phrase phrase id", 11, textDefinitionPhrase.getPhraseId());

			assertEquals("subtextelement size", 0, textDefinitionPhrase.getSubPhrases().size());

		} catch (ParseException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link ch.slf.lwp.bulletinedit.textcat.text.TextDefinitionParser#parse(java.lang.String)}
	 * .
	 */
	public void testParseTwoTextelements() {
		String textDef = "10[11,12]";

		try {
			TextDefinition text = parser.parse(textDef);

			assertNotNull("A text", text);

			List<TextDefinition.Sentence> sentences = text.getSentences();
			assertEquals("sentence size", 1, sentences.size());

			TextDefinition.Sentence s = sentences.get(0);
			assertEquals("sentence id", 10, s.getSentenceId());

			List<TextDefinition.Phrase> textDefinitionPhrases = s.getPhrases();
			assertEquals("textelement size", 2, textDefinitionPhrases.size());

			TextDefinition.Phrase textDefinitionPhrase = textDefinitionPhrases.get(0);
			assertEquals("phrase phrase id", 11, textDefinitionPhrase.getPhraseId());
			assertEquals("subtextelement size", 0, textDefinitionPhrase.getSubPhrases().size());

			textDefinitionPhrase = textDefinitionPhrases.get(1);
			assertEquals("phrase phrase id", 12, textDefinitionPhrase.getPhraseId());
			assertEquals("subtextelement size", 0, textDefinitionPhrase.getSubPhrases().size());

		} catch (ParseException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link ch.slf.lwp.bulletinedit.textcat.text.TextDefinitionParser#parse(java.lang.String)}
	 * .
	 */
	public void testParseSimpleSubtextelements() {
		String textDef = "10[11[12]]";

		try {
			TextDefinition text = parser.parse(textDef);

			assertNotNull("A text", text);

			List<TextDefinition.Sentence> sentences = text.getSentences();
			assertEquals("sentence size", 1, sentences.size());

			TextDefinition.Sentence s = sentences.get(0);
			assertEquals("sentence id",  10, s.getSentenceId());

			List<TextDefinition.Phrase> textDefinitionPhrases = s.getPhrases();
			assertEquals("textelement size", 1, textDefinitionPhrases.size());

			TextDefinition.Phrase textDefinitionPhrase = textDefinitionPhrases.get(0);
			assertEquals("phrase phrase id", 11, textDefinitionPhrase.getPhraseId());

			assertEquals("subtextelement size", 1, textDefinitionPhrase.getSubPhrases().size());

			TextDefinition.Phrase textDefinitionSubPhrase = textDefinitionPhrase.getSubPhrases().get(0);
			assertEquals("sub phrase phrase id", 12, textDefinitionSubPhrase.getPhraseId());

		} catch (ParseException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link ch.slf.lwp.bulletinedit.textcat.text.TextDefinitionParser#parse(java.lang.String)}
	 * .
	 */
	public void testParseTwoSubtextelements() {
		String textDef = "10[11[12,13]]";

		try {
			TextDefinition text = parser.parse(textDef);

			assertNotNull("A text", text);

			List<TextDefinition.Sentence> sentences = text.getSentences();
			assertEquals("sentence size", 1, sentences.size());

			TextDefinition.Sentence s = sentences.get(0);
			assertEquals("sentence id", 10, s.getSentenceId());

			List<TextDefinition.Phrase> textDefinitionPhrases = s.getPhrases();
			assertEquals("textelement size", 1, textDefinitionPhrases.size());

			TextDefinition.Phrase textDefinitionPhrase = textDefinitionPhrases.get(0);
			assertEquals("phrase phrase id", 11, textDefinitionPhrase.getPhraseId());

			List<TextDefinition.Phrase> textDefinitionSubPhrases = textDefinitionPhrase.getSubPhrases();
			TextDefinition.Phrase textDefinitionSubPhrase;

			assertEquals("subtextelement size", 2, textDefinitionSubPhrases.size());

			textDefinitionSubPhrase = textDefinitionSubPhrases.get(0);
			assertEquals("sub phrase 1 phrase id", 12, textDefinitionSubPhrase.getPhraseId());

			textDefinitionSubPhrase = textDefinitionSubPhrases.get(1);
			assertEquals("sub phrase 2 phrase id", 13, textDefinitionSubPhrase.getPhraseId());

		} catch (ParseException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link ch.slf.lwp.bulletinedit.textcat.text.TextDefinitionParser#parse(java.lang.String)}
	 * .
	 */
	public void testParseTwoTextelementsWithTwoSubtextelements() {
		String textDef = "10[11[12,13],14[15,16]]";

		try {
			TextDefinition text = parser.parse(textDef);

			assertNotNull("A text", text);

			List<TextDefinition.Sentence> sentences = text.getSentences();
			assertEquals("sentence size", 1, sentences.size());

			TextDefinition.Sentence s = sentences.get(0);
			assertEquals("sentence id", 10, s.getSentenceId());

			List<TextDefinition.Phrase> textDefinitionPhrases = s.getPhrases();
			TextDefinition.Phrase textDefinitionPhrase;
			TextDefinition.Phrase textDefinitionSubPhrase;
			assertEquals("textelement size", 2, textDefinitionPhrases.size());

			// textelement 1:
			textDefinitionPhrase = textDefinitionPhrases.get(0);
			assertEquals("textelement 1: phrase phrase id", 11, textDefinitionPhrase.getPhraseId());

			List<TextDefinition.Phrase> textDefinitionSubPhrases = textDefinitionPhrase.getSubPhrases();
			assertEquals("textelement 1: subtextelement size", 2, textDefinitionSubPhrases.size());

			textDefinitionSubPhrase = textDefinitionSubPhrases.get(0);
			assertEquals("textelement 1: sub phrase 1 phrase id",  12, textDefinitionSubPhrase.getPhraseId());

			textDefinitionSubPhrase = textDefinitionSubPhrases.get(1);
			assertEquals("textelement 1: sub phrase 2 phrase id",  13, textDefinitionSubPhrase.getPhraseId());

			// textelement 2:
			textDefinitionPhrase = textDefinitionPhrases.get(1);
			assertEquals("textelement 2: phrase phrase id",  14, textDefinitionPhrase.getPhraseId());

			textDefinitionSubPhrases = textDefinitionPhrase.getSubPhrases();
			assertEquals("textelement 2: subtextelement size", 2, textDefinitionSubPhrases.size());

			textDefinitionSubPhrase = textDefinitionSubPhrases.get(0);
			assertEquals("textelement 2: sub phrase 1 phrase id",  15, textDefinitionSubPhrase.getPhraseId());

			textDefinitionSubPhrase = textDefinitionSubPhrases.get(1);
			assertEquals("textelement 2: sub phrase 2 phrase id",  16, textDefinitionSubPhrase.getPhraseId());

		} catch (ParseException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test method for
	 * {@link ch.slf.lwp.bulletinedit.textcat.text.TextDefinitionParser#parse(java.lang.String)}
	 * .
	 */
	public void testParseTwoTextelementsWithSubSubElements() {
		String textDef = "10[11[12[13,14],15],16[17,18[19]]]";

		try {
			TextDefinition text = parser.parse(textDef);

			assertNotNull("A text", text);

			List<TextDefinition.Sentence> sentences = text.getSentences();
			assertEquals("sentence size", 1, sentences.size());

			TextDefinition.Sentence s = sentences.get(0);
			assertEquals("sentence id", 10, s.getSentenceId());

			List<TextDefinition.Phrase> textDefinitionPhrases = s.getPhrases();
			TextDefinition.Phrase textDefinitionPhrase;
			TextDefinition.Phrase textDefinitionSubPhrase;
			TextDefinition.Phrase textDefinitionSubSubPhrase;
			assertEquals("textelement size", 2, textDefinitionPhrases.size());

			// textelement 1:
			textDefinitionPhrase = textDefinitionPhrases.get(0);
			assertEquals("textelement 1: phrase phrase id", 11, textDefinitionPhrase.getPhraseId());

			List<TextDefinition.Phrase> textDefinitionSubPhrases = textDefinitionPhrase.getSubPhrases();
			assertEquals("textelement 1: subtextelement size", 2, textDefinitionSubPhrases.size());

			textDefinitionSubPhrase = textDefinitionSubPhrases.get(0);
			assertEquals("textelement 1: sub phrase 1 phrase id",  12, textDefinitionSubPhrase.getPhraseId());
			
			//  start subsub phrases phrase 12
			List<TextDefinition.Phrase> textDefinitionSubSubPhrases = textDefinitionSubPhrase.getSubPhrases();
			assertEquals("Sub Phrase 12: subtextelement size", 2, textDefinitionSubSubPhrases.size());
			
			textDefinitionSubSubPhrase = textDefinitionSubSubPhrases.get(0);
			assertEquals("Sub Phrase 12: sub sub phrase 1 phrase id",  13, textDefinitionSubSubPhrase.getPhraseId());
			
			textDefinitionSubSubPhrase = textDefinitionSubSubPhrases.get(1);
			assertEquals("Sub Phrase 12: sub sub phrase 2 phrase id",  14, textDefinitionSubSubPhrase.getPhraseId());
			// end subsub phrases phrase 12

			textDefinitionSubPhrase = textDefinitionSubPhrases.get(1);
			assertEquals("textelement 1: sub phrase 2 phrase id",  15, textDefinitionSubPhrase.getPhraseId());
			
			// textelement 2:
			textDefinitionPhrase = textDefinitionPhrases.get(1);
			assertEquals("textelement 2: phrase phrase id",  16, textDefinitionPhrase.getPhraseId());

			textDefinitionSubPhrases = textDefinitionPhrase.getSubPhrases();
			assertEquals("textelement 2: subtextelement size", 2, textDefinitionSubPhrases.size());

			textDefinitionSubPhrase = textDefinitionSubPhrases.get(0);
			assertEquals("textelement 2: sub phrase 1 phrase id",  17, textDefinitionSubPhrase.getPhraseId());

			textDefinitionSubPhrase = textDefinitionSubPhrases.get(1);
			assertEquals("textelement 2: sub phrase 2 phrase id",  18, textDefinitionSubPhrase.getPhraseId());
			
			//  start subsub phrases phrase 18
			textDefinitionSubSubPhrases = textDefinitionSubPhrase.getSubPhrases();
			assertEquals("Sub Phrase 18: subtextelement size", 1, textDefinitionSubSubPhrases.size());
			
			textDefinitionSubSubPhrase = textDefinitionSubSubPhrases.get(0);
			assertEquals("Sub Phrase 18: sub sub phrase 1 phrase id",  19, textDefinitionSubSubPhrase.getPhraseId());
			// end subsub phrases phrase 18
			
			// to String
			assertEquals("String test", textDef, text.toString());

		} catch (ParseException e) {
			fail(e.getMessage());
		}
	}


	/**
	 * Test method for
	 * {@link ch.slf.lwp.bulletinedit.textcat.text.TextDefinitionParser#parse(java.lang.String)}
	 * .
	 */
	public void testParseTwoSentences() {
		String textDef = "10[11].12[13]";

		try {
			TextDefinition text = parser.parse(textDef);

			assertNotNull("A text", text);

			List<TextDefinition.Sentence> sentences = text.getSentences();
			assertEquals("sentence size", 2, sentences.size());

			TextDefinition.Sentence s;

			// sentence 1
			s = sentences.get(0);
			assertEquals("sentence id", 10, s.getSentenceId());

			List<TextDefinition.Phrase> textDefinitionPhrases = s.getPhrases();
			assertEquals("textelement size", 1, textDefinitionPhrases.size());

			TextDefinition.Phrase textDefinitionPhrase = textDefinitionPhrases.get(0);
			assertEquals("phrase phrase id", 11, textDefinitionPhrase.getPhraseId());

			assertEquals("subtextelement size", 0, textDefinitionPhrase.getSubPhrases().size());

			// sentence 2
			s = sentences.get(1);
			assertEquals("sentence id", 12, s.getSentenceId());

			textDefinitionPhrases = s.getPhrases();
			assertEquals("textelement size", 1, textDefinitionPhrases.size());

			textDefinitionPhrase = textDefinitionPhrases.get(0);
			assertEquals("phrase phrase id", 13, textDefinitionPhrase.getPhraseId());

			assertEquals("subtextelement size", 0, textDefinitionPhrase.getSubPhrases().size());

		} catch (ParseException e) {
			fail(e.getMessage());
		}
	}
	
	public void simpleRecursionsTest()
	{
		// syntax ok, however to deep and complex to check it explicit. Therefore I just compare the string representation. 
		String textDef = "1[2[3[4[5,6],7]],8,9].10[11,12[13,[14],15,16[17,18[19[20[21],22],23]]]]";

		try {
			TextDefinition text = parser.parse(textDef);

			assertNotNull("A text", text);
			assertEquals("Simple string test", textDef, text.toString());

		} catch (ParseException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link ch.slf.lwp.bulletinedit.textcat.text.TextDefinitionParser#parse(java.lang.String)}
	 * .
	 */
	public void testParserError() {
		String textDef = "10[].12[13]";

		try {
			parser.parse(textDef);
			fail("Parser Exception expected!");
		} catch (ParseException e) {
			assertEquals("Error offset", 3, e.getErrorOffset());
		}

	}

	/**
	 * Test method for
	 * {@link ch.slf.lwp.bulletinedit.textcat.text.TextDefinitionParser#parse(java.lang.String)}
	 * .
	 */
	public void testUnexpectedEnd() {
		String textDef = "12[13";

		try {
			parser.parse(textDef);
			fail("Parser Exception expected!");
		} catch (ParseException e) {
			assertEquals("Error offset", 5, e.getErrorOffset());
		}

	}

}
