package ch.slf.lwp.bulletinedit.textcat.json;

/**
 * JSON wrapper JSON items
 * 
 * @author weiss
 */
public class Json {

	private Item[] items;

	/**
	 * @return the items
	 */
	public Item[] getItems() {
		return items;
	}

	/**
	 * @param items
	 *            the items to set
	 */
	public void setItems(Item[] items) {
		this.items = items;
	}

}
