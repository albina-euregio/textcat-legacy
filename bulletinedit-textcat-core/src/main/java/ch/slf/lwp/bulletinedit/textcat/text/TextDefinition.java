package ch.slf.lwp.bulletinedit.textcat.text;

import java.util.ArrayList;
import java.util.List;

/**
 * Language dependent text definition. The definition is based on unique id
 * numbers from sentences and the according phrases and sub phrases. The phrase
 * id number sequence is equivalent to the sentence structure. The subphrase id
 * sequence is equivalent to the occurrence of the subphrase option inside the
 * phrase.
 * 
 * @author weiss
 */
public class TextDefinition {

	public final static char SENTENCE_SEPERATOR = '.';
	public final static char PHRASE_SEPERATOR = ',';
//	public final static char SUBPHRASE_SEPERATOR = ',';
	public final static char PHRASE_LIST_START = '[';
	public final static char PHRASE_LIST_END = ']';
//	public final static char SUBPHRASE_LIST_START = '[';
//	public final static char SUBPHRASE_LIST_END = ']';

	private List<TextDefinition.Sentence> sentences = new ArrayList<TextDefinition.Sentence>();

	/**
	 * @return the sentences
	 */
	public List<TextDefinition.Sentence> getSentences() {
		return sentences;
	}

	public void addSentence(TextDefinition.Sentence s) {
		sentences.add(s);
	}

	@Override
	public String toString() {
		return this.toStringBuilder(new StringBuilder()).toString();
	}

	private StringBuilder toStringBuilder(StringBuilder s) {

		for (int i = 0; i < sentences.size(); i++) {
			TextDefinition.Sentence sentence = sentences.get(i);
			sentence.toStringBuilder(s);

			if (i + 1 < sentences.size()) {
				s.append(SENTENCE_SEPERATOR);
			}
		}

		return s;
	}
	
	interface Component {
		
		void addPhrase(TextDefinition.Phrase p);
	}

	public class Sentence implements Component {

		private int sentenceId;
		private List<TextDefinition.Phrase> phrases = new ArrayList<TextDefinition.Phrase>();

		/**
		 * @return the sentenceId
		 */
		public int getSentenceId() {
			return sentenceId;
		}

		/**
		 * @param sentenceId
		 *            the sentenceId to set
		 */
		public void setSentenceId(int sentenceId) {
			this.sentenceId = sentenceId;
		}

		/**
		 * @return the phrases
		 */
		public List<TextDefinition.Phrase> getPhrases() {
			return phrases;
		}

		@Override
		public void addPhrase(TextDefinition.Phrase p) {
			phrases.add(p);
		}

		/**
		 * returns the number of phrases in this sentence.
		 * 
		 * @return the number of phrases in this sentence
		 */
		public int phraseSize() {
			return phrases.size();
		}

		@Override
		public String toString() {
			return this.toStringBuilder(new StringBuilder()).toString();
		}

		private StringBuilder toStringBuilder(StringBuilder s) {
			s.append(this.sentenceId);

			if (!phrases.isEmpty()) {
				s.append(PHRASE_LIST_START);

				for (int i = 0; i < phrases.size(); i++) {
					TextDefinition.Phrase p = phrases.get(i);
					p.toStringBuilder(s);

					if (i + 1 < phrases.size()) {
						s.append(PHRASE_SEPERATOR);
					}
				}

				s.append(PHRASE_LIST_END);
			}

			return s;
		}

	}

	public class Phrase  implements Component {

		private int phraseId;
		private List<TextDefinition.Phrase> subPhrases = new ArrayList<TextDefinition.Phrase>();

		/**
		 * @return the phraseId
		 */
		public int getPhraseId() {
			return phraseId;
		}

		/**
		 * @param phraseId
		 *            the phraseId to set
		 */
		public void setPhraseId(int phraseId) {
			this.phraseId = phraseId;
		}

		/**
		 * @return the subPhrases
		 */
		public List<TextDefinition.Phrase> getSubPhrases() {
			return subPhrases;
		}

		@Override
		public void addPhrase(TextDefinition.Phrase sP) {
			subPhrases.add(sP);
		}

		@Override
		public String toString() {
			return this.toStringBuilder(new StringBuilder()).toString();
		}

		private StringBuilder toStringBuilder(StringBuilder s) {
			s.append(this.phraseId);

			if (!subPhrases.isEmpty()) {
				s.append(PHRASE_LIST_START);

				for (int i = 0; i < subPhrases.size(); i++) {
					TextDefinition.Phrase sp = subPhrases.get(i);
					sp.toStringBuilder(s);

					if (i + 1 < subPhrases.size()) {
						s.append(PHRASE_SEPERATOR);
					}
				}

				s.append(PHRASE_LIST_END);
			}

			return s;
		}

		public int subPhraseSize() {
			return subPhrases.size();
		}

	}

}
