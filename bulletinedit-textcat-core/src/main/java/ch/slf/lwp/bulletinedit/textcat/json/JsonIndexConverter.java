package ch.slf.lwp.bulletinedit.textcat.json;

import java.util.HashMap;
import java.util.Map;

import ch.slf.lwp.bulletinedit.textcat.Constants;
import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructureModule;

/**
 * Convert the database id to the required id sequence
 * 
 * @author gw
 */
public class JsonIndexConverter {

	private Map<Integer, Integer> phraseIdMap = new HashMap<Integer, Integer>();
	private Sequence phraseSeq = new Sequence();

	private Map<Integer, Integer> optionIdMap = new HashMap<Integer, Integer>();
	private Sequence optionSeq = new Sequence();

	private Map<Integer, Integer> sentenceIdMap = new HashMap<Integer, Integer>();
	private Sequence sentenceSeq = new Sequence();

	public int getSentenceId(Sentence sentence) {

		synchronized (sentenceIdMap) {
			if (sentenceIdMap.containsKey(sentence.getId())) {
				return sentenceIdMap.get(sentence.getId());
			} else {
				int id = sentenceSeq.nextValue();
				sentenceIdMap.put(sentence.getId(), id);
				return id;
			}
		}

	}

	public int getOptionId(PhraseOption phraseOption) {
        return getOptionId(phraseOption.getId());
    }

   	public int getSentenceStructureModuleId(SentenceStructureModule module) {
		// Clesius
		//return getOptionId(module.getId());
	    if (module.getItemPartNo()==1)
	    		return getOptionId(module.getId());
	    else
            return getOptionId(module.getId()+ Constants.FAKE_ID_INCREMENT);
	}

/*    private int getOptionIdIt(int phraseOptionId) {

        synchronized (optionIdMap) {
            if (optionIdMap.containsKey(phraseOptionId)) {
                return optionIdMap.get(phraseOptionId);
            } else {
                int id = optionSeq.nextValue();
                optionIdMap.put(phraseOptionId, id);
                return id;
            }
        }
    }
*/
	private int getOptionId(int phraseOptionId) {

		synchronized (optionIdMap) {
			if (optionIdMap.containsKey(phraseOptionId)) {
				return optionIdMap.get(phraseOptionId);
			} else {
				int id = optionSeq.nextValue();
				optionIdMap.put(phraseOptionId, id);
				return id;
			}
		}
	}

	public int getPhraseId(Phrase phrase) {

		synchronized (phraseIdMap) {
			if (phraseIdMap.containsKey(phrase.getId())) {
				return phraseIdMap.get(phrase.getId());
			} else {
				int id = phraseSeq.nextValue();
				phraseIdMap.put(phrase.getId(), id);
				return id;
			}
		}
	}

	private static class Sequence {

		private int value = -1;

		public int nextValue() {
			return ++value;
		}

	}

}
