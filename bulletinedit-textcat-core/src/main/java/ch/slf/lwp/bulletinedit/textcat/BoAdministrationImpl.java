package ch.slf.lwp.bulletinedit.textcat;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.dao.DaoPhraseOption;
import ch.slf.lwp.bulletinedit.textcat.dao.DaoSentence;
import ch.slf.lwp.bulletinedit.textcat.dao.DaoTextCat;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

import static ch.slf.lwp.bulletinedit.textcat.model.DomainName.INTEGRATION_TEST;

/**
 * Business logic for sentence catalog administration.
 * 
 * @author gw
 */
public class BoAdministrationImpl implements BoAdministration {

	private final Logger log = LogManager.getLogger(this.getClass());

	private DaoSentence daoS;
	private DaoPhraseOption daoO;
	private DaoTextCat daoTextCat;

	/**
	 * @param daoS
	 *            the sentence DAO to set
	 */
	public void setDaoS(DaoSentence daoS) {
		this.daoS = daoS;
	}

	/**
	 * @param daoO
	 *            the phrase option DAO to set
	 */
	public void setDaoO(DaoPhraseOption daoO) {
		this.daoO = daoO;
	}

	/**
	 * @param daoTextCat the daoTextCat to set
	 */
	public void setDaoTextCat(DaoTextCat daoTextCat) {
		this.daoTextCat = daoTextCat;
	}

	@Override
	@Deprecated
	public PhraseOption getMultiplePhraseOption(String phraseOptionName, Language language, Version baseVersion, DomainName domainName) throws TextCatException {
		PhraseOption o = daoO.getPhraseOption(phraseOptionName, language, baseVersion, domainName);
		return o;
	}

	@Override
	public void updateTextCat(Version previousBasisVersion, ModelTextCat... textCats) throws TextCatException {
		Version newBaseVersion = null;
		DomainName domainName = null;

		// some tests
		if (textCats.length != Language.values().length) {
			throw new TextCatException("Not enough TextCat languages ");
		}

		if (previousBasisVersion == null) {
			throw new TextCatException("Old base Version is missing");
		} else {

			for (ModelTextCat textCat : textCats) {

				if (previousBasisVersion.compareTo(textCat.getBasisVersion()) >= 0) {
					throw new TextCatException("Old base version is equal or heigher the new one.");
				}

				if (newBaseVersion == null) {
					newBaseVersion = textCat.getBasisVersion();

					if (newBaseVersion == null) {
						throw new TextCatException("Missing base version of new textcat");
					}

				} else {

					if (newBaseVersion.compareTo(textCat.getBasisVersion()) != 0) {
						throw new TextCatException("The base version of the new textcats are not equal");
					}
				}

				if (domainName == null) {
					domainName = textCat.getDomainName();

					if (domainName == null) {
						throw new TextCatException("Missing domain name");
					}

				} else {

					if (domainName.equals(textCat.getDomainName()) == false) {
						throw new TextCatException("The domain names of the new textcats are not equal");
					}
				}

			}
		}

		// update the textcat version
		
		log.info("start update textacts");

		try {
			// mark old version as delted
			daoTextCat.markVersionAsDeleted(previousBasisVersion, domainName);

			for (ModelTextCat textCat : textCats) {
				
				log.info("Start update: " + textCat.toString());

				// update phrases
				for (PhraseOption phraseOption : textCat.getPhraseOptions()) {
					daoO.insertPhraseOption(phraseOption);
				}

				// update sentences
				for (Sentence sentence : textCat.getSentences()) {
					daoS.insertSentence(sentence);
				}

			}
		} catch (Throwable e) {
			// undo work
			log.error("updat textcat failed, try undo changes", e);

			try {
				daoTextCat.deleteVersion(newBaseVersion, domainName);
				daoTextCat.undoDeletionMark(previousBasisVersion, domainName);
			} catch (Exception e1) {
				log.fatal("undo changes failed, try undo changes", e);
				throw new TextCatException("update textcat failed, undo changes failed. For details see nested exception", e1);
			}

			throw new TextCatException("update textcat failed, changes are undone. For details see nested exception", e);
		}

	}
	//we use always replace
	@Override
	public void replaceTextCat(Version previousBasisVersion, ModelTextCat... textCats) throws TextCatException {
		Version newBaseVersion = null;

		if (previousBasisVersion == null) {
			throw new TextCatException("Old base Version is missing");
		} else {

			for (ModelTextCat textCat : textCats) {

				if (previousBasisVersion.compareTo(textCat.getBasisVersion()) > 0) {
					throw new TextCatException("Old base version is heigher the new one.");
				}

				if (newBaseVersion == null) {
					newBaseVersion = textCat.getBasisVersion();

					if (newBaseVersion == null) {
						throw new TextCatException("Missing base version of new textcat");
					}

				} else {

					if (newBaseVersion.compareTo(textCat.getBasisVersion()) != 0) {
						throw new TextCatException("The base version of the new textcats are not equal");
					}
				}

			}
		}
		
		log.info("start replacing textacts");

		//Before import data delete all -10 domain
		deleteDomain(newBaseVersion,DomainName.INTEGRATION_TEST);

		//set domain to temp (-10)
		DomainName rightDomain=textCats[0].getDomainName();

		// replace the textcat version
		for (ModelTextCat textCat : textCats) {

		    textCat.setDomainName(INTEGRATION_TEST);

		    log.info("Start replacing: " + textCat.toString());
			
			Version oldLanguageVersion = null;

			// update phrases
			for (PhraseOption phraseOption : textCat.getPhraseOptions()) {
				phraseOption.setDomainName(INTEGRATION_TEST);

				Version h = replaceOrInsertPhraseOption(phraseOption, previousBasisVersion);

				if (h != null) {
					if (oldLanguageVersion == null) {
						oldLanguageVersion = h;
					} else {
						if (oldLanguageVersion.compareTo(h) != 0) {
							throw new TextCatException("Old language versions are not equal!");
						}
					}
				}
			}

			// update sentences
			for (Sentence sentence : textCat.getSentences()) {
				sentence.setDomainName(INTEGRATION_TEST);
				Version h = replaceOrInsertSentence(sentence, previousBasisVersion);

				if (h != null) {
					if (oldLanguageVersion == null) {
						oldLanguageVersion = h;
					} else {
						if (oldLanguageVersion.compareTo(h) != 0) {
							throw new TextCatException("Old language versions are not equal!");
						}
					}
				}
			}

			if (oldLanguageVersion != null) {

				if (oldLanguageVersion.equals(textCat.getVersion()) == false) {
					daoTextCat.deleteLanguageVersion(textCat.getLanguage(), oldLanguageVersion, textCat.getDomainName());
				} else {
					log.warn("Unable to delete possible removed sentences, because the language versions are equal");
				}

			}
		}
		//finishied import change from -10 to right domain
		int ID_Domain=0;
		switch (rightDomain) {
			case PRODUCTION:
				ID_Domain=1;
				break;
			case STAGING:
				ID_Domain=2;
				break;
			case INTEGRATION_TEST:
				ID_Domain=-10;
				break;
		}
		deleteDomain(newBaseVersion,rightDomain);
		daoTextCat.recodeDomain(-10,ID_Domain);
	}

	Version replaceOrInsertPhraseOption(PhraseOption option, Version previousBasisVersion) {
		// load old phrase option
		PhraseOption oldO = daoO.getPhraseOption(option.getName(), option.getLanguage(), previousBasisVersion, option.getDomainName());
		Version oldVersion = null;

		if (oldO != null) {
			oldVersion = oldO.getVersion();
			oldO.updateClone(option);
			daoO.updatePhraseOption(oldO);
		} else {
			daoO.insertPhraseOption(option);
		}

		return oldVersion;
	}

	Version replaceOrInsertSentence(Sentence sentence, Version previousBasisVersion) {
		Sentence oldS = daoS.getSentence(sentence.getName(), sentence.getLanguage(), previousBasisVersion, sentence.getDomainName());
		Version oldVersion = null;

		if (oldS != null) {
			oldVersion = oldS.getVersion();
			oldS.updateClone(sentence);
			daoS.updateSentence(oldS);
		} else {
			daoS.insertSentence(sentence);
		}

		return oldVersion;
	}

	public void deleteDomain(Version baseVersion, DomainName domainName) {
		daoO.deletePhraseOptions(baseVersion, domainName);
		daoS.deleteSentence(baseVersion, domainName);
	}

	public String recodeDomain(int oldDomainId, int newDomainId ){

        try {
            daoTextCat.recodeDomain(oldDomainId,newDomainId);
            return "OK";
        } catch (TextCatException e) {
            e.printStackTrace();
        }
        return "ERROR";
    }

}
