package ch.slf.lwp.bulletinedit.textcat.json;

/**
 * JSON item entry representing a sentence
 * 
 * @author weiss
 */
public class SentenceItem extends AbstractCatalogItem {

	private String name;
	private String header;
	private String options;
	private boolean joker;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the name
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * @param header
	 *            the name to set
	 */
	public void setHeader(String header) {
		this.header = header;
	}


	/**
	 * @return the options
	 */
	public String getOptions() {
		return options;
	}

	/**
	 * @param options
	 *            the options to set
	 */
	public void setOptions(String options) {
		this.options = options;
	}

	/**
	 * @return the joker
	 */
	public boolean isJoker() {
		return joker;
	}

	/**
	 * @param joker
	 *            the joker to set
	 */
	public void setJoker(boolean joker) {
		this.joker = joker;
	}

}
