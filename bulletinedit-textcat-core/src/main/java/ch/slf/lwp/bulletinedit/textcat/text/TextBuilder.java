package ch.slf.lwp.bulletinedit.textcat.text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.StringUtils;
import ch.slf.lwp.bulletinedit.textcat.TextCatException;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.applicationTexts.AppTextDefinition;
import ch.slf.lwp.bulletinedit.textcat.dao.DaoPhraseOption;
import ch.slf.lwp.bulletinedit.textcat.dao.DaoSentence;
import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructureModule;
import ch.slf.lwp.bulletinedit.textcat.model.util.SubPhraseOptionName;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;
import ch.slf.lwp.bulletinedit.textcat.text.SentenceConfiguration.ModuleConfiguration;
import ch.slf.lwp.bulletinedit.textcat.text.SentenceConfiguration.PhraseOptionConfiguration;
import ch.slf.lwp.bulletinedit.textcat.text.SentenceConfiguration.SubPhraseOptionConfiguration;

/**
 * @author gw
 */
public class TextBuilder {

    private DaoSentence daoSentence;
    private DaoPhraseOption daoPhraseOption;

    public TextBuilder(DaoSentence daoSentence, DaoPhraseOption daoPhraseOption) {
        this.daoSentence = daoSentence;
        this.daoPhraseOption = daoPhraseOption;
    }

    // build an static application text from a Application Text Definition
    public String build(AppTextDefinition appTextDefinition, Language targetLanguage, DomainName domainName) throws TextCatException {
        TextStringBuilder sb = new TextStringBuilder();
        for (AppTextDefinition.SentenceDefinition sentenceDefinition : appTextDefinition.getSentences()) {
            // create language independent sentence configuration
            SentenceConfiguration configuration = buildSentenceConfiguration(sentenceDefinition, appTextDefinition.getLanguage(), appTextDefinition.getVersion(), domainName);
            // get Sentence Fragements
            TextFragment[] fragments = getSentenceFragments(configuration, targetLanguage, domainName);
            // append to text string builder
            sb.appendSentence(fragments);
        }
        return sb.toString();
    }

    public String build(TextDefinition textDefinition, Language targetLanguage, DomainName domainName) throws TextCatException {

        TextStringBuilder sb = new TextStringBuilder();
        for (TextDefinition.Sentence sentenceDefinition : textDefinition.getSentences()) {
            // create language independent sentence configuration
            SentenceConfiguration configuration = buildSentenceConfiguration(sentenceDefinition, domainName);
            // get Sentence Fragements
            TextFragment[] fragments = getSentenceFragments(configuration, targetLanguage, domainName);
            // append to text string builder
            sb.appendSentence(fragments);
        }

        return sb.toString();
    }

    // build a SenecteConfiguration for a static Application Text
    private SentenceConfiguration buildSentenceConfiguration(AppTextDefinition.SentenceDefinition sentenceDefinition, Language language, Version version, DomainName domainName) throws TextCatException {

        HashMap<String, PhraseOption> phraseOptionCache = new HashMap<String, PhraseOption>();

        Sentence sentence = daoSentence.getSentence(sentenceDefinition.getSentenceName(), language, version, domainName);
        if (sentence == null) {
            throw new TextCatException("Invalid SentenceName for Application Text Definition :" + sentenceDefinition.getSentenceName() + " Domain:" + domainName);
        }
        SentenceConfiguration configuration = new SentenceConfiguration(sentence.getName(), sentence.getBasisVersion());
        Map<Integer, AppTextDefinition.ItemDefinition> moduleItemMap = sentenceDefinition.getModuleItemMap();
        for (Integer moduleNo : moduleItemMap.keySet()) {
            ModuleConfiguration mc = configuration.addModuleConfiguration(moduleNo, moduleItemMap.get(moduleNo).getItemNo());
            // Split submodule are not allowed for application texts, so
            // ItemPartNo is always 1
            PhraseOption module = sentence.getSentenceModule(moduleNo);
            int itemNo = moduleItemMap.get(moduleNo).getItemNo();
            Phrase phrase = module.getPhrase(itemNo, 1);
            if (phrase == null) {
                throw new TextCatException("Invalid Item :" + itemNo + " for phrase option: " + module.getName() + "(" + module.getId() + ")");
            }
            addSubPhraseOptionConfiguration(mc, phrase, moduleItemMap.get(moduleNo).getSubModuleItems(), language, domainName, phraseOptionCache);
        }
        return configuration;
    }

    private SentenceConfiguration buildSentenceConfiguration(TextDefinition.Sentence sentenceDefinition, DomainName domainName) throws TextCatException {

        HashMap<String, PhraseOption> phraseOptionCache = new HashMap<String, PhraseOption>();

        Sentence sentence = daoSentence.getSentence(sentenceDefinition.getSentenceId());
        Language language = sentence.getLanguage();

        SentenceConfiguration configuration = new SentenceConfiguration(sentence.getName(), sentence.getBasisVersion());

        if (sentenceDefinition.phraseSize() != sentence.getStructureSize()) {
            throw new TextCatException("Sentence definition dosn't match sentence structure: " + sentenceDefinition.toString() + " --- " + sentence.getStructure().toString());
        }

        Iterator<SentenceStructureModule> structureModuleIt = sentence.moduleIterator();

        for (TextDefinition.Phrase textDefinitionPhrase : sentenceDefinition.getPhrases()) {
            /*
             * both lists have the same size, therefore it.next() should always
             * work.
             */

            SentenceStructureModule structureModule = structureModuleIt.next();

            /*
             * The phrase id order match the structure element order, therefore
             * i know the module and item part number.
             */

            int itemNo = structureModule.getItemNo(textDefinitionPhrase.getPhraseId());

            ModuleConfiguration mc = configuration.addModuleConfiguration(structureModule.getModuleNo(), itemNo);

            // handle sub options
            Phrase phrase = structureModule.getPhrase(itemNo);
            if (phrase != null)
                addSubPhraseOptionConfiguration(mc, phrase, textDefinitionPhrase, language, domainName, phraseOptionCache);
        }

        return configuration;
    }

    /**
     * Recursive method call
     *
     * @param poc                  the current phrase option configuration
     * @param phrase               the current phrase
     * @param textDefinitionPhrase the current text definition phrase
     * @param language             the language
     * @param domainName           the domain name
     * @param phraseOptionCache    the phrase option cache to avoid db access
     * @throws TextCatException
     */
    private void addSubPhraseOptionConfiguration(PhraseOptionConfiguration poc, Phrase phrase, TextDefinition.Phrase textDefinitionPhrase, Language language, DomainName domainName, HashMap<String, PhraseOption> phraseOptionCache) throws TextCatException {

        List<SubPhraseOptionName> subOptionNames = StringUtils.getSubPhraseOptionNames(phrase.getValue());

        if (textDefinitionPhrase.subPhraseSize() != subOptionNames.size()) {
            throw new TextCatException("Phrase definition dosn't match sub phrase size: " + textDefinitionPhrase.toString() + " --- " + subOptionNames.size());
        }

        Iterator<SubPhraseOptionName> subPhraseOptionNameIt = subOptionNames.iterator();

        for (TextDefinition.Phrase textDefinitionSubPhrase : textDefinitionPhrase.getSubPhrases()) {

            // like the phrase order, the sub phrase order is also equals
            SubPhraseOptionName subPhraseOptionName = subPhraseOptionNameIt.next();
            PhraseOption phraseOption = phraseOptionCache.get(subPhraseOptionName.getPhraseOptionName());

            if (phraseOption == null) {
                phraseOption = daoPhraseOption.getPhraseOption(subPhraseOptionName.getPhraseOptionName(), language, poc.getBasisVersion(), domainName);
                phraseOptionCache.put(subPhraseOptionName.getPhraseOptionName(), phraseOption);
            }

            int subItemNo = phraseOption.getItemNo(textDefinitionSubPhrase.getPhraseId());
            PhraseOptionConfiguration subPoc = poc.addSubPhraseOptionConfiguration(subPhraseOptionName, subItemNo);

            // Sub Sub handling
            Phrase subPhrase = phraseOption.getPhrase(subItemNo, subPhraseOptionName.getItemPartNo());
            addSubPhraseOptionConfiguration(subPoc, subPhrase, textDefinitionSubPhrase, language, domainName, phraseOptionCache);
        }
    }

    // Build Subphrase Configuration for Application Texts
    // Sub-Submodules are not implemented for Application Texts
    private void addSubPhraseOptionConfiguration(PhraseOptionConfiguration poc, Phrase phrase, List<Integer> itemNoList, Language language, DomainName domainName, HashMap<String, PhraseOption> phraseOptionCache) throws TextCatException {
        List<SubPhraseOptionName> subOptionNames = StringUtils.getSubPhraseOptionNames(phrase.getValue());

        if (itemNoList.size() != subOptionNames.size()) {
            throw new TextCatException("Phrase definition dosn't match sub phrase size: " + itemNoList.size() + " --- " + subOptionNames.size());
        }

        Iterator<SubPhraseOptionName> subPhraseOptionNameIt = subOptionNames.iterator();

        for (Integer subItemNo : itemNoList) {

            // like the phrase order, the sub phrase order is also equals
            SubPhraseOptionName subPhraseOptionName = subPhraseOptionNameIt.next();
            PhraseOption phraseOption = phraseOptionCache.get(subPhraseOptionName.getPhraseOptionName());

            if (phraseOption == null) {
                phraseOption = daoPhraseOption.getPhraseOption(subPhraseOptionName.getPhraseOptionName(), language, poc.getBasisVersion(), domainName);
                phraseOptionCache.put(subPhraseOptionName.getPhraseOptionName(), phraseOption);
            }
            poc.addSubPhraseOptionConfiguration(subPhraseOptionName, subItemNo);
        }
    }

    /**
     * Returns the required text fragments of the requested language
     *
     * @param configuration
     * @param targetLanguage
     * @param domainName
     * @return
     * @throws TextCatException
     */
    private TextFragment[] getSentenceFragments(SentenceConfiguration configuration, Language targetLanguage, DomainName domainName) throws TextCatException {

        Sentence sentence = daoSentence.getSentence(configuration.getSentenceName(), targetLanguage, configuration.getBasisVersion(), domainName);

        if (sentence == null) {
            throw new TextCatException("Sentence not found." + "\n\tSentence name: " + configuration.getSentenceName() + "\n\tLanguage: " + targetLanguage.toString() + "\n\tBasisVersion: " + configuration.getBasisVersion().toString() + "\n\tDomain: " + domainName.toString());
        }

        ArrayList<TextFragment> textFragments = new ArrayList<TextFragment>(sentence.getStructureSize());
        HashMap<String, PhraseOption> subPhraseOptionCache = new HashMap<String, PhraseOption>();

        //Clesius strucuture not modules!!
        //List listN = new ArrayList();
        Iterator<SentenceStructureModule> structureModuleIt = sentence.moduleIterator();
        while (structureModuleIt.hasNext()) {

            SentenceStructureModule structureModule = structureModuleIt.next();

            ModuleConfiguration moduleConfiguration = configuration.getModuleConfiguration(structureModule.getModuleNo());
            Phrase phrase = structureModule.getPhrase(moduleConfiguration.getItemNo());
            textFragments.addAll(phrase2Textfragments(phrase, moduleConfiguration, targetLanguage, domainName, subPhraseOptionCache));

        }

        return   textFragments.toArray(new TextFragment[0]);
    }

    /**
     * Group 1: space control signs before <br/>
     * Group 2: phrase option name <br/>
     * Group 3: pos number <br/>
     * Group 4: space control signs after <br/>
     */
    private final static Pattern SUB_PHRASE_OPTION_CONTROL_PATTERN = Pattern.compile("(?:\\((-{0,2})\\))?\\s*\\{(.+?)(?:#(.+?))?\\}\\s*(?:\\((-?)\\))?");

    List<TextFragmentSimple> phrase2Textfragments(Phrase phrase, PhraseOptionConfiguration phraseOptionConfiguration, Language targetLanguage, DomainName domainName, HashMap<String, PhraseOption> subPhraseOptionCache) throws TextCatException {
        List<TextFragmentSimple> fragments = new ArrayList<TextFragmentSimple>();
//CLESIUS
      /*  if (phrase != null) {*/

            String phraseValue = phrase.getValue();

            int appendPosition = 0;
            int matchStart = 0;
            boolean lastSpaceAfter = true;
            Matcher m = SUB_PHRASE_OPTION_CONTROL_PATTERN.matcher(phraseValue);

            while (m.find()) {

                // build sub phrase option name
                SubPhraseOptionName spon = new SubPhraseOptionName();
                spon.setPhraseOptionNameWithItemPartNo(m.group(2));

                if (m.group(3) != null) {
                    spon.setPosNo(Integer.parseInt(m.group(3)));
                }

                String beforeFlags = m.group(1);
                String afterFlags = m.group(4);

                // flags for the phrase option
                // space before is also relevant as space after for the text value
                // before.
                // space after is also relevant as space before for the next
                // fragment
                boolean spaceBefore = !(beforeFlags != null && beforeFlags.length() >= 1);
                boolean spaceAfter = !(afterFlags != null && afterFlags.length() == 1);
                boolean removePunctuationBefore = beforeFlags != null && beforeFlags.length() == 2;

                matchStart = m.start();

                if (appendPosition != matchStart) {
                    // there is text before the sub phrase option
                    String s = phraseValue.substring(appendPosition, matchStart).trim();

                    if (s.isEmpty() == false) {
                        fragments.add(new TextFragmentSimple(s).setSpaceBefore(lastSpaceAfter).setSpaceAfter(spaceBefore));
                    }
                } else {
                    // there is no text before, therefore the last space after will influence the pulldowns space before.
                    spaceBefore = lastSpaceAfter && spaceBefore;
                }

                appendPosition = m.end();

                // handle sub phrase option
                SubPhraseOptionConfiguration subPhraseOptionConfig = phraseOptionConfiguration.getSubPhraseOptionConfiguration(spon);

                if (subPhraseOptionConfig != null) {
                    PhraseOption subPhraseOption = subPhraseOptionCache.get(subPhraseOptionConfig.getPhraseOptionName());

                    if (subPhraseOption == null) {
                        subPhraseOption = daoPhraseOption.getPhraseOption(subPhraseOptionConfig.getPhraseOptionName(), targetLanguage, subPhraseOptionConfig.getBasisVersion(), domainName);
                        subPhraseOptionCache.put(subPhraseOptionConfig.getPhraseOptionName(), subPhraseOption);
                    }

                    Phrase subPhrase = subPhraseOption.getPhrase(subPhraseOptionConfig.getItemNo(), spon.getItemPartNo());
                    // recursive call
                    List<TextFragmentSimple> subFragments = phrase2Textfragments(subPhrase, subPhraseOptionConfig, targetLanguage, domainName, subPhraseOptionCache);

                    // set the overriding control flags to the first and last
                    // fragment.
                    if (subFragments.isEmpty() == false) {
                        subFragments.get(0).setRemovePunctuationBefore(removePunctuationBefore).setSpaceBefore(spaceBefore);
                        subFragments.get(subFragments.size() - 1).setSpaceAfter(spaceAfter);
                        fragments.addAll(subFragments);
                    }
                } else {
                    throw new TextCatException("Missing sub phrase configuration for sub phrase option '" + spon + "' in phrase '" + phrase.getValue() + "' (id: " + phrase.getId() + ")");
                }

                lastSpaceAfter = spaceAfter;
            }

            // handle last text part if exists
            if (appendPosition < phraseValue.length()) {
                fragments.add(new TextFragmentSimple(phraseValue.substring(appendPosition)).setSpaceBefore(lastSpaceAfter));
            }

            // set the before control flags of the origin phrase to the first
            // element and the last control flags to the last one.
            if (fragments.isEmpty() == false) {
                fragments.get(0).setRemovePunctuationBefore(phrase.isRemovePunctuationBefore()).setSpaceBefore(phrase.isSpaceBefore());
                fragments.get(fragments.size() - 1).setSpaceAfter(phrase.isSpaceAfter());
            }
        //}
        return fragments;
    }

}
