package ch.slf.lwp.bulletinedit.textcat.json;

/**
 * Superclass for all id depended JSON item entries.
 * 
 * @author gw
 */
public abstract class AbstractCatalogItem implements Item {

	private int id;
	private int dbid;
	private String rgn;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the dbid
	 */
	public int getDbid() {
		return dbid;
	}

	/**
	 * @param dbid
	 *            the dbid to set
	 */
	public void setDbid(int dbid) {
		this.dbid = dbid;
	}

	/**
	 * @return the rgn
	 */
	public String getRgn() {
		return rgn;
	}

	/**
	 * @param rgn
	 *
	 */
	public void setRgn(String  rgn) {
		this.rgn = rgn;
	}

}
