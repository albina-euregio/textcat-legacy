package ch.slf.lwp.bulletinedit.textcat.model.util;

import java.util.Iterator;

import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructure.StructureElement;

public class SentenceStructureModule {
	
	private PhraseOption phraseOption;
	private SentenceStructure.StructureElement structureElement;
	
	public SentenceStructureModule(PhraseOption phraseOption, StructureElement structureElement) {
		super();
		this.phraseOption = phraseOption;
		this.structureElement = structureElement;
	}

	public int getModuleNo() {
		return structureElement.getModuleNo();
	}
	
	public int getItemPartNo() {
		return structureElement.getItemPartNo();
	}
	
	public int getId() {
		return phraseOption.getId();
	}
	
	public int getItemNo(int phraseId) {
		return phraseOption.getItemNo(phraseId);
	}
	
	public String getPhraseOptionHeader() {
		return phraseOption.getHeader();
	}

	public String getPhraseOptionNewHeader() {
		return phraseOption.getHeader();
	}

	public Phrase getPhrase(int itemNo) {
			return phraseOption.getPhrase(itemNo, getItemPartNo());
	}
	
	public Iterator<Phrase> phraseIterator() {
		return phraseOption.phraseIterator(getItemPartNo());
	}

}
