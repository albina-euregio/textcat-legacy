package ch.slf.lwp.bulletinedit.textcat.model;

/**
 * Available domain names
 * 
 * @author weiss
 */
public enum DomainName {
	//	DESCRIPTION_OF_DANGER, BULLETINEDIT, INTEGRATION_TEST
	PRODUCTION, STAGING, INTEGRATION_TEST
}
