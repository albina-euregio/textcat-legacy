package ch.slf.lwp.bulletinedit.textcat.text;

import java.text.ParseException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;

/**
 * Class for parsing the language dependent text definition.<br/>
 * 
 * <p style="font-weight: bolder;">Implementatio details:</p>
 * <p>
 * The TextDefinitionParser is a state machine. The following image describes his states and transitions:<br/>
 * <img src="doc-files/ParserStatemachine.png" style="width:600px;"/><br/>
 * <dl>
 * <dt>Start state</dt><dd><span style="font-style:italic;">Sentence Start</span></dd>
 * <dt>Final state</dt><dd><span style="font-style:italic;">Sentence End</span></dd>
 * </dl>
 * </p>
 * <p style="font-weight: bolder;">Important transitions</p>
 * <dl>
 * <dt>Current state: <span style="font-style:italic;">Sentence Id</span> &rArr; Read: <span style="font-style:italic;">[</span></dt>
 * <dd>
 * <ul>
 * <li>Instantiate a new {@link ch.slf.lwp.bulletinedit.textcat.text.TextDefinition.Sentence} object with the currently readed id and add it to the <span style="font-style:italic;">currentComponent</span> stack. Also add it to the <span style="font-style:italic;">TextDefinition</span></li> 
 * <li>Add a <span style="font-style:italic;">SentenceEnd</span> state to the <span style="font-style:italic;">phraseListEndNextState</span> stack.</li>
 * </ul>
 * </dd>
 * 
 * <dt>Current state: <span style="font-style:italic;">Phrase Id</span> &rArr; Read: <span style="font-style:italic;">[</span></dt>
 * <dd>
 * <ul>
 * <li>
 * <ol>
 * <li>Instantiate a new {@link ch.slf.lwp.bulletinedit.textcat.text.TextDefinition.Phrase} object with the currently readed Id and add it to the top component of the <span style="font-style:italic;">currentComponent</span> stack.</li>
 * <li>Add the create object to the <span style="font-style:italic;">currentComponent</span> stack.</li>
 * </ol>
 * </li>
 * <li>Add a <span style="font-style:italic;">SentenceEnd</span> state to the <span style="font-style:italic;">phraseListEndNextState</span> stack.</li>
 * </ul>
 * </dd>
 * 
 * <dt>Current state: <span style="font-style:italic;">Phrase Id</span> &rArr; Read: <span style="font-style:italic;">,</span></dt>
 * <dd>Instantiate a new {@link ch.slf.lwp.bulletinedit.textcat.text.TextDefinition.Phrase} object with the currently readed Id and add it to the top component of the <span style="font-style:italic;">currentComponent</span> stack.</dd>
 * 
 * <dt>Current state: <span style="font-style:italic;">Phrase Id</span> &rArr; Read: <span style="font-style:italic;">]</span></dt>
 * <dd>
 * <ul>
 * <li>Instantiate a new {@link ch.slf.lwp.bulletinedit.textcat.text.TextDefinition.Phrase} object with the currently readed Id and add it to the top component of the <span style="font-style:italic;">currentComponent</span> stack.</li> 
 * <li>Remove the top state of the <span style="font-style:italic;">phraseListEndNextState</span> stack and use it as new next state</li>
 * </ul>
 * </dd>
 * </dl>
 * @author gw
 */
public class TextDefinitionParser {

	private enum StateName {
		SENTENCE_START, SENTENCE_ID, PHRASE_LIST_START, PHRASE_ID, PHRASE_LIST_END, SENTENCE_END
	}

	private HashMap<StateName, State> states;

	public TextDefinitionParser() {
		states = new HashMap<TextDefinitionParser.StateName, TextDefinitionParser.State>();
		states.put(StateName.SENTENCE_START, new SentenceStart());
		states.put(StateName.SENTENCE_ID, new SentenceId());
		states.put(StateName.PHRASE_LIST_START, new PhraseListStart());
		states.put(StateName.PHRASE_ID, new PhraseId());
		states.put(StateName.PHRASE_LIST_END, new PhraseListEnd());
		states.put(StateName.SENTENCE_END, new SentenceEnd());
	}
	
	/**
	 * Parse the id based text definition.<br/>
	 * Creates a new TextDefinitionParser and parse the given string representation of the text definition.
	 * 
	 * @param textDefinition the definition string to parse.
	 * @return the text definition
	 * @throws ParseException if the sting can not be parsed
	 */
	public static TextDefinition parseDefinition(String textDefinition) throws ParseException {
		TextDefinitionParser parser = new TextDefinitionParser();
		return parser.parse(textDefinition);
	}

	/**
	 * Parse the id based text definition.
	 * 
	 * @param textDefinition
	 *            the definition string to parse.
	 * @return the text definition
	 * @throws ParseException
	 *             if the sting can not be parsed
	 */
	public TextDefinition parse(String textDefinition) throws ParseException {

		Machine m = new Machine();
		m.text = new TextDefinition();
		m.memory = new StringBuilder();
		m.currentState = states.get(StateName.SENTENCE_START);

		int index = 0;
		for (; index < textDefinition.length(); index++) {

			char ch = textDefinition.charAt(index);

			if (m.currentState.handle(ch, m) == false) {
				throw new ParseException(m.currentState.getErrorMsg(), index);
			}
		}

		if (m.currentState.isfinal() == false) {
			throw new ParseException("Text definition ends unexpected.", index);
		}

		return m.text;

	}

	/**
	 * Everything the states must know or access.
	 * 
	 * This class exists to avoid definition specific instance variables.
	 */
	private static class Machine {
		State currentState;
		TextDefinition text;
		Deque<TextDefinition.Component> currentComponent = new ArrayDeque<TextDefinition.Component>();
		Deque<State> phraseListEndNextState =  new ArrayDeque<State>();
		StringBuilder memory;
	}

	/**
	 * Abstract parser state.
	 */
	private static abstract class State {

		private String error;

		/**
		 * Handles the read character. Is responsible to change the machine
		 * state and creates the output.
		 * 
		 * @param ch
		 *            the read character
		 * @param m
		 *            the machine
		 * @return <code>true</code> if the read character is accepted,
		 *         <code>false</code> otherwise.
		 */
		abstract boolean handle(char ch, Machine m);

		/**
		 * Creates the error message.
		 * 
		 * @param expected
		 *            list of expected characters.
		 * @param current
		 *            the current handled character.
		 */
		void createErrorMsg(String expected, char current) {
			this.error = "Expected one of " + expected + ", but found '" + current + "'";
		}

		/**
		 * The error message if the character is not accepted.
		 * 
		 * @return a error message
		 */
		String getErrorMsg() {
			return error;
		}

		/**
		 * Returns <code>true</code> if it is a final state, <code>false</code>
		 * otherwise.
		 * 
		 * @return <code>true</code> if it is a final state, <code>false</code>
		 *         otherwise.
		 */
		boolean isfinal() {
			return false;
		};
	}

	/**
	 * Start State of the parser machine as well as the start state for a
	 * sentence.
	 */
	private class SentenceStart extends State {

		@Override
		boolean handle(char ch, Machine m) {

			if (Character.isDigit(ch)) {
				m.memory.append(ch);
				m.currentState = states.get(StateName.SENTENCE_ID);
			} else {
				createErrorMsg("< 0123456789 >", ch);
				return false;
			}

			return true;
		}

	}

	/**
	 * State to handle the sentence id
	 */
	private class SentenceId extends State {

		@Override
		boolean handle(char ch, Machine m) {

			if (Character.isDigit(ch)) {
				m.memory.append(ch);
			} else if (ch == TextDefinition.PHRASE_LIST_START) {
				TextDefinition.Sentence s = m.text.new Sentence();
				s.setSentenceId(Integer.parseInt(m.memory.toString()));
				m.memory.setLength(0);
				m.text.addSentence(s);
				m.currentComponent.push(s);
				m.phraseListEndNextState.push(states.get(StateName.SENTENCE_END));

				m.currentState = states.get(StateName.PHRASE_LIST_START);
			} else {
				createErrorMsg("< 0123456789" + TextDefinition.PHRASE_LIST_START + " >", ch);
				return false;
			}

			return true;
		}

	}

	/**
	 * Start state of the phrase list
	 */
	private class PhraseListStart extends State {

		@Override
		boolean handle(char ch, Machine m) {

			if (Character.isDigit(ch)) {
				m.memory.append(ch);
				m.currentState = states.get(StateName.PHRASE_ID);
			} else {
				createErrorMsg("< 0123456789 >", ch);
				return false;
			}

			return true;
		}

	}

	/**
	 * Handles the phrase id
	 */
	private class PhraseId extends State {

		@Override
		boolean handle(char ch, Machine m) {

			if (Character.isDigit(ch)) {
				m.memory.append(ch);
			} else if (ch == TextDefinition.PHRASE_SEPERATOR) {
				handlePhraseId(m);
			} else if (ch == TextDefinition.PHRASE_LIST_START) {
				m.currentComponent.push(handlePhraseId(m));
				m.phraseListEndNextState.push(states.get(StateName.PHRASE_LIST_END));
				m.currentState = states.get(StateName.PHRASE_LIST_START);
			} else if (ch == TextDefinition.PHRASE_LIST_END) {
				handlePhraseId(m);
				m.currentState = m.phraseListEndNextState.pop();
			} else {
				createErrorMsg("< 0123456789" + TextDefinition.PHRASE_LIST_START + TextDefinition.PHRASE_SEPERATOR + TextDefinition.PHRASE_LIST_END + " >", ch);
				return false;
			}

			return true;
		}

		private TextDefinition.Phrase handlePhraseId(Machine m) {
			TextDefinition.Phrase p = m.text.new Phrase();
			p.setPhraseId(Integer.parseInt(m.memory.toString()));
			m.memory.setLength(0);
			m.currentComponent.peek().addPhrase(p);
			return p;
		}

	}

	/**
	 * Sub phrase finish state
	 */
	private class PhraseListEnd extends State {

		@Override
		boolean handle(char ch, Machine m) {
			m.currentComponent.pop();
			switch (ch) {
			case TextDefinition.PHRASE_SEPERATOR:
				m.currentState = states.get(StateName.PHRASE_ID);
				break;
			case TextDefinition.PHRASE_LIST_END:
				m.currentState = m.phraseListEndNextState.pop();
				break;

			default:
				createErrorMsg("< " + TextDefinition.PHRASE_SEPERATOR + TextDefinition.PHRASE_LIST_END + " >", ch);
				return false;
			}

			return true;
		}

	}

	/**
	 * Sentence end state
	 */
	private class SentenceEnd extends State {

		@Override
		boolean handle(char ch, Machine m) {
			m.currentComponent.pop();

			if (ch == TextDefinition.SENTENCE_SEPERATOR) {
				m.currentState = states.get(StateName.SENTENCE_START);
			} else {
				createErrorMsg("< " + TextDefinition.SENTENCE_SEPERATOR + " >", ch);
				return false;
			}

			return true;
		}

		@Override
		boolean isfinal() {
			return true;
		};

	}

}
