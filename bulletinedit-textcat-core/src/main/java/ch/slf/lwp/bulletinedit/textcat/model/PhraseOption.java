package ch.slf.lwp.bulletinedit.textcat.model;

import java.util.Iterator;

import ch.slf.lwp.bulletinedit.textcat.Constants;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

public interface PhraseOption {

	/**
	 * @return the id
	 */
	public abstract int getId();

	/**
	 * @return the name
	 */
	public abstract String getName();

	/**
	 * @param name
	 *            the name to set
	 */
	public abstract void setName(String name);

	/**
	 * @return the language
	 */
	public abstract Language getLanguage();

	/**
	 * @param language
	 *            the language to set
	 */
	public abstract void setLanguage(Language language);

	/**
	 * @return the version
	 */
	public abstract Version getVersion();

	/**
	 * @param version
	 *            the version to set
	 */
	public abstract void setVersion(Version version);

	/**
	 * @return the basisVersion
	 */
	public abstract Version getBasisVersion();

	/**
	 * @param basisVersion
	 *            the basisVersion to set
	 */
	public abstract void setBasisVersion(Version basisVersion);

	/**
	 * @return the header
	 */
	public abstract String getHeader();

	/**
	 * @param header
	 *            the header to set
	 */
	public abstract void setHeader(String header);

	/**
	 * @return the remark
	 */
	public abstract String getRemark();

	/**
	 * @param remark
	 *            the remark to set
	 */
	public abstract void setRemark(String remark);

	/**
	 * @return the deleted
	 */
	public abstract boolean isDeleted();

	/**
	 * @param deleted
	 *            the deleted to set
	 */
	public abstract void setDeleted(boolean deleted);

	/**
	 * @return the domainName
	 */
	public abstract DomainName getDomainName();


	/**
	 * @param domainName
	 *  the domainName to set
	 */
	public abstract void setDomainName(DomainName domainName);



	/**
	 * Returns the itemNo according to the phrase id or
	 * {@link Constants#DOES_NOT_CONTAINS} if this phrase option does not
	 * contain the phrase.
	 * 
	 * @param phraseId
	 * @return the itemNo
	 */
	public abstract int getItemNo(int phraseId);

	/**
	 * Returns the phrase
	 * 
	 * @param itemNo
	 * @param itemPartNo
	 * @return the phrase
	 */
	public abstract Phrase getPhrase(int itemNo, int itemPartNo);

	/**
	 * Returns an iterator over the phrases inside the requested part in order
	 * of the item sort order.
	 * 
	 * @return an iterator over the phrase
	 */
	public abstract Iterator<Phrase> phraseIterator(int itemPartNo);

	/**
	 * Returns an iterator over the phrase option items in order of their item
	 * number.
	 * 
	 * @return an iterator over the phrase option items
	 */
	public abstract Iterator<PhraseOptionItem> phraseOptionItemIterator();

	/**
	 * Create and add a new empty PhraseOptionItem
	 * 
	 * @return the create phrase option item
	 */
	public abstract PhraseOptionItem createPhraseOptionItem();

	/**
	 * Append the item.
	 * 
	 * @param item the item to append
	 */
	public abstract void appendPhraseOptionItem(PhraseOptionItem item);

	/**
	 * remove all empty phrase option items from the end.
	 * The item order is based on the item number.
	 */
	public abstract void trim();

	/**
	 * Returns the number of items in this option.
	 * 
	 * @return the number of items in this option.
	 */
	public abstract int itemSize();

	/**
	 * Update this options with the values of the specified option and makes the
	 * specified option as clone of this option.
	 * 
	 * @param option
	 */
	public abstract void updateClone(PhraseOption option);

}