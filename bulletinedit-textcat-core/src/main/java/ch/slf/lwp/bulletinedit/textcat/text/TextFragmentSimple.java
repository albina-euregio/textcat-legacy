package ch.slf.lwp.bulletinedit.textcat.text;

/**
 * @author gw
 */
public class TextFragmentSimple implements TextFragment {

	private String text;
	private boolean removePunctuationBefore = false;
	private boolean spaceAfter = true;
	private boolean spaceBefore = true;

	public TextFragmentSimple(String text) {
		super();
		this.text = text;
	}

	public TextFragmentSimple setRemovePunctuationBefore(boolean removePunctuationBefore) {
		if (removePunctuationBefore) {
			this.removePunctuationBefore = removePunctuationBefore;
		}

		return this;
	}

	public TextFragmentSimple setSpaceAfter(boolean spaceAfter) {
		if (!spaceAfter) {
			this.spaceAfter = spaceAfter;
		}

		return this;
	}

	public TextFragmentSimple setSpaceBefore(boolean spaceBefore) {
		if (!spaceBefore) {
			this.spaceBefore = spaceBefore;
		}

		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.text.TextFragment#getText()
	 */
	@Override
	public String getText() {
		return this.text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.text.TextFragment#isRemovePunctuationBefore
	 * ()
	 */
	@Override
	public boolean isRemovePunctuationBefore() {
		return this.removePunctuationBefore;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.text.TextFragment#isSpaceAfter()
	 */
	@Override
	public boolean isSpaceAfter() {
		return this.spaceAfter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.text.TextFragment#isSpaceBefore()
	 */
	@Override
	public boolean isSpaceBefore() {
		return this.spaceBefore;
	}

}
