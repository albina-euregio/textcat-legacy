package ch.slf.lwp.bulletinedit.textcat.text;

/**
 * 
 * @author gw
 */
public class TextStringBuilder {

	private static final String PUNCTUATION_MARKS = ".!?:,;";
	private static final String TO_UPPER_CASE = ".!?:>";

	private StringBuilder text = new StringBuilder();
	private TextFragment last = null;

	/**
	 * Append the fragments of one sentence to the existing text.
	 * 
	 * @param fragments
	 *            the text fragment to append
	 */
	public void appendSentence(TextFragment... fragments) {

		boolean toUpperCaseCheck = true;

		for (TextFragment f : fragments) {
			
			// always the first none empty fragment must be checked
			// for upper case!

			if (f.getText().isEmpty() == false) {
				appendFragment(f, toUpperCaseCheck );
				toUpperCaseCheck = false;
			}
		}
	}

	/**
	 * Append the text fragment to the string builder.
	 * 
	 * @param f
	 *            a not empty text fragment
	 * @param toUpperCaseCheck
	 *            indicate that the first letter of the text fragment should
	 *            maybe turned to upper case.
	 */
	private void appendFragment(TextFragment f, boolean toUpperCaseCheck) {

		String value = f.getText();

		if (toUpperCaseCheck) {

			if (last == null || TO_UPPER_CASE.indexOf(text.charAt(text.length() - 1)) >= 0) {
				// first text element or the last element ends with a
				// punctuation mark which force a big letter.

				// because e is not empty value.length is at least 1
				String first = value.substring(0, 1);
				String tail = value.substring(1);
				value = first.toUpperCase() + tail;
			}

		}

		if (last != null) {

			if (last.isSpaceAfter() && f.isSpaceBefore()) {
				// add white space
				text.append(" ");
			} else {

				// if e.isRemovePunctuationBefore == true then e.isSpaceBefore
				// is always false!

				if (f.isRemovePunctuationBefore()) {
					// remove last punctuation mark
					char lastChar = text.charAt(text.length() - 1);

					if (PUNCTUATION_MARKS.indexOf(lastChar) >= 0) {
						text.setLength(text.length() - 1);
					}
				}
			}

		}

		// append text value
		text.append(value);
		last = f;
	}

	@Override
	public String toString() {
		return text.toString();
	}

}
