package ch.slf.lwp.bulletinedit.textcat.model.util;

import ch.slf.lwp.bulletinedit.textcat.Constants;

/**
 * This is a sub phrase option name. The name is unique inside a phrase option
 * item. The name is build with the phrase option name extended by a pos number,
 * separated by a special sign.
 * 
 * @author gw
 */
public class SubPhraseOptionName {

	public static final int SUB_PHRASEOPTION_FIRST_OCCURRENCE_POS_NO = 1;
	public static final String SUB_PHRASEOPTION_POS_NO_SEP = "#";
	
	public static final String SUP_PHRASEOPTION_SECOND_ITEMPARTNO_SUFFIX = "_NO";

	/**
	 * The phrase option number
	 */
	private String phraseOptionName;
	private String phraseOptionName_IT;

	/**
	 * The position number inside a phrase value
	 */
	private int posNo = SUB_PHRASEOPTION_FIRST_OCCURRENCE_POS_NO;
	
	private int itemPartNo = Constants.FIRST_ITEM_PART_NO;

	/**
	 * @return the phraseOptionName
	 */
	public String getPhraseOptionName() {
		return phraseOptionName;
	}

	/**
	 * @return the phraseOptionName_IT
	 */
	public String getPhraseOptionName_IT() {
		return phraseOptionName_IT;
	}

	/**
	 * @param phraseOptionName
	 *            the phraseOptionName to set
	 */
	public SubPhraseOptionName setPhraseOptionName(String phraseOptionName) {
		this.phraseOptionName = phraseOptionName;
		return this;
	}

	/**
	 * @param phraseOptionName_IT
	 *            the phraseOptionName to set
	 */
	public SubPhraseOptionName setPhraseOptionName_IT(String phraseOptionName_IT) {
		this.phraseOptionName_IT = phraseOptionName_IT;
		return this;
	}


	/**
	 * @return the posNo
	 */
	public int getPosNo() {
		return posNo;
	}

	/**
	 * @param posNo
	 *            the posNo to set
	 */
	public SubPhraseOptionName setPosNo(int posNo) {
		this.posNo = posNo;
		return this;
	}

	@Override
	public int hashCode() {
		return 31 * (phraseOptionName != null ? phraseOptionName.hashCode() : 0) + (23* posNo) + itemPartNo;
	}

	@Override
	public boolean equals(Object o) {

		if (o instanceof SubPhraseOptionName == false) {
			return false;
		}

		SubPhraseOptionName spoid = (SubPhraseOptionName) o;

		if (phraseOptionName == null) {
			return spoid.phraseOptionName == null && posNo == spoid.posNo && itemPartNo == spoid.itemPartNo;
		} else {
			return phraseOptionName.equals(spoid.phraseOptionName) && posNo == spoid.posNo && itemPartNo == spoid.itemPartNo;
		}

	}

	@Override
	public String toString() {
		return phraseOptionName  + (this.itemPartNo != Constants.FIRST_ITEM_PART_NO ? SUP_PHRASEOPTION_SECOND_ITEMPARTNO_SUFFIX : "" ) + SUB_PHRASEOPTION_POS_NO_SEP + Integer.toString(posNo);
	}

	public int getItemPartNo() {
		return this.itemPartNo;
	}
	
	public SubPhraseOptionName setItemPartNo(int itemPartNo) {
		this.itemPartNo = itemPartNo;
		return this;
	}
	
	/**
	 * @param phraseOptionName
	 *            the phraseOptionName to set
	 */
	public SubPhraseOptionName setPhraseOptionNameWithItemPartNo(String phraseOptionName) {
		
		if(phraseOptionName != null && phraseOptionName.endsWith(SUP_PHRASEOPTION_SECOND_ITEMPARTNO_SUFFIX)) {
			this.itemPartNo = Constants.FIRST_ITEM_PART_NO + 1;
			int endIndex = phraseOptionName.length() - SUP_PHRASEOPTION_SECOND_ITEMPARTNO_SUFFIX.length();
			this.phraseOptionName = phraseOptionName.substring(0,endIndex);
		} else {
			this.phraseOptionName = phraseOptionName;
		}
		
		return this;
	}

}
