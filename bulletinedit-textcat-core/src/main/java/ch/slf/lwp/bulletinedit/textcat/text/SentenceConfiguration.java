package ch.slf.lwp.bulletinedit.textcat.text;

import java.util.HashMap;
import java.util.Map;

import ch.slf.lwp.bulletinedit.textcat.Constants;
import ch.slf.lwp.bulletinedit.textcat.model.util.SubPhraseOptionName;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

/**
 * A language independent sentence configuration. The configuration describes
 * which option item is selected of the sentence module, as well as the selected
 * option items of the possible sub phrase options
 * 
 * @author weiss
 */
public class SentenceConfiguration {

	private String sentenceName;
	private Version basisVersion;
	/*
	 * Hold the module configurations, the access key is the module number
	 */
	private HashMap<Integer, ModuleConfiguration> modulesConf = new HashMap<Integer, ModuleConfiguration>();

	public SentenceConfiguration(String sentenceName, Version basisVersion) {
		this.sentenceName = sentenceName;
		this.basisVersion = basisVersion;
	}

	/**
	 * @return the sentenceName
	 */
	public String getSentenceName() {
		return sentenceName;
	}

	/**
	 * @return the basisVersion
	 */
	public Version getBasisVersion() {
		return this.basisVersion;
	}

	public ModuleConfiguration getModuleConfiguration(int moduleNo) {

        ModuleConfiguration mc = modulesConf.get(moduleNo);

        if (mc == null) {
            mc = new ModuleConfiguration(moduleNo, Constants.FIRST_ITEM_NO);
        }

	    //Clesius
        //public ModuleConfiguration getModuleConfiguration(int moduleNo, int itemPartNo) {
        // ModuleConfiguration mc;
        //        if (itemPartNo==1)
        //	        mc = modulesConf.get(moduleNo);
        //        else
        //            mc = new ModuleConfiguration(moduleNo, Constants.SECOND_ITEM_PART_NO);
        //
        //		if (mc == null) {
        //			if (itemPartNo==1)
        //				mc = new ModuleConfiguration(moduleNo, Constants.FIRST_ITEM_NO);
        //			else
        //				mc = new ModuleConfiguration(moduleNo, Constants.SECOND_ITEM_PART_NO);
        //		}


		return mc;
	}

	public ModuleConfiguration addModuleConfiguration(int moduleNo, int itemNo) {

		ModuleConfiguration mc = modulesConf.get(moduleNo);

		if (mc == null) {
			mc = new ModuleConfiguration(moduleNo, itemNo);
			modulesConf.put(moduleNo, mc);
		} else {
			mc.setItemNo(itemNo);
		}

		return mc;
	}

	public class PhraseOptionConfiguration {

		private int itemNo;

		private Map<SpoKey, SubPhraseOptionConfiguration> subPhraseConfigurations = new HashMap<SpoKey, SubPhraseOptionConfiguration>();

		private PhraseOptionConfiguration(int itemNo) {
			this.itemNo = itemNo;
		}

		/**
		 * @return the itemNo
		 */
		public int getItemNo() {
			return itemNo;
		}

		void setItemNo(int itemNo) {
			this.itemNo = itemNo;
		}

		public Version getBasisVersion() {
			return basisVersion;
		}

		public SubPhraseOptionConfiguration addSubPhraseOptionConfiguration(SubPhraseOptionName subPhraseOptionName, int itemNo) {
			SpoKey key = new SpoKey(subPhraseOptionName.getPhraseOptionName(), subPhraseOptionName.getPosNo());
			SubPhraseOptionConfiguration spoc = subPhraseConfigurations.get(key);

			if (spoc == null) {
				spoc = new SubPhraseOptionConfiguration(subPhraseOptionName, itemNo);
				subPhraseConfigurations.put(key, spoc);
			} else {
				spoc.setItemNo(itemNo);
			}

			return spoc;
		}

		/**
		 * @return the subPhraseOptionsConfiguration
		 */
		public SubPhraseOptionConfiguration getSubPhraseOptionConfiguration(SubPhraseOptionName subPhraseOptionName) {

			SpoKey key = new SpoKey(subPhraseOptionName.getPhraseOptionName(), subPhraseOptionName.getPosNo());
			SubPhraseOptionConfiguration spoc = subPhraseConfigurations.get(key);
			return spoc;
		}

		private class SpoKey {

			private String phraseOptionName;
			private int posNo;

			private SpoKey(String phraseOptionName, int posNo) {
				this.phraseOptionName = phraseOptionName;
				this.posNo = posNo;
			}

			@Override
			public int hashCode() {
				return 31 * (phraseOptionName != null ? phraseOptionName.hashCode() : 0) + posNo;
			}

			@Override
			public boolean equals(Object o) {

				if (o instanceof SpoKey == false) {
					return false;
				}

				SpoKey key = (SpoKey) o;

				if (phraseOptionName == null) {
					return key.phraseOptionName == null && posNo == key.posNo;
				} else {
					return phraseOptionName.equals(key.phraseOptionName) && posNo == key.posNo;
				}

			}
		}

	}

	public class ModuleConfiguration extends PhraseOptionConfiguration {

		private int moduleNo;

		private ModuleConfiguration(int moduleNo, int itemNo) {
			super(itemNo);
			this.moduleNo = moduleNo;
		}

		/**
		 * @return the moduleNo
		 */
		public int getModuleNo() {
			return moduleNo;
		}

	}

	public class SubPhraseOptionConfiguration extends PhraseOptionConfiguration {

		private SubPhraseOptionName subPhraseOptionName;

		private SubPhraseOptionConfiguration(SubPhraseOptionName subPhraseOptionName, int itemNo) {
			super(itemNo);
			this.subPhraseOptionName = subPhraseOptionName;
		}

		/**
		 * @return the phraseOptionName
		 */
		public String getPhraseOptionName() {
			return subPhraseOptionName.getPhraseOptionName();
		}

	}

}
