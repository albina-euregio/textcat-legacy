package ch.slf.lwp.bulletinedit.textcat.model;

public interface Phrase {

	/**
	 * @return the id
	 */
	public abstract int getId();

	/**
	 * @return the value
	 */
	public abstract String getValue();

	/**
	 * @param value
	 *            the value to set
	 */
	public abstract void setValue(String value);


	/**
	 * @return the rgn
	 */
	public abstract String getRgn();

	/**
	 * @param rgn
	 *            the rgn to set
	 */
	public abstract void setRgn(String rgn);



	/**
	 * @return the itemPartNo
	 */
	public abstract int getItemPartNo();

	/**
	 * @param itemPartNo
	 *            the itemPartNo to set
	 */
	public abstract void setItemPartNo(int itemPartNo);

	/**
	 * @return the removePunctuationBefore
	 */
	public abstract boolean isRemovePunctuationBefore();

	/**
	 * @param removePunctuationBefore
	 *            the removePunctuationBefore to set
	 */
	public abstract void setRemovePunctuationBefore(boolean removePunctuationBefore);

	/**
	 * @return the spaceAfter
	 */
	public abstract boolean isSpaceAfter();

	/**
	 * @param spaceAfter
	 *            the spaceAfter to set
	 */
	public abstract void setSpaceAfter(boolean spaceAfter);

	/**
	 * @return the spaceBefore
	 */
	public abstract boolean isSpaceBefore();

	/**
	 * @param spaceBefore
	 *            the spaceBefore to set
	 */
	public abstract void setSpaceBefore(boolean spaceBefore);

	/**
	 * @return the incorrect
	 */
	public abstract boolean isIncorrect();

	/**
	 * @param incorrect
	 *            the incorrect to set
	 */
	public abstract void setIncorrect(boolean incorrect);

}