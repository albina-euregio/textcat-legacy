package ch.slf.lwp.bulletinedit.textcat.json;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ch.slf.lwp.bulletinedit.textcat.Constants;
import ch.slf.lwp.bulletinedit.textcat.StringUtils;
import ch.slf.lwp.bulletinedit.textcat.TextCatException;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructureModule;

/**
 * Creates the JSON Objects.
 * 
 * This implementation works one and only for the German language of the
 * 'PRODUCTION' domain, because split phrase option are not
 * supported!
 * 
 * @author gw
 */
public class JsonUtil {

	public static final String STRING_LIST_SEP = ";";

	/**
	 * Create the JSON sentence item object.
	 * 
	 * @param sentence
	 *            the sentence to write
	 * @return the JSON sentence item object.
	 */
	public static SentenceItem sentence2Json(Sentence sentence, JsonIndexConverter converter) {

		SentenceItem sItem = new SentenceItem();
		sItem.setId(converter.getSentenceId(sentence));
		sItem.setDbid(sentence.getId());
		sItem.setName(sentence.getName());
		sItem.setHeader(sentence.getHeader());
		sItem.setJoker(sentence.isJokerSentence());

		StringBuilder options = new StringBuilder();

		Iterator<SentenceStructureModule> oit = sentence.moduleIterator();

		while (oit.hasNext()) {
			SentenceStructureModule o = oit.next();
			options.append(converter.getSentenceStructureModuleId(o));

			if (oit.hasNext()) {
				options.append(STRING_LIST_SEP);
			}
		}

		sItem.setOptions(options.toString());

		return sItem;
	}

	/**
	 * create the JSON option item object.
	 * 
	 * The phrases list is an optional parameter. If phrases is not
	 * <code>null</code> all phrases from the option are added to the list.
	 * 
	 * @param option
	 *            the option to write
	 * @param phrases
	 *            list to fetch all phrases.
	 * @return the JSON option item
	 * @throws TextCatException if an error occurred.
	 * @throws IllegalArgumentException
	 *             if the option language is not German.
	 */
	public static OptionItem option2Json(PhraseOption option, List<Phrase> phrases, JsonIndexConverter converter) throws TextCatException {

		//if (option.getLanguage() != Language.de) {
		//	throw new IllegalArgumentException("Only the German language is acepted");
		//}
		
		Iterator<PhraseOptionItem> itemIt = option.phraseOptionItemIterator();

		OptionItem oi = new OptionItem();
		oi.setId(converter.getOptionId(option));
        oi.setDbid(option.getId());
		oi.setName(option.getHeader() != null ? option.getHeader() : "");

		//Clesius reset fake_ID_Increment
        int dbid=option.getId();
		if (dbid >  Constants.FAKE_ID_INCREMENT )
		    oi.setDbid(option.getId() -Constants.FAKE_ID_INCREMENT );
        else
            oi.setDbid(option.getId());

        oi.setName(option.getName() != null ? option.getName() : "");
		oi.setHeader(option.getHeader() != null ? option.getHeader() : "");

		//Clesius add name_It
		//oi.setNewHeader(option.getNewHeader() != null ? option.getNewHeader() : "");

		StringBuilder sb = new StringBuilder();
		
		while(itemIt.hasNext()) {
			PhraseOptionItem item = itemIt.next();
			Phrase phrase = item.getPhrase(Constants.FIRST_ITEM_PART_NO);
			
			if(phrase != null) {
				
				// set separator if not the first one.
				if(sb.length() > 0) {
					sb.append(STRING_LIST_SEP);
				}
				
				sb.append(converter.getPhraseId(phrase));
				phrases.add(phrase);
			} else {

				//Clesius get also second_item_part_no
                phrase = item.getPhrase(Constants.SECOND_ITEM_PART_NO );
                if(phrase != null) {

                    // set separator if not the first one.
                    if(sb.length() > 0) {
                        sb.append(STRING_LIST_SEP);
                    }

                    sb.append(converter.getPhraseId(phrase));
                    phrases.add(phrase);
                }

                // this should never happens
				// Clesius
				//throw new TextCatException("Invalide option item. First phrase failed! Option name: " + option.getName() + "; Item no: " + item.getItemNo());
			}
		}

		oi.setPhrases(sb.toString());

		return oi;
	}

	/**
	 * Writes the JSON phrase item object.
	 * 
	 * @param phrase
	 *            the phrase to write
	 * @param replacements
	 *            the replacement map for the sub phrase option sequences.
	 * @return the JSON phrase item object.
	 */
	public static PhraseItem phrase2Json(Phrase phrase, Map<String, String> replacements, JsonIndexConverter converter) {

		PhraseItem pi = new PhraseItem();
		pi.setId(converter.getPhraseId(phrase));
		pi.setDbid(phrase.getId());
		pi.setValue(StringUtils.replacePhraseOptionNames(phrase.getValue(), replacements));
		pi.setRgn(phrase.getRgn());
		return pi;
	}
}
