package ch.slf.lwp.bulletinedit.textcat;

/**
 * Thrown when an textcat operation failed.
 * 
 * @author gw
 */
public class TextCatException extends Exception {

	private static final long serialVersionUID = 1L;

	public TextCatException() {}

	public TextCatException(String message) {
		super(message);
	}

	public TextCatException(Throwable cause) {
		super(cause);
	}

	public TextCatException(String message, Throwable cause) {
		super(message, cause);
	}

}
