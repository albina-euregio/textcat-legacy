package ch.slf.lwp.bulletinedit.textcat.model;

import java.util.Iterator;
import java.util.List;

import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructure;
import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructureModule;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

public interface Sentence {

	/**
	 * @return the id
	 */
	public abstract int getId();

	/**
	 * @return the name
	 */
	public abstract String getName();

	/**
	 * @param name
	 *            the name to set
	 */
	public abstract void setName(String name);


	/**
	 * @return the name
	 */
	public abstract String getHeader();

	/**
	 * @param header
	 *            the name to set
	 */
	public abstract void setHeader(String header);

	/**
	 * @return the language
	 */
	public abstract Language getLanguage();

	/**
	 * @param language
	 *            the language to set
	 */
	public abstract void setLanguage(Language language);

	/**
	 * @return the version
	 */
	public abstract Version getVersion();

	/**
	 * @param version
	 *            the version to set
	 */
	public abstract void setVersion(Version version);

	/**
	 * @return the basisVersion
	 */
	public abstract Version getBasisVersion();

	/**
	 * @param basisVersion
	 *            the basisVersion to set
	 */
	public abstract void setBasisVersion(Version basisVersion);

	/**
	 * @return the structure
	 */
	public abstract SentenceStructure getStructure();

	/**
	 * @param structure
	 *            the structure to set
	 */
	public abstract void setStructure(SentenceStructure structure);

	/**
	 * @return the remark
	 */
	public abstract String getRemark();

	/**
	 * @param remark
	 *            the remark to set
	 */
	public abstract void setRemark(String remark);

	/**
	 * @return the deleted
	 */
	public abstract boolean isDeleted();

	/**
	 * @param deleted
	 *            the deleted to set
	 */
	public abstract void setDeleted(boolean deleted);

	/**
	 * @return the jokerSentence
	 */
	public abstract boolean isJokerSentence();

	/**
	 * @param jokerSentence
	 *            the jokerSentence to set
	 */
	public abstract void setJokerSentence(boolean jokerSentence);

	/**
	 * @return the domainName
	 */
	public abstract DomainName getDomainName();

	/**
	 * @param domainName
	 *            the jokerSentence to set
	 */
	public abstract void setDomainName(DomainName domainName);


	/**
	 * @return the keywords
	 */
	public abstract List<String> getKeywords();

	/**
	 * @param keywords
	 *            the keywords to set
	 */
	public abstract void setKeywords(List<String> keywords);

	/**
	 * @return the topics
	 */
	public abstract List<String> getTopics();

	/**
	 * @param topics
	 *            the topics to set
	 */
	public abstract void setTopics(List<String> topics);

	/**
	 * @param keyword
	 *            the keyword to add
	 */
	public abstract void addKeyword(String keyword);

	/**
	 * @param topic
	 *            the topic to add
	 */
	public abstract void addTopic(String topic);

	/**
	 * returns the number of structure elements.
	 * 
	 * @return the number of structure elements.
	 */
	public abstract int getStructureSize();


	/**Clesius
	 * Add a sentence module.
	 * Bypass if an module with equal moduleNo already exists, it will be replaced
	 * ModuleNo*10
	 *
	 * @param posNo the module position
	 * @param moduleNo the module number
	 * @param phraseOption the module
	 */
	public abstract void addSentenceModule(int posNo, int moduleNo, PhraseOption phraseOption);

	/**
	 * Add a sentence module.
	 * If an module with equal moduleNo already exists, it will be replaced
	 * 
	 * @param moduleNo the module number
	 * @param phraseOption the module
	 */
	public abstract void addSentenceModule(int moduleNo, PhraseOption phraseOption);

	/**
	 * Creates a new empty sentence module.
	 * If a module with equal module number already exists, it will be replaced.
	 * 
	 * @param moduleNo the module number
	 * @return the create phrase option
	 */
	public abstract PhraseOption createSentenceModule(int moduleNo);

	/**
	 * Returns the sentence module with the according number.
	 * If the module does not exists <code>null</code> will be returned.
	 * 
	 * @param moduleNo the module number
	 * @return the sentence module or <code>null</code>
	 */
	public abstract PhraseOption getSentenceModule(int moduleNo);

	/**
	 * Returns an iterator over the sentence structure modules in order of the
	 * sentence structure. Because of split phrase options, one phrase option
	 * pass more than once.
	 * 
	 * @return an iterator over the phrase options
	 */
	public abstract Iterator<SentenceStructureModule> moduleIterator();

	/**
	 * Update this sentence with the values of the specified sentence and makes
	 * the
	 * specified sentence as clone of this option.
	 * 
	 * @param option
	 */
	public abstract void updateClone(Sentence sentence);
}