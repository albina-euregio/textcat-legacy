package ch.slf.lwp.bulletinedit.textcat.model.util;

import ch.slf.lwp.bulletinedit.textcat.TextCatException;

/**
 * Represent the version.
 * 
 * @author gw
 *
 */
public class Version implements Comparable<Version>{

	private static String VERSION_SEP = ".";

	private int mainNumber;
	private int correctionNumber;

	public Version(int mainNumber, int correctionNumber) {
		this.mainNumber = mainNumber;
		this.correctionNumber = correctionNumber;
	}

	public static Version create(String versionString) throws TextCatException {

		if (versionString == null) {
			throw new TextCatException("'NULL' as version string is not allowed");
		}

		String[] s = versionString.split("\\" + VERSION_SEP);

		if (s.length != 2) {
			throw new TextCatException("Malformed version string: " + versionString + " - expected: <int>" + VERSION_SEP + "<int>");
		}

		Version v;
		try {
			v = new Version(Integer.parseInt(s[0]), Integer.parseInt(s[1]));
		} catch (NumberFormatException e) {
			throw new TextCatException("Malformed version string: " + versionString + " - expected: <int>" + VERSION_SEP + "<int>", e);
		}

		return v;

	}

	/**
	 * @return the mainNumber
	 */
	public int getMainNumber() {
		return mainNumber;
	}

	/**
	 * @return the correctionNumber
	 */
	public int getCorrectionNumber() {
		return correctionNumber;
	}

	public boolean equalMainNumber(Version v) {
		return this.mainNumber == v.mainNumber;
	}

	@Override
	public String toString() {
		return Integer.toString(mainNumber) + VERSION_SEP + Integer.toString(correctionNumber);
	}

	@Override
	public boolean equals(Object o) {

		if (o instanceof Version) {
			Version v = (Version) o;
			return compare(this, v) == 0;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return 31 * mainNumber + correctionNumber;
	}

	@Override
	public int compareTo(Version o) {
		//wenn selbst kleiner ==> minus
		int no = compare(this, o);
		return Integer.signum(no);
	}
	
	private int compare(Version self, Version other) {
		int no = self.mainNumber - other.mainNumber;
		
		if(no == 0) {
			// gleiche main version, correction version is used
			no = self.correctionNumber - other.correctionNumber;
		}
		
		return no;
	}

}
