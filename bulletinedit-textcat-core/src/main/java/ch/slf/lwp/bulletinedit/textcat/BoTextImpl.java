package ch.slf.lwp.bulletinedit.textcat;

import java.text.ParseException;
import java.util.EnumMap;
import java.util.Map;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.applicationTexts.AppTextDefinition;
import ch.slf.lwp.bulletinedit.textcat.applicationTexts.AppTextDefinitionParser;
import ch.slf.lwp.bulletinedit.textcat.dao.DaoPhraseOption;
import ch.slf.lwp.bulletinedit.textcat.dao.DaoSentence;
import ch.slf.lwp.bulletinedit.textcat.dao.DaoTextCat;
import ch.slf.lwp.bulletinedit.textcat.json.JsonTextCat;
import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.JokerSentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructure;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;
import ch.slf.lwp.bulletinedit.textcat.text.TextBuilder;
import ch.slf.lwp.bulletinedit.textcat.text.TextDefinition;
import ch.slf.lwp.bulletinedit.textcat.text.TextDefinitionParser;

/**
 * Business logic for text creation.
 * 
 * @author gw
 */
public class BoTextImpl implements BoText {
	
	private final Logger log = LogManager.getLogger(this.getClass());

	private DaoSentence daoSentence;
	private DaoPhraseOption daoPhraseOption;
	private DaoTextCat daoTextCat;
	private TextCatFactory textCatFactory;

	/**
	 * @param daoSentence
	 *            the daoSentence to set
	 */
	public void setDaoSentence(DaoSentence daoSentence) {
		this.daoSentence = daoSentence;
	}

	/**
	 * @param daoPhraseOption
	 *            the daoPhraseOption to set
	 */
	public void setDaoPhraseOption(DaoPhraseOption daoPhraseOption) {
		this.daoPhraseOption = daoPhraseOption;
	}

	/**
	 * @param daoTextCat
	 *            the daoTextCat to set
	 */
	public void setDaoTextCat(DaoTextCat daoTextCat) {
		this.daoTextCat = daoTextCat;
	}

	/**
	 * @param textCatFactory
	 *            the textCatFactory to set
	 */
	public void setTextCatFactory(TextCatFactory textCatFactory) {
		this.textCatFactory = textCatFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.BoText#buildText(java.lang.String,
	 * ch.slf.lwp.bulletinedit.textcat.Language,
	 * ch.slf.lwp.bulletinedit.textcat.DomainName)
	 */
	@Override
	public String buildText(String textDefinitionString, Language targetLanguage, DomainName domainName) throws TextCatException {
		try {

			TextDefinition textDefinition = TextDefinitionParser.parseDefinition(textDefinitionString);
			TextBuilder textBuilder = new TextBuilder(daoSentence, daoPhraseOption);
			String text = textBuilder.build(textDefinition, targetLanguage, domainName);
			return text;
		} catch (ParseException e) {
			throw new TextCatException("Parse textdefinition string failed", e);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.BoText#buildAppText(java.lang.String,
	 * ch.slf.lwp.bulletinedit.textcat.Language,
	 * ch.slf.lwp.bulletinedit.textcat.DomainName)
	 */
	@Override
	public String buildAppText(String appTextDefinitionString, Language targetLanguage, DomainName domainName) throws TextCatException {

		try {

			AppTextDefinition appTextDefinition = AppTextDefinitionParser.parseDefinition(appTextDefinitionString);
			TextBuilder textBuilder = new TextBuilder(daoSentence, daoPhraseOption);
			String text = textBuilder.build(appTextDefinition, targetLanguage, domainName);
			return text;
		} catch (ParseException e) {
			throw new TextCatException("Parse textdefinition string failed", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.BoText#addJokerSentence(ch.slf.lwp.
	 * bulletinedit.textcat.modelsw.JokerSentence)
	 */
	@Override
	public Map<Language, Sentence> addJokerSentence(JokerSentence jokerSentence, DomainName domainName) throws TextCatException {

		final Map<Language, Sentence> sentences = new EnumMap<>(Language.class);
		final Version version = Version.create("1.0");

		for (Language language : Language.values()) {

			Sentence sentence = textCatFactory.createSentence(jokerSentence.getDomainName());
			sentence.setName(jokerSentence.getName());

			//Clesius
			sentence.setHeader(jokerSentence.getName());

			sentence.setVersion(version);
			sentence.setBasisVersion(version);
			sentence.setDeleted(false);
			sentence.setStructure(new SentenceStructure().add(Constants.FIRST_MODULE_NO, Constants.FIRST_ITEM_PART_NO));
			sentence.setLanguage(language);
			sentence.setJokerSentence(true);

			PhraseOption option = sentence.createSentenceModule(Constants.FIRST_MODULE_NO);
			option.setBasisVersion(version);
			option.setDeleted(false);
			option.setLanguage(language);
			option.setName(StringUtils.createUniquePhraseOptionNo(sentence.getName(), Constants.FIRST_MODULE_NO));

			//Clesius
			//option.setNewHeader(StringUtils.createUniquePhraseOptionNo(sentence.getName(), Constants.FIRST_MODULE_NO));
			option.setHeader(StringUtils.createUniquePhraseOptionNo(sentence.getName(), Constants.FIRST_MODULE_NO));
			//option.setName_IT(StringUtils.createUniquePhraseOptionNo(sentence.getName(), Constants.FIRST_MODULE_NO));

			option.setVersion(version);

			PhraseOptionItem item = option.createPhraseOptionItem();
			item.setBasisVersion(version);
			item.setDeleted(false);
			item.setItemNo(Constants.FIRST_ITEM_NO);
			item.setVersion(version);

			Phrase phrase = item.createPhrase();
			phrase.setIncorrect(false);
			phrase.setItemPartNo(Constants.FIRST_ITEM_PART_NO);
			phrase.setRemovePunctuationBefore(false);
			phrase.setSpaceAfter(true);
			phrase.setSpaceBefore(true);
			phrase.setValue(jokerSentence.getValue(language));

			sentence = daoSentence.insertSentence(sentence);
			sentences.put(language, sentence);
		}
		return sentences;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.BoText#getJsonTextCatalog()
	 */
	@Override
	public JsonTextCat getJsonTextCatalog(Language language,DomainName domainName) throws TextCatException {
		return this.reloadJsonTextCatalog(new JsonTextCat(),language, domainName);
	}

	@Override
	public JsonTextCat reloadJsonTextCatalog(JsonTextCat jsonTextCat, Language language,DomainName domainName) throws TextCatException {

		if (jsonTextCat == null) {
			jsonTextCat = new JsonTextCat();
		} else {
			jsonTextCat = jsonTextCat.getEmptyInstance();
		}
		daoTextCat.fillTextCat(jsonTextCat,language,domainName);
		//daoTextCat.fillTextCat(jsonTextCat);
		return jsonTextCat;
	}

	@Override
	public int getTextLength(String textDefinitionString, Language targetLanguage, DomainName domainName) throws TextCatException {
		return buildText(textDefinitionString, targetLanguage, domainName).length();
	}

	@Override
	public int getMaxTextLength(String textDefinitionString, DomainName domainName) throws TextCatException {
		int lenght = 0;
		
		for (Language language : Language.values()) {
			
			try {
				
				int h = getTextLength(textDefinitionString, language, domainName);
				
				if(lenght < h)  {
					lenght = h;
				}
				
			} catch (Exception e) {
				log.warn("retrive text length for language " + language + " failed: " + e.getMessage());
			}
		}
		
		return lenght;
	}

}
