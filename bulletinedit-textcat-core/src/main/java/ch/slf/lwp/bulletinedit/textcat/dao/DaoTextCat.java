package ch.slf.lwp.bulletinedit.textcat.dao;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCat;
import ch.slf.lwp.bulletinedit.textcat.TextCatException;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

/**
 * Text cat data access object.
 * 
 * This interface must be implemented by the special data layer implementation.
 * All existing methods have to be act as an atomic operation.
 * <p>
 * For Spring:<br/>
 * The implementation has to provide a spring bean with the id:
 * 'textcat_daoTextCat' <br/>
 * &lt;bean id="textcat_daoTextCat" class="..." &gt; &lt;/bean&gt;
 * </p>
 * 
 * @author gw
 */
public interface DaoTextCat {

	/**
	 * Initialize the required TextCat.
	 * 
	 * @param textCat the TextCat to fill
	 * @throws TextCatException
	 */
	public abstract void fillTextCat(TextCat textCat, Language language,DomainName domainName) throws TextCatException;

	/**
	 * Mark a textcat version as deleted.
	 * All existing languages with this base version will be marked as deleted.
	 * 
	 * @param baseVersion the textcat version
	 * @param domainName the domain name
	 * @throws TextCatException
	 */
	public abstract void markVersionAsDeleted(Version baseVersion, DomainName domainName) throws TextCatException;

	/**
	 * Undo the deletion mark of a textcat version.
	 * All languages with this base version will be reactivated
	 * 
	 * @param baseVersion the textact version
	 * @param domainName the domain name
	 * @throws TextCatException
	 */
	public abstract void undoDeletionMark(Version baseVersion, DomainName domainName) throws TextCatException;

	/**
	 * Delete this textcat version.
	 * All existing languages with this base version will be physically deleted.
	 * Note that this operation can not be undo.
	 * 
	 * @param baseVersion the textcat version
	 * @param domainName the domain name
	 * @throws TextCatException
	 */
	public abstract void deleteVersion(Version baseVersion, DomainName domainName) throws TextCatException;

	/**
	 * Delete this language version.
	 * All language entries with this version will be physically deleted. Note
	 * that this operation can not be undo.
	 * 
	 * @param language the language
	 * @param version the language version
	 * @param domainName the domain name
	 * @throws TextCatException
	 */
	public abstract void deleteLanguageVersion(Language language, Version version, DomainName domainName) throws TextCatException;

	public abstract void recodeDomain(int oldDomainID, int newDomainID) throws TextCatException;




}