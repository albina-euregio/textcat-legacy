package ch.slf.lwp.bulletinedit.textcat.dao;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

public interface DaoSentence {

	public abstract Sentence getSentence(int sentenceId);

	public abstract Sentence getSentence(String sentenceName, Language language, Version baseVersion, DomainName domainName);

	public abstract Sentence insertSentence(Sentence sentence);

	public abstract Sentence updateSentence(Sentence sentence);

	public abstract boolean deleteSentence(Version baseVersion, DomainName domainName);

}
