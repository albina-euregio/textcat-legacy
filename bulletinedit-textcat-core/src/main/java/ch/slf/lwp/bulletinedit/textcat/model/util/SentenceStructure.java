package ch.slf.lwp.bulletinedit.textcat.model.util;

import java.util.ArrayList;
import java.util.Iterator;

public class SentenceStructure implements Iterable<SentenceStructure.StructureElement> {
	
	private static final String MODULE_SEP = ",";
	private static final String MODULE_ITEM_PART_SEP = ".";
	
	private ArrayList<StructureElement> elements = new ArrayList<StructureElement>();
	
	public static SentenceStructure parse(String structure) {
		
		SentenceStructure s = new SentenceStructure();
		
		String[] els = structure.split("\\" + MODULE_SEP);
		
		for (String el : els) {
			int idx = el.indexOf(MODULE_ITEM_PART_SEP);
			int moduleNo = Integer.parseInt(el.substring(0, idx));
			int itemPartNo = Integer.parseInt(el.substring(idx + 1));
			s.add(moduleNo, itemPartNo);
		}
		
		return s;
	}
	
	public SentenceStructure add(int moduleNo, int itemPartNo) {
		elements.add(new StructureElement(moduleNo, itemPartNo));
		return this;
	}
	
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		
		for (StructureElement element : elements) {
			
			if(sb.length() > 0) {
				sb.append(MODULE_SEP);
			}
			sb.append(element.toString());
		}
		
		return sb.toString();
	}
	
	public int size() {
		return elements.size();
	}

	@Override
	public Iterator<StructureElement> iterator() {
		return new Iterator<StructureElement>() {
			
			private Iterator<StructureElement> it = elements.iterator();

			@Override
			public boolean hasNext() {
				return it.hasNext();
			}

			@Override
			public StructureElement next() {
				return it.next();
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	public static class StructureElement {

		private int moduleNo;
		private int itemPartNo;
		
		private StructureElement(int moduleNo, int itemPartNo) {
			this.moduleNo = moduleNo;
			this.itemPartNo = itemPartNo;
		}

		public int getModuleNo() {
			return moduleNo;
		}

		public int getItemPartNo() {
			return itemPartNo;
		}
		
		@Override
		public String toString() {
			return Integer.toString(moduleNo) + MODULE_ITEM_PART_SEP + Integer.toString(itemPartNo);
		}
		
	}

}
