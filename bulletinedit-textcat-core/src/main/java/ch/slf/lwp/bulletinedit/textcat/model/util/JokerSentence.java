package ch.slf.lwp.bulletinedit.textcat.model.util;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import java.util.EnumMap;
import java.util.UUID;


public class JokerSentence {

	private final String name;
	private DomainName domainName;
	private final EnumMap<Language, String> values = new EnumMap<>(Language.class);

	public JokerSentence() {
		this.name = "joker_" + UUID.randomUUID().toString();
	}

	public String getName() {
		return this.name;
	}

	public DomainName getDomainName() {
		return domainName;
	}

	public void setDomainName(DomainName domainName) {
		this.domainName = domainName;
	}

	public void setValue(String value, Language language) {
		values.put(language, value);
	}

	public String getValue(Language language) {
		return values.get(language);
	}

}
