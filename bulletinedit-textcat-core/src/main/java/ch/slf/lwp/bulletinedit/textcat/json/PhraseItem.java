package ch.slf.lwp.bulletinedit.textcat.json;

/**
 * JSON item entry representing a phrase.
 * 
 * @author weiss
 */
public class PhraseItem extends AbstractCatalogItem {

	private String value;

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
