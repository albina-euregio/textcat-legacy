package ch.slf.lwp.bulletinedit.textcat.model;

public interface RegionPhrase {

    public abstract int getRegionId();
    public abstract int getPhraseId();
}
