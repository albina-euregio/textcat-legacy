package ch.slf.lwp.bulletinedit.textcat;

import java.util.Locale;

public interface TextCatLanguages {
    enum Language {
        /**
         * German
         */
        de(Locale.GERMAN),
        /**
         * Italian
         */
        it(Locale.ITALIAN),
        /**
         * English
         */
        en(Locale.ENGLISH),
        /**
         * French
         */
        fr(Locale.FRENCH),
        /**
         * Spanish
         */
        es(new Locale("es")),
        /**
         * Catalan
         */
        ca(new Locale("ca")),
        /**
         * Occitan
         */
        oc(new Locale("oc"));

        private final Locale locale;

        Language(Locale locale) {
            this.locale = locale;
        }

        public Locale getLocale() {
            return locale;
        }

    }
}
