package ch.slf.lwp.bulletinedit.textcat.applicationTexts;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import ch.slf.lwp.bulletinedit.textcat.applicationTexts.AppTextDefinition.SentenceDefinition;

/**
 * Format of a AppTextDefinition:
 * sentenceName[ItemNoOfModule1[ItemNoOfSubmodule1, ItemNoOfSubmodule2],
 * ItemNoOfModule2, ....] If more complex static
 **/
public class AppTextDefinitionParser {

	// sentenceName[ItemNoOfModule1[ItemNoOfSubmodule1, ItemNoOfSubmodule2],
	// ItemNoOfModule2, ....]
	public final static char ITEM_NO_START = '[';
	public final static char ITEM_NO_END = ']';
	public static final String ITEM_SEPARATOR = ",";
	public static final String SENTENCE_SEPARATOR = ".";

	// sentenceName1[ItemNoOfModule1[ItemNoOfSubmodule1, ItemNoOfSubmodule2],
	// ItemNoOfModule2, ....].sentenceName2[ItemNoOfModule1]

	// Regex to validate text definition
	public static final String APP_TEXT_DEFINITION_FORMAT = "((\\" + SENTENCE_SEPARATOR + "?)(\\w+\\" + ITEM_NO_START + "(" + ITEM_SEPARATOR + "?[0-9](((\\" + ITEM_NO_START + "(" + ITEM_SEPARATOR + "?[0-9])+\\" + ITEM_NO_END + ")?)+))+\\" + ITEM_NO_END + "))+";
	// Regex to split sentences
	private static final String REGEX_SPLIT_SENTENCES = "\\" + SENTENCE_SEPARATOR;
	// Regex to get sentenceName
	private static final String REGEX_SENTENCE_NAME = "\\" + ITEM_NO_START;
	// Regex to get List of Item Numbers of Modules and Submodules
	private static final String REGEX_ITEM_NUMBERS = "^\\w+\\" + ITEM_NO_START + "|\\" + ITEM_NO_END + "$";
	// Regex to split String of ItemNumbers into ItemNumber by Module
	private static final String REGEX_SPLIT_MODULES = ",(?![^\\" + ITEM_NO_START + "]*\\" + ITEM_NO_END + ")";
	// Regex to split itemNo and subItemNos
	private static final String REGEX_SPLIT_ITEM_SUBITEMS = "[\\" + ITEM_NO_START + " \\" + ITEM_NO_END + "]";
	// Regex to split summodules item numbers
	private static final String REGEX_SPLIT_SUBITEMS = ITEM_SEPARATOR;

	/**
	 * Parse the name based application text definition.<br/>
	 * Creates a new AppTextDefinitionParser and parse the given string
	 * representation of the application text definition.
	 * 
	 * @param appTextDefinition
	 *            the definition string to parse.
	 * @return the application text definition
	 * @throws ParseException
	 *             if the sting can not be parsed
	 */
	public static AppTextDefinition parseDefinition(String appTextDefinition) throws ParseException {
		AppTextDefinitionParser parser = new AppTextDefinitionParser();
		return parser.parse(appTextDefinition);
	}

	public AppTextDefinition parse(String appTextDefinitionString) throws ParseException {

		String sentenceName;
		AppTextDefinition appTextDefinition = null;

		if (!appTextDefinitionString.matches(APP_TEXT_DEFINITION_FORMAT)) {
			throw new ParseException("Not a valid AppTextDefinition:" + appTextDefinitionString, 0);
		}

		try {
			appTextDefinition = new AppTextDefinition();
			String[] sentenceDefinitionStrings = appTextDefinitionString.split(REGEX_SPLIT_SENTENCES);
			for (String sentenceDefinitionString : sentenceDefinitionStrings) {
				sentenceName = sentenceDefinitionString.split(REGEX_SENTENCE_NAME)[0];
				SentenceDefinition sentenceDefinition= appTextDefinition.addSentence(sentenceName);
				String itemListString = sentenceDefinitionString.split(REGEX_ITEM_NUMBERS)[1];
				String[] itemStrings = itemListString.split(REGEX_SPLIT_MODULES);
				// item_no are stored within the list in the order of the
				// modules,
				// so the position within the list is according to the module_no
				int moduleNo = 1;
				for (String itemString : itemStrings) {
					String[] itemSubItemStrings = itemString.split(REGEX_SPLIT_ITEM_SUBITEMS);
					int itemNo = Integer.parseInt(itemSubItemStrings[0]);
					List<Integer> subItemNos = new ArrayList<Integer>();
					if (itemSubItemStrings.length > 1) {
						String subItemsString = itemString.split(REGEX_SPLIT_ITEM_SUBITEMS)[1];
						String[] SubItemStrings = subItemsString.split(REGEX_SPLIT_SUBITEMS);
						for (String subItemString : SubItemStrings) {
							subItemNos.add(Integer.parseInt(subItemString));
						}
					}
					sentenceDefinition.addModule(moduleNo, itemNo, subItemNos);
					moduleNo++;
				}
			}

		} catch (Exception e) {
			throw new ParseException("Not a valid AppTextDefinition:" + appTextDefinitionString + "Exception:" + e, 0);
		}

		return appTextDefinition;
	}
}
