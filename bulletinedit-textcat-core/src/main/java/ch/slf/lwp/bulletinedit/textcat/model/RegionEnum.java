package ch.slf.lwp.bulletinedit.textcat.model;

public enum RegionEnum {
    SWITZERLAND(1),

    TYROL(2),

    SOUTH_TYROL(3),

    TRENTINO(4);

    private Integer regionID;

    RegionEnum(Integer regionID) {
        this.regionID = regionID;
    }

    public Integer getRegionID() {
        return regionID;
    }
}
