package ch.slf.lwp.bulletinedit.textcat;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;

/**
 * Factory to create the special implementations.
 * 
 * @author gw
 */
public interface TextCatFactory {

	/**
	 * Returns a new Sentence object.
	 * 
	 * @param domainName
	 *            the domain name
	 * @return a new Sentence object
	 */
	public Sentence createSentence(DomainName domainName);

	/**
	 * Returns a new PhraseOption object.
	 * 
	 * @param domainName
	 *            the domain name
	 * @return a new PhraseOption object
	 */
	public PhraseOption createPhraseOption(DomainName domainName);

}
