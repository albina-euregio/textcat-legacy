package ch.slf.lwp.bulletinedit.textcat.json;

import java.util.*;

import org.apache.logging.log4j.LogManager;

import ch.slf.lwp.bulletinedit.textcat.Constants;
import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.StringUtils;
import ch.slf.lwp.bulletinedit.textcat.TextCat;
import ch.slf.lwp.bulletinedit.textcat.TextCatException;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructureModule;
import ch.slf.lwp.bulletinedit.textcat.model.util.SubPhraseOptionName;

import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

/**
 * Creates the JSON objects for the sentence catalog. This implementation works
 * one and only for the German language of the 'PRODUCTION' domain,
 * because the special definition of unsplit phrase options is used multiple
 * times.
 *
 * @author gw
 */
public class JsonTextCat implements TextCat {

    // converts the database id to the "json" id.
    private JsonIndexConverter converter;

    // the key is the converted database id
    private HashMap<Integer, OptionItem> jsonOptions;
    private HashMap<Integer, PhraseItem> jsonPhrases;
    private HashMap<Integer, SentenceItem> jsonSentences;

    // the key is the word
    private HashMap<String, IndexItem> jsonWord2SentenceIndex;
    private HashMap<String, IndexItem> jsonWord2SentenceIndexWithSub;

    private Version basis_version;
    private String initMsg = "Init OK";

    public JsonTextCat() {
        this.converter = new JsonIndexConverter();
    }

    public JsonTextCat getEmptyInstance() {
        JsonTextCat t = new JsonTextCat();
        t.setJsonIndexConverter(converter);
        return t;
    }

    /**
     * Set the JSON index converter.
     *
     * @param converter
     */
    void setJsonIndexConverter(JsonIndexConverter converter) {
        this.converter = converter;
    }

    /*
     * (non-Javadoc)
     * @see ch.slf.lwp.bulletinedit.textcat.TextCat#getLingo()
     */
    @Override
    public Language getLanguage() {
        return Language.de;
    }

    /*
     * (non-Javadoc)
     * @see ch.slf.lwp.bulletinedit.textcat.TextCat#getDomainName()
     */
    @Override
    public DomainName getDomainName() {
        return DomainName.PRODUCTION;
    }

    /*
     * (non-Javadoc)
     * @see ch.slf.lwp.bulletinedit.textcat.TextCat#init(java.util.List,
     * java.util.List)
     */
    @Override
    public void init(List<? extends Sentence> sentences, List<? extends PhraseOption> options) throws TextCatException {

        jsonOptions = new HashMap<Integer, OptionItem>();
        jsonPhrases = new HashMap<Integer, PhraseItem>();
        jsonSentences = new HashMap<Integer, SentenceItem>();
        jsonWord2SentenceIndex = new HashMap<String, IndexItem>();
        jsonWord2SentenceIndexWithSub = new HashMap<String, IndexItem>();

        HashMap<String, PhraseOption> optionIdx = initPhraseOptions(options);
        initSentences(sentences);
        initWord2SentenceIndex(sentences, optionIdx);

        if (!isOrderContractValid()) {
            throw new TextCatException("Json-Id order is invalide.");
        }
    }

    /**
     * Returns the JSON sentence objects
     *
     * @return the JSON sentence objects
     */
    public Json getJsonSentences() {
        Json j = new CatalogJson();
        // j.setItems(jsonSentences.values().toArray(new Item[0]));
        j.setItems(toSortedArray(jsonSentences.values()));
        return j;
    }

    /**
     * Returns the JSON options objects
     *
     * @return the JSON options objects
     */
    public Json getJsonOptions() {
        Json j = new CatalogJson();
        // j.setItems(jsonOptions.values().toArray(new Item[0]));
        j.setItems(toSortedArray(jsonOptions.values()));
        return j;
    }

    /**
     * Returns the JSON phrases objects
     *
     * @return the JSON phrases objects
     */
    public Json getJsonPhrases() {
        Json j = new CatalogJson();
        // j.setItems(jsonPhrases.values().toArray(new Item[0]));
        j.setItems(toSortedArray(jsonPhrases.values()));
        return j;
    }

    public Json getJsonWord2SentenceIndx() {
        Json j = new Json();
        j.setItems(toSortedIndexArray(jsonWord2SentenceIndex.values()));
        return j;
    }

    public Json getFullJsonWord2SentenceIndx() {
        Json j = new Json();
        j.setItems(toSortedIndexArray(jsonWord2SentenceIndexWithSub.values()));
        return j;
    }

    /**
     * @return the basis_version
     */
    public Version getBasis_version() {
        return basis_version;
    }

    /**
     * @return the initMsg
     */
    public String getInitMsg() {
        return initMsg;
    }

    /**
     * Test if the indexes are serially numbered. This means, between the lowest
     * index (0) and the highest index is no gap.
     *
     * @return true if the index contract is valid.
     */
    private boolean isOrderContractValid() {
        return (isIdOrderValid(jsonSentences.keySet())) && (isIdOrderValid(jsonOptions.keySet())) && (isIdOrderValid(jsonPhrases.keySet()));
    }

    boolean isIdOrderValid(Collection<Integer> ids) {
        List<Integer> toSort = new ArrayList<Integer>(ids);

        Collections.sort(toSort);

        for (int test = 0; test < toSort.size(); test++) {

            if (test != toSort.get(test)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Initialize the whole option and phrase JSON strings.
     *
     * @param options
     * @throws IllegalArgumentException if the option language is not German.
     */
    HashMap<String, PhraseOption> initPhraseOptions(List<? extends PhraseOption> options) throws TextCatException {

        Map<String, String> arguments = new HashMap<String, String>();
        List<Phrase> phrases = new ArrayList<Phrase>();
        HashMap<String, PhraseOption> optionIdx = new HashMap<String, PhraseOption>();

        for (PhraseOption option : options) {
            // fill argument map to replace the phrase option no with the JSON
            // id inside the phrases.
            arguments.put(option.getName(), Integer.toString(converter.getOptionId(option)));
            OptionItem oi = JsonUtil.option2Json(option, phrases, converter);
            jsonOptions.put(oi.getId(), oi);
            // add to idx
            optionIdx.put(option.getName(), option);
        }

        jsonPhrases(phrases, arguments);
        return optionIdx;
    }

    void initSentences(List<? extends Sentence> sentences) {

        for (Sentence s : sentences) {
            SentenceItem si = JsonUtil.sentence2Json(s, converter);
            jsonSentences.put(si.getId(), si);

            if (!s.isJokerSentence()) {

                if (basis_version == null) {
                    basis_version = s.getBasisVersion();
                } else {
                    if (!basis_version.equals(s.getBasisVersion())) {
                        LogManager.getLogger(this.getClass()).warn("Init Textcat: Basis versions are not equal!");
                        initMsg = "WARN: Basis versions are not equal!";
                    }
                }
            }
        }
    }

    void jsonPhrases(List<Phrase> phrases, Map<String, String> arguments) {

        for (Phrase phrase : phrases) {
            PhraseItem pi = JsonUtil.phrase2Json(phrase, arguments, converter);
            jsonPhrases.put(pi.getId(), pi);
        }
    }

    void initWord2SentenceIndex(List<? extends Sentence> sentences, HashMap<String, PhraseOption> optionIdx) {

        StringBuilder sentenceIndexWords = new StringBuilder();
        HashMap<String, HashSet<String>> subOptionWordSets = new HashMap<String, HashSet<String>>();

        for (Sentence sentence : sentences) {

            HashMap<String, PhraseOption> subOptions = new HashMap<String, PhraseOption>();
            Iterator<SentenceStructureModule> sentenceModuleIt = sentence.moduleIterator();

            while (sentenceModuleIt.hasNext()) {
                SentenceStructureModule module = sentenceModuleIt.next();

                //Clesius
                sentenceIndexWords.append(module.getPhraseOptionHeader()).append(" ");

                /*if (sentence.getLanguage().name() == "it")
                    sentenceIndexWords.append(module.getPhraseOptionHeader_IT()).append(" ");
                else
                    sentenceIndexWords.append(module.getPhraseOptionHeader()).append(" ");
                */
                Iterator<Phrase> phraseIt = module.phraseIterator();

                while (phraseIt.hasNext()) {
                    Phrase phrase = phraseIt.next();

                    //Clesius
                    if (phrase != null) {
                        String value_phrase = phrase.getValue();
                        // remember sub phrase options
                        for (SubPhraseOptionName subPhraseOptionName : StringUtils.getSubPhraseOptionNames(phrase.getValue())) {
                            PhraseOption subPhraseOption = optionIdx.get(subPhraseOptionName.getPhraseOptionName());
                            if (subPhraseOption != null) {
                                subOptions.put(subPhraseOptionName.getPhraseOptionName(), subPhraseOption);

                                //Clesius
                                //replace header with header_it

                                if (sentence.getLanguage().name() == "it") {
                                    replaceAll(sentenceIndexWords, "{" + subPhraseOptionName.getPhraseOptionName() + "}", subPhraseOption.getHeader() );
                                    value_phrase = value_phrase.replace("{" + subPhraseOptionName.getPhraseOptionName() + "}", subPhraseOption.getHeader());
                                }

                            }
                        }

                        sentenceIndexWords.append(value_phrase).append(" ");
                    }
                    //else
                    //    LogManager.getLogger(this.getClass()).warn("Init Textcat: phrase NULL!");
                }
            }
            //Clesius
            //clean words
            String sentenceIndexWords$ = new String();
            sentenceIndexWords$ = sentenceIndexWords.toString();
            sentenceIndexWords$ = sentenceIndexWords$.replace("(", "");
            sentenceIndexWords$ = sentenceIndexWords$.replace(")", "");
            sentenceIndexWords$ = sentenceIndexWords$.replace("\"", "");
            sentenceIndexWords$ = sentenceIndexWords$.replace("_", " ");
            sentenceIndexWords$ = sentenceIndexWords$.replace("§", " ");

            HashSet<String> words = splitIntoWords(sentenceIndexWords$);

            Iterator<String> it = sentence.getKeywords().iterator();
            while (it.hasNext()) {
                words.add(it.next().toLowerCase());
            }

            it = sentence.getTopics().iterator();
            while (it.hasNext()) {
                words.add(it.next().toLowerCase());
            }

            // sentence name
            if (sentence.getName() != null)
                words.add(sentence.getName().toLowerCase()) ;

            // sentence header
            if (sentence.getHeader() != null)
                words.add(sentence.getHeader().toLowerCase()) ;

            sentenceIndexWords.setLength(0); // clear string builder

            //Clesius StopWordlist
            //https://github.com/stopwords-iso/stopwords-de/blob/master/stopwords-de.txt etc.

            //reduced
            //https://github.com/Alir3z4/stop-words/tree/bd8cc1434faeb3449735ed570a4a392ab5d35291

            String tmpStopWords = new String();
            switch (sentence.getLanguage().name()) {
                case "de":
                    //    tmpStopWords = "a,ab,aber,ach,acht,achte,achten,achter,achtes,ag,alle,allein,allem,allen,aller,allerdings,alles,allgemeinen,als,also,am,an,ander,andere,anderem,anderen,anderer,anderes,anderm,andern,anderr,anders,au,auch,auf,aus,ausser,ausserdem,außer,außerdem,b,bald,bei,beide,beiden,beim,beispiel,bekannt,bereits,besonders,besser,besten,bin,bis,bisher,bist,c,d,d.h,da,dabei,dadurch,dafür,dagegen,daher,dahin,dahinter,damals,damit,danach,daneben,dank,dann,daran,darauf,daraus,darf,darfst,darin,darum,darunter,darüber,das,dasein,daselbst,dass,dasselbe,davon,davor,dazu,dazwischen,daß,dein,deine,deinem,deinen,deiner,deines,dem,dementsprechend,demgegenüber,demgemäss,demgemäß,demselben,demzufolge,den,denen,denn,denselben,der,deren,derer,derjenige,derjenigen,dermassen,dermaßen,derselbe,derselben,des,deshalb,desselben,dessen,deswegen,dich,die,diejenige,diejenigen,dies,diese,dieselbe,dieselben,diesem,diesen,dieser,dieses,dir,doch,dort,drei,drin,dritte,dritten,dritter,drittes,du,durch,durchaus,durfte,durften,dürfen,dürft,e,eben,ebenso,ehrlich,ei,ei,,eigen,eigene,eigenen,eigener,eigenes,ein,einander,eine,einem,einen,einer,eines,einig,einige,einigem,einigen,einiger,einiges,einmal,eins,elf,en,ende,endlich,entweder,er,ernst,erst,erste,ersten,erster,erstes,es,etwa,etwas,euch,euer,eure,eurem,euren,eurer,eures,f,folgende,früher,fünf,fünfte,fünften,fünfter,fünftes,für,g,gab,ganz,ganze,ganzen,ganzer,ganzes,gar,gedurft,gegen,gegenüber,gehabt,gehen,geht,gekannt,gekonnt,gemacht,gemocht,gemusst,genug,gerade,gern,gesagt,geschweige,gewesen,gewollt,geworden,gibt,ging,gleich,gott,gross,grosse,grossen,grosser,grosses,groß,große,großen,großer,großes,gut,gute,guter,gutes,h,hab,habe,haben,habt,hast,hat,hatte,hatten,hattest,hattet,heisst,her,heute,hier,hin,hinter,hoch,hätte,hätten,i,ich,ihm,ihn,ihnen,ihr,ihre,ihrem,ihren,ihrer,ihres,im,immer,in,indem,infolgedessen,ins,irgend,ist,j,ja,jahr,jahre,jahren,je,jede,jedem,jeden,jeder,jedermann,jedermanns,jedes,jedoch,jemand,jemandem,jemanden,jene,jenem,jenen,jener,jenes,jetzt,k,kam,kann,kannst,kaum,kein,keine,keinem,keinen,keiner,keines,kleine,kleinen,kleiner,kleines,kommen,kommt,konnte,konnten,kurz,können,könnt,könnte,l,lang,lange,leicht,leide,lieber,los,m,machen,macht,machte,mag,magst,mahn,mal,man,manche,manchem,manchen,mancher,manches,mann,mehr,mein,meine,meinem,meinen,meiner,meines,mensch,menschen,mich,mir,mit,mittel,mochte,mochten,morgen,muss,musst,musste,mussten,muß,mußt,möchte,mögen,möglich,mögt,müssen,müsst,müßt,n,na,nach,nachdem,nahm,natürlich,neben,nein,neue,neuen,neun,neunte,neunten,neunter,neuntes,nicht,nichts,nie,niemand,niemandem,niemanden,noch,nun,nur,o,ob,oben,oder,offen,oft,ohne,ordnung,p,q,r,recht,rechte,rechten,rechter,rechtes,richtig,rund,s,sa,sache,sagt,sagte,sah,satt,schlecht,schluss,schon,sechs,sechste,sechsten,sechster,sechstes,sehr,sei,seid,seien,sein,seine,seinem,seinen,seiner,seines,seit,seitdem,selbst,sich,sie,sieben,siebente,siebenten,siebenter,siebentes,sind,so,solang,solche,solchem,solchen,solcher,solches,soll,sollen,sollst,sollt,sollte,sollten,sondern,sonst,soweit,sowie,später,startseite,statt,steht,suche,t,tag,tage,tagen,tat,teil,tel,tritt,trotzdem,tun,u,uhr,um,und,und?,uns,unse,unsem,unsen,unser,unsere,unserer,unses,unter,v,vergangenen,viel,viele,vielem,vielen,vielleicht,vier,vierte,vierten,vierter,viertes,vom,von,vor,w,wahr?,wann,war,waren,warst,wart,warum,was,weg,wegen,weil,weit,weiter,weitere,weiteren,weiteres,welche,welchem,welchen,welcher,welches,wem,wen,wenig,wenige,weniger,weniges,wenigstens,wenn,wer,werde,werden,werdet,weshalb,wessen,wie,wieder,wieso,will,willst,wir,wird,wirklich,wirst,wissen,wo,woher,wohin,wohl,wollen,wollt,wollte,wollten,worden,wurde,wurden,während,währenddem,währenddessen,wäre,würde,würden,x,y,z,z.b,zehn,zehnte,zehnten,zehnter,zehntes,zeit,zu,zuerst,zugleich,zum,zunächst,zur,zurück,zusammen,zwanzig,zwar,zwei,zweite,zweiten,zweiter,zweites,zwischen,zwölf,über,überhaupt,übrigens";
                    tmpStopWords = "aber,alle,allem,allen,aller,alles,als,also,am,an,ander,andere,anderem,anderen,anderer,anderes,anderm,andern,anders,auch,auf,aus,bei,bin,bis,bist,da,damit,dann,das,dass,dasselbe,dazu,daß,dein,deine,deinem,deinen,deiner,deines,dem,demselben,den,denn,denselben,der,derer,derselbe,derselben,des,desselben,dessen,dich,die,dies,diese,dieselbe,dieselben,diesem,diesen,dieser,dieses,dir,doch,dort,du,durch,ein,eine,einem,einen,einer,eines,einig,einige,einigem,einigen,einiger,einiges,einmal,er,es,etwas,euch,euer,eure,eurem,euren,eurer,eures,für,gegen,gewesen,hab,habe,haben,hat,hatte,hatten,hier,hin,hinter,ich,ihm,ihn,ihnen,ihr,ihre,ihrem,ihren,ihrer,ihres,im,in,indem,ins,ist,jede,jedem,jeden,jeder,jedes,jene,jenem,jenen,jener,jenes,jetzt,kann,kein,keine,keinem,keinen,keiner,keines,können,könnte,machen,man,manche,manchem,manchen,mancher,manches,mein,meine,meinem,meinen,meiner,meines,mich,mir,mit,muss,musste,nach,nicht,nichts,noch,nun,nur,ob,oder,ohne,sehr,sein,seine,seinem,seinen,seiner,seines,selbst,sich,sie,sind,so,solche,solchem,solchen,solcher,solches,soll,sollte,sondern,sonst,um,und,uns,unser,unsere,unserem,unseren,unserer,unseres,unter,viel,vom,von,vor,war,waren,warst,was,weg,weil,weiter,welche,welchem,welchen,welcher,welches,wenn,werde,werden,wie,wieder,will,wir,wird,wirst,wo,wollen,wollte,während,würde,würden,zu,zum,zur,zwar,zwischen,über";
                    break;
                case "it":
                    //   tmpStopWords = "a,abbastanza,abbia,abbiamo,abbiano,abbiate,accidenti,ad,adesso,affinche,agl,agli,ahime,ahimã¨,ahimè,ai,al,alcuna,alcuni,alcuno,all,alla,alle,allo,allora,altre,altri,altrimenti,altro,altrove,altrui,anche,ancora,anni,anno,ansa,anticipo,assai,attesa,attraverso,avanti,avemmo,avendo,avente,aver,avere,averlo,avesse,avessero,avessi,avessimo,aveste,avesti,avete,aveva,avevamo,avevano,avevate,avevi,avevo,avrai,avranno,avrebbe,avrebbero,avrei,avremmo,avremo,avreste,avresti,avrete,avrà,avrò,avuta,avute,avuti,avuto,basta,ben,bene,benissimo,berlusconi,brava,bravo,buono,c,casa,caso,cento,certa,certe,certi,certo,che,chi,chicchessia,chiunque,ci,ciascuna,ciascuno,cima,cinque,cio,cioe,cioã¨,cioè,circa,citta,città,cittã,ciã²,ciò,co,codesta,codesti,codesto,cogli,coi,col,colei,coll,coloro,colui,come,cominci,comprare,comunque,con,concernente,conciliarsi,conclusione,consecutivi,consecutivo,consiglio,contro,cortesia,cos,cosa,cosi,cosã¬,così,cui,d,da,dagl,dagli,dai,dal,dall,dalla,dalle,dallo,dappertutto,davanti,degl,degli,dei,del,dell,della,delle,dello,dentro,detto,deve,devo,di,dice,dietro,dire,dirimpetto,diventa,diventare,diventato,dopo,doppio,dov,dove,dovra,dovrà,dovrã,dovunque,due,dunque,durante,e,ebbe,ebbero,ebbi,ecc,ecco,ed,effettivamente,egli,ella,entrambi,eppure,era,erano,eravamo,eravate,eri,ero,esempio,esse,essendo,esser,essere,essi,ex,fa,faccia,facciamo,facciano,facciate,faccio,facemmo,facendo,facesse,facessero,facessi,facessimo,faceste,facesti,faceva,facevamo,facevano,facevate,facevi,facevo,fai,fanno,farai,faranno,fare,farebbe,farebbero,farei,faremmo,faremo,fareste,faresti,farete,farà,farò,fatto,favore,fece,fecero,feci,fin,finalmente,finche,fine,fino,forse,forza,fosse,fossero,fossi,fossimo,foste,fosti,fra,frattempo,fu,fui,fummo,fuori,furono,futuro,generale,gente,gia,giacche,giorni,giorno,giu,già,giã,gli,gliela,gliele,glieli,glielo,gliene,governo,grande,grazie,gruppo,ha,haha,hai,hanno,ho,i,ie,ieri,il,improvviso,in,inc,indietro,infatti,inoltre,insieme,intanto,intorno,invece,io,l,la,lasciato,lato,lavoro,le,lei,li,lo,lontano,loro,lui,lungo,luogo,là,lã,ma,macche,magari,maggior,mai,male,malgrado,malissimo,mancanza,marche,me,medesimo,mediante,meglio,meno,mentre,mesi,mezzo,mi,mia,mie,miei,mila,miliardi,milioni,minimi,ministro,mio,modo,molta,molti,moltissimo,molto,momento,mondo,mosto,nazionale,ne,negl,negli,nei,nel,nell,nella,nelle,nello,nemmeno,neppure,nessun,nessuna,nessuno,niente,no,noi,nome,non,nondimeno,nonostante,nonsia,nostra,nostre,nostri,nostro,novanta,nove,nulla,nuovi,nuovo,o,od,oggi,ogni,ognuna,ognuno,oltre,oppure,ora,ore,osi,ossia,ottanta,otto,paese,parecchi,parecchie,parecchio,parte,partendo,peccato,peggio,per,perche,perchã¨,perchè,perché,percio,perciã²,perciò,perfino,pero,persino,persone,perã²,però,piedi,pieno,piglia,piu,piuttosto,piã¹,più,po,pochissimo,poco,poi,poiche,possa,possedere,posteriore,posto,potrebbe,preferibilmente,presa,press,prima,primo,principalmente,probabilmente,promesso,proprio,puo,pure,purtroppo,puã²,può,qua,qualche,qualcosa,qualcuna,qualcuno,quale,quali,qualunque,quando,quanta,quante,quanti,quanto,quantunque,quarto,quasi,quattro,quel,quella,quelle,quelli,quello,quest,questa,queste,questi,questo,qui,quindi,quinto,realmente,recente,recentemente,registrazione,relativo,riecco,rispetto,salvo,sara,sarai,saranno,sarebbe,sarebbero,sarei,saremmo,saremo,sareste,saresti,sarete,sarà,sarã,sarò,scola,scopo,scorso,se,secondo,seguente,seguito,sei,sembra,sembrare,sembrato,sembrava,sembri,sempre,senza,sette,si,sia,siamo,siano,siate,siete,sig,solito,solo,soltanto,sono,sopra,soprattutto,sotto,spesso,srl,sta,stai,stando,stanno,starai,staranno,starebbe,starebbero,starei,staremmo,staremo,stareste,staresti,starete,starà,starò,stata,state,stati,stato,stava,stavamo,stavano,stavate,stavi,stavo,stemmo,stessa,stesse,stessero,stessi,stessimo,stesso,steste,stesti,stette,stettero,stetti,stia,stiamo,stiano,stiate,sto,su,sua,subito,successivamente,successivo,sue,sugl,sugli,sui,sul,sull,sulla,sulle,sullo,suo,suoi,tale,tali,talvolta,tanto,te,tempo,terzo,th,ti,titolo,torino,tra,tranne,tre,trenta,triplo,troppo,trovato,tu,tua,tue,tuo,tuoi,tutta,tuttavia,tutte,tutti,tutto,uguali,ulteriore,ultimo,un,una,uno,uomo,va,vai,vale,vari,varia,varie,vario,verso,vi,via,vicino,visto,vita,voi,volta,volte,vostra,vostre,vostri,vostro,ã¨,è";
                    tmpStopWords = "a,abbia,abbiamo,abbiano,abbiate,ad,adesso,agl,agli,ai,al,all,alla,alle,allo,allora,altre,altri,altro,anche,ancora,avemmo,avendo,avere,avesse,avessero,avessi,avessimo,aveste,avesti,avete,aveva,avevamo,avevano,avevate,avevi,avevo,avrai,avranno,avrebbe,avrebbero,avrei,avremmo,avremo,avreste,avresti,avrete,avrà,avrò,avuta,avute,avuti,avuto,c,che,chi,ci,coi,col,come,con,contro,cui,da,dagl,dagli,dai,dal,dall,dalla,dalle,dallo,degl,degli,dei,del,dell,della,delle,dello,dentro,di,dov,dove,e,ebbe,ebbero,ebbi,ecco,ed,era,erano,eravamo,eravate,eri,ero,essendo,faccia,facciamo,facciano,facciate,faccio,facemmo,facendo,facesse,facessero,facessi,facessimo,faceste,facesti,faceva,facevamo,facevano,facevate,facevi,facevo,fai,fanno,farai,faranno,fare,farebbe,farebbero,farei,faremmo,faremo,fareste,faresti,farete,farà,farò,fece,fecero,feci,fino,fosse,fossero,fossi,fossimo,foste,fosti,fra,fu,fui,fummo,furono,giù,gli,ha,hai,hanno,ho,i,il,in,io,l,la,le,lei,li,lo,loro,lui,ma,me,mi,mia,mie,miei,mio,ne,negl,negli,nei,nel,nell,nella,nelle,nello,no,noi,non,nostra,nostre,nostri,nostro,o,per,perché,però,più,pochi,poco,qua,quale,quanta,quante,quanti,quanto,quasi,quella,quelle,quelli,quello,questa,queste,questi,questo,qui,quindi,sarai,saranno,sarebbe,sarebbero,sarei,saremmo,saremo,sareste,saresti,sarete,sarà,sarò,se,sei,senza,si,sia,siamo,siano,siate,siete,sono,sopra,sotto,sta,stai,stando,stanno,starai,staranno,stare,starebbe,starebbero,starei,staremmo,staremo,stareste,staresti,starete,starà,starò,stava,stavamo,stavano,stavate,stavi,stavo,stemmo,stesse,stessero,stessi,stessimo,stesso,steste,stesti,stette,stettero,stetti,stia,stiamo,stiano,stiate,sto,su,sua,sue,sugl,sugli,sui,sul,sull,sulla,sulle,sullo,suo,suoi,te,ti,tra,tu,tua,tue,tuo,tuoi,tutti,tutto,un,una,uno,vai,vi,voi,vostra,vostre,vostri,vostro,è";
                    break;
                case "fr":
                    //tmpStopWords = "a,abord,absolument,afin,ah,ai,aie,aient,aies,ailleurs,ainsi,ait,allaient,allo,allons,allô,alors,anterieur,anterieure,anterieures,apres,après,as,assez,attendu,au,aucun,aucune,aucuns,aujourd,aujourd'hui,aupres,auquel,aura,aurai,auraient,aurais,aurait,auras,aurez,auriez,aurions,aurons,auront,aussi,autre,autrefois,autrement,autres,autrui,aux,auxquelles,auxquels,avaient,avais,avait,avant,avec,avez,aviez,avions,avoir,avons,ayant,ayez,ayons,b,bah,bas,basee,bat,beau,beaucoup,bien,bigre,bon,boum,bravo,brrr,c,car,ce,ceci,cela,celle,celle-ci,celle-là,celles,celles-ci,celles-là,celui,celui-ci,celui-là,celà,cent,cependant,certain,certaine,certaines,certains,certes,ces,cet,cette,ceux,ceux-ci,ceux-là,chacun,chacune,chaque,cher,chers,chez,chiche,chut,chère,chères,ci,cinq,cinquantaine,cinquante,cinquantième,cinquième,clac,clic,combien,comme,comment,comparable,comparables,compris,concernant,contre,couic,crac,d,da,dans,de,debout,dedans,dehors,deja,delà,depuis,dernier,derniere,derriere,derrière,des,desormais,desquelles,desquels,dessous,dessus,deux,deuxième,deuxièmement,devant,devers,devra,devrait,different,differentes,differents,différent,différente,différentes,différents,dire,directe,directement,dit,dite,dits,divers,diverse,diverses,dix,dix-huit,dix-neuf,dix-sept,dixième,doit,doivent,donc,dont,dos,douze,douzième,dring,droite,du,duquel,durant,dès,début,désormais,e,effet,egale,egalement,egales,eh,elle,elle-même,elles,elles-mêmes,en,encore,enfin,entre,envers,environ,es,essai,est,et,etant,etc,etre,eu,eue,eues,euh,eurent,eus,eusse,eussent,eusses,eussiez,eussions,eut,eux,eux-mêmes,exactement,excepté,extenso,exterieur,eûmes,eût,eûtes,f,fais,faisaient,faisant,fait,faites,façon,feront,fi,flac,floc,fois,font,force,furent,fus,fusse,fussent,fusses,fussiez,fussions,fut,fûmes,fût,fûtes,g,gens,h,ha,haut,hein,hem,hep,hi,ho,holà,hop,hormis,hors,hou,houp,hue,hui,huit,huitième,hum,hurrah,hé,hélas,i,ici,il,ils,importe,j,je,jusqu,jusque,juste,k,l,la,laisser,laquelle,las,le,lequel,les,lesquelles,lesquels,leur,leurs,longtemps,lors,lorsque,lui,lui-meme,lui-même,là,lès,m,ma,maint,maintenant,mais,malgre,malgré,maximale,me,meme,memes,merci,mes,mien,mienne,miennes,miens,mille,mince,mine,minimale,moi,moi-meme,moi-même,moindres,moins,mon,mot,moyennant,multiple,multiples,même,mêmes,n,na,naturel,naturelle,naturelles,ne,neanmoins,necessaire,necessairement,neuf,neuvième,ni,nombreuses,nombreux,nommés,non,nos,notamment,notre,nous,nous-mêmes,nouveau,nouveaux,nul,néanmoins,nôtre,nôtres,o,oh,ohé,ollé,olé,on,ont,onze,onzième,ore,ou,ouf,ouias,oust,ouste,outre,ouvert,ouverte,ouverts,o|,où,p,paf,pan,par,parce,parfois,parle,parlent,parler,parmi,parole,parseme,partant,particulier,particulière,particulièrement,pas,passé,pendant,pense,permet,personne,personnes,peu,peut,peuvent,peux,pff,pfft,pfut,pif,pire,pièce,plein,plouf,plupart,plus,plusieurs,plutôt,possessif,possessifs,possible,possibles,pouah,pour,pourquoi,pourrais,pourrait,pouvait,prealable,precisement,premier,première,premièrement,pres,probable,probante,procedant,proche,près,psitt,pu,puis,puisque,pur,pure,q,qu,quand,quant,quant-à-soi,quanta,quarante,quatorze,quatre,quatre-vingt,quatrième,quatrièmement,que,quel,quelconque,quelle,quelles,quelqu'un,quelque,quelques,quels,qui,quiconque,quinze,quoi,quoique,r,rare,rarement,rares,relative,relativement,remarquable,rend,rendre,restant,reste,restent,restrictif,retour,revoici,revoilà,rien,s,sa,sacrebleu,sait,sans,sapristi,sauf,se,sein,seize,selon,semblable,semblaient,semble,semblent,sent,sept,septième,sera,serai,seraient,serais,serait,seras,serez,seriez,serions,serons,seront,ses,seul,seule,seulement,si,sien,sienne,siennes,siens,sinon,six,sixième,soi,soi-même,soient,sois,soit,soixante,sommes,son,sont,sous,souvent,soyez,soyons,specifique,specifiques,speculatif,stop,strictement,subtiles,suffisant,suffisante,suffit,suis,suit,suivant,suivante,suivantes,suivants,suivre,sujet,superpose,sur,surtout,t,ta,tac,tandis,tant,tardive,te,tel,telle,tellement,telles,tels,tenant,tend,tenir,tente,tes,tic,tien,tienne,tiennes,tiens,toc,toi,toi-même,ton,touchant,toujours,tous,tout,toute,toutefois,toutes,treize,trente,tres,trois,troisième,troisièmement,trop,très,tsoin,tsouin,tu,té,u,un,une,unes,uniformement,unique,uniques,uns,v,va,vais,valeur,vas,vers,via,vif,vifs,vingt,vivat,vive,vives,vlan,voici,voie,voient,voilà,vont,vos,votre,vous,vous-mêmes,vu,vé,vôtre,vôtres,w,x,y,z,zut,à,â,ça,ès,étaient,étais,était,étant,état,étiez,étions,été,étée,étées,étés,êtes,être,ô";
                    tmpStopWords = "a,ai,aie,aient,aies,ait,alors,as,au,aucun,aura,aurai,auraient,aurais,aurait,auras,aurez,auriez,aurions,aurons,auront,aussi,autre,aux,avaient,avais,avait,avant,avec,avez,aviez,avions,avoir,avons,ayant,ayez,ayons,bon,car,ce,ceci,cela,ces,cet,cette,ceux,chaque,ci,comme,comment,d,dans,de,dedans,dehors,depuis,des,deux,devoir,devrait,devrez,devriez,devrions,devrons,devront,dois,doit,donc,dos,droite,du,dès,début,dù,elle,elles,en,encore,es,est,et,eu,eue,eues,eurent,eus,eusse,eussent,eusses,eussiez,eussions,eut,eux,eûmes,eût,eûtes,faire,fais,faisez,fait,faites,fois,font,force,furent,fus,fusse,fussent,fusses,fussiez,fussions,fut,fûmes,fût,fûtes,haut,hors,ici,il,ils,j,je,juste,l,la,le,les,leur,leurs,lui,là,m,ma,maintenant,mais,me,mes,moi,moins,mon,mot,même,n,ne,ni,nom,nommé,nommée,nommés,nos,notre,nous,nouveau,nouveaux,on,ont,ou,où,par,parce,parole,pas,personne,personnes,peu,peut,plupart,pour,pourquoi,qu,quand,que,quel,quelle,quelles,quels,qui,sa,sans,se,sera,serai,seraient,serais,serait,seras,serez,seriez,serions,serons,seront,ses,seulement,si,sien,soi,soient,sois,soit,sommes,son,sont,sous,soyez,soyons,suis,sujet,sur,t,ta,tandis,te,tellement,tels,tes,toi,ton,tous,tout,trop,très,tu,un,une,valeur,voient,vois,voit,vont,vos,votre,vous,vu,y,à,ça,étaient,étais,était,étant,état,étiez,étions,été,étés,êtes,être";
                    break;
                case "en":
                    //tmpStopWords = "'ll,'tis,'twas,'ve,a,a's,able,ableabout,about,above,abroad,abst,accordance,according,accordingly,across,act,actually,ad,added,adj,adopted,ae,af,affected,affecting,affects,after,afterwards,ag,again,against,ago,ah,ahead,ai,ain't,aint,al,all,allow,allows,almost,alone,along,alongside,already,also,although,always,am,amid,amidst,among,amongst,amoungst,amount,an,and,announce,another,any,anybody,anyhow,anymore,anyone,anything,anyway,anyways,anywhere,ao,apart,apparently,appear,appreciate,appropriate,approximately,aq,ar,are,area,areas,aren,aren't,arent,arise,around,arpa,as,aside,ask,asked,asking,asks,associated,at,au,auth,available,aw,away,awfully,az,b,ba,back,backed,backing,backs,backward,backwards,bb,bd,be,became,because,become,becomes,becoming,been,before,beforehand,began,begin,beginning,beginnings,begins,behind,being,beings,believe,below,beside,besides,best,better,between,beyond,bf,bg,bh,bi,big,bill,billion,biol,bj,bm,bn,bo,both,bottom,br,brief,briefly,bs,bt,but,buy,bv,bw,by,bz,c,c'mon,c's,ca,call,came,can,can't,cannot,cant,caption,case,cases,cause,causes,cc,cd,certain,certainly,cf,cg,ch,changes,ci,ck,cl,clear,clearly,click,cm,cmon,cn,co,co.,com,come,comes,computer,con,concerning,consequently,consider,considering,contain,containing,contains,copy,corresponding,could,could've,couldn,couldn't,couldnt,course,cr,cry,cs,cu,currently,cv,cx,cy,cz,d,dare,daren't,darent,date,de,dear,definitely,describe,described,despite,detail,did,didn,didn't,didnt,differ,different,differently,directly,dj,dk,dm,do,does,doesn,doesn't,doesnt,doing,don,don't,done,dont,doubtful,down,downed,downing,downs,downwards,due,during,dz,e,each,early,ec,ed,edu,ee,effect,eg,eh,eight,eighty,either,eleven,else,elsewhere,empty,end,ended,ending,ends,enough,entirely,er,es,especially,et,et-al,etc,even,evenly,ever,evermore,every,everybody,everyone,everything,everywhere,ex,exactly,example,except,f,face,faces,fact,facts,fairly,far,farther,felt,few,fewer,ff,fi,fifteen,fifth,fifty,fify,fill,find,finds,fire,first,five,fix,fj,fk,fm,fo,followed,following,follows,for,forever,former,formerly,forth,forty,forward,found,four,fr,free,from,front,full,fully,further,furthered,furthering,furthermore,furthers,fx,g,ga,gave,gb,gd,ge,general,generally,get,gets,getting,gf,gg,gh,gi,give,given,gives,giving,gl,gm,gmt,gn,go,goes,going,gone,good,goods,got,gotten,gov,gp,gq,gr,great,greater,greatest,greetings,group,grouped,grouping,groups,gs,gt,gu,gw,gy,h,had,hadn't,hadnt,half,happens,hardly,has,hasn,hasn't,hasnt,have,haven,haven't,havent,having,he,he'd,he'll,he's,hed,hell,hello,help,hence,her,here,here's,hereafter,hereby,herein,heres,hereupon,hers,herself,herse”,hes,hi,hid,high,higher,highest,him,himself,himse”,his,hither,hk,hm,hn,home,homepage,hopefully,how,how'd,how'll,how's,howbeit,however,hr,ht,htm,html,http,hu,hundred,i,i'd,i'll,i'm,i've,i.e.,id,ie,if,ignored,ii,il,ill,im,immediate,immediately,importance,important,in,inasmuch,inc,inc.,indeed,index,indicate,indicated,indicates,information,inner,inside,insofar,instead,int,interest,interested,interesting,interests,into,invention,inward,io,iq,ir,is,isn,isn't,isnt,it,it'd,it'll,it's,itd,itll,its,itself,itse”,ive,j,je,jm,jo,join,jp,just,k,ke,keep,keeps,kept,keys,kg,kh,ki,kind,km,kn,knew,know,known,knows,kp,kr,kw,ky,kz,l,la,large,largely,last,lately,later,latest,latter,latterly,lb,lc,least,length,less,lest,let,let's,lets,li,like,liked,likely,likewise,line,little,lk,ll,long,longer,longest,look,looking,looks,low,lower,lr,ls,lt,ltd,lu,lv,ly,m,ma,made,mainly,make,makes,making,man,many,may,maybe,mayn't,maynt,mc,md,me,mean,means,meantime,meanwhile,member,members,men,merely,mg,mh,microsoft,might,might've,mightn't,mightnt,mil,mill,million,mine,minus,miss,mk,ml,mm,mn,mo,more,moreover,most,mostly,move,mp,mq,mr,mrs,ms,msie,mt,mu,much,mug,must,must've,mustn't,mustnt,mv,mw,mx,my,myself,myse”,mz,n,na,name,namely,nay,nc,nd,ne,near,nearly,necessarily,necessary,need,needed,needing,needn't,neednt,needs,neither,net,netscape,never,neverf,neverless,nevertheless,new,newer,newest,next,nf,ng,ni,nine,ninety,nl,no,no-one,nobody,non,none,nonetheless,noone,nor,normally,nos,not,noted,nothing,notwithstanding,novel,now,nowhere,np,nr,nu,null,number,numbers,nz,o,obtain,obtained,obviously,of,off,often,oh,ok,okay,old,older,oldest,om,omitted,on,once,one,one's,ones,only,onto,open,opened,opening,opens,opposite,or,ord,order,ordered,ordering,orders,org,other,others,otherwise,ought,oughtn't,oughtnt,our,ours,ourselves,out,outside,over,overall,owing,own,p,pa,page,pages,part,parted,particular,particularly,parting,parts,past,pe,per,perhaps,pf,pg,ph,pk,pl,place,placed,places,please,plus,pm,pmid,pn,point,pointed,pointing,points,poorly,possible,possibly,potentially,pp,pr,predominantly,present,presented,presenting,presents,presumably,previously,primarily,probably,problem,problems,promptly,proud,provided,provides,pt,put,puts,pw,py,q,qa,que,quickly,quite,qv,r,ran,rather,rd,re,readily,really,reasonably,recent,recently,ref,refs,regarding,regardless,regards,related,relatively,research,reserved,respectively,resulted,resulting,results,right,ring,ro,room,rooms,round,ru,run,rw,s,sa,said,same,saw,say,saying,says,sb,sc,sd,se,sec,second,secondly,seconds,section,see,seeing,seem,seemed,seeming,seems,seen,sees,self,selves,sensible,sent,serious,seriously,seven,seventy,several,sg,sh,shall,shan't,shant,she,she'd,she'll,she's,shed,shell,shes,should,should've,shouldn,shouldn't,shouldnt,show,showed,showing,shown,showns,shows,si,side,sides,significant,significantly,similar,similarly,since,sincere,site,six,sixty,sj,sk,sl,slightly,sm,small,smaller,smallest,sn,so,some,somebody,someday,somehow,someone,somethan,something,sometime,sometimes,somewhat,somewhere,soon,sorry,specifically,specified,specify,specifying,sr,st,state,states,still,stop,strongly,su,sub,substantially,successfully,such,sufficiently,suggest,sup,sure,sv,sy,system,sz,t,t's,take,taken,taking,tc,td,tell,ten,tends,test,text,tf,tg,th,than,thank,thanks,thanx,that,that'll,that's,that've,thatll,thats,thatve,the,their,theirs,them,themselves,then,thence,there,there'd,there'll,there're,there's,there've,thereafter,thereby,thered,therefore,therein,therell,thereof,therere,theres,thereto,thereupon,thereve,these,they,they'd,they'll,they're,they've,theyd,theyll,theyre,theyve,thick,thin,thing,things,think,thinks,third,thirty,this,thorough,thoroughly,those,thou,though,thoughh,thought,thoughts,thousand,three,throug,through,throughout,thru,thus,til,till,tip,tis,tj,tk,tm,tn,to,today,together,too,took,top,toward,towards,tp,tr,tried,tries,trillion,truly,try,trying,ts,tt,turn,turned,turning,turns,tv,tw,twas,twelve,twenty,twice,two,tz,u,ua,ug,uk,um,un,under,underneath,undoing,unfortunately,unless,unlike,unlikely,until,unto,up,upon,ups,upwards,us,use,used,useful,usefully,usefulness,uses,using,usually,uucp,uy,uz,v,va,value,various,vc,ve,versus,very,vg,vi,via,viz,vn,vol,vols,vs,vu,w,want,wanted,wanting,wants,was,wasn,wasn't,wasnt,way,ways,we,we'd,we'll,we're,we've,web,webpage,website,wed,welcome,well,wells,went,were,weren,weren't,werent,weve,wf,what,what'd,what'll,what's,what've,whatever,whatll,whats,whatve,when,when'd,when'll,when's,whence,whenever,where,where'd,where'll,where's,whereafter,whereas,whereby,wherein,wheres,whereupon,wherever,whether,which,whichever,while,whilst,whim,whither,who,who'd,who'll,who's,whod,whoever,whole,wholl,whom,whomever,whos,whose,why,why'd,why'll,why's,widely,width,will,willing,wish,with,within,without,won,won't,wonder,wont,words,work,worked,working,works,world,would,would've,wouldn,wouldn't,wouldnt,ws,www,x,y,ye,year,years,yes,yet,you,you'd,you'll,you're,you've,youd,youll,young,younger,youngest,your,youre,yours,yourself,yourselves,youve,yt,yu,z,za,zero,zm,zr";
                    tmpStopWords = "a,about,above,after,again,against,all,am,an,and,any,are,aren't,as,at,be,because,been,before,being,below,between,both,but,by,can't,cannot,could,couldn't,did,didn't,do,does,doesn't,doing,don't,down,during,each,few,for,from,further,had,hadn't,has,hasn't,have,haven't,having,he,he'd,he'll,he's,her,here,here's,hers,herself,him,himself,his,how,how's,i,i'd,i'll,i'm,i've,if,in,into,is,isn't,it,it's,its,itself,let's,me,more,most,mustn't,my,myself,no,nor,not,of,off,on,once,only,or,other,ought,our,ours,ourselves,out,over,own,same,shan't,she,she'd,she'll,she's,should,shouldn't,so,some,such,than,that,that's,the,their,theirs,them,themselves,then,there,there's,these,they,they'd,they'll,they're,they've,this,those,through,to,too,under,until,up,very,was,wasn't,we,we'd,we'll,we're,we've,were,weren't,what,what's,when,when's,where,where's,which,while,who,who's,whom,why,why's,with,won't,would,wouldn't,you,you'd,you'll,you're,you've,your,yours,yourself,yourselves";
                    break;
                default:
                    tmpStopWords = "";
                    break;
            }

            List<String> stopWords = new ArrayList<String>(Arrays.asList(tmpStopWords.split(",")));
            stopWords.add(",");
            stopWords.add(":");
            stopWords.add("-");
            stopWords.add(".");
            stopWords.add("-,");
            stopWords.add("<br/>");
            stopWords.add("+++");

            String dbg;

            for (String w : words) {
                if (!stopWords.contains(w)) {

                    if (jsonWord2SentenceIndex.containsKey(w)) {
                        jsonWord2SentenceIndex.get(w).addSentenceId(converter.getSentenceId(sentence));
                    } else {
                        IndexItem item = new IndexItem();
                        item.setWord(w);
                        item.addSentenceId(converter.getSentenceId(sentence));
                        jsonWord2SentenceIndex.put(w, item);
                    }

                    if (jsonWord2SentenceIndexWithSub.containsKey(w)) {
                        jsonWord2SentenceIndexWithSub.get(w).addSentenceId(converter.getSentenceId(sentence));
                    } else {
                        IndexItem item = new IndexItem();
                        item.setWord(w);
                        item.addSentenceId(converter.getSentenceId(sentence));
                        jsonWord2SentenceIndexWithSub.put(w, item);
                    }

                }
            }

            // handle Subphrases
            words = new HashSet<String>();
            for (String phraseOptionName : subOptions.keySet()) {
                if (!subOptionWordSets.containsKey(phraseOptionName)) {

                    PhraseOption o = subOptions.get(phraseOptionName);
                    Iterator<Phrase> phraseIt = o.phraseIterator(Constants.FIRST_ITEM_PART_NO);
                    StringBuilder values = new StringBuilder();
                    values = new StringBuilder(o.getHeader()).append(" ");

                    while (phraseIt.hasNext()) {
                        String ww= phraseIt.next().getValue();
                        // sub sub phrase
                        if (ww.contains("{")){
                            List<String> listvalues2=new ArrayList<String>();
                            for (String ssp : splitIntoWordsNoLower(ww)){

                                if (ssp.contains("{")){
                                    ssp=ssp.replace("{","").replace("}","");
                                    if (subOptionWordSets.containsKey(ssp)) {

                                        if (subOptionWordSets.get(phraseOptionName)!=null)
                                            subOptionWordSets.get(phraseOptionName).addAll(subOptionWordSets.get(ssp));
                                        else
                                            subOptionWordSets.put(phraseOptionName,subOptionWordSets.get(ssp));

                                        words.addAll(subOptionWordSets.get(ssp));
                                    }else{
                                        PhraseOption o2 = optionIdx.get(ssp);
                                        if (o2!=null){
                                            Iterator<Phrase> phraseIt2 = o2.phraseIterator(Constants.FIRST_ITEM_PART_NO);
                                            listvalues2.add(o2.getHeader());
                                            while (phraseIt2.hasNext()) {
                                                String ww2= phraseIt2.next().getValue();

                                                // sub sub sub phrase NEVER USED
                                                if (ww2.contains("{")){
                                                    List<String> listvalues3=new ArrayList<String>();
                                                    for (String sssp : splitIntoWordsNoLower(ww2)){
                                                        if (sssp.contains("{")){
                                                            sssp=sssp.replace("{","").replace("}","");
                                                            if (subOptionWordSets.containsKey(sssp)) {

                                                                if (subOptionWordSets.get(phraseOptionName)!=null)
                                                                    subOptionWordSets.get(phraseOptionName).addAll(subOptionWordSets.get(sssp));
                                                                else
                                                                    subOptionWordSets.put(phraseOptionName,subOptionWordSets.get(sssp));

                                                                words.addAll(subOptionWordSets.get(sssp));
                                                            }else{
                                                                PhraseOption o3 = optionIdx.get(sssp);
                                                                if (o3!=null){
                                                                    Iterator<Phrase> phraseIt3 = o3.phraseIterator(Constants.FIRST_ITEM_PART_NO);
                                                                    listvalues3.add(o3.getHeader());
                                                                    while (phraseIt3.hasNext()) {
                                                                        String ww3= phraseIt3.next().getValue();
                                                                        ww3=ww3.toLowerCase();
                                                                        listvalues3.add(ww3);
                                                                        values.append(ww3).append(" ");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (listvalues3.size()>0)
                                                        words.addAll(listvalues3);
                                                }
                                                ww2=ww2.toLowerCase();
                                                if (subOptionWordSets.get(phraseOptionName)!=null)
                                                    subOptionWordSets.get(phraseOptionName).addAll(splitIntoWords(ww2));
                                                else
                                                    subOptionWordSets.put(phraseOptionName, splitIntoWords(ww2));


                                                listvalues2.add(ww2);
                                                values.append(ww2).append(" ");
                                            }
                                        }
                                    }
                                }
                            }
                            if (listvalues2.size()>0)
                                words.addAll(listvalues2);
                        }
                        ww=ww.toLowerCase();
                        values.append(ww).append(" ");

                    }

                    //Clesisus
                    //clean words
                    String values$ = values.toString();
                    values$ = values$.replace("(", "");
                    values$ = values$.replace(")", "");
                    values$ = values$.replace("\"", "");
                    values$ = values$.replace("_", " ");
                    values$ = values$.replace("§", " ");

                    if (subOptionWordSets.get(phraseOptionName)!=null)
                        subOptionWordSets.get(phraseOptionName).addAll(splitIntoWords(values$));
                    else
                        subOptionWordSets.put(phraseOptionName, splitIntoWords(values$));
                }

                words.addAll(subOptionWordSets.get(phraseOptionName));
            }
            for (String w : words) {
                w=w.replace("{","").replace("}","");
                w=w.toLowerCase();
                List<String> singleWords = new ArrayList<String>(Arrays.asList(w.split(" ")));
                for (String ww : singleWords) {
                    ww = ww .replaceAll("\"","");
                    ww = ww .trim();
                    if (!stopWords.contains(ww) && !ww.isEmpty()) {
                        if (jsonWord2SentenceIndexWithSub.containsKey(ww)) {
                            jsonWord2SentenceIndexWithSub.get(ww).addSentenceId(converter.getSentenceId(sentence));
                        } else {
                            IndexItem item = new IndexItem();
                            item.setWord(ww);
                            item.addSentenceId(converter.getSentenceId(sentence));
                            jsonWord2SentenceIndexWithSub.put(ww, item);
                        }
                    }
                }
            }
        }

    }

    /**
     * Split the given strings into its words.
     *
     * The returning word set contains each word only one times in lower case.
     *
     * @param s the string to split
     * @return the word set
     */
    HashSet<String> splitIntoWords(String s) {
        HashSet<String> ret = new HashSet<String>();
        // normalise 2 lower case
        s = s.toLowerCase();
        Scanner sc = new Scanner(s);
        sc.useDelimiter("[\\s\\.\\?\\!,\\{\\}]");

        while (sc.hasNext()) {
            String word = sc.next();
            //Clesius
            word = word.trim();
            word = word.replaceAll("\"","");
            if (!word.isEmpty()) {
                ret.add(word);
            }
        }
        sc.close();
        return ret;
    }

    HashSet<String> splitIntoWordsNoLower(String s) {
        HashSet<String> ret = new HashSet<String>();
        Scanner sc = new Scanner(s);
        sc.useDelimiter("[\\s\\.\\?\\!]");

        while (sc.hasNext()) {
            String word = sc.next();
            //Clesius
            word = word.trim();
            word = word.replaceAll("\"","");
            if (!word.isEmpty()) {
                ret.add(word);
            }
        }
        sc.close();
        return ret;
    }

    private Item[] toSortedArray(Collection<? extends AbstractCatalogItem> c) {

        List<AbstractCatalogItem> toSort = new ArrayList<AbstractCatalogItem>(c);
        toSort.sort(Comparator.comparingInt(AbstractCatalogItem::getId));
        return toSort.toArray(new Item[0]);
    }

    private Item[] toSortedIndexArray(Collection<? extends IndexItem> c) {

        List<IndexItem> toSort = new ArrayList<IndexItem>(c);
        toSort.sort(Comparator.comparing(IndexItem::getWord));
        return toSort.toArray(new Item[0]);
    }

    private static void replaceAll(StringBuilder builder, String from, String to) {
        int index = builder.indexOf(from);
        while (index != -1) {
            builder.replace(index, index + from.length(), to);
            index += to.length(); // Move to the end of the replacement
            index = builder.indexOf(from, index);
        }
    }
}
