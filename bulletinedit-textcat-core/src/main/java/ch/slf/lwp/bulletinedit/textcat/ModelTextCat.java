package ch.slf.lwp.bulletinedit.textcat;

import java.util.List;

import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

public class ModelTextCat implements TextCat {

	private Language language;
	private DomainName domainName;
	private Version basisVersion;
	private Version version;

	private List<? extends PhraseOption> options;
	private List<? extends Sentence> sentences;

	public void setLanguage(Language language) {
		this.language = language;
	}

	public void setDomainName(DomainName domainName) {
		this.domainName = domainName;
	}

	public void setBasisVersion(Version basisVersion) {
		this.basisVersion = basisVersion;
	}

	public void setVersion(Version version) {
		this.version = version;
	}

	@Override
	public Language getLanguage() {
		return this.language;
	}

	@Override
	public DomainName getDomainName() {
		return this.domainName;
	}

	@Override
	public void init(List<? extends Sentence> sentences, List<? extends PhraseOption> options) throws TextCatException {
		this.sentences = sentences;
		this.options = options;
	}

	public Version getBasisVersion() {
		return this.basisVersion;
	}

	public List<? extends PhraseOption> getPhraseOptions() {
		return this.options;
	}

	public List<? extends Sentence> getSentences() {
		return this.sentences;
	}

	public Version getVersion() {
		return this.version;
	}
	
	@Override
	public String toString() {
		return "ModelTextCat:\n\tUsed domain name: " + domainName + "\n\tUsed import language: " + language + "\n\tUsed version: " + version.toString() + "\n\tUsed basis version: " + basisVersion.toString();
	}


}
