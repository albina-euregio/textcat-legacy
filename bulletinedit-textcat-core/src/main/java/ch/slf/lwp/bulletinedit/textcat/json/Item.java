package ch.slf.lwp.bulletinedit.textcat.json;

/**
 * Marker interface for JSON items.
 * 
 * @author weiss
 */
public interface Item {
};
