package ch.slf.lwp.bulletinedit.textcat.text;

public interface TextFragment {

	/**
	 * Returns the text of this fragment. All place holders of this element are
	 * replaced. If there is no text available, an empty string will be
	 * returned;
	 * 
	 * @return the text of this fragment.
	 */
	public abstract String getText();

	/**
	 * If true, the punctuation before must be removed.
	 * 
	 * @return punctuation removal flag
	 */
	public abstract boolean isRemovePunctuationBefore();

	/**
	 * If false, there is no white space between this fragment and the next
	 * fragment, even if the next fragment {@link #isSpaceBefore()} returns
	 * true.
	 * 
	 * @return the space after flag
	 */
	public abstract boolean isSpaceAfter();

	/**
	 * If false, there is no white space between this fragment and the last
	 * fragment, even if the last element {@link #isSpaceAfter()} returns true.
	 * 
	 * @return the space after flag
	 */
	public abstract boolean isSpaceBefore();

}