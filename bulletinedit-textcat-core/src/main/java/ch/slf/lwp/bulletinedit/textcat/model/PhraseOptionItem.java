package ch.slf.lwp.bulletinedit.textcat.model;

import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

public interface PhraseOptionItem {

	/**
	 * @return the itemNo
	 */
	public abstract int getItemNo();

	/**
	 * @param itemNo
	 *            the itemNo to set
	 */
	public abstract void setItemNo(int itemNo);

	/**
	 * @return the deleted
	 */
	public abstract boolean isDeleted();

	/**
	 * @param deleted
	 *            the deleted to set
	 */
	public abstract void setDeleted(boolean deleted);

	/**
	 * @return the version
	 */
	public abstract Version getVersion();

	/**
	 * @param version
	 *            the version to set
	 */
	public abstract void setVersion(Version version);

	/**
	 * @return the basisVersion
	 */
	public abstract Version getBasisVersion();

	/**
	 * @param basisVersion
	 *            the basisVersion to set
	 */
	public abstract void setBasisVersion(Version basisVersion);

	/**
	 * returns the phrase of the given part
	 * 
	 * @param itemPartNo
	 *            the item part no
	 * @return the phrase
	 */
	public abstract Phrase getPhrase(int itemPartNo);
	
	/**
	 * Create and add a new empty phrase.
	 * 
	 * @return the created phrase
	 */
	public abstract Phrase createPhrase();

	/**
	 * Append the new phrase
	 * @param phrase the phrase to append
	 */
	public abstract void appendPhrase(Phrase phrase);

}