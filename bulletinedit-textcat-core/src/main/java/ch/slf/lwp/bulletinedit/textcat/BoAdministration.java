package ch.slf.lwp.bulletinedit.textcat;

import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

public interface BoAdministration {

	/**
	 * Returns the multiple usable phrase option.
	 * 
	 * @param phraseOptionName the phrase option name
	 * @param language the language
	 * @param baseVersion the texcat version
	 * @param domainName the domain name
	 * @return the phrase option, if found or <code>null</code> otherwise.
	 * @throws TextCatException if an error occurred.
	 */
	@Deprecated
	public abstract PhraseOption getMultiplePhraseOption(String phraseOptionName, Language language, Version baseVersion, DomainName domainName) throws TextCatException;

	/**
	 * Update the TextCat.
	 * If the base version of the new TextCat equals the given one, an exception
	 * is thrown.
	 * 
	 * @param previousBasisVersion the TextCat version which should be updated
	 * @param textCats the new TextCat versions
	 * @throws TextCatException if an error occurred.
	 */
	public abstract void updateTextCat(Version previousBasisVersion, ModelTextCat... textCats) throws TextCatException;

	/**
	 * Replace the TextCat.
	 * If the base version of the new TextCat equals the given one, an exception
	 * is thrown.
	 * 
	 * @param previousBasisVersion the TextCat version which should be replaced
	 * @param textCats the new TextCat versions
	 * @throws TextCatException if an error occurred.
	 */
	public abstract void replaceTextCat(Version previousBasisVersion, ModelTextCat... textCats) throws TextCatException;

	/**
	 * Delete the TextCat.
	 * If the base version of the new TextCat equals the given one, an exception
	 * is thrown.
	 *
	 * @param baseVersion the TextCat version which should be replaced
	 * @param domainName the new TextCat versions
	 * @throws TextCatException if an error occurred.
	 */
	public abstract void deleteDomain(Version baseVersion, DomainName domainName) throws TextCatException;

}