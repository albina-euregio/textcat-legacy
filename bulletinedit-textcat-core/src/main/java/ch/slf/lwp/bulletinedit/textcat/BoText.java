package ch.slf.lwp.bulletinedit.textcat;

import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.json.JsonTextCat;
import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.JokerSentence;

import java.util.Map;

public interface BoText {

	/**
	 * Create the text represented by the given text definition.
	 * 
	 * @param textDefinitionString
	 *            the text definition as string
	 * @param targetLanguage
	 *            the required text language
	 * @param domainName
	 *            the domain name
	 * @return the created text
	 * @throws TextCatException
	 *             if an error occurred.
	 */
	public abstract String buildText(String textDefinitionString, Language targetLanguage, DomainName domainName) throws TextCatException;

	
	/**
	 * Create the static application text represented by the given application text definition.
	 * 
	 * @param appTextDefinitionString
	 *            the application text definition as string
	 * @param targetLanguage
	 *            the required text language
	 * @param domainName
	 *            the domain name
	 * @return the created text
	 * @throws TextCatException
	 *             if an error occurred.
	 */
	public abstract String buildAppText(String appTextDefinitionString, Language targetLanguage, DomainName domainName) throws TextCatException;

	
	/**
	 * Returns the length of the created text.
	 * 
	 * @param textDefinitionString
	 *            the text definition as string
	 * @param targetLanguage
	 *            the required text language
	 * @param domainName
	 *            the domain name
	 * @return the created text
	 * @throws TextCatException
	 *             if an error occurred.
	 */
	public abstract int getTextLength(String textDefinitionString, Language targetLanguage, DomainName domainName) throws TextCatException;

	/**
	 * Returns the max length of the created text.
	 * 
	 * @param textDefinitionString
	 *            the text definition as string
	 * @param targetLanguage
	 *            the required text language
	 * @param domainName
	 *            the domain name
	 * @return the created text
	 * @throws TextCatException
	 *             if an error occurred.
	 */
	public abstract int getMaxTextLength(String textDefinitionString, DomainName domainName) throws TextCatException;

	/**
	 * Add a joker sentence to the sentence catalog.
	 * 
	 * @param jokerSentence
	 *            the joker sentence
	 * @return the added joker sentence
	 * @throws TextCatException
	 *             if an error occurred.
	 */
	public abstract Map<Language, Sentence> addJokerSentence(JokerSentence jokerSentence, DomainName domainName) throws TextCatException;

	/**
	 * Create a JSON sentence catalog representation.
	 * 
	 * @return the JSON sentence catalog representation.
	 * @throws TextCatException
	 *             if an error occurred.
	 */
	public abstract JsonTextCat getJsonTextCatalog(Language language, DomainName domainName) throws TextCatException;

	/**
	 * Reload the JSON sentence catalog representation.
	 * 
	 * If the jsonTextCat parameter is null, a new JsonTextCat will be returned,
	 * otherwise a new, modified catalog will be returned. The given catalog
	 * keep untouched, however, the id mapping between the DB and the JSON
	 * inside the new catalog are still valid.
	 * 
	 * The reload will fail, if a something is missing and the order JSON order
	 * contract is injured. For example, if a sentence is deleted.
	 * 
	 * @param jsonTextCat
	 *            the old JSON TextCat
	 * @return the reloaded catalog
	 * @throws TextCatException
	 */
	public abstract JsonTextCat reloadJsonTextCatalog(JsonTextCat jsonTextCat,  Language language, DomainName domainName) throws TextCatException;

}