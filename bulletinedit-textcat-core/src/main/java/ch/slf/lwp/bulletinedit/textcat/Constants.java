package ch.slf.lwp.bulletinedit.textcat;

/**
 * This class contains the text cat constants.
 * 
 * @author weiss
 */
public final class Constants {

	public static final int FIRST_ITEM_PART_NO = 1;
	public static final int FIRST_ITEM_NO = 1;
	public static final int FIRST_MODULE_NO = 1;
	public static final int DOES_NOT_CONTAINS = -1;

	public static final int FAKE_ID_INCREMENT = 100000;
	public static final int SECOND_ITEM_PART_NO = 2;

	/**
	 * Private constructor to avoid instantiation
	 */
	private Constants() {}
}
