package ch.slf.lwp.bulletinedit.textcat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.slf.lwp.bulletinedit.textcat.model.util.SubPhraseOptionName;

/**
 * Utility methods for String operations.
 * 
 * @author gw
 */
public class StringUtils {

	/**
	 * Pattern which match a sub phrase option name inside a phrase value. The
	 * pattern include the start and end sequence.
	 * 
	 * The expression extract two matches. The first group is the phrase option
	 * name and the second the pos number.
	 */
	private static final Pattern SUB_PHRASE_OPTION_PATTERN = Pattern.compile("\\{(.+?)(?:#(.+?))?\\}");

	/**
	 * Indicate the start of a sub phrase option name inside the phrase
	 */
	private final static String SUB_PHRASE_OPTION_START = "{";

	/**
	 * Indicate the end of a sub phrase option name inside the phrase
	 */
	private final static String SUB_PHRASE_OPTION_END = "}";

	// phrase option number
	/**
	 * Separator between the parent no and the sub phrase option number part of
	 * single usage phrase option
	 */
	private final static String PHRASEOPTION_PARENT_SEP = "§";

	/**
	 * Separator between the parent no and the phrase option number part of
	 * single usage phrase option
	 */
	private final static String PHRASEOPTION_SEP = "@";

	/**
	 * avoid instantiation
	 */
	private StringUtils() {}

	/**
	 * Insert the sub phrase option value.
	 * 
	 * @param phraseValue
	 *            the phrase option value
	 * @param replacements
	 *            the mapping
	 * @return the phrase value with the inserted values
	 * @throws IllegalStateException
	 *             if phraseValue is null
	 */
	public static String insertSubPhraseOptionValue(String phraseValue, Map<SubPhraseOptionName, String> replacements) {
		if (phraseValue == null) {
			throw new IllegalArgumentException("Given phrase value is 'NULL'");
		}

		if (phraseValue.indexOf(SUB_PHRASE_OPTION_START) > -1) {

			Matcher m = SUB_PHRASE_OPTION_PATTERN.matcher(phraseValue);
			StringBuffer sb = new StringBuffer();

			while (m.find()) {

				SubPhraseOptionName spon = new SubPhraseOptionName();
				spon.setPhraseOptionNameWithItemPartNo(m.group(1));

				if (m.group(2) != null) {
					spon.setPosNo(Integer.parseInt(m.group(2)));
				}

				String replacement = replacements.get(spon);

				if (replacement == null) {
					replacement = m.group();
				}

				m.appendReplacement(sb, replacement);

			}

			m.appendTail(sb);
			phraseValue = sb.toString();
		}

		return phraseValue;
	}

	/**
	 * Extract all sub phrase option names. The order of the returning list is
	 * equal to the order of the occurrence inside the phrase value.
	 * 
	 * @param phraseValue
	 *            the phrase value
	 * @return the extracted sub phrase option names
	 * @throws IllegalStateException
	 *             if phraseValue is null
	 */
	public static List<SubPhraseOptionName> getSubPhraseOptionNames(String phraseValue) {
		if (phraseValue == null) {
			throw new IllegalArgumentException("Given phrase value is 'NULL'");
		}

		ArrayList<SubPhraseOptionName> subPhraseOptionNames = new ArrayList<SubPhraseOptionName>();

		if (phraseValue.indexOf(SUB_PHRASE_OPTION_START) > -1) {

			Matcher m = SUB_PHRASE_OPTION_PATTERN.matcher(phraseValue);

			while (m.find()) {

				SubPhraseOptionName spon = new SubPhraseOptionName();
				spon.setPhraseOptionNameWithItemPartNo(m.group(1));

				if (m.group(2) != null) {
					spon.setPosNo(Integer.parseInt(m.group(2)));
				}

				subPhraseOptionNames.add(spon);
			}
		}

		return subPhraseOptionNames;
	}

	/**
	 * Replace the phrase option names inside the phrase value with the given
	 * replacements. A pos number will be added if none exists. If the mapping
	 * does not contains a replacement for the phrase option name, nothing will
	 * be done with this entry.
	 * 
	 * @param phraseValue
	 *            the phrase value.
	 * @param replacements
	 *            the mapping phrase option name to replacement.
	 * @return the new phrase value.
	 */
	public static String replacePhraseOptionNames(String phraseValue, Map<String, String> replacements) {
		if (phraseValue == null) {
			throw new IllegalArgumentException("Given phrase value is 'NULL'");
		}

		if (phraseValue.indexOf(SUB_PHRASE_OPTION_START) > -1) {

			Matcher m = SUB_PHRASE_OPTION_PATTERN.matcher(phraseValue);
			StringBuffer sb = new StringBuffer();

			while (m.find()) {

				String phraseOptionName = m.group(1);
				String posNo = m.group(2);
				String replacement = replacements.get(phraseOptionName);

				if (replacement != null) {

					if (posNo == null) {
						posNo = Integer.toString(SubPhraseOptionName.SUB_PHRASEOPTION_FIRST_OCCURRENCE_POS_NO);
					}

					// expand replacment
					replacement = SUB_PHRASE_OPTION_START + replacement + SubPhraseOptionName.SUB_PHRASEOPTION_POS_NO_SEP + posNo + SUB_PHRASE_OPTION_END;
				} else {
					replacement = m.group();
				}

				m.appendReplacement(sb, replacement);
			}

			m.appendTail(sb);
			phraseValue = sb.toString();
		}

		return phraseValue;
	}

	/**
	 * Expand all short sub phrase option names inside the phrase value to their
	 * full name.
	 * 
	 * @param phraseValue
	 *            the phrase value
	 * @param parentName
	 *            the parent name
	 * @return the phrase value with the expanded names
	 */
	public static String expandShortSubPhraseOptionName(String phraseValue, String parentName) {

		if (phraseValue == null) {
			throw new IllegalArgumentException("Given phrase value is 'NULL'");
		}

		if (phraseValue.indexOf(SUB_PHRASE_OPTION_START) > -1) {

			Matcher m = SUB_PHRASE_OPTION_PATTERN.matcher(phraseValue);
			StringBuffer sb = new StringBuffer();

			while (m.find()) {
				String phraseOptionName = m.group(1);
				String posNo = m.group(2);
				phraseOptionName = createUniqueSubPhraseOptionName(parentName, phraseOptionName);

				if (posNo != null) {
					m.appendReplacement(sb, SUB_PHRASE_OPTION_START + phraseOptionName + SubPhraseOptionName.SUB_PHRASEOPTION_POS_NO_SEP + posNo + SUB_PHRASE_OPTION_END);
				} else {
					m.appendReplacement(sb, SUB_PHRASE_OPTION_START + phraseOptionName + SUB_PHRASE_OPTION_END);
				}
			}

			m.appendTail(sb);
			phraseValue = sb.toString();
		}

		return phraseValue;
	}

	/**
	 * Create a unique sub phrase option name. Create a unique sub phrase
	 * option name based on the phrase option name used in the phrase and
	 * the according parent no.
	 * 
	 * @param parentName
	 *            the parent number
	 * @param phraseOptionName
	 *            the sub phrase option name
	 * @return the created id
	 * @throws IllegalArgumentException
	 *             if phrase or parentNo is <code>null</code>
	 */
	public static String createUniqueSubPhraseOptionName(String parentName, String phraseOptionName) {

		if (phraseOptionName == null) {
			throw new IllegalArgumentException("SubPhraseOptionSequence is null");
		}

		if (phraseOptionName.indexOf(PHRASEOPTION_PARENT_SEP) != 0) {
			// the parent still included, or a multiple usage phrase option
			return phraseOptionName;
		} else {

			if (parentName == null) {
				throw new IllegalArgumentException("Parent No is null");
			}

			if (parentName.indexOf(PHRASEOPTION_SEP) > -1) {
				parentName = parentName.substring(0, parentName.indexOf(PHRASEOPTION_SEP));
			}

			return parentName + phraseOptionName;
		}
	}
	
	/**
	 * Create a unique phrase option name for individual phrase options.
	 * 
	 * @param sentenceName
	 *            the according sentence no
	 * @param moduleNo
	 *            the phrase option structure no
	 * @return the created Id
	 */
	public static String createUniquePhraseOptionNo(String sentenceName, int moduleNo) {
		return sentenceName + PHRASEOPTION_SEP + Integer.toString(moduleNo);
	}


}
