package ch.slf.lwp.bulletinedit.textcat.dao;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

public interface DaoPhraseOption {

	public abstract PhraseOption getPhraseOption(String phraseOptionName, Language language, Version baseVersion, DomainName domainName);

	public abstract PhraseOption insertPhraseOption(PhraseOption phraseOption);

	public abstract PhraseOption updatePhraseOption(PhraseOption phraseOption);

	public abstract boolean deletePhraseOptions(Version baseVersion, DomainName domainName);

}
