package ch.slf.lwp.bulletinedit.textcat.applicationTexts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

public class AppTextDefinition {

	// Define which version is used for static application texts
	// Another way would be do use always the newest version, but because we do
	// not use different versions
	// for static texts yet, it does not make a difference for now.
	public static final Version VERSION = new Version(1, 0);

	// use German as reference Language
	public static final Language LANGUAGE = Language.de;
	
	private List<SentenceDefinition> sentences = new ArrayList<SentenceDefinition>();
	
	public SentenceDefinition addSentence(String sentenceName){
		SentenceDefinition sentence = new SentenceDefinition(sentenceName);
		sentences.add(sentence);
		return sentence;
		
	}

	public Version getVersion() {
		return VERSION;
	}

	public Language getLanguage() {
		return LANGUAGE;
	}

	public List<SentenceDefinition> getSentences() {
		return sentences;
	}

	public class SentenceDefinition{
		private String sentenceName;
		private Map<Integer, ItemDefinition> moduleItemMap = new HashMap<Integer, ItemDefinition>();
		
		public SentenceDefinition(String sentenceName) {
			this.setSentenceName(sentenceName);
		}
		
		public Map<Integer, ItemDefinition> getModuleItemMap() {
			return moduleItemMap;
		}
		
		public void addModule(Integer moduleNo, Integer itemNo, List<Integer> subMenuItemNos) {
			ItemDefinition item = new ItemDefinition(itemNo, subMenuItemNos);
			moduleItemMap.put(moduleNo, item);
		}
		
		public String getSentenceName() {
			return sentenceName;
		}

		public void setSentenceName(String sentenceName) {
			this.sentenceName = sentenceName;
		}
	}

	public class ItemDefinition {
		private int itemNo;
		private List<Integer> subModuleItems = new ArrayList<Integer>();

		ItemDefinition(int itemNo, List<Integer> subModuleItems) {
			this.setSubModuleItems(subModuleItems);
			this.itemNo = itemNo;
		}

		public int getItemNo() {
			return itemNo;
		}

		public void setItemNo(int itemNo) {
			this.itemNo = itemNo;
		}

		public List<Integer> getSubModuleItems() {
			return subModuleItems;
		}

		public void setSubModuleItems(List<Integer> subModuleItems) {
			this.subModuleItems = subModuleItems;
		}
	}

}
