package ch.slf.lwp.bulletinedit.textcat.json;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Represent a JSON index item entry.
 * 
 * @author weiss
 */
public class IndexItem implements Item {

	private String word;
	private List<Long> sentences = new ArrayList<Long>();

	public void setWord(String word) {
		this.word = word;
	}

	public String getWord() {
		return word;
	}

	/**
	 * The according sentence id as string list.
	 * 
	 * @return the sentence id
	 */
	public String getSentences() {
		Collections.sort(sentences);
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < sentences.size(); i++) {

			if (i != 0) {
				sb.append(JsonUtil.STRING_LIST_SEP);
			}

			sb.append(sentences.get(i));

		}

		return sb.toString();
	}

	public void addSentenceId(long id) {
		if (sentences.contains(id) == false) {
			sentences.add(id);
		}
	}

}
