package ch.slf.lwp.bulletinedit.textcat;

import java.util.List;

import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;

public interface TextCat {

	/**
	 * Returns the text cat language.
	 * 
	 * @return the language
	 */
	public abstract Language getLanguage();

	/**
	 * Returns the text cat domain name.
	 * 
	 * @return the domain name
	 */
	public abstract DomainName getDomainName();

	/**
	 * Initialize the text cat.
	 * 
	 * The phrase option list must also contain the sentence phrase options.
	 * 
	 * @param sentences
	 *            the sentence list
	 * @param options
	 *            the phrase option list
	 * 
	 * @throws TextCatException
	 *             if an error occurred.
	 * @throws IllegalArgumentException
	 *             if the something does not fit. See Implementation classes for
	 *             details.
	 */
	public abstract void init(List<? extends Sentence> sentences, List<? extends PhraseOption> options) throws TextCatException;

}