package ch.slf.lwp.bulletinedit.textcat.json;

/**
 * JSON item entry representing a phrase options.
 * 
 * @author weiss
 */
public class OptionItem extends AbstractCatalogItem {

	private String name;
	//private String name_IT;
	private String phrases;
	private String header;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the header
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * @param header
	 *           the header to set
	 */
	public void setHeader(String header) {
		this.header = header;
	}


	/**
	 * @return the phrases
	 */
	public String getPhrases() {
		return phrases;
	}

	/**
	 * @param phrases
	 *            the phrases to set
	 */
	public void setPhrases(String phrases) {
		this.phrases = phrases;
	}

}
