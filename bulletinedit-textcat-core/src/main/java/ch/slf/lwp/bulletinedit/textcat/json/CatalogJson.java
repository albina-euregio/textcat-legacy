package ch.slf.lwp.bulletinedit.textcat.json;

/**
 * JSON wrapper for id depended JSON items
 * 
 * @author weiss
 */
public class CatalogJson extends Json {

	private String identifier = "id";

	/**
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * set the items
	 * 
	 * @param items
	 *            the items
	 */
	public void setItems(AbstractCatalogItem[] items) {
		super.setItems(items);
	}

}
