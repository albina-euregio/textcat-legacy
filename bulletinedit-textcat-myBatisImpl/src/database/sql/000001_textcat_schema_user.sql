create user albina identified by albina;
GRANT RESOURCE TO albina;
GRANT CONNECT TO albina;
GRANT CREATE VIEW TO albina;
GRANT CREATE SESSION TO albina;
GRANT UNLIMITED TABLESPACE TO albina;