/*
* Mapp textcat schema into dangerrating_2012_webapp schema
*/



/*
NOT used!!!
*/
-- ----------------------------------------------
-- synonyms
-- ----------------------------------------------
-- tables
create or replace synonym dangerrating_2012_webapp.domain for ALBINA.domain;
create or replace synonym dangerrating_2012_webapp.keyword for ALBINA.keyword;
create or replace synonym dangerrating_2012_webapp.optionPhrase for ALBINA.optionPhrase;
create or replace synonym dangerrating_2012_webapp.phrase_option for ALBINA.phrase_option;
create or replace synonym dangerrating_2012_webapp.phrase_option_item for ALBINA.phrase_option_item;
create or replace synonym dangerrating_2012_webapp.sentence for ALBINA.sentence;
create or replace synonym dangerrating_2012_webapp.sentence__keyword for ALBINA.sentence__keyword;
create or replace synonym dangerrating_2012_webapp.sentence__topic for ALBINA.sentence__topic;
create or replace synonym dangerrating_2012_webapp.sentence_module for ALBINA.sentence_module;
create or replace synonym dangerrating_2012_webapp.topic for ALBINA.topic;
-- sequences
create or replace synonym dangerrating_2012_webapp.seq_keyword_id for ALBINA.seq_keyword_id;
create or replace synonym dangerrating_2012_webapp.seq_phrase_id for ALBINA.seq_phrase_id;
create or replace synonym dangerrating_2012_webapp.seq_phrase_option_id for ALBINA.seq_phrase_option_id;
create or replace synonym dangerrating_2012_webapp.seq_phrase_option_item_id for ALBINA.seq_phrase_option_item_id;
create or replace synonym dangerrating_2012_webapp.seq_sentence_id for ALBINA.seq_sentence_id;
create or replace synonym dangerrating_2012_webapp.seq_topic_id for ALBINA.seq_topic_id;
