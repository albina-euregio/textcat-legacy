/*run one at a time

CREATE OR REPLACE TRIGGER tg_domain_datelast before
  UPDATE ON domain
  FOR EACH row
BEGIN 
  IF not UPDATING('datelast') THEN
    :new.datelast := sysdate;
  END IF;
END;

/*
CREATE OR REPLACE TRIGGER tg_keyword_datelast before
  UPDATE ON keyword
  FOR EACH row
BEGIN
  IF not UPDATING('datelast') THEN
    :new.datelast := sysdate;
  END IF;
END;
*/

CREATE OR REPLACE TRIGGER tg_phrase_datelast before
  UPDATE ON Phrase
  FOR EACH row
BEGIN
  IF not UPDATING('datelast') THEN 
    :new.datelast := sysdate;
  END IF;
END;


CREATE OR REPLACE TRIGGER tg_phrase_option_datelast before
  UPDATE ON phrase_option
  FOR EACH row
BEGIN
  IF not UPDATING('datelast') THEN 
    :new.datelast := sysdate;
  END IF;
END;


CREATE OR REPLACE TRIGGER tg_phrase_option_item_datelast before
  UPDATE ON phrase_option_item
  FOR EACH row
BEGIN
  IF not UPDATING('datelast') THEN
    :new.datelast := sysdate;
  END IF;
END;


CREATE OR REPLACE TRIGGER tg_sentence_datelast before
  UPDATE ON sentence
  FOR EACH row
BEGIN
  IF not UPDATING('datelast') THEN
    :new.datelast := sysdate;
  END IF;
END;


CREATE OR REPLACE TRIGGER tg_sentence_module_datelast before
  UPDATE ON sentence_module
  FOR EACH row
BEGIN
  IF not UPDATING('datelast') THEN
    :new.datelast := sysdate;
  END IF;
END;

/*
CREATE OR REPLACE TRIGGER tg_sentence__keyword_datelast before
  UPDATE ON sentence__keyword
  FOR EACH row
BEGIN
  IF not UPDATING('datelast') THEN
    :new.datelast := sysdate;
  END IF;
END;


CREATE OR REPLACE TRIGGER tg_sentence__topic_datelast before
  UPDATE ON sentence__topic
  FOR EACH row
BEGIN
  IF not UPDATING('datelast') THEN 
    :new.datelast := sysdate;
  END IF;
END;


CREATE OR REPLACE TRIGGER tg_topic_datelast before
  UPDATE ON topic
  FOR EACH row
BEGIN
  IF not UPDATING('datelast') THEN
    :new.datelast := sysdate;
  END IF;
END;

*/