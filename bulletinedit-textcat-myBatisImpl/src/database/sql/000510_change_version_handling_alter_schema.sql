/*
* change schema for the new version handling
*/

-- basis_version is required
alter table sentence modify (basis_version VARCHAR2 (20 CHAR) not null);
alter table phrase_option modify (basis_version VARCHAR2 (20 CHAR) not null);
alter table phrase_option_item modify (basis_version VARCHAR2 (20 CHAR) not null);

-- alter unique constraints
alter table sentence drop constraint uc_sentences_name;
alter table phrase_option drop constraint uc_phrase_option_name;

alter table sentence add constraint uc_sentences_name_v unique (name, language, domain_id, version);
alter table phrase_option add constraint uc_phrase_option_name_v unique (name, language, domain_id, version);

alter table sentence add constraint uc_sentences_name_bv unique (name, language, domain_id, basis_version);
alter table phrase_option add constraint uc_phrase_option_name_bv unique (name, language, domain_id, basis_version);


CREATE UNIQUE INDEX UC_SENTENCES_NAME ON SENTENCE ("NAME","LANGUAGE","DOMAIN_ID");
CREATE UNIQUE INDEX UC_PHRASE_OPTION_NAME ON PHRASE_OPTION ("NAME","LANGUAGE","DOMAIN_ID");


