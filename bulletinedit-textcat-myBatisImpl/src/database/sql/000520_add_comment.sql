/*
* Add Database comment on table and table column
*/

--------------------------------------------------------
--  Comment for table DOMAIN
--------------------------------------------------------
COMMENT ON TABLE "ALBINA"."DOMAIN" IS 'The Sentence catalog domain.';

COMMENT ON COLUMN "ALBINA"."DOMAIN"."DOMAIN_ID" IS 'The unique domain id.';
COMMENT ON COLUMN "ALBINA"."DOMAIN"."NAME" IS 'The unique domain name.';
COMMENT ON COLUMN "ALBINA"."DOMAIN"."DATENEW" IS 'The creation date of the data set.';
COMMENT ON COLUMN "ALBINA"."DOMAIN"."DATELAST" IS 'The last change date of the data set, modified by a trigger if not manually set.';

/*
--------------------------------------------------------
--  Comment for table KEYWORD
--------------------------------------------------------
COMMENT ON TABLE "ALBINA"."KEYWORD" IS 'The Sentence keywords.';

COMMENT ON COLUMN "ALBINA"."KEYWORD"."KEYWORD_ID" IS 'The unique keyword id.';
COMMENT ON COLUMN "ALBINA"."KEYWORD"."NAME" IS 'The keyword.';
COMMENT ON COLUMN "ALBINA"."KEYWORD"."DATENEW" IS 'The creation date of the data set.';
COMMENT ON COLUMN "ALBINA"."KEYWORD"."DATELAST" IS 'The last change date of the data set, modified by a trigger if not manually set.';
*/

--------------------------------------------------------
--  Comment for table PHRASE
--------------------------------------------------------
COMMENT ON TABLE "ALBINA"."PHRASE" IS 'The optionPhrase option item optionPhrase values.';

COMMENT ON COLUMN "ALBINA"."PHRASE"."PHRASE_ID" IS 'The unique optionPhrase id.';
COMMENT ON COLUMN "ALBINA"."PHRASE"."VALUE" IS 'The real optionPhrase value, including the subphrase option sequence.';
COMMENT ON COLUMN "ALBINA"."PHRASE"."SPACE_BEFORE" IS 'Boolean flag, indicates that a blank is required before the optionPhrase value in a running text.';
COMMENT ON COLUMN "ALBINA"."PHRASE"."SPACE_AFTER" IS 'Boolean flag, indicates that a blank is required after the optionPhrase value in a running text.';
COMMENT ON COLUMN "ALBINA"."PHRASE"."REMOVE_PUNCTUATION_BEFORE" IS 'Boolean flag, indicates that in a running text the punctation mark before the optionPhrase value must be removed.';
COMMENT ON COLUMN "ALBINA"."PHRASE"."ITEM_PART_NO" IS 'The part number of the pulldown item.';
COMMENT ON COLUMN "ALBINA"."PHRASE"."INCORRECT" IS 'Mark the optionPhrase as incorrect.';
COMMENT ON COLUMN "ALBINA"."PHRASE"."PHRASE_OPTION_ITEM_ID" IS 'The optionPhrase option item id.';
COMMENT ON COLUMN "ALBINA"."PHRASE"."DATENEW" IS 'The creation date of the data set.';
COMMENT ON COLUMN "ALBINA"."PHRASE"."DATELAST" IS 'The last change date of the data set, modified by a trigger if not manually set.';


--------------------------------------------------------
--  Comment for table PHRASE_OPTION
--------------------------------------------------------
COMMENT ON TABLE "ALBINA"."PHRASE_OPTION" IS 'The prase option meta data.';

COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION"."PHRASE_OPTION_ID" IS 'The unique optionPhrase option id.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION"."NAME" IS 'The optionPhrase option name, unique inside a language and version.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION"."LANGUAGE" IS 'The optionPhrase option language.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION"."HEADER" IS 'The optionPhrase option header.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION"."VERSION" IS 'The language version.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION"."BASIS_VERSION" IS 'The sentence catalog version, equal to the according german language version.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION"."REMARK" IS 'A remark for that optionPhrase option.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION"."DELETED" IS 'Boolean flag, indicates that the optionPhrase option is deleted.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION"."DOMAIN_ID" IS 'The domain id.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION"."DATENEW" IS 'The creation date of the data set.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION"."DATELAST" IS 'The last change date of the data set, modified by a trigger if not manually set.';


--------------------------------------------------------
--  Comment for table PHRASE_OPTION_ITEM
--------------------------------------------------------
COMMENT ON TABLE "ALBINA"."PHRASE_OPTION_ITEM" IS 'The optionPhrase option item meta data.';

COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION_ITEM"."PHRASE_OPTION_ITEM_ID" IS 'The unique optionPhrase option id.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION_ITEM"."ITEM_NO" IS 'The unique item number inside the optionPhrase option.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION_ITEM"."DELETED" IS 'Boolean flag, indicates that the optionPhrase option item is deleted.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION_ITEM"."PHRASE_OPTION_ID" IS 'The optionPhrase option id.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION_ITEM"."VERSION" IS 'The language version.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION_ITEM"."BASIS_VERSION" IS 'The sentence catalog version, equal to the according german language version.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION_ITEM"."DATENEW" IS 'The creation date of the data set.';
COMMENT ON COLUMN "ALBINA"."PHRASE_OPTION_ITEM"."DATELAST" IS 'The last change date of the data set, modified by a trigger if not manually set.';


--------------------------------------------------------
--  Comment for table SENTENCE
--------------------------------------------------------
COMMENT ON TABLE "ALBINA"."SENTENCE" IS 'The sentence meta data.';

COMMENT ON COLUMN "ALBINA"."SENTENCE"."SENTENCE_ID" IS 'The unique sentence id.';
COMMENT ON COLUMN "ALBINA"."SENTENCE"."NAME" IS 'The sentence name, unique inside a language and version.';
COMMENT ON COLUMN "ALBINA"."SENTENCE"."HEADER" IS 'The sentence header.';
COMMENT ON COLUMN "ALBINA"."SENTENCE"."LANGUAGE" IS 'The sentence language.';
COMMENT ON COLUMN "ALBINA"."SENTENCE"."STRUCTURE" IS 'The sentence sturcture, shows the module and module part order.';
COMMENT ON COLUMN "ALBINA"."SENTENCE"."VERSION" IS 'The language version.';
COMMENT ON COLUMN "ALBINA"."SENTENCE"."BASIS_VERSION" IS 'The sentence catalog version, equal to the according german language version.';
COMMENT ON COLUMN "ALBINA"."SENTENCE"."REMARK" IS 'A rimark for that sentence.';
COMMENT ON COLUMN "ALBINA"."SENTENCE"."DELETED" IS 'Boolean flag, indicates that the sentence is deleted.';
COMMENT ON COLUMN "ALBINA"."SENTENCE"."JOKER_SENTENCE" IS 'Boolean flag, indicates if it is a joker sentence or not ("1" ==> joker sentence).';
COMMENT ON COLUMN "ALBINA"."SENTENCE"."DOMAIN_ID" IS 'The domain id.';
COMMENT ON COLUMN "ALBINA"."SENTENCE"."DATENEW" IS 'The creation date of the data set.';
COMMENT ON COLUMN "ALBINA"."SENTENCE"."DATELAST" IS 'The last change date of the data set, modified by a trigger if not manually set.';


--------------------------------------------------------
--  Comment for table SENTENCE_MODULE
--------------------------------------------------------
COMMENT ON TABLE "ALBINA"."SENTENCE_MODULE" IS 'Mapping table between sentence and optionPhrase option (The sentence modules).';

COMMENT ON COLUMN "ALBINA"."SENTENCE_MODULE"."SENTENCE_ID" IS 'The sentence id.';
COMMENT ON COLUMN "ALBINA"."SENTENCE_MODULE"."PHRASE_OPTION_ID" IS 'The optionPhrase option id.';
COMMENT ON COLUMN "ALBINA"."SENTENCE_MODULE"."MODULE_NO" IS 'The module number for the optionPhrase option inside the sentence.';
COMMENT ON COLUMN "ALBINA"."SENTENCE_MODULE"."DATENEW" IS 'The creation date of the data set.';
COMMENT ON COLUMN "ALBINA"."SENTENCE_MODULE"."DATELAST" IS 'The last change date of the data set, modified by a trigger if not manually set.';


--------------------------------------------------------
--  Comment for table SENTENCE__KEYWORD
--------------------------------------------------------
COMMENT ON TABLE "ALBINA"."SENTENCE__KEYWORD" IS 'Mapping table between sentence and keyword.';

COMMENT ON COLUMN "ALBINA"."SENTENCE__KEYWORD"."SENTENCE_ID" IS 'The sentence id.';
COMMENT ON COLUMN "ALBINA"."SENTENCE__KEYWORD"."KEYWORD_ID" IS 'The keyword id.';
COMMENT ON COLUMN "ALBINA"."SENTENCE__KEYWORD"."DATENEW" IS 'The creation date of the data set.';
COMMENT ON COLUMN "ALBINA"."SENTENCE__KEYWORD"."DATELAST" IS 'The last change date of the data set, modified by a trigger if not manually set.';

/*
--------------------------------------------------------
--  Comment for table SENTENCE__TOPIC
--------------------------------------------------------
COMMENT ON TABLE "ALBINA"."SENTENCE__TOPIC" IS 'Mapping table between sentence and topic.';

COMMENT ON COLUMN "ALBINA"."SENTENCE__TOPIC"."SENTENCE_ID" IS 'The sentence id.';
COMMENT ON COLUMN "ALBINA"."SENTENCE__TOPIC"."TOPIC_ID" IS 'The topic id.';
COMMENT ON COLUMN "ALBINA"."SENTENCE__TOPIC"."DATENEW" IS 'The creation date of the data set.';
COMMENT ON COLUMN "ALBINA"."SENTENCE__TOPIC"."DATELAST" IS 'The last change date of the data set, modified by a trigger if not manually set.';


--------------------------------------------------------
--  Comment for table TOPIC
--------------------------------------------------------
COMMENT ON TABLE "ALBINA"."TOPIC" IS 'The topic(s) of a sentence.';

COMMENT ON COLUMN "ALBINA"."TOPIC"."TOPIC_ID" IS 'The unique topic id.';
COMMENT ON COLUMN "ALBINA"."TOPIC"."NAME" IS 'The topic.';
COMMENT ON COLUMN "ALBINA"."TOPIC"."DATENEW" IS 'The creation date of the data set.';
COMMENT ON COLUMN "ALBINA"."TOPIC"."DATELAST" IS 'The last change date of the data set, modified by a trigger if not manually set.';
*/
commit;