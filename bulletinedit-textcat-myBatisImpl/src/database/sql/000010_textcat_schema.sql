CREATE TABLE domain
    ( 
     domain_id NUMBER (10)  NOT NULL , 
     name VARCHAR2 (200 CHAR)  NOT NULL,
     datenew DATE DEFAULT sysdate NOT NULL,
     datelast DATE DEFAULT sysdate NOT NULL 
    )
;

ALTER TABLE domain 
    ADD CONSTRAINT pk_domain PRIMARY KEY ( domain_id ) ;

/*
CREATE TABLE keyword 
    ( 
     keyword_id NUMBER (10)  NOT NULL , 
     name VARCHAR2 (50 CHAR)  NOT NULL ,
     datenew DATE DEFAULT sysdate NOT NULL,
     datelast DATE DEFAULT sysdate NOT NULL
    ) 
;

ALTER TABLE keyword 
    ADD CONSTRAINT pk_keyword PRIMARY KEY ( keyword_id ) ;
*/



CREATE TABLE phrase
    (
     phrase_id NUMBER (10)  NOT NULL ,
     value VARCHAR2 (500 CHAR) ,
     space_before CHAR (1)  NOT NULL ,
     space_after CHAR (1)  NOT NULL ,
     remove_punctuation_before CHAR (1)  NOT NULL ,
     item_part_no NUMBER (10)  NOT NULL ,
     incorrect CHAR (1)  NOT NULL ,
     phrase_option_item_id NUMBER (10) NOT NULL,
     datenew DATE DEFAULT sysdate NOT NULL,
     datelast DATE DEFAULT sysdate NOT NULL
    )
;

ALTER TABLE phrase
    ADD CONSTRAINT pk_phrase PRIMARY KEY ( phrase_id ) ;



CREATE TABLE phrase_option 
    ( 
     phrase_option_id NUMBER (10)  NOT NULL , 
     name VARCHAR2 (100 CHAR)  NOT NULL , 
     language VARCHAR2 (2 CHAR)  NOT NULL , 
     header VARCHAR2 (200 CHAR) , 
     version VARCHAR2 (20 CHAR)  NOT NULL , 
     basis_version VARCHAR2 (20 CHAR) , 
     remark VARCHAR2 (1000 CHAR) , 
     deleted CHAR (1)  NOT NULL , 
     domain_id NUMBER (10) ,
     datenew DATE DEFAULT sysdate NOT NULL,
     datelast DATE DEFAULT sysdate NOT NULL
    )
;

ALTER TABLE phrase_option 
    ADD CONSTRAINT pk_phrase_option PRIMARY KEY ( phrase_option_id ) ;

ALTER TABLE phrase_option 
    ADD CONSTRAINT uc_phrase_option_name UNIQUE ( name , language , domain_id ) ;


CREATE TABLE phrase_option_item 
    ( 
     phrase_option_item_id NUMBER (10)  NOT NULL , 
     item_no NUMBER (10)  NOT NULL , 
     deleted CHAR (1)  NOT NULL , 
     phrase_option_id NUMBER (10) , 
     version VARCHAR2 (20 CHAR)  NOT NULL , 
     basis_version VARCHAR2 (20 CHAR) ,
     datenew DATE DEFAULT sysdate NOT NULL,
     datelast DATE DEFAULT sysdate NOT NULL
    )
;

ALTER TABLE phrase_option_item 
    ADD CONSTRAINT pk_phrase_option_item PRIMARY KEY ( phrase_option_item_id ) ;


CREATE TABLE sentence 
    ( 
     sentence_id NUMBER (10)  NOT NULL , 
     name VARCHAR2 (100 CHAR)  NOT NULL ,
     header VARCHAR2 (200 CHAR) ,
     language VARCHAR2 (2 CHAR)  NOT NULL , 
     structure VARCHAR2 (100 CHAR)  NOT NULL , 
     version VARCHAR2 (20 CHAR)  NOT NULL , 
     basis_version VARCHAR2 (20 CHAR) , 
     remark VARCHAR2 (1000 CHAR) , 
     deleted CHAR (1)  NOT NULL , 
     joker_sentence CHAR (1)  NOT NULL , 
     domain_id NUMBER (10) ,
     datenew DATE DEFAULT sysdate NOT NULL,
     datelast DATE DEFAULT sysdate NOT NULL
    )
;

ALTER TABLE sentence 
    ADD CONSTRAINT pk_sentences PRIMARY KEY ( sentence_id ) ;

ALTER TABLE sentence 
    ADD CONSTRAINT uc_sentences_name UNIQUE ( name , language , domain_id ) ;

/*
CREATE TABLE sentence__keyword 
    ( 
     sentence_id NUMBER (10)  NOT NULL , 
     keyword_id NUMBER (10)  NOT NULL ,
     datenew DATE DEFAULT sysdate NOT NULL,
     datelast DATE DEFAULT sysdate NOT NULL
    )
;

ALTER TABLE sentence__keyword 
    ADD CONSTRAINT pk_sentences__keywords PRIMARY KEY ( sentence_id, keyword_id ) ;


CREATE TABLE sentence__topic 
    ( 
     sentence_id NUMBER (10)  NOT NULL , 
     topic_id NUMBER (10)  NOT NULL ,
     datenew DATE DEFAULT sysdate NOT NULL,
     datelast DATE DEFAULT sysdate NOT NULL
    )
;



ALTER TABLE sentence__topic 
    ADD CONSTRAINT pk_sentences__topics PRIMARY KEY ( sentence_id, topic_id ) ;

*/

CREATE TABLE sentence_module 
    ( 
     sentence_id NUMBER (10)  NOT NULL , 
     phrase_option_id NUMBER (10)  NOT NULL , 
     module_no NUMBER (10)  NOT NULL ,
     datenew DATE DEFAULT sysdate NOT NULL,
     datelast DATE DEFAULT sysdate NOT NULL
    )
;

ALTER TABLE sentence_module 
    ADD CONSTRAINT pk_sentence_module PRIMARY KEY ( sentence_id, phrase_option_id ) ;

/*
CREATE TABLE topic 
    ( 
     topic_id NUMBER (10)  NOT NULL , 
     name VARCHAR2 (50 CHAR)  NOT NULL ,
     datenew DATE DEFAULT sysdate NOT NULL,
     datelast DATE DEFAULT sysdate NOT NULL
    )
;

ALTER TABLE topic 
    ADD CONSTRAINT pk_topic PRIMARY KEY ( topic_id ) ;

*/

ALTER TABLE phrase_option_item 
    ADD CONSTRAINT fk_component FOREIGN KEY 
    ( 
     phrase_option_id
    ) 
    REFERENCES phrase_option 
    ( 
     phrase_option_id
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;


ALTER TABLE phrase_option 
    ADD CONSTRAINT fk_domain_phrase_option FOREIGN KEY 
    ( 
     domain_id
    ) 
    REFERENCES domain 
    ( 
     domain_id
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;


ALTER TABLE sentence 
    ADD CONSTRAINT fk_domain_sentence FOREIGN KEY 
    ( 
     domain_id
    ) 
    REFERENCES domain 
    ( 
     domain_id
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;

/*
ALTER TABLE sentence__keyword 
    ADD CONSTRAINT fk_keyword_sentence_keyword FOREIGN KEY 
    ( 
     keyword_id
    ) 
    REFERENCES keyword 
    ( 
     keyword_id
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;
*/

ALTER TABLE sentence_module 
    ADD CONSTRAINT fk_phrase_option FOREIGN KEY 
    ( 
     phrase_option_id
    ) 
    REFERENCES phrase_option 
    ( 
     phrase_option_id
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;


ALTER TABLE sentence_module 
    ADD CONSTRAINT fk_sentence FOREIGN KEY 
    ( 
     sentence_id
    ) 
    REFERENCES sentence 
    ( 
     sentence_id
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;

/*
ALTER TABLE sentence__keyword 
    ADD CONSTRAINT fk_sentence_sentence_keyword FOREIGN KEY 
    ( 
     sentence_id
    ) 
    REFERENCES sentence 
    ( 
     sentence_id
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;


ALTER TABLE sentence__topic 
    ADD CONSTRAINT fk_sentence_sentence_topic FOREIGN KEY 
    ( 
     sentence_id
    ) 
    REFERENCES sentence 
    ( 
     sentence_id
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;


ALTER TABLE sentence__topic 
    ADD CONSTRAINT fk_topic_sentence_topic FOREIGN KEY 
    ( 
     topic_id
    ) 
    REFERENCES topic 
    ( 
     topic_id
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;

*/

ALTER TABLE PHRASE ADD CONSTRAINT "FK_SUBCOMPONENT" FOREIGN KEY ("PHRASE_OPTION_ITEM_ID") REFERENCES PHRASE_OPTION_ITEM("PHRASE_OPTION_ITEM_ID") ON DELETE CASCADE ENABLE;

/*
ALTER TABLE optionPhrase
    ADD CONSTRAINT fk_subcomponent FOREIGN KEY
    (
     phrase_option_item_id
    )
    REFERENCES phrase_option_item
    (
     phrase_option_item_id
    )
    ON DELETE CASCADE
    NOT DEFERRABLE
;
*/

