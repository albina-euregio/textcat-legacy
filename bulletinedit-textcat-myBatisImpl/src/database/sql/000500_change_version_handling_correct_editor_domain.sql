/*
* Correct version for EDITOR Domain
*/

-- fill base version in all languages
update sentence
  set basis_version = version
where basis_version is null;

update phrase_option
  set basis_version = version
where basis_version is null;

update phrase_option_item
  set basis_version = version
where basis_version is null;

-- adapt language versions and basis versions
update sentence
   set basis_version = '1.0',
       version = '1.0'
 where domain_id = 2;

update phrase_option
   set basis_version = '1.0',
       version = '1.0'
 where domain_id = 2;

update phrase_option_item
   set basis_version = '1.0',
       version = '1.0'
 where phrase_option_id in (select po.phrase_option_id from phrase_option po where po.domain_id = 2);

commit;