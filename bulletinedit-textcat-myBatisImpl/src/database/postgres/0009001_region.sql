CREATE TABLE REGION (
	REGION_ID DOUBLE PRECISION,
	REGION VARCHAR(100),
	CONSTRAINT REGION_PK PRIMARY KEY (REGION_ID)
) ;
COMMENT ON TABLE REGION IS 'Tirol, South Tyrol, Trentino, Switzerland' ;



CREATE TABLE REGION_PHRASE (
	REGION_ID DOUBLE PRECISION,
	PHRASE_ID DOUBLE PRECISION
) ;
ALTER TABLE REGION_PHRASE ADD CONSTRAINT REGION_PHRASE_PK PRIMARY KEY (PHRASE_ID,REGION_ID) ;
ALTER TABLE REGION_PHRASE ADD CONSTRAINT REGION_PHRASE_PHRASE_FK FOREIGN KEY (PHRASE_ID) REFERENCES PHRASE(PHRASE_ID) ON DELETE CASCADE ;
ALTER TABLE REGION_PHRASE ADD CONSTRAINT REGION_PHRASE_REGION_FK FOREIGN KEY (REGION_ID) REFERENCES REGION(REGION_ID) ;


INSERT INTO REGION (REGION_ID, REGION) VALUES ('1', 'Switzerland')
INSERT INTO REGION (REGION_ID, REGION) VALUES ('2', 'Tyrol')
INSERT INTO REGION (REGION_ID, REGION) VALUES ('3', 'South Tyrol')
INSERT INTO REGION (REGION_ID, REGION) VALUES ('4', 'Trentino')

ALTER TABLE "ALBINA"."PHRASE_OPTION" ADD CONSTRAINT "UC_PHRASE_OPTION_NAME" UNIQUE ("NAME","LANGUAGE","DOMAIN_ID");

ALTER TABLE "ALBINA"."PHRASE_OPTION_ITEM" DROP CONSTRAINT "FK_SUBCOMPONENT";
ALTER TABLE "ALBINA"."PHRASE_OPTION_ITEM" ADD CONSTRAINT "FK_COMPONENT" FOREIGN KEY ("PHRASE_OPTION_ID") REFERENCES "ALBINA"."PHRASE_OPTION"("PHRASE_OPTION_ID") ON DELETE CASCADE;

ALTER TABLE "ALBINA"."PHRASE" ADD CONSTRAINT "FK_SUBCOMPONENT" FOREIGN KEY ("PHRASE_OPTION_ITEM_ID") REFERENCES "ALBINA"."PHRASE_OPTION_ITEM"("PHRASE_OPTION_ITEM_ID") ON DELETE CASCADE;

ALTER TABLE "ALBINA"."SENTENCE" ADD CONSTRAINT "UC_SENTENCES_NAME" UNIQUE ("NAME","LANGUAGE","DOMAIN_ID");

CREATE UNIQUE INDEX "ALBINA"."UC_PHRASE_OPTION_NAME" ON "ALBINA"."PHRASE_OPTION" ("NAME","LANGUAGE","DOMAIN_ID");

CREATE UNIQUE INDEX "ALBINA"."UC_SENTENCES_NAME" ON "ALBINA"."SENTENCE" ("NAME","LANGUAGE","DOMAIN_ID");


