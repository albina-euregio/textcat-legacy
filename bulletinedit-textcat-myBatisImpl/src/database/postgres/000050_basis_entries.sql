/*
* Basis entries
*/
-- ---------------------------------
-- Domain
-- ---------------------------------
insert into domain (domain_id,name) values (-10, 'INTEGRATION_TEST');
insert into domain (domain_id, name) values (1, 'PRODUCTION');
insert into domain (domain_id,name) values (2, 'STAGING');
