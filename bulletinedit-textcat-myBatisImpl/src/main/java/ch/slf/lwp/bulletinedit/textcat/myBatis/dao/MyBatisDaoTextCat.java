/**
 * 
 */
package ch.slf.lwp.bulletinedit.textcat.myBatis.dao;

import ch.slf.lwp.bulletinedit.textcat.dao.DaoTextCat;

/**
 * @author weiss
 *
 */
public interface MyBatisDaoTextCat extends DaoTextCat {

}
