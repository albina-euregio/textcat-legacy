package ch.slf.lwp.bulletinedit.textcat.myBatis.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;
import ch.slf.lwp.bulletinedit.textcat.dao.DaoSentence;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;
import ch.slf.lwp.bulletinedit.textcat.myBatis.DbConstants;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.MyBatisPhraseOption;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.MyBatisSentence;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.extension.Keyword;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.extension.SentenceModule;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.extension.Topic;
import ch.slf.lwp.bulletinedit.textcat.myBatis.persistence.KeywordMapper;
import ch.slf.lwp.bulletinedit.textcat.myBatis.persistence.SentenceMapper;
import ch.slf.lwp.bulletinedit.textcat.myBatis.persistence.TopicMapper;

/**
 * MyBatis implementation of the TextCat sentence DAO.
 *
 * @see DaoSentence
 * @author gw
 */
public class MyBatisDaoSentenceImpl implements MyBatisDaoSentence {

    private SentenceMapper sentenceMapper;
    private KeywordMapper keywordMapper;
    private TopicMapper topicMapper;
    private MyBatisDaoPhraseOption myBatisDaoO;

    /**
     * @param sentenceMapper the sentenceMapper to set
     */
    public void setSentenceMapper(SentenceMapper sentenceMapper) {
        this.sentenceMapper = sentenceMapper;
    }

    /**
     * @param keywordMapper the keywordMapper to set
     */
    public void setKeywordMapper(KeywordMapper keywordMapper) {
        this.keywordMapper = keywordMapper;
    }

    /**
     * @param topicMapper the topicMapper to set
     */
    public void setTopicMapper(TopicMapper topicMapper) {
        this.topicMapper = topicMapper;
    }

    /**
     * @param myBatisDaoO the myBatisDaoO to set
     */
    public void setMyBatisDaoO(MyBatisDaoPhraseOption myBatisDaoO) {
        this.myBatisDaoO = myBatisDaoO;
    }

    @Override
    @Transactional
    public Sentence getSentence(int sentenceId) {
        MyBatisSentence sentence = sentenceMapper.getSentenceById(sentenceId, false);

        if (sentence != null) {
            fillSentence(sentence);
        }

        return sentence;
    }

    @Override
    @Transactional
    public Sentence getSentence(String sentenceName, Language language, Version basisVersion, DomainName domainName) {
        MyBatisSentence sentence = sentenceMapper.getSentence(sentenceName, language, basisVersion, domainName, false);

        if (sentence != null) {
            fillSentence(sentence);
        }

        return sentence;
    }

    @Override
    @Transactional
    public Sentence insertSentence(Sentence sentence) {

        if (!(sentence instanceof MyBatisSentence)) {
            throw new IllegalArgumentException("The given class " + sentence.getClass().getName() + " is not a supported type of the MyBatis implementation");
        }

        MyBatisSentence s = (MyBatisSentence) sentence;
        sentenceMapper.insertSentence(s);

        // topic
        for (String topicName : s.getTopics()) {
            handleTopic(s.getId(), topicName);
        }

        // keyword
        for (String keywordName : s.getKeywords()) {
            handleKeyword(s.getId(), keywordName);
        }

        // phrase options

        for (MyBatisSentence.Module module : s.getModules()) {

            MyBatisPhraseOption option = module.getPhraseOption();

            if (option.getId() == DbConstants.NOT_A_DB_ID) {
                myBatisDaoO.insertPhraseOption(option);
            }

            // connect sentence with phrase option
            sentenceMapper.insertSentenceModule(s.getId(), option.getId(), module.getModuleNo());
        }

        return s;
    }

    @Override
    @Transactional
    public Sentence updateSentence(Sentence sentence) {

        if (!(sentence instanceof MyBatisSentence)) {
            throw new IllegalArgumentException("The given class " + sentence.getClass().getName() + " is not a supported type of the MyBatis implementation");
        }

        MyBatisSentence s = (MyBatisSentence) sentence;
        sentenceMapper.updateSentence(s);

        // remove mapping entry for keywords and topics, because it is easer to remove all than to check if still exists and so on.

        //sentenceMapper.removeSentenceKeywords(s.getId());
        //sentenceMapper.removeSentenceTopics(s.getId());


        // topic
        //for (String topicName : s.getTopics()) {
        //    handleTopic(s.getId(), topicName);
        //}

        // keyword
        //for (String keywordName : s.getKeywords()) {
        //    handleKeyword(s.getId(), keywordName);
        //}

        // disconnect modules
        sentenceMapper.removeModules(s.getId());

        // phrase options

        for (MyBatisSentence.Module module : s.getModules()) {

            MyBatisPhraseOption option = module.getPhraseOption();

            if (option.getId() == DbConstants.NOT_A_DB_ID) {
                myBatisDaoO.insertPhraseOption(option);
            } else {
                myBatisDaoO.updatePhraseOption(option);
            }

            // connect sentence with phrase option
            sentenceMapper.insertSentenceModule(s.getId(), option.getId(), module.getModuleNo());
        }

        return s;
    }

    private void handleKeyword(int sentenceId, String keywordName) {
        Keyword keyword = keywordMapper.selectKeyword(keywordName);

        if (keyword == null) {
            keyword = new Keyword();
            keyword.setName(keywordName);
            keywordMapper.insertKeyword(keyword);
        }

        sentenceMapper.insertSentenceKeyword(sentenceId, keyword.getId());
    }

    private void handleTopic(int sentenceId, String topicName) {

        Topic topic = topicMapper.selectTopic(topicName);

        if (topic == null) {
            topic = new Topic();
            topic.setName(topicName);
            topicMapper.insertTopic(topic);
        }

        sentenceMapper.insertSentenceTopic(sentenceId, topic.getId());
    }

    private void fillSentence(MyBatisSentence sentence) {

        List<SentenceModule> ms = sentenceMapper.getSentenceModules(sentence.getId());

//Clesius for language with exception like 3.2 we need cycle on structure element


       /* Integer i=1;
        List listN = new ArrayList();
         for ( SentenceStructure.StructureElement  sentenceElement : sentence.getStructure()) {

			Integer _it=0;
             SentenceModule sentenceModule=new SentenceModule();
             for (SentenceModule sentenceModule1 : ms) {*/

        for (SentenceModule sentenceModule : ms) {

               /*if (sentenceModule1.getModuleNo()== sentenceElement.getModuleNo()) {
                   sentenceModule=sentenceModule1;
                   break;
               }
               else
                   sentenceModule=null;
           }
             if (sentenceModule.getPhraseOptionId()==1998)
                 _it=0;

            Integer test=sentenceElement.getItemPartNo();
            if( test==2){
                _it=100000;
            }
            Integer ModuleNo =sentenceModule.getModuleNo();

            if (listN.contains(ModuleNo))
                ModuleNo=ModuleNo*10;*/

            PhraseOption option = myBatisDaoO.getPhraseOption(sentenceModule.getPhraseOptionId());
            sentence.addSentenceModule(sentenceModule.getModuleNo(), option);
	  /*
             PhraseOption option = myBatisDaoO.getPhraseOption(sentenceModule.getPhraseOptionId()+_it);
             sentence.addSentenceModule(i,ModuleNo , option);
             listN.add(ModuleNo);
             i=i+1;
             */
        }

//        sentence.setKeywords(keywordMapper.getSentenceKeywords(sentence.getId()));
//        sentence.setTopics(topicMapper.getSentenceTopics(sentence.getId()));
    }

    @Override
    @Transactional
    public boolean deleteSentence(Version baseVersion, DomainName domainName) {
        sentenceMapper.deleteSentences(baseVersion,domainName);
        return true;
    }
}
