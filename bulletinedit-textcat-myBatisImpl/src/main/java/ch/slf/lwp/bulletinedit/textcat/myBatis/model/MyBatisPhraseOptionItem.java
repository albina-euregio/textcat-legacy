package ch.slf.lwp.bulletinedit.textcat.myBatis.model;

import java.util.ArrayList;
import java.util.List;

import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;
import ch.slf.lwp.bulletinedit.textcat.myBatis.DbConstants;

public class MyBatisPhraseOptionItem implements PhraseOptionItem {

	private int id = DbConstants.NOT_A_DB_ID;
	private int itemNo;
	private boolean deleted;
	private Version version;
	private Version basisVersion;

	// myBatis attributes
	private int optionId = DbConstants.NOT_A_DB_ID;
	private List<MyBatisPhrase> phrases = new ArrayList<MyBatisPhrase>();

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;

		for (MyBatisPhrase phrase : phrases) {
			phrase.setOptionItemId(id);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem#getItemNo()
	 */
	@Override
	public int getItemNo() {
		return itemNo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem#setItemNo(int)
	 */
	@Override
	public void setItemNo(int itemNo) {
		this.itemNo = itemNo;
	}

	/*
	 * (non-Javadoc)
	 * @see ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem#isDeleted()
	 */
	@Override
	public boolean isDeleted() {
		return deleted;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem#setDeleted(boolean
	 * )
	 */
	@Override
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;

		for (MyBatisPhrase phrase : phrases) {
			phrase.setDeleted(deleted);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem#getVersion()
	 */
	@Override
	public Version getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem#setVersion(java
	 * .lang.String)
	 */
	@Override
	public void setVersion(Version version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem#getBasisVersion()
	 */
	@Override
	public Version getBasisVersion() {
		return basisVersion;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem#setBasisVersion
	 * (java.lang.String)
	 */
	@Override
	public void setBasisVersion(Version basisVersion) {
		this.basisVersion = basisVersion;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem#getPhrase(int)
	 */
	@Override
	public Phrase getPhrase(int itemPartNo) {
		for (MyBatisPhrase phrase : phrases) {
			if (phrase.getItemPartNo() == itemPartNo) {
				return phrase;
			}
		}

		return null;
	}

	/**
	 * Returns <code>true</code> if the phrase with the requested id a phrase of
	 * this item.
	 * 
	 * @param phraseId the phrase id
	 * @return <code>true</code> if the phrase with the requested id a phrase of
	 *         this item
	 */
	boolean containsPhrase(int phraseId) {

		for (MyBatisPhrase phrase : phrases) {
			if (phrase.getId() == phraseId) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @return the optionId
	 */
	public int getOptionId() {
		return optionId;
	}

	/**
	 * @param optionId
	 *            the optionId to set
	 */
	public void setOptionId(int optionId) {
		this.optionId = optionId;
	}

	/**
	 * @return the phrases
	 */
	public List<MyBatisPhrase> getPhrases() {
		return phrases;
	}

	/**
	 * @param phrases the phrases to set
	 */
	public void setPhrases(List<MyBatisPhrase> phrases) {
		this.phrases = phrases;
	}

	@Override
	public Phrase createPhrase() {
		MyBatisPhrase phrase = new MyBatisPhrase();
		phrase.setOptionItemId(id);
		phrase.setDeleted(this.deleted);
		phrases.add(phrase);
		return phrase;
	}

	public boolean onlyEmptyPhrases() {

		for (Phrase phrase : phrases) {

			if (phrase.getValue().isEmpty() == false) {
				return false;
			}
		}

		return true;
	}

	@Override
	public void appendPhrase(Phrase phrase) {

		if ((phrase instanceof MyBatisPhrase) == false) {
			throw new IllegalArgumentException(phrase.getClass().getName() + " is not a valide phrase instance for " + this.getClass().getName());
		}

		MyBatisPhrase p = (MyBatisPhrase) phrase;
		p.setOptionItemId(id);

		phrases.add(p);
	}

	public void updateClone(MyBatisPhraseOptionItem item) {
		item.setId(this.id);
		this.setBasisVersion(item.getBasisVersion());
		this.setDeleted(item.isDeleted());
		this.setItemNo(item.getItemNo());
		this.setOptionId(item.getOptionId());
		this.setVersion(item.getVersion());

		// handle phrases
		for (MyBatisPhrase thisPhrase : this.phrases) {
			MyBatisPhrase itemPhrase = (MyBatisPhrase) item.getPhrase(thisPhrase.getItemPartNo());

			if (itemPhrase != null) {
				thisPhrase.updateClone(itemPhrase);
			} else {
				thisPhrase.setDeleted(true);
				item.appendPhrase(thisPhrase);
			}
		}

		// append new phrases to this
		for (MyBatisPhrase itemPhrase : item.phrases) {
			MyBatisPhrase thisPhrase = (MyBatisPhrase) this.getPhrase(itemPhrase.getItemPartNo());

			if (thisPhrase == null) {
				this.appendPhrase(itemPhrase);
			}
		}

	}

}
