package ch.slf.lwp.bulletinedit.textcat.myBatis.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import ch.slf.lwp.bulletinedit.textcat.myBatis.model.extension.Topic;

public interface TopicMapper {

	public void insertTopic(Topic topic);

	public Topic selectTopic(String name);

	/**
	 * Fetch all topics of a sentence.
	 * 
	 * @param sentenceId
	 *            the sentence database id
	 * @return all sentence topics
	 */
	public List<String> getSentenceTopics(@Param("sentenceId") int sentenceId);
}
