package ch.slf.lwp.bulletinedit.textcat.myBatis.persistence;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import ch.slf.lwp.bulletinedit.textcat.TextCatException;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

/**
 * @author gw
 * 
 */
public class VersionTypeHandler extends BaseTypeHandler {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.ibatis.type.BaseTypeHandler#setNonNullParameter(java.sql.
	 * PreparedStatement, int, java.lang.Object,
	 * org.apache.ibatis.type.JdbcType)
	 */
	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, Object parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.ibatis.type.BaseTypeHandler#getNullableResult(java.sql.ResultSet
	 * , java.lang.String)
	 */
	@Override
	public Object getNullableResult(ResultSet rs, String columnName) throws SQLException {

		String versionString = rs.getString(columnName);

		if (versionString != null) {
			try {
				Version v = Version.create(versionString);
				return v;
			} catch (TextCatException e) {
				throw new SQLException("Instatite version object failed: " + e.getMessage(), e);
			}
		}

		return null;
	}
	
	   @Override
	    public Object getNullableResult(ResultSet rs, int columnIndex) throws SQLException {

	        String versionString = rs.getString(columnIndex);

	        if (versionString != null) {
	            try {
	                Version v = Version.create(versionString);
	                return v;
	            } catch (TextCatException e) {
	                throw new SQLException("Instatite version object failed: " + e.getMessage(), e);
	            }
	        }

	        return null;
	    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.ibatis.type.BaseTypeHandler#getNullableResult(java.sql.
	 * CallableStatement, int)
	 */
	@Override
	public Object getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {

		String versionString = cs.getString(columnIndex);

		if (versionString != null) {
			try {
				Version v = Version.create(versionString);
				return v;
			} catch (TextCatException e) {
				throw new SQLException("Instatite version object failed: " + e.getMessage(), e);
			}
		}

		return null;
	}

}
