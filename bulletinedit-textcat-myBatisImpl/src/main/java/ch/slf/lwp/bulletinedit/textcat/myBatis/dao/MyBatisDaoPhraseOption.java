package ch.slf.lwp.bulletinedit.textcat.myBatis.dao;

import ch.slf.lwp.bulletinedit.textcat.dao.DaoPhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;

/**
 * Necessary Interface to work with the transaction annotation. Spring will
 * create a wrapper for the real class and because of this the type dosn't match
 * any longer.
 * 
 * @author weiss
 */
public interface MyBatisDaoPhraseOption extends DaoPhraseOption {

	/**
	 * Indeed, this method should only visible in package, but the ...
	 * interface.
	 * 
	 * @param id
	 *            the phrase option database id
	 * @return the phrase option or <code>null</code> if no phrase option is
	 *         found
	 */
	public abstract PhraseOption getPhraseOption(int id);

}