package ch.slf.lwp.bulletinedit.textcat.myBatis;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatFactory;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.MyBatisPhraseOption;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.MyBatisSentence;

/**
 * MyBatis implementation of the {@link TextCatFactory}
 * 
 * @author gw
 */
public class MyBatisTextCatFactory implements TextCatFactory {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.TextCatFactory#createSentence()
	 */
	@Override
	public Sentence createSentence(DomainName domainName) {
		MyBatisSentence s = new MyBatisSentence();
		s.setDomainName(domainName);
		return s;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.TextCatFactory#createPhraseOption()
	 */
	@Override
	public PhraseOption createPhraseOption(DomainName domainName) {
		MyBatisPhraseOption o = new MyBatisPhraseOption();
		o.setDomainName(domainName);
		return o;
	}

}
