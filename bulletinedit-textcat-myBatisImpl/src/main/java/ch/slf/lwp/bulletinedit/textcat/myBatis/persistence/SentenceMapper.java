package ch.slf.lwp.bulletinedit.textcat.myBatis.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.MyBatisSentence;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.extension.SentenceModule;

public interface SentenceMapper {

	/**
	 * Returns a sentence by its sentence number and language
	 * 
	 * @param sentenceName
	 *            the sentence name
	 * @param language
	 *            the sentence language
	 * @param basisVersion
	 *            the basis version
	 * @param domainName
	 *            the sentence domain name
	 * @param undeleted
	 *            use only undeleted or all sentences
	 * @return the sentence or <code>null</code> if not exists.
	 */
	public MyBatisSentence getSentence(@Param("sentenceName") String sentenceName, @Param("language") Language language, @Param("basisVersion") Version basisVersion, @Param("domainName") DomainName domainName, @Param("undeleted") boolean undeleted);

	/**
	 * Returns a sentence body without phrase options by its id.
	 * 
	 * @param sentenceId
	 *            the sentence id
	 * @param undeleted
	 *            use only undeleted or all sentences
	 * @return the sentence or <code>null</code> if not exists.
	 */
	public MyBatisSentence getSentenceById(@Param("sentenceId") int sentenceId, @Param("undeleted") boolean undeleted);

	/**
	 * Returns all sentence phrase options.
	 * 
	 * @param sentenceId
	 *            the sentence database id
	 * @return the phrase options database id.
	 */
	public List<SentenceModule> getSentenceModules(@Param("sentenceId") int sentenceId);

	/**
	 * Insert a new sentence.
	 * 
	 * @param myBatisSentence
	 *            the sentence to insert
	 */
	public void insertSentence(MyBatisSentence myBatisSentence);

	/**
	 * Connect a sentence with its phrase option. The structure number is the
	 * position name of the phrase option inside the sentence.
	 * 
	 * @param sentenceId
	 *            the sentence database id
	 * @param phraseOptionId
	 *            the phrase option database id
	 * @param moduleNo
	 *            the module number for the phrase option inside the sentence
	 *            structure
	 */
	public void insertSentenceModule(@Param("sentenceId") int sentenceId, @Param("phraseOptionId") int phraseOptionId, @Param("moduleNo") int moduleNo);

	/**
	 * Connect a sentence with a keyword.
	 * 
	 * @param sentenceId
	 *            the sentence database id
	 * @param keywordId
	 *            the keyword database id
	 */
	public void insertSentenceKeyword(@Param("sentenceId") int sentenceId, @Param("keywordId") int keywordId);

	/**
	 * Connect a sentence with a topic.
	 * 
	 * @param sentenceId
	 *            the sentence database id
	 * @param topicId
	 *            the topic database id
	 */
	public void insertSentenceTopic(@Param("sentenceId") int sentenceId, @Param("topicId") int topicId);

	/**
	 * 
	 * @param language
	 * @param domainName
	 * @param undeleted
	 * @return
	 */
	public List<MyBatisSentence> getAllSentences(@Param("language") Language language, @Param("domainName") DomainName domainName, @Param("undeleted") boolean undeleted);

	/**
	 * 
	 * @param language
	 * @param domainName
	 * @param undeleted
	 * @return
	 */
	public List<SentenceModule> getAllSentencModules(@Param("language") Language language, @Param("domainName") DomainName domainName, @Param("undeleted") boolean undeleted);

	/**
	 * Update the sentence
	 * 
	 * @param myBatisSentence
	 *            the sentence to update
	 */
	public void updateSentence(MyBatisSentence myBatisSentence);

	/**
	 * Delete all keyword mapping entries of the given sentence id.
	 * 
	 * @param sentenceId
	 */
	public void removeSentenceKeywords(@Param("sentenceId") int sentenceId);

	/**
	 * Delete all topic mapping entries of the given sentence id.
	 * 
	 * @param sentenceId
	 */
	public void removeSentenceTopics(@Param("sentenceId") int sentenceId);

	public void updateSentencesDeleteFalg(@Param("basisVersion") Version basisVersion, @Param("domainName") DomainName domainName, @Param("deleted") boolean b);

	public void deleteSentences(@Param("basisVersion") Version basisVersion, @Param("domainName") DomainName domainName);

	public void deleteSentenceByLanguage(@Param("language") Language language, @Param("version") Version version, @Param("domainName") DomainName domainName);

	public void recodeSentences(@Param("oldDomainID") int oldDomainID, @Param("newDomainID") int newDomainID);

	public void removeModules(@Param("sentenceId") int sentenceId);
}
