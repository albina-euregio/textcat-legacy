package ch.slf.lwp.bulletinedit.textcat.myBatis.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.MyBatisPhrase;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.MyBatisPhraseOption;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.MyBatisPhraseOptionItem;

/**
 * The phrase option mapper contains all database access methods for phrase
 * options, including methods for phrase option item and phrases.
 * 
 * @author gw
 */
public interface PhraseOptionMapper {

	/**
	 * Select a phrase option by its database id or by its number and language.
	 * 
	 * @param id
	 *            the phrase option database id
	 * @return the phrase option
	 */
	public MyBatisPhraseOption getPhraseOptionById(@Param("phraseOptionId") int id);

	/**
	 * Select a phrase option by its number and language.
	 * 
	 * @param phraseOptionName
	 *            the phrase option name
	 * @param language
	 *            the phrase option language
	 * @param domainName
	 *            the domain name
	 * @param basisVersion
	 *            the basis version
	 * @param undeleted
	 *            use only undeleted or all phrase option
	 * @return the phrase option
	 */
	public MyBatisPhraseOption getPhraseOption(@Param("phraseOptionName") String phraseOptionName, @Param("language") Language language, @Param("basisVersion") Version basisVersion, @Param("domainName") DomainName domainName, @Param("undeleted") boolean undeleted);

	/**
	 * Insert a new phrase option. The id will be created automatically.
	 * 
	 * @param phraseOption
	 *            the phraseOption to insert
	 */
	public void insertPhraseOption(MyBatisPhraseOption phraseOption);

	/**
	 * Insert the new phrase option.
	 * 
	 * @param phraseOptionItem
	 *            the phrase option to insert.
	 */
	public void insertPhraseOptionItem(MyBatisPhraseOptionItem phraseOptionItem);

	/**
	 * Insert a new phrase. The id will be created automatically.
	 * 
	 * @param phrase
	 *            the phrase to insert.
	 */
	public void insertPhrase(MyBatisPhrase phrase);

	/**
	 *
	 * @param language
	 * @param domainName
	 * @param undeleted
	 * @return
	 */
	public List<MyBatisPhraseOption> getAllPhraseOptions(@Param("language") Language language, @Param("domainName") DomainName domainName, @Param("undeleted") boolean undeleted);


	/**
	 * 
	 * @param language
	 * @param domainName
	 * @param undeleted
	 * @return
	 */
	public List<MyBatisPhraseOption> getAllPhraseOptionsIt(@Param("language") Language language, @Param("domainName") DomainName domainName, @Param("undeleted") boolean undeleted);


	/**
	 * Update the phrase option.
	 * @param phraseOption the phrase option to update
	 */
	public void updatePhraseOption(MyBatisPhraseOption phraseOption);

	/**
	 * Update the phrase option item.
	 * @param item the item to update
	 */
	public void updatePhraseOptionItem(MyBatisPhraseOptionItem item);

	/**
	 * Update the phrase.
	 * @param phrase the phrase to update
	 */
	public void updatePhrase(MyBatisPhrase phrase);

	public void updatePhraseOptionsDeleteFlag(@Param("basisVersion") Version basisVersion, @Param("domainName") DomainName domainName, @Param("deleted") boolean b);

	public void updatePhraseOptionItemsDeleteFlag(@Param("basisVersion") Version basisVersion, @Param("domainName") DomainName domainName, @Param("deleted") boolean b);

	public boolean deletePhraseOptions(@Param("basisVersion") Version basisVersion, @Param("domainName") DomainName domainName);

	public void deletePhraseOptionsByLanguage(@Param("language") Language language, @Param("version") Version version, @Param("domainName") DomainName domainName);

	public void deletePhraseOptionItem(@Param("phraseOptionItemId") int id);

	public void deletePhrase(@Param("phraseId") int id);

	public void recodePhraseOptions(@Param("oldDomainID") int oldDomainID, @Param("newDomainID") int newDomainID);

}
