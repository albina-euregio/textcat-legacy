package ch.slf.lwp.bulletinedit.textcat.myBatis.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import ch.slf.lwp.bulletinedit.textcat.myBatis.model.extension.Keyword;

public interface KeywordMapper {

	/**
	 * Insert a new keyword
	 * 
	 * @param keyword
	 *            the keyword to insert.
	 */
	public void insertKeyword(Keyword keyword);

	/**
	 * Fetch a keyword by its string representation
	 * 
	 * @param keyword
	 *            the keyword
	 * @return the Keyword object
	 */
	public Keyword selectKeyword(String name);

	/**
	 * Fetch all keywords of a sentence.
	 * 
	 * @param sentenceId
	 *            the sentence database id
	 * @return all sentence keywords
	 */
	public List<String> getSentenceKeywords(@Param("sentenceId") int sentenceId);

}
