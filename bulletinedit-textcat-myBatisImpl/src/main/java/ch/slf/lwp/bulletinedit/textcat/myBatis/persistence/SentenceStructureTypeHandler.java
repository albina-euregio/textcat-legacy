package ch.slf.lwp.bulletinedit.textcat.myBatis.persistence;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructure;

public class SentenceStructureTypeHandler extends BaseTypeHandler {

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, Object parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.toString());
	}

	@Override
	public Object getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String structure = rs.getString(columnName);

		if (structure != null) {
			return SentenceStructure.parse(structure);
		}

		return null;
	}

	@Override
	public Object getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String structure = cs.getString(columnIndex);

		if (structure != null) {
			return SentenceStructure.parse(structure);
		}

		return null;
	}
	
    @Override
    public Object getNullableResult(ResultSet rs,  int columnIndex) throws SQLException {
        String structure = rs.getString(columnIndex);

        if (structure != null) {
            return SentenceStructure.parse(structure);
        }

        return null;
    }
	

}
