package ch.slf.lwp.bulletinedit.textcat.myBatis.model.extension;

import ch.slf.lwp.bulletinedit.textcat.myBatis.DbConstants;

/**
 * MyBatis implementation for a topic
 * 
 * @author gw
 */
public class Topic {

	private int id = DbConstants.NOT_A_DB_ID;
	private String name;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {

		if (o instanceof Topic) {

			Topic t = (Topic) o;

			if (this.name != null) {
				return this.name.equals(t.name);
			}
		}

		return false;
	}

	@Override
	public int hashCode() {

		if (this.name != null) {
			return this.name.hashCode();
		}

		return super.hashCode();
	}

}
