package ch.slf.lwp.bulletinedit.textcat.myBatis.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructure;
import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructure.StructureElement;
import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructureModule;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;
import ch.slf.lwp.bulletinedit.textcat.myBatis.DbConstants;

/**
 * 
 * @author gw
 */
public class MyBatisSentence implements Sentence {

	private int id = DbConstants.NOT_A_DB_ID;
	private String name;
	private String header;
	private Language language;
	private Version version;
	private Version basisVersion;
	private SentenceStructure structure;
	private String remark;
	private boolean deleted;
	private boolean jokerSentence;

	private DomainName domainName;
	private List<String> keywords = new ArrayList<String>();
	private List<String> topics = new ArrayList<String>();
	
	//Sentence modules with the moduleNo as select key.
	private HashMap<Integer, Module> modules = new HashMap<Integer, MyBatisSentence.Module>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#getId()
	 */
	@Override
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#getName()
	 */
	@Override
	public String getName() {
		return name;
	}


	@Override
	public void setName(String name) {
		this.name = name;
	}



	@Override
	public void setHeader(String header) {
		this.header= header;
	}

	@Override
	public String getHeader() {
		return header;
	}

		/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#getLanguage()
	 */
	@Override
	public Language getLanguage() {
		return language;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.Sentence#setLanguage(ch.slf.lwp
	 * .bulletinedit.textcat.Language)
	 */
	@Override
	public void setLanguage(Language language) {
		this.language = language;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#getVersion()
	 */
	@Override
	public Version getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.Sentence#setVersion(java.lang.String
	 * )
	 */
	@Override
	public void setVersion(Version version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#getBasisVersion()
	 */
	@Override
	public Version getBasisVersion() {
		return basisVersion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.Sentence#setBasisVersion(java.lang
	 * .String)
	 */
	@Override
	public void setBasisVersion(Version basisVersion) {
		this.basisVersion = basisVersion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#getStructure()
	 */
	@Override
	public SentenceStructure getStructure() {
		return structure;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.Sentence#setStructure(ch.slf.lwp
	 * .bulletinedit.textcat.model.util.SentenceStructure)
	 */
	@Override
	public void setStructure(SentenceStructure structure) {
		this.structure = structure;
	}

	/**
	 * @return the sentence structure as string
	 */
	public String getStructureString() {
		return structure.toString();
	}

	/**
	 * Set the sentence structure as string
	 * @param structureString the structure string to set
	 */
	public void setStructureString(String structureString) {
		this.structure = SentenceStructure.parse(structureString);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#getRemark()
	 */
	@Override
	public String getRemark() {
		return remark;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.Sentence#setRemark(java.lang.String
	 * )
	 */
	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#isDeleted()
	 */
	@Override
	public boolean isDeleted() {
		return deleted;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#setDeleted(boolean)
	 */
	@Override
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#isJokerSentence()
	 */
	@Override
	public boolean isJokerSentence() {
		return jokerSentence;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.Sentence#setJokerSentence(boolean)
	 */
	@Override
	public void setJokerSentence(boolean jokerSentence) {
		this.jokerSentence = jokerSentence;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#getDomainName()
	 */
	@Override
	public DomainName getDomainName() {
		return domainName;
	}

	/**
	 * @param domainName
	 *            the domainName to set
	 */
	public void setDomainName(DomainName domainName) {
		this.domainName = domainName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#getKeywords()
	 */
	@Override
	public List<String> getKeywords() {
		return keywords;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#setKeywords(List)
	 */
	@Override
	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#getTopics()
	 */
	@Override
	public List<String> getTopics() {
		return topics;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#setTopics(List)
	 */
	@Override
	public void setTopics(List<String> topics) {
		this.topics = topics;
	}

	/*
	 * end of getter and setter
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.Sentence#addKeyword(java.lang.String
	 * )
	 */
	@Override
	public void addKeyword(String keyword) {
		this.keywords.add(keyword);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.Sentence#addTopic(java.lang.String)
	 */
	@Override
	public void addTopic(String topic) {
		this.topics.add(topic);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#getStructureSize()
	 */
	@Override
	public int getStructureSize() {
		return structure.size();
	}


	//Clesius
	//posNo per moduli doppi
	@Override
	public void addSentenceModule(int posNo, int moduleNo, PhraseOption phraseOption) {
		if ((phraseOption instanceof MyBatisPhraseOption) == false) {
			throw new IllegalArgumentException("PhraseOption implementation " + phraseOption.getClass().getName() + "is not supported");
		}

		modules.put(moduleNo, new Module(moduleNo, (MyBatisPhraseOption)phraseOption));
		//modules.put(posNo, new Module(moduleNo, (MyBatisPhraseOption)phraseOption));
	}

		@Override
	public void addSentenceModule(int moduleNo, PhraseOption phraseOption) {
		
		if ((phraseOption instanceof MyBatisPhraseOption) == false) {
			throw new IllegalArgumentException("PhraseOption implementation " + phraseOption.getClass().getName() + "is not supported");
		}
		
		modules.put(moduleNo, new Module(moduleNo, (MyBatisPhraseOption)phraseOption));
	}

	@Override
	public PhraseOption createSentenceModule(int moduleNo) {
		MyBatisPhraseOption phraseOption = new MyBatisPhraseOption();
		phraseOption.setDomainName(domainName);
		phraseOption.setLanguage(language);
		phraseOption.setVersion(this.version);
		phraseOption.setBasisVersion(this.basisVersion);
		addSentenceModule(moduleNo, phraseOption);
		return phraseOption;
	}

	/**
	 * @return the module set
	 */
	public Collection<Module> getModules() {
		return modules.values();
	}
	
	public static class Module {

		private int moduleNo;
		private MyBatisPhraseOption phraseOption;
		
		private Module(int moduleNo, MyBatisPhraseOption phraseOption) {
			this.moduleNo = moduleNo;
			this.phraseOption = phraseOption;
		}

		public int getModuleNo() {
			return moduleNo;
		}

		public MyBatisPhraseOption getPhraseOption() {
			return phraseOption;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see ch.slf.lwp.bulletinedit.textcat.model.Sentence#moduleIterator()
	 */
	@Override
	public Iterator<SentenceStructureModule> moduleIterator() {
		return new SentenceStructureModuleIterator(modules, structure.iterator());
	}

	private static class SentenceStructureModuleIterator implements Iterator<SentenceStructureModule> {

		private HashMap<Integer, Module> modules;
		private Iterator<SentenceStructure.StructureElement> it;

		private SentenceStructureModuleIterator(HashMap<Integer, Module> modules, Iterator<StructureElement> it) {
			this.modules = modules;
			this.it = it;
		}

		@Override
		public boolean hasNext() {
			return it.hasNext();
		}

		@Override
		public SentenceStructureModule next() {
			SentenceStructure.StructureElement structureElement = it.next();
			SentenceStructureModule m = new SentenceStructureModule(modules.get(structureElement.getModuleNo()).getPhraseOption(), structureElement);
			return m;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("The remove method is not supported");
		}
		
	}

	@Override
	public PhraseOption getSentenceModule(int moduleNo) {
		Module m = modules.get(moduleNo);
		
		if(m != null) {
			return m.getPhraseOption();
		}
		
		return null;
	}

	@Override
	public void updateClone(Sentence sentence)
	{
		if((sentence instanceof MyBatisSentence)== false) {
			throw new IllegalArgumentException(sentence.getClass().getName() + " is not a valide phrase option item instance for " + this.getClass().getName());
		}
		
		MyBatisSentence s = (MyBatisSentence)sentence;
		
		s.setId(this.getId());
		this.setBasisVersion(s.getBasisVersion());
		this.setDeleted(s.isDeleted());
		this.setDomainName(s.getDomainName());
		this.setJokerSentence(s.isJokerSentence());
		this.setKeywords(s.getKeywords());
		this.setLanguage(s.getLanguage());
		this.setName(s.getName());
		this.setRemark(s.getRemark());
		this.setStructure(s.getStructure());
		this.setTopics(s.getTopics());
		this.setVersion(s.getVersion());
		this.modules = s.modules;
	}

}
