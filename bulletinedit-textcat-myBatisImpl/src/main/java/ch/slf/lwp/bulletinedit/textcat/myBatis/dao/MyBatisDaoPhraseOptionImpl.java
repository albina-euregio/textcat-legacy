package ch.slf.lwp.bulletinedit.textcat.myBatis.dao;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;
import ch.slf.lwp.bulletinedit.textcat.dao.DaoPhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;
import ch.slf.lwp.bulletinedit.textcat.myBatis.DbConstants;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.MyBatisPhrase;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.MyBatisPhraseOption;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.MyBatisPhraseOptionItem;
import ch.slf.lwp.bulletinedit.textcat.myBatis.persistence.PhraseOptionMapper;
import ch.slf.lwp.bulletinedit.textcat.myBatis.persistence.RegionPhraseMapper;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * MyBatis implementation of the TextCat phrase option DAO.
 *
 * @author gw
 * @see DaoPhraseOption
 */
public class MyBatisDaoPhraseOptionImpl implements MyBatisDaoPhraseOption {

    private PhraseOptionMapper phraseOptionMapper;
    private RegionPhraseMapper regionPhraseMapper;

    /**
     * @param phraseOptionMapper the phraseOptionMapper to set
     */
    public void setPhraseOptionMapper(PhraseOptionMapper phraseOptionMapper) {
        this.phraseOptionMapper = phraseOptionMapper;
    }

    public void setRegionPhraseMapper(RegionPhraseMapper regionPhraseMapper) {
        this.regionPhraseMapper = regionPhraseMapper;
    }

    @Override
    @Transactional
    public PhraseOption getPhraseOption(String phraseOptionName, Language language, Version basisVersion, DomainName domainName) {
        return phraseOptionMapper.getPhraseOption(phraseOptionName, language, basisVersion, domainName, false);
    }

    @Override
    @Transactional
    public PhraseOption insertPhraseOption(PhraseOption phraseOption) {

        Pattern pattern = Pattern.compile(".*(@{4}).*");

        if (!(phraseOption instanceof MyBatisPhraseOption)) {
            throw new IllegalArgumentException("The given class " + phraseOption.getClass().getName() + " is not a supported type of the MyBatis implementation");
        }

        MyBatisPhraseOption o = (MyBatisPhraseOption) phraseOption;

        phraseOptionMapper.insertPhraseOption(o);

        // Cache for phrases
        List<MyBatisPhrase> phrases = new ArrayList<MyBatisPhrase>();

        for (MyBatisPhraseOptionItem item : o.getItems()) {
            phraseOptionMapper.insertPhraseOptionItem(item);
         	   phrases.addAll(item.getPhrases());
        }

        /*
        for (MyBatisPhrase phrase : phrases) {
            phraseOptionMapper.insertPhrase(phrase);
        }
        */


        for (MyBatisPhrase phrase : phrases) {
            Matcher matcher = pattern.matcher(phrase.getValue());
            //if value contains regions information, extract regions id and clean string for database insertion
            if (matcher.matches()) {

                List<Integer> regionIds = new ArrayList<>();
                for (String s : phrase.getValue().substring(phrase.getValue().lastIndexOf("@") + 1, phrase.getValue().length()).split(",")) {
                    regionIds.add(Integer.parseInt(s));
                }
                ;
                phrase.setValue(phrase.getValue().substring(0, phrase.getValue().indexOf("@")));
                phraseOptionMapper.insertPhrase(phrase);
                //add record in region_phrase
                for (Integer regionId : regionIds) {
                    Boolean phraseExists = regionPhraseMapper.regionPhraseExists(phrase.getId(), regionId);
                    if (!phraseExists) {
                        regionPhraseMapper.insertRegionPhrase(regionId, phrase.getId());
                    }
                }
            } else
                phraseOptionMapper.insertPhrase(phrase);
        }

        return o;
    }

    @Override
    @Transactional
    public PhraseOption updatePhraseOption(PhraseOption phraseOption) {

        if (!(phraseOption instanceof MyBatisPhraseOption)) {
            throw new IllegalArgumentException("The given class " + phraseOption.getClass().getName() + " is not a supported type of the MyBatis implementation");
        }

        MyBatisPhraseOption o = (MyBatisPhraseOption) phraseOption;

        phraseOptionMapper.updatePhraseOption(o);

        // Cache for phrases
        List<MyBatisPhrase> phrases = new ArrayList<MyBatisPhrase>();

        for (MyBatisPhraseOptionItem item : o.getItems()) {

            if (item.getId() == DbConstants.NOT_A_DB_ID) {
                phraseOptionMapper.insertPhraseOptionItem(item);
            } else {
                if (item.isDeleted()) {
                    phraseOptionMapper.deletePhraseOptionItem(item.getId());
                } else {
                    phraseOptionMapper.updatePhraseOptionItem(item);
                }
            }

            phrases.addAll(item.getPhrases());
        }

        for (MyBatisPhrase phrase : phrases) {
            if (phrase.getId() == DbConstants.NOT_A_DB_ID) {
                phraseOptionMapper.insertPhrase(phrase);
            } else {
                if (phrase.isDeleted()) {
                    phraseOptionMapper.deletePhrase(phrase.getId());
                } else {
                    phraseOptionMapper.updatePhrase(phrase);
                }
            }
        }

        return o;
    }

    @Override
    @Transactional
    public boolean deletePhraseOptions(Version baseVersion, DomainName domainName) {
        phraseOptionMapper.deletePhraseOptions(baseVersion,domainName);
        return true;
    }

    @Override
    @Transactional
    public PhraseOption getPhraseOption(int id) {
        return phraseOptionMapper.getPhraseOptionById(id);
    }

}
