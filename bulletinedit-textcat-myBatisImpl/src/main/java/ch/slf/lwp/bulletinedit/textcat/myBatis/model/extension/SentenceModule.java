package ch.slf.lwp.bulletinedit.textcat.myBatis.model.extension;

import ch.slf.lwp.bulletinedit.textcat.myBatis.DbConstants;

/**
 * Represent a record inside the sentence_m
 * 
 * @author gw
 */
public class SentenceModule {

	private int sentenceId = DbConstants.NOT_A_DB_ID;
	private int phraseOptionId = DbConstants.NOT_A_DB_ID;
	private int moduleNo;
	/**
	 * @return the sentenceId
	 */
	public int getSentenceId() {
		return sentenceId;
	}
	/**
	 * @param sentenceId the sentenceId to set
	 */
	public void setSentenceId(int sentenceId) {
		this.sentenceId = sentenceId;
	}
	/**
	 * @return the phraseOptionId
	 */
	public int getPhraseOptionId() {
		return phraseOptionId;
	}
	/**
	 * @param phraseOptionId the phraseOptionId to set
	 */
	public void setPhraseOptionId(int phraseOptionId) {
		this.phraseOptionId = phraseOptionId;
	}
	/**
	 * @return the moduleNo
	 */
	public int getModuleNo() {
		return moduleNo;
	}
	/**
	 * @param moduleNo the moduleNo to set
	 */
	public void setModuleNo(int moduleNo) {
		this.moduleNo = moduleNo;
	}

}
