package ch.slf.lwp.bulletinedit.textcat.myBatis.model;

import ch.slf.lwp.bulletinedit.textcat.model.RegionPhrase;

public class MyBatisRegionPhrase implements RegionPhrase {

    private int phraseId;
    private int regionId;

    @Override
    public int getRegionId() {
        return this.regionId;
    }

    @Override
    public int getPhraseId() {
        return this.phraseId;
    }
}
