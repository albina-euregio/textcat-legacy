package ch.slf.lwp.bulletinedit.textcat.myBatis.dao;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCat;
import ch.slf.lwp.bulletinedit.textcat.TextCatException;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;
import ch.slf.lwp.bulletinedit.textcat.dao.DaoTextCat;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.MyBatisPhraseOption;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.MyBatisSentence;
import ch.slf.lwp.bulletinedit.textcat.myBatis.model.extension.SentenceModule;
import ch.slf.lwp.bulletinedit.textcat.myBatis.persistence.*;
import org.apache.logging.log4j.LogManager;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * MyBatis implementation of the TextCat TextCat DAO.
 *
 * @author gw
 * @see DaoTextCat
 */
public class MyBatisDaoTextCatImpl implements MyBatisDaoTextCat {

    private PhraseOptionMapper phraseOptionMapper;
    private SentenceMapper sentenceMapper;
    private KeywordMapper keywordMapper;
    private TopicMapper topicMapper;
    private RegionPhraseMapper regionPhraseMapper;

    /**
     * @param phraseOptionMapper the phraseOptionMapper to set
     */
    public void setPhraseOptionMapper(PhraseOptionMapper phraseOptionMapper) {
        this.phraseOptionMapper = phraseOptionMapper;
    }

    /**
     * @param sentenceMapper the sentenceMapper to set
     */
    public void setSentenceMapper(SentenceMapper sentenceMapper) {
        this.sentenceMapper = sentenceMapper;
    }

    public void setKeywordMapper(KeywordMapper keywordMapper) {
        this.keywordMapper = keywordMapper;
    }

    public void setTopicMapper(TopicMapper topicMapper) {
        this.topicMapper = topicMapper;
    }

    @Override
    public void fillTextCat(TextCat textCat, Language language, DomainName domainName) throws TextCatException {

        boolean undeleted = true;

        if (domainName == null)
            domainName = textCat.getDomainName();

        List<MyBatisPhraseOption> options;

        //Clesius
        if (language == Language.it)
            options = phraseOptionMapper.getAllPhraseOptionsIt(language, domainName, undeleted);
        else
            options = phraseOptionMapper.getAllPhraseOptions(language, domainName, undeleted);

        Map<Integer, MyBatisPhraseOption> optionMap = new HashMap<Integer, MyBatisPhraseOption>(options.size());

        for (MyBatisPhraseOption option : options) {
            optionMap.put(option.getId(), option);
        }

        List<MyBatisSentence> sentences = sentenceMapper.getAllSentences(language, domainName, undeleted);
        Map<Integer, MyBatisSentence> sentenceMap = new HashMap<Integer, MyBatisSentence>(sentences.size());

        for (MyBatisSentence sentence : sentences) {
            sentenceMap.put(sentence.getId(), sentence);
//			sentence.setKeywords(keywordMapper.getSentenceKeywords(sentence.getId()));
//			sentence.setTopics(topicMapper.getSentenceTopics(sentence.getId()));
        }

        for (SentenceModule sentenceModule : sentenceMapper.getAllSentencModules(language, domainName, undeleted)) {
            MyBatisSentence s = sentenceMap.get(sentenceModule.getSentenceId());
            MyBatisPhraseOption o = optionMap.get(sentenceModule.getPhraseOptionId());

            if (s != null && o != null) {
                s.addSentenceModule(sentenceModule.getModuleNo(), o);
            } else {
                String msg = "Add phrase option to sentence failed: ";
                msg += s == null ? "\n \t - Sentence with database id " + sentenceModule.getSentenceId() + " not found." : "";
                msg += o == null ? "\n\t - Phrase option with database id " + sentenceModule.getPhraseOptionId() + " not found." : "";
                LogManager.getLogger(this.getClass()).error(msg);
                throw new TextCatException(msg);
            }

        }

        textCat.init(sentences, options);

    }

    @Override
    @Transactional
    public void markVersionAsDeleted(Version baseVersion, DomainName domainName) throws TextCatException {
        this.sentenceMapper.updateSentencesDeleteFalg(baseVersion, domainName, true);
        this.phraseOptionMapper.updatePhraseOptionsDeleteFlag(baseVersion, domainName, true);
        this.phraseOptionMapper.updatePhraseOptionItemsDeleteFlag(baseVersion, domainName, true);
    }

    @Override
    @Transactional
    public void undoDeletionMark(Version baseVersion, DomainName domainName) throws TextCatException {
        this.sentenceMapper.updateSentencesDeleteFalg(baseVersion, domainName, false);
        this.phraseOptionMapper.updatePhraseOptionsDeleteFlag(baseVersion, domainName, false);
        this.phraseOptionMapper.updatePhraseOptionItemsDeleteFlag(baseVersion, domainName, false);
    }

    @Override
    @Transactional
    public void deleteVersion(Version baseVersion, DomainName domainName) throws TextCatException {
        this.sentenceMapper.deleteSentences(baseVersion, domainName);
        this.phraseOptionMapper.deletePhraseOptions(baseVersion, domainName);
    }

    @Override
    @Transactional
    public void deleteLanguageVersion(Language language, Version version, DomainName domainName) throws TextCatException {
        this.sentenceMapper.deleteSentenceByLanguage(language, version, domainName);
        this.phraseOptionMapper.deletePhraseOptionsByLanguage(language, version, domainName);
    }

    @Override
    @Transactional
    public void recodeDomain(int oldDomainID, int newDomainID) throws TextCatException{
        this.sentenceMapper.recodeSentences(oldDomainID, newDomainID);
        this.phraseOptionMapper.recodePhraseOptions(oldDomainID, newDomainID);
    }

}
