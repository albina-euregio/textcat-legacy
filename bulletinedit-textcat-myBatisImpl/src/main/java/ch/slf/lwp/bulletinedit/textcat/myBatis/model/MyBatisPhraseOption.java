package ch.slf.lwp.bulletinedit.textcat.myBatis.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import ch.slf.lwp.bulletinedit.textcat.Constants;
import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;
import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;
import ch.slf.lwp.bulletinedit.textcat.myBatis.DbConstants;

/**
 * 
 * @author gw
 */
public class MyBatisPhraseOption implements PhraseOption {

	private int id = DbConstants.NOT_A_DB_ID; // get
	private String name; // get
	private Language language; // get
	private Version version; // get
	private Version basisVersion;
	private String header; // get
	private String remark;
	private boolean deleted;
	private DomainName domainName; // get

	// myBatis Attributes
	private Collection<MyBatisPhraseOptionItem> items = new ArrayList<MyBatisPhraseOptionItem>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#getId()
	 */
	@Override
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;

		for (MyBatisPhraseOptionItem item : items) {
			item.setOptionId(id);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#setName(java.lang.
	 * String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#getLanguage()
	 */
	@Override
	public Language getLanguage() {
		return language;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#setLanguage(ch.slf
	 * .lwp.bulletinedit.textcat.Language)
	 */
	@Override
	public void setLanguage(Language language) {
		this.language = language;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#getVersion()
	 */
	@Override
	public Version getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#setVersion(java.lang
	 * .String)
	 */
	@Override
	public void setVersion(Version version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#getBasisVersion()
	 */
	@Override
	public Version getBasisVersion() {
		return basisVersion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#setBasisVersion(java
	 * .lang.String)
	 */
	@Override
	public void setBasisVersion(Version basisVersion) {
		this.basisVersion = basisVersion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#getHeader()
	 */
	@Override
	public String getHeader() {
		return header;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#setHeader(java.lang
	 * .String)
	 */
	@Override
	public void setHeader(String header) {
		this.header = header;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#getRemark()
	 */
	@Override
	public String getRemark() {
		return remark;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#setRemark(java.lang
	 * .String)
	 */
	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#isDeleted()
	 */
	@Override
	public boolean isDeleted() {
		return deleted;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#setDeleted(boolean)
	 */
	@Override
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#getDomainName()
	 */
	@Override
	public DomainName getDomainName() {
		return domainName;
	}

	/**
	 * @param domainName
	 *            the domainName to set
	 */
	public void setDomainName(DomainName domainName) {
		this.domainName = domainName;
	}

	/*
	 * end of getter and setter
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#getItemNo(int)
	 */
	@Override
	public int getItemNo(int phraseId) {
		for (MyBatisPhraseOptionItem item : items) {
			if (item.containsPhrase(phraseId)) {
				return item.getItemNo();
			}
		}

		return Constants.DOES_NOT_CONTAINS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#getPhrase(int,
	 * int)
	 */
	@Override
	public Phrase getPhrase(int itemNo, int itemPartNo) {

		for (MyBatisPhraseOptionItem item : items) {
			if (item.getItemNo() == itemNo) {
				return item.getPhrase(itemPartNo);
			}
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#phraseIterator(int)
	 */
	@Override
	public Iterator<Phrase> phraseIterator(int itemPartNo) {

		return new PhraseIterator(itemPartNo, getSortedItems(false).iterator());
	}

	private static class PhraseIterator implements Iterator<Phrase> {

		private int itemPartNo;
		private Iterator<MyBatisPhraseOptionItem> it;

		private PhraseIterator(int itemPartNo, Iterator<MyBatisPhraseOptionItem> it) {
			this.itemPartNo = itemPartNo;
			this.it = it;
		}

		@Override
		public boolean hasNext() {
			return it.hasNext();
		}

		@Override
		public Phrase next() {
			MyBatisPhraseOptionItem item = it.next();
			return item.getPhrase(itemPartNo);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("The remove operation is not supported");
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.model.PhraseOption#phraseOptionItemIterator
	 * ()
	 */
	@Override
	public Iterator<PhraseOptionItem> phraseOptionItemIterator() {

		return new Iterator<PhraseOptionItem>() {

			private Iterator<MyBatisPhraseOptionItem> it = getSortedItems(false).iterator();

			@Override
			public boolean hasNext() {
				return it.hasNext();
			}

			@Override
			public PhraseOptionItem next() {
				return it.next();
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException("The remove operation is not supported");
			}
		};
	}

	private List<MyBatisPhraseOptionItem> getSortedItems(boolean reverse) {
		List<MyBatisPhraseOptionItem> sorted = new ArrayList<MyBatisPhraseOptionItem>(items);

		if (reverse) {
			Collections.sort(sorted, new Comparator<MyBatisPhraseOptionItem>() {
				@Override
				public int compare(MyBatisPhraseOptionItem o1, MyBatisPhraseOptionItem o2) {
					int c = Integer.signum(o2.getItemNo() - o1.getItemNo());

					if (c == 0) {
						// sortOrder is equals
						c = o2.toString().compareTo(o1.toString());

					}

					return c;
				}
			});
		} else {
			Collections.sort(sorted, new Comparator<MyBatisPhraseOptionItem>() {
				@Override
				public int compare(MyBatisPhraseOptionItem o1, MyBatisPhraseOptionItem o2) {
					int c = Integer.signum(o1.getItemNo() - o2.getItemNo());

					if (c == 0) {
						// sortOrder is equals
						c = o1.toString().compareTo(o2.toString());

					}

					return c;
				}
			});
		}

		return sorted;
	}

	/**
	 * @return the items
	 */
	public Collection<MyBatisPhraseOptionItem> getItems() {
		return items;
	}

	/**
	 * @param items
	 *            the items to set
	 */
	public void setItems(Collection<MyBatisPhraseOptionItem> items) {
		this.items = items;
	}

	@Override
	public PhraseOptionItem createPhraseOptionItem() {
		MyBatisPhraseOptionItem item = new MyBatisPhraseOptionItem();
		item.setOptionId(this.id);
		items.add(item);

		return item;
	}

	@Override
	public void trim() {
		Iterator<MyBatisPhraseOptionItem> itemIt = getSortedItems(true).iterator();
		
		while(itemIt.hasNext()) {
			MyBatisPhraseOptionItem item = itemIt.next();
			
			if(item.onlyEmptyPhrases()) {
				items.remove(item);
			} else {
				return;
			}
		}

	}

	@Override
	public void appendPhraseOptionItem(PhraseOptionItem item) {

		if((item instanceof MyBatisPhraseOptionItem)== false) {
			throw new IllegalArgumentException(item.getClass().getName() + " is not a valide phrase option item instance for " + this.getClass().getName());
		}
		
		MyBatisPhraseOptionItem i = (MyBatisPhraseOptionItem)item;
		i.setOptionId(id);
		items.add(i);
	}

	@Override
	public int itemSize() {
		return items.size();
	}

	@Override
	public void updateClone(PhraseOption option)
	{
		if((option instanceof MyBatisPhraseOption)== false) {
			throw new IllegalArgumentException(option.getClass().getName() + " is not a valide phrase option item instance for " + this.getClass().getName());
		}
		
		MyBatisPhraseOption o = (MyBatisPhraseOption)option;
		
		o.setId(this.id);
		
		this.setBasisVersion(o.getBasisVersion());
		this.setDeleted(o.isDeleted());
		this.setDomainName(o.getDomainName());
		this.setHeader(o.getHeader());
		this.setLanguage(o.getLanguage());
		this.setName(o.getName());
		this.setRemark(o.getRemark());
		this.setVersion(o.getVersion());
		
		// clone update items
		List<MyBatisPhraseOptionItem> thisList = this.getSortedItems(false);
		List<MyBatisPhraseOptionItem> oList = o.getSortedItems(false);
		
		int minSize = Math.min(thisList.size(), oList.size());
		
		int itemNo = 0;
		
		for(; itemNo < minSize; itemNo++) {
			thisList.get(itemNo).updateClone(oList.get(itemNo));
		}
		
		if(thisList.size() > oList.size()) {
			
			for(; itemNo < thisList.size(); itemNo++) {
				MyBatisPhraseOptionItem thisItem = thisList.get(itemNo);
				thisItem.setDeleted(true);
				o.appendPhraseOptionItem(thisItem);
			}
			
		}
		
		if(thisList.size() < oList.size()) {
			for(; itemNo < oList.size(); itemNo++) {
				appendPhraseOptionItem(oList.get(itemNo));
			}
		}
		
	}

}
