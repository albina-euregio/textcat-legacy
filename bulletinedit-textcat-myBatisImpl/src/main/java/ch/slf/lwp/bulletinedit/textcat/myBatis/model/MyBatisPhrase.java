package ch.slf.lwp.bulletinedit.textcat.myBatis.model;

import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.myBatis.DbConstants;

public class MyBatisPhrase implements Phrase {

	private int id = DbConstants.NOT_A_DB_ID;
	private String value = "";
	private int itemPartNo;
	private boolean removePunctuationBefore;
	private boolean spaceAfter;
	private boolean spaceBefore;
	private boolean incorrect;
	private String rgn = "";


	// non persitence
	private boolean deleted = false;

	// myBatis attributes
	private int optionItemId = DbConstants.NOT_A_DB_ID;

	/**
	 * @return the id
	 */
	@Override
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the value
	 */
	@Override
	public String getValue() {
		return value != null ? value : "";
	}

	/**
	 * @param value
	 *            the value to set
	 */
	@Override
	public void setValue(String value) {
		this.value = value != null ? value : "";
	}


	/**
	 * @return the rgn
	 */
	@Override
	public String getRgn() {return rgn != null ? rgn : "";	}

	/**
	 * @param rgn
	 *            the value to set
	 */
	@Override
	public void setRgn(String rgn) {this.rgn= rgn!= null ? rgn : "";}

	/**
	 * @return the itemPartNo
	 */
	@Override
	public int getItemPartNo() {
		return itemPartNo;
	}

	/**
	 * @param itemPartNo
	 *            the itemPartNo to set
	 */
	@Override
	public void setItemPartNo(int itemPartNo) {
		this.itemPartNo = itemPartNo;
	}

	/**
	 * @return the removePunctuationBefore
	 */
	@Override
	public boolean isRemovePunctuationBefore() {
		return removePunctuationBefore;
	}

	/**
	 * @param removePunctuationBefore
	 *            the removePunctuationBefore to set
	 */
	@Override
	public void setRemovePunctuationBefore(boolean removePunctuationBefore) {
		this.removePunctuationBefore = removePunctuationBefore;
	}

	/**
	 * @return the spaceAfter
	 */
	@Override
	public boolean isSpaceAfter() {
		return spaceAfter;
	}

	/**
	 * @param spaceAfter
	 *            the spaceAfter to set
	 */
	@Override
	public void setSpaceAfter(boolean spaceAfter) {
		this.spaceAfter = spaceAfter;
	}

	/**
	 * @return the spaceBefore
	 */
	@Override
	public boolean isSpaceBefore() {
		return spaceBefore;
	}

	/**
	 * @param spaceBefore
	 *            the spaceBefore to set
	 */
	@Override
	public void setSpaceBefore(boolean spaceBefore) {
		this.spaceBefore = spaceBefore;
	}

	/**
	 * @return the incorrect
	 */
	@Override
	public boolean isIncorrect() {
		return incorrect;
	}

	/**
	 * @param incorrect
	 *            the incorrect to set
	 */
	@Override
	public void setIncorrect(boolean incorrect) {
		this.incorrect = incorrect;
	}

	/**
	 * @return the optionItemId
	 */
	public int getOptionItemId() {
		return optionItemId;
	}

	/**
	 * @param optionItemId
	 *            the optionItemId to set
	 */
	public void setOptionItemId(int optionItemId) {
		this.optionItemId = optionItemId;
	}

	/**
	 * @return the deleted
	 */
	public boolean isDeleted()
	{
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted)
	{
		this.deleted = deleted;
	}

	public void updateClone(MyBatisPhrase phrase)
	{
		phrase.setId(this.getId());
		
		this.setDeleted(phrase.isDeleted());
		this.setIncorrect(phrase.isIncorrect());
		this.setItemPartNo(phrase.getItemPartNo());
		this.setOptionItemId(phrase.getOptionItemId());
		this.setRemovePunctuationBefore(phrase.isRemovePunctuationBefore());
		this.setSpaceAfter(phrase.isSpaceAfter());
		this.setSpaceBefore(phrase.isSpaceBefore());
		this.setValue(phrase.getValue());
	}

}
