package ch.slf.lwp.bulletinedit.textcat.myBatis.persistence;

import org.apache.ibatis.annotations.Param;

public interface RegionPhraseMapper {

    public void insertRegionPhrase(@Param("regionId") int regionId, @Param("phraseId") int phraseId);

    public Boolean regionPhraseExists(@Param("phrase") int phraseId, @Param("regionId") int regionId);

    public Integer getPhraseIdByValue(@Param("phrase") String phrase);

}
