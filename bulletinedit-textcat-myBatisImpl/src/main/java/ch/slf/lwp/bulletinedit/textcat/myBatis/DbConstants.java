/**
 * 
 */
package ch.slf.lwp.bulletinedit.textcat.myBatis;

/**
 * @author weiss
 *
 */
public class DbConstants {

	/**
	 * Id value for everything which is no representation inside the database
	 */
	public static final int NOT_A_DB_ID = -1;
}
