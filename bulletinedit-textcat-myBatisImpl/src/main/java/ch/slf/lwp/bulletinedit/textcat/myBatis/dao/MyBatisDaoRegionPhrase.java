package ch.slf.lwp.bulletinedit.textcat.myBatis.dao;

import ch.slf.lwp.bulletinedit.textcat.model.RegionPhrase;

public interface MyBatisDaoRegionPhrase {
    public RegionPhrase insertRegionPhrase(Integer regionId, Integer phraseId);

    public boolean regionPhraseExists(String phrase, Integer regionId);

}
