package ch.slf.lwp.bulletinedit.textcat.myBatis.model;

import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;
import junit.framework.TestCase;

public class MyBatisPhraseOptionItemTest extends TestCase {
	
	private MyBatisPhraseOptionItem item;

	protected void setUp() throws Exception {
		super.setUp();
		item = new MyBatisPhraseOptionItem();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		item = null;
	}

	public void testUpdateClone() {
		item.setBasisVersion(new Version(1, 0));
		item.setVersion(new Version(2, 0));
		item.setDeleted(true);
		item.setId(10);
		item.setItemNo(11);
		item.setOptionId(12);
		addPhrase(item, "old value 1", 10);
		
		MyBatisPhraseOptionItem newItem = new MyBatisPhraseOptionItem();
		newItem.setBasisVersion(new Version(1, 1));
		newItem.setVersion(new Version(2, 1));
		newItem.setDeleted(false);
		newItem.setId(20);
		newItem.setItemNo(21);
		newItem.setOptionId(22);
		addPhrase(newItem, "new value 1", 10);
		
		item.updateClone(newItem);
		
		// check old item
		assertEquals("Old basis version", new Version(1,1).toString(), item.getBasisVersion().toString());
		assertEquals("Old version", new Version(2,1).toString(), item.getVersion().toString());
		assertEquals("Old deleted", false, item.isDeleted());
		assertEquals("Old id", 10, item.getId());
		assertEquals("Old item no", 21, item.getItemNo());
		assertEquals("Old option id", 22, item.getOptionId());
		assertEquals("Old phrase count", 1, item.getPhrases().size());
		
		// compare items
		assertEquals("Compare basis version", newItem.getBasisVersion().toString(), item.getBasisVersion().toString());
		assertEquals("Compare version", newItem.getVersion().toString(), item.getVersion().toString());
		assertEquals("Compare deleted", newItem.isDeleted(), item.isDeleted());
		assertEquals("Compare id", newItem.getId(), item.getId());
		assertEquals("Compare item no", newItem.getItemNo(), item.getItemNo());
		assertEquals("Compare option id", newItem.getOptionId(), item.getOptionId());
		assertEquals("Compare phrase count", newItem.getPhrases().size(), item.getPhrases().size());
		
		// check phrase option item id
		assertEquals("Old phrase option item id", 10, ((MyBatisPhrase)item.getPhrase(10)).getOptionItemId());
		assertEquals("Compare phrase option item id", ((MyBatisPhrase)newItem.getPhrase(10)).getOptionItemId(), ((MyBatisPhrase)item.getPhrase(10)).getOptionItemId());

		assertEquals("Compare phrase value", ((MyBatisPhrase)newItem.getPhrase(10)).getValue(), ((MyBatisPhrase)item.getPhrase(10)).getValue());
	}

	public void testUpdateCloneNewHasMoreParts() {
		item.setId(10);
		addPhrase(item, "old value 1", 10);
		
		MyBatisPhraseOptionItem newItem = new MyBatisPhraseOptionItem();
		newItem.setId(20);
		addPhrase(newItem, "new value 1", 10);
		addPhrase(newItem, "new value 2", 20);
		
		item.updateClone(newItem);
		
		// check old item
		assertEquals("Old phrase count", 2, item.getPhrases().size());
		// compare items
		assertEquals("Compare phrase count", newItem.getPhrases().size(), item.getPhrases().size());
		
		// check phrase option item id
		assertEquals("Old phrase option item id", 10, ((MyBatisPhrase)item.getPhrase(10)).getOptionItemId());
		assertEquals("Compare phrase option item id", ((MyBatisPhrase)newItem.getPhrase(10)).getOptionItemId(), ((MyBatisPhrase)item.getPhrase(10)).getOptionItemId());
		assertEquals("Compare phrase value", ((MyBatisPhrase)newItem.getPhrase(10)).getValue(), ((MyBatisPhrase)item.getPhrase(10)).getValue());
		assertEquals("Old is deleted", false, ((MyBatisPhrase)item.getPhrase(10)).isDeleted());
		assertEquals("Compare phrase value", ((MyBatisPhrase)newItem.getPhrase(10)).isDeleted(), ((MyBatisPhrase)item.getPhrase(10)).isDeleted());

		assertEquals("Old phrase option item id", 10, ((MyBatisPhrase)item.getPhrase(20)).getOptionItemId());
		assertEquals("Compare phrase option item id", ((MyBatisPhrase)newItem.getPhrase(20)).getOptionItemId(), ((MyBatisPhrase)item.getPhrase(20)).getOptionItemId());
		assertEquals("Compare phrase value", ((MyBatisPhrase)newItem.getPhrase(20)).getValue(), ((MyBatisPhrase)item.getPhrase(20)).getValue());
		assertEquals("Old is deleted", false, ((MyBatisPhrase)item.getPhrase(20)).isDeleted());
		assertEquals("Compare phrase value", ((MyBatisPhrase)newItem.getPhrase(20)).isDeleted(), ((MyBatisPhrase)item.getPhrase(20)).isDeleted());
	}
	
	public void testUpdateCloneOldHasMoreParts() {
		item.setId(10);
		addPhrase(item, "old value 1", 10);
		addPhrase(item, "old value 2", 20);
		
		MyBatisPhraseOptionItem newItem = new MyBatisPhraseOptionItem();
		newItem.setId(20);
		addPhrase(newItem, "new value 1", 10);
		
		item.updateClone(newItem);
		
		// check old item
		assertEquals("New phrase count", 2, newItem.getPhrases().size());
		// compare items
		assertEquals("Compare phrase count", newItem.getPhrases().size(), item.getPhrases().size());
		
		// check phrase option item id
		assertEquals("Old phrase option item id", 10, ((MyBatisPhrase)item.getPhrase(10)).getOptionItemId());
		assertEquals("Compare phrase option item id", ((MyBatisPhrase)newItem.getPhrase(10)).getOptionItemId(), ((MyBatisPhrase)item.getPhrase(10)).getOptionItemId());
		assertEquals("Compare phrase value", ((MyBatisPhrase)newItem.getPhrase(10)).getValue(), ((MyBatisPhrase)item.getPhrase(10)).getValue());
		assertEquals("Old is deleted", false, ((MyBatisPhrase)item.getPhrase(10)).isDeleted());
		assertEquals("Compare phrase value", ((MyBatisPhrase)newItem.getPhrase(10)).isDeleted(), ((MyBatisPhrase)item.getPhrase(10)).isDeleted());

		assertEquals("Old phrase option item id", 10, ((MyBatisPhrase)item.getPhrase(20)).getOptionItemId());
		assertEquals("Compare phrase option item id", ((MyBatisPhrase)newItem.getPhrase(20)).getOptionItemId(), ((MyBatisPhrase)item.getPhrase(20)).getOptionItemId());
		assertEquals("Compare phrase value", ((MyBatisPhrase)newItem.getPhrase(20)).getValue(), ((MyBatisPhrase)item.getPhrase(20)).getValue());
		assertEquals("Old is deleted", true, ((MyBatisPhrase)item.getPhrase(20)).isDeleted());
		assertEquals("Compare phrase value", ((MyBatisPhrase)newItem.getPhrase(20)).isDeleted(), ((MyBatisPhrase)item.getPhrase(20)).isDeleted());
	}


	
	private void addPhrase(MyBatisPhraseOptionItem item, String value, int itemPartNo) {
		Phrase phrase = item.createPhrase();
		phrase.setValue(value);
		phrase.setItemPartNo(itemPartNo);
	}

}
