package ch.slf.lwp.bulletinedit.textcat.myBatis.model;

import java.util.Iterator;

import junit.framework.TestCase;
import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;
import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

public class MyBatisPhraseOptionTest extends TestCase {

	private MyBatisPhraseOption option;
	private int optionId = -9;

	protected void setUp() throws Exception {
		super.setUp();
		option = new MyBatisPhraseOption();
		option.setId(optionId);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		option = null;
	}

	public void testHandlePhraseOptionItem_Create() {
		PhraseOptionItem item3 = option.createPhraseOptionItem();
		PhraseOptionItem item1 = option.createPhraseOptionItem();
		PhraseOptionItem item2 = option.createPhraseOptionItem();
		item1.setItemNo(1);
		item2.setItemNo(2);
		item3.setItemNo(3);

		assertEquals("Item size", 3, option.getItems().size());

		Iterator<PhraseOptionItem> it = option.phraseOptionItemIterator();
		assertEquals("1: first item", item1, it.next());
		assertEquals("1: second item", item2, it.next());
		assertEquals("1: third item", item3, it.next());

		item1.setItemNo(4);
		it = option.phraseOptionItemIterator();
		assertEquals("2: first item", item2, it.next());
		assertEquals("2: second item", item3, it.next());
		assertEquals("2: third item", item1, it.next());

	}

	public void testTrimSingle() {
		PhraseOptionItem item1 = option.createPhraseOptionItem();
		item1.setItemNo(1);
		addPhrase(item1, 1, "");

		PhraseOptionItem item2 = option.createPhraseOptionItem();
		item2.setItemNo(2);
		addPhrase(item2, 1, "Schnee");

		PhraseOptionItem item3 = option.createPhraseOptionItem();
		item3.setItemNo(3);
		addPhrase(item3, 1, "");

		option.trim();

		assertEquals("Item size", 2, option.getItems().size());

		Iterator<PhraseOptionItem> it = option.phraseOptionItemIterator();
		assertEquals("1: first item", item1, it.next());
		assertEquals("1: second item", item2, it.next());
	}

	public void testTrimSplit() {
		PhraseOptionItem item1 = option.createPhraseOptionItem();
		item1.setItemNo(1);
		addPhrase(item1, 1, "Otto");
		addPhrase(item1, 2, "");

		PhraseOptionItem item2 = option.createPhraseOptionItem();
		item2.setItemNo(2);
		addPhrase(item2, 1, "");
		addPhrase(item2, 2, "Schnee");

		PhraseOptionItem item3 = option.createPhraseOptionItem();
		item3.setItemNo(3);
		addPhrase(item3, 1, "");
		addPhrase(item3, 2, "");

		option.trim();

		assertEquals("Item size", 2, option.getItems().size());

		Iterator<PhraseOptionItem> it = option.phraseOptionItemIterator();
		assertEquals("1: first item", item1, it.next());
		assertEquals("1: second item", item2, it.next());
	}

	public void testPhraseIterator() {
		PhraseOptionItem item1 = option.createPhraseOptionItem();
		item1.setItemNo(1);
		Phrase phrase1_1 = addPhrase(item1, 1, "");
		Phrase phrase1_2 = addPhrase(item1, 2, "");

		PhraseOptionItem item2 = option.createPhraseOptionItem();
		item2.setItemNo(2);
		Phrase phrase2_1 = addPhrase(item2, 1, "");
		Phrase phrase2_2 = addPhrase(item2, 2, "Schnee");

		PhraseOptionItem item3 = option.createPhraseOptionItem();
		item3.setItemNo(3);
		Phrase phrase3_1 = addPhrase(item3, 1, "");
		Phrase phrase3_2 = addPhrase(item3, 2, "");

		Iterator<Phrase> it = option.phraseIterator(1);

		assertEquals("itemPartNo 1 - 1", phrase1_1, it.next());
		assertEquals("itemPartNo 1 - 2", phrase2_1, it.next());
		assertEquals("itemPartNo 1 - 3", phrase3_1, it.next());
		assertEquals("itemPartNo 1 - No more items", false, it.hasNext());

		it = option.phraseIterator(2);

		assertEquals("itemPartNo 2 - 1", phrase1_2, it.next());
		assertEquals("itemPartNo 2 - 2", phrase2_2, it.next());
		assertEquals("itemPartNo 2 - 3", phrase3_2, it.next());
		assertEquals("itemPartNo 2 - No more items", false, it.hasNext());

	}
	
	public void testUpdateClone() {
		option.setBasisVersion(new Version(1,0));
		option.setDeleted(true);
		option.setDomainName(DomainName.INTEGRATION_TEST);
		option.setHeader("old header");
		option.setLanguage(Language.de);
		option.setName("old name");
		option.setRemark("old remark");
		option.setVersion(new Version(2,0));
		addItem(option, 10);
		addItem(option, 20);
		
		MyBatisPhraseOption newOption = new MyBatisPhraseOption();
		newOption.setId(100);
		newOption.setBasisVersion(new Version(1,1));
		newOption.setDeleted(false);
		newOption.setDomainName(DomainName.STAGING);
		newOption.setHeader("new header");
		newOption.setLanguage(Language.en);
		newOption.setName("new name");
		newOption.setRemark("new remark");
		newOption.setVersion(new Version(2,1));
		addItem(newOption, 10);
		addItem(newOption, 20);
		
		option.updateClone(newOption);
		
		// check old option
		assertEquals("Old id", optionId, option.getId());
		assertEquals("Old basis version", new Version(1,1).toString(), option.getBasisVersion().toString());
		assertEquals("Old deleted", false, option.isDeleted());
		assertEquals("Old doamain name", DomainName.STAGING, option.getDomainName());
		assertEquals("Old header", "new header", option.getHeader());
		assertEquals("Old language", Language.en, option.getLanguage());
		assertEquals("Old name", "new name", option.getName());
		assertEquals("Old remark", "new remark", option.getRemark());
		assertEquals("Old version", new Version(2,1).toString(), option.getVersion().toString());
		
		// compare options
		assertEquals("Compare id", newOption.getId(), option.getId());
		assertEquals("Compare basis version", newOption.getBasisVersion().toString(), option.getBasisVersion().toString());
		assertEquals("Compare deleted", newOption.isDeleted(), option.isDeleted());
		assertEquals("Compare doamain name", newOption.getDomainName(), option.getDomainName());
		assertEquals("Compare header", newOption.getHeader(), option.getHeader());
		assertEquals("Compare language", newOption.getLanguage(), option.getLanguage());
		assertEquals("Compare name", newOption.getName(), option.getName());
		assertEquals("Compare remark", newOption.getRemark(), option.getRemark());
		assertEquals("Compare version", newOption.getVersion().toString(), option.getVersion().toString());
		
		// items
		assertEquals("Old item size", 2, option.getItems().size());
		assertEquals("Compare item size", newOption.getItems().size(), option.getItems().size());
		
		Iterator<PhraseOptionItem> items = option.phraseOptionItemIterator();
		Iterator<PhraseOptionItem> newItems = newOption.phraseOptionItemIterator();
		
		while(items.hasNext()) {
			MyBatisPhraseOptionItem h = (MyBatisPhraseOptionItem)items.next();
			assertEquals("Old option id", optionId, h.getOptionId());
			
			if(h.getItemNo() == 10) {
				assertEquals("item 10", false, h.isDeleted());
			} else if (h.getItemNo() == 20) {
				assertEquals("item 20", false, h.isDeleted());
			} else {
				fail("Unexpected item " + h.getItemNo());
			}
		}

		while(newItems.hasNext()) {
			MyBatisPhraseOptionItem h = (MyBatisPhraseOptionItem)newItems.next();
			assertEquals("New option id", optionId, h.getOptionId());
			
			if(h.getItemNo() == 10) {
				assertEquals("item 10", false, h.isDeleted());
			} else if (h.getItemNo() == 20) {
				assertEquals("item 20", false, h.isDeleted());
			} else {
				fail("Unexpected item " + h.getItemNo());
			}
		}

	}
	
	public void testUpdateCloneNewHasMore() {

		addItem(option, 10);
		
		MyBatisPhraseOption newOption = new MyBatisPhraseOption();
		newOption.setId(100);
		addItem(newOption, 10);
		addItem(newOption, 20);

		option.updateClone(newOption);

		// items
		assertEquals("Old item size", 2, option.getItems().size());
		assertEquals("Compare item size", newOption.getItems().size(), option.getItems().size());
		
		Iterator<PhraseOptionItem> items = option.phraseOptionItemIterator();
		Iterator<PhraseOptionItem> newItems = newOption.phraseOptionItemIterator();
		
		while(items.hasNext()) {
			MyBatisPhraseOptionItem h = (MyBatisPhraseOptionItem)items.next();
			assertEquals("Old option id", optionId, h.getOptionId());
			
			if(h.getItemNo() == 10) {
				assertEquals("item 10", false, h.isDeleted());
			} else if (h.getItemNo() == 20) {
				assertEquals("item 20", false, h.isDeleted());
			} else {
				fail("Unexpected item " + h.getItemNo());
			}
		}

		while(newItems.hasNext()) {
			MyBatisPhraseOptionItem h = (MyBatisPhraseOptionItem)newItems.next();
			assertEquals("New option id", optionId, h.getOptionId());
			
			if(h.getItemNo() == 10) {
				assertEquals("item 10", false, h.isDeleted());
			} else if (h.getItemNo() == 20) {
				assertEquals("item 20", false, h.isDeleted());
			} else {
				fail("Unexpected item " + h.getItemNo());
			}
		}

	}

	public void testUpdateCloneOldHasMore() {

		addItem(option, 20);
		addItem(option, 10);
		
		MyBatisPhraseOption newOption = new MyBatisPhraseOption();
		newOption.setId(100);
		addItem(newOption, 10);

		option.updateClone(newOption);

		// items
		assertEquals("Old item size", 2, option.getItems().size());
		assertEquals("Compare item size", newOption.getItems().size(), option.getItems().size());
		
		Iterator<PhraseOptionItem> items = option.phraseOptionItemIterator();
		Iterator<PhraseOptionItem> newItems = newOption.phraseOptionItemIterator();
		
		while(items.hasNext()) {
			MyBatisPhraseOptionItem h = (MyBatisPhraseOptionItem)items.next();
			assertEquals("Old option id", optionId, h.getOptionId());
			
			if(h.getItemNo() == 10) {
				assertEquals("item 10", false, h.isDeleted());
			} else if (h.getItemNo() == 20) {
				assertEquals("item 20", true, h.isDeleted());
			} else {
				fail("Unexpected item " + h.getItemNo());
			}
		}

		while(newItems.hasNext()) {
			MyBatisPhraseOptionItem h = (MyBatisPhraseOptionItem)newItems.next();
			assertEquals("New option id", optionId, h.getOptionId());
			
			if(h.getItemNo() == 10) {
				assertEquals("item 10", false, h.isDeleted());
			} else if (h.getItemNo() == 20) {
				assertEquals("item 20", true, h.isDeleted());
			} else {
				fail("Unexpected item " + h.getItemNo());
			}
		}

	}


	private Phrase addPhrase(PhraseOptionItem item, int itemPartNo, String value) {
		Phrase phrase = item.createPhrase();
		phrase.setItemPartNo(itemPartNo);
		phrase.setValue(value);
		return phrase;
	}
	
	private void addItem(MyBatisPhraseOption option, int itemNo) {
		PhraseOptionItem item = option.createPhraseOptionItem();
		item.setItemNo(itemNo);
	}

}
