package ch.slf.lwp.bulletinedit.textcat.myBatis.model;

import junit.framework.TestCase;

public class MyBatisPhraseTest extends TestCase {

	MyBatisPhrase mPhrase;

	protected void setUp() throws Exception {
		super.setUp();
		mPhrase = new MyBatisPhrase();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		mPhrase = null;
	}

	/**
	 * Test the verbal given pre and post condition
	 */
	public void testGetPhraseSetPhrase() {
		mPhrase.setValue(null);
		String actual = mPhrase.getValue();

		assertEquals("Pre2Post", "", actual);
	}

	/**
	 * Test the verbal given post condition, because MyBatis dosn't call the set
	 * method for null values! However, for a empty string,
	 * <code>null<code>  will be saved inside the database!
	 * Therefore <code>null<code> is not allowed as return value. Nullpointer exception!
	 */
	public void testGetPhraseWithoutSetPhrase() {
		String actual = mPhrase.getValue();
		assertEquals("Pre2Post", "", actual);
	}
	
	/**
	 * test the update and clone methode
	 */
	public void testUpdatAndClone() {
		
		mPhrase.setDeleted(true);
		mPhrase.setId(10);
		mPhrase.setIncorrect(true);
		mPhrase.setItemPartNo(11);
		mPhrase.setOptionItemId(12);
		mPhrase.setRemovePunctuationBefore(true);
		mPhrase.setSpaceAfter(true);
		mPhrase.setSpaceBefore(true);
		mPhrase.setValue("old value");
		
		MyBatisPhrase newPhrase = new MyBatisPhrase();
		newPhrase.setDeleted(false);
		newPhrase.setId(20);
		newPhrase.setIncorrect(false);
		newPhrase.setItemPartNo(21);
		newPhrase.setOptionItemId(22);
		newPhrase.setRemovePunctuationBefore(false);
		newPhrase.setSpaceAfter(false);
		newPhrase.setSpaceBefore(false);
		newPhrase.setValue("new value");
		
		mPhrase.updateClone(newPhrase);
		
		// check old phrase
		assertEquals("Old phrase id", 10,  mPhrase.getId());
		assertEquals("Old phrase deleted", false,  mPhrase.isDeleted());
		assertEquals("Old phrase incorrect", false,  mPhrase.isIncorrect());
		assertEquals("Old phrase item part no", 21,  mPhrase.getItemPartNo());
		assertEquals("Old phrase option item id", 22,  mPhrase.getOptionItemId());
		assertEquals("Old phrase remove punctuation before", false,  mPhrase.isRemovePunctuationBefore());
		assertEquals("Old phrase space after", false,  mPhrase.isSpaceAfter());
		assertEquals("Old phrase space before", false,  mPhrase.isSpaceBefore());
		assertEquals("Old phrase value", "new value",  mPhrase.getValue());
		
		// compare old with new
		assertEquals("Compare phrase id", mPhrase.getId(),  newPhrase.getId());
		assertEquals("Compare phrase deleted", newPhrase.isDeleted(),  mPhrase.isDeleted());
		assertEquals("Compare phrase incorrect", newPhrase.isIncorrect(),  mPhrase.isIncorrect());
		assertEquals("Compare phrase item part no", newPhrase.getItemPartNo(),  mPhrase.getItemPartNo());
		assertEquals("Compare phrase remove punctuation before", newPhrase.isRemovePunctuationBefore(),  mPhrase.isRemovePunctuationBefore());
		assertEquals("Compare phrase space after", newPhrase.isSpaceAfter(),  mPhrase.isSpaceAfter());
		assertEquals("Compare phrase space before", newPhrase.isSpaceBefore(),  mPhrase.isSpaceBefore());
		assertEquals("Compare phrase value", mPhrase.getValue(),  mPhrase.getValue());
	}

}
