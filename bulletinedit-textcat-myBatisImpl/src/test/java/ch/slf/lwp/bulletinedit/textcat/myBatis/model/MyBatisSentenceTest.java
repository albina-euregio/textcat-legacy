package ch.slf.lwp.bulletinedit.textcat.myBatis.model;

import junit.framework.TestCase;
import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

public class MyBatisSentenceTest extends TestCase {

	MyBatisSentence sentence = null;

	protected void setUp() throws Exception {
		super.setUp();
		sentence = new MyBatisSentence();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		sentence = null;
	}

	public void testUpdateClone() {

		sentence.setBasisVersion(new Version(1, 0));
		sentence.setDeleted(true);
		sentence.setDomainName(DomainName.INTEGRATION_TEST);
		sentence.setId(10);
		sentence.setJokerSentence(false);
		sentence.addKeyword("old keyword");
		sentence.setLanguage(Language.de);
		sentence.setName("old name");
		sentence.setRemark("old remark");
		sentence.setStructureString("1.1");
		sentence.addTopic("old topic");
		sentence.setVersion(new Version(2, 0));
		sentence.addSentenceModule(3, new MyBatisPhraseOption());

		MyBatisSentence newSentence = new MyBatisSentence();
		newSentence.setBasisVersion(new Version(1, 1));
		newSentence.setDeleted(false);
		newSentence.setDomainName(DomainName.STAGING  );
		newSentence.setId(20);
		newSentence.setJokerSentence(true);
		newSentence.addKeyword("new keyword");
		newSentence.setLanguage(Language.en);
		newSentence.setName("new name");
		newSentence.setRemark("new remark");
		newSentence.setStructureString("2.1");
		newSentence.addTopic("new topic");
		newSentence.setVersion(new Version(2, 1));
		newSentence.addSentenceModule(2, new MyBatisPhraseOption());

		sentence.updateClone(newSentence);

		// check old
		assertEquals("Old basis version", new Version(1, 1).toString(), sentence.getBasisVersion().toString());
		assertEquals("Old deleted", false, sentence.isDeleted());
		assertEquals("Old domain name", DomainName.STAGING, sentence.getDomainName());
		assertEquals("Old id", 10, sentence.getId());
		assertEquals("Old joker sentence", true, sentence.isJokerSentence());
		assertEquals("Old keyword", "new keyword", sentence.getKeywords().get(0));
		assertEquals("Old language", Language.en, sentence.getLanguage());
		assertEquals("Old name", "new name", sentence.getName());
		assertEquals("Old remark", "new remark", sentence.getRemark());
		assertEquals("Old structure", "2.1", sentence.getStructureString());
		assertEquals("Old topic", "new topic", sentence.getTopics().get(0));
		assertEquals("Old version", new Version(2, 1).toString(), sentence.getVersion().toString());

		// compare old
		assertEquals("Old basis version", newSentence.getBasisVersion().toString(), sentence.getBasisVersion().toString());
		assertEquals("Old deleted", newSentence.isDeleted(), sentence.isDeleted());
		assertEquals("Old domain name", newSentence.getDomainName(), sentence.getDomainName());
		assertEquals("Old id", newSentence.getId(), sentence.getId());
		assertEquals("Old joker sentence", newSentence.isJokerSentence(), sentence.isJokerSentence());
		assertEquals("Old keyword", newSentence.getKeywords().get(0), sentence.getKeywords().get(0));
		assertEquals("Old language", newSentence.getLanguage(), sentence.getLanguage());
		assertEquals("Old name", newSentence.getName(), sentence.getName());
		assertEquals("Old remark", newSentence.getRemark(), sentence.getRemark());
		assertEquals("Old structure", newSentence.getStructureString(), sentence.getStructureString());
		assertEquals("Old topic", newSentence.getTopics().get(0), sentence.getTopics().get(0));
		assertEquals("Old version", newSentence.getVersion().toString(), sentence.getVersion().toString());

		// check modules
		assertEquals("Old module size", 1, sentence.getModules().size());
		assertEquals("Compare module size", newSentence.getModules().size(), sentence.getModules().size());

		assertNull("Old: Fetch old module", sentence.getSentenceModule(3));
		assertNull("New: Fetch old module", newSentence.getSentenceModule(3));

		assertNotNull("Old: Fetch new module", sentence.getSentenceModule(2));
		assertNotNull("New: Fetch new module", newSentence.getSentenceModule(2));
	}

}
