## Regions

Text files may specify a region for certain phrase lines, such as in `Gebiet.txt`:

```
Begin: Tyrol
…
Line: Stubaier Alpen
End: Tyrol
```

The API returns phrase with their region (as `rgn`, as string, space delimited IDs):

```json
{ "id": 766, "dbid": 740, "rgn": " 2", "value": "Stubaier Alpen" }
```

The frontend may be called with a certain region `r`: `c_pm.html?l=de&r=2`

The frontend hides phrases with a different region: `style='${option.Rgn}'`
