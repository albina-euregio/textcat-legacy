import ch.slf.lwp.bulletinedit.textcat.*;
import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

public class TextcatRunner {
    private static Logger log = LogManager.getLogger(TextcatRunner.class);

    public static void main(String args[]) throws TextCatException, IOException {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("classpath*:META-INF/*spring.xml");
        buildText(ctx);
//        exportJson(ctx);
    }

    private static void buildText(ClassPathXmlApplicationContext ctx) throws TextCatException {
        BoText boTextSvc = ctx.getBean(BoText.class);
//        System.out.println(boTextSvc.buildText("11[5452,2079,2656[7314[5754],6771[5753],4059[2275,4881]],4769]", TextCatLanguages.Language.de,DomainName.DESCRIPTION_OF_DANGER));
        System.out.println(boTextSvc.buildText("11[5452,2079,2656[7314[5754],6771[5753],4059[2275,4881]],4769]", TextCatLanguages.Language.it, DomainName.PRODUCTION));
//        System.out.println(boTextSvc.buildText("341[29274,25693,26343[31246[29576],30677[29575],27801[25939,28642]],28511,28413]", TextCatLanguages.Language.de,DomainName.DESCRIPTION_OF_DANGER));
    }
//
//    private static void exportJson(ClassPathXmlApplicationContext ctx) throws TextCatException, IOException {
//        MyBatisDaoTextCatImpl myBatisDaoTextCatImpl=ctx.getBean(MyBatisDaoTextCatImpl.class);
//        JsonTextCat jsonTextCat=new JsonTextCat();
//        jsonTextCat.init(
//                myBatisDaoTextCatImpl.get
//        );
//        Json sentences=jsonTextCat.getJsonSentences();
//        Json options=jsonTextCat.getJsonOptions();
//        Json phrases=jsonTextCat.getJsonPhrases();
//        Json idx=jsonTextCat.getJsonWord2SentenceIndx();
//        Json idx2=jsonTextCat.getFullJsonWord2SentenceIndx();
//
//        ObjectMapper converter = new ObjectMapper();
//        FileUtils.writeStringToFile(new File("./sentences.json"),converter.writeValueAsString(sentences));
//        FileUtils.writeStringToFile(new File("./options.json"),converter.writeValueAsString(options));
//        FileUtils.writeStringToFile(new File("./phrases.json"),converter.writeValueAsString(phrases));
//        FileUtils.writeStringToFile(new File("./indexes.json"),converter.writeValueAsString(idx));
//        FileUtils.writeStringToFile(new File("./indexes-sub.json"),converter.writeValueAsString(idx2));
//    }

}
