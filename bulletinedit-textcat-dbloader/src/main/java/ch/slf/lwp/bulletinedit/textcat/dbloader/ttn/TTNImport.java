package ch.slf.lwp.bulletinedit.textcat.dbloader.ttn;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.ZipException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.dbloader.common.AppStatus;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.AppStatus.Status;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

/**
 * @author gw
 * 
 */
public class TTNImport {

	private static Logger log = LogManager.getLogger(TTNImport.class);
	private static AppStatus status = new AppStatus();

	// spring application context
	private static ApplicationContext ctx;

	private static final DomainName DEFAULT_DOMAIN = DomainName.PRODUCTION;

	// options
	private static final String OPTION_FILE = "f";
	private static final String OPTION_FILE_LONG = "file";

	private static final String OPTION_DOMAIN = "d";
	private static final String OPTION_DOMAIN_LONG = "domain";

	private static final String OPTION_BASIS_VERSION = "bv";
	private static final String OPTION_BASIS_VERSION_LONG = "basisversion";

	private static final String OPTION_PREVIOUS_BASIS_VERSION = "prebv";
	private static final String OPTION_PREVIOUS_BASIS_VERSION_LONG = "prebasisversion";

	private static final String OPTION_REPLACE = "r";
	private static final String OPTION_REPLACE_LONG = "replace";

	/**
	 * Initialize the command line options.
	 * 
	 * @return the command line options
	 */
	@SuppressWarnings("static-access")
	private static Options initOptions() {
		Options options = new Options();

		Option zipFile = OptionBuilder.withArgName("FILE,LANGUAGE,VERSION").hasArgs(3).withValueSeparator(',').withDescription("TTN zip file with language and version (may be repeated)").withLongOpt(OPTION_FILE_LONG).create(OPTION_FILE);
		options.addOption(zipFile);

		Option domain = new Option(OPTION_DOMAIN, OPTION_DOMAIN_LONG, true, "The domain name. If no name is given, '" + DEFAULT_DOMAIN + "' used as domain name.");
		domain.setRequired(false);
		domain.setArgName("DOMAIN");
		options.addOption(domain);

		Option basisVersion = new Option(OPTION_BASIS_VERSION, OPTION_BASIS_VERSION_LONG, true, "Basis version");
		basisVersion.setArgName("BASIS VERSION");
		basisVersion.setRequired(true);
		options.addOption(basisVersion);

		Option previousBasisVersion = new Option(OPTION_PREVIOUS_BASIS_VERSION, OPTION_PREVIOUS_BASIS_VERSION_LONG, true, "Previous basis version");
		previousBasisVersion.setArgName("PREVIOUS BASIS VERSION");
		previousBasisVersion.setRequired(true);
		options.addOption(previousBasisVersion);

		Option replace = OptionBuilder.hasArg(false).withDescription("Replace flag").withLongOpt(OPTION_REPLACE_LONG).create(OPTION_REPLACE);
		options.addOption(replace);

		return options;
	}

	/**
	 * Print help to the standard out.
	 * 
	 * @param options
	 *            the command line options
	 */
	public static void printHelp(Options options) {
		HelpFormatter hf = new HelpFormatter();
		hf.setWidth(80);
		hf.printHelp("Import", "\nDatabase import for TTN files.\nOption list:", options, "", true);
	}

	static void parseFileArgument(CommandLine cmdl, ArrayList<TTNFile> files) {

		if (cmdl.hasOption(TTNImport.OPTION_FILE)) {
			log.info("Parse file parameter: " + TTNImport.OPTION_FILE);
			String[] values = cmdl.getOptionValues(TTNImport.OPTION_FILE);

			if (values.length == 0) {
				log.error("Not enough arguments for file parameter.");
				status.addStatus(Status.ERROR);
				return;
			} else if (values.length % 3 != 0) {
				log.error("TTN zip file spec needs to be a multiple of 3");
				status.addStatus(Status.ERROR);
				return;
			}

			for (int i = 0; i < values.length; i += 3) {
				TTNFile ttnFile = new TTNFile();
				ttnFile.file = new File(values[i]);

				if (!ttnFile.file.exists()) {
					log.error("Import file " + ttnFile.file.getAbsolutePath() + " does not exists.");
					status.addStatus(Status.ERROR);
				}

				ttnFile.language = Language.valueOf(values[i + 1].toLowerCase());

				ttnFile.version = null;

				try {
					ttnFile.version = Version.create(values[i + 2]);
				} catch (Exception e) {
					log.error("Invalid Version argument");
					status.addStatus(Status.ERROR);
				}

				if (status.getTotalStatus() == Status.OK) {
					files.add(ttnFile);
				} else {
					log.error("Parse file parameter " + TTNImport.OPTION_FILE + " failed");
					status.addStatus(Status.ERROR);
				}

			}
		}
	}

	/**
	 * @param args
	 * @return status
	 */
	public static String go(String[] args) {

		/* reset status on each run, otherwise after first error remains in FATAL status */
		status = new AppStatus();

		// for nice command line output
		boolean printCmdlMessage = true;

		// read command line
		Options options = initOptions();

		try {
			// setup application context
			ctx = new ClassPathXmlApplicationContext("classpath*:META-INF/*spring.xml");

			if (log.isDebugEnabled()) {
				log.debug("Spring initalized");
			}

			CommandLineParser parser = new GnuParser();
			CommandLine cmdl = parser.parse(options, args);

			DomainName domainName = DEFAULT_DOMAIN;
			if (cmdl.hasOption(OPTION_DOMAIN)) {
				domainName = DomainName.valueOf(cmdl.getOptionValue(OPTION_DOMAIN).toUpperCase());
			}

			Version basisVersion = null;

			try {
				basisVersion = Version.create(cmdl.getOptionValue(OPTION_BASIS_VERSION));
			} catch (Exception e) {
				log.error("Invalide basis version argument");
				status.addStatus(Status.ERROR);
			}

			Version previousBasisVersion = null;

			try {
				previousBasisVersion = Version.create(cmdl.getOptionValue(OPTION_PREVIOUS_BASIS_VERSION));
			} catch (Exception e) {
				log.error("Invalide previous basis version argument");
				status.addStatus(Status.ERROR);
			}

			ArrayList<TTNFile> files = new ArrayList<>();
			parseFileArgument(cmdl, files);

			boolean replace = cmdl.hasOption(OPTION_REPLACE);

			if (status.getTotalStatus() == Status.OK) {

				TTNTextCatImport imp = ctx.getBean(TTNTextCatImport.class);
				imp.setBasisVersion(basisVersion);
				imp.setDomainName(domainName);
				imp.setReplace(replace);
				imp.setPreviousBasisVersion(previousBasisVersion);

				for (TTNFile ttnFile : files) {
					imp.addZipFile(ttnFile.file, ttnFile.version, ttnFile.language);
				}

				status.mergeAppStatus(imp.runImport());
			}

		} catch (BeansException e) {
			status.addStatus(Status.FATAL);
			log.fatal("Can't initalize spring configuration.", e);
		} catch (ParseException e) {
			status.addStatus(Status.ERROR);
			System.out.println(e.getMessage() + "\n\n");
			printHelp(options);
			// skip finished status text and log file reference
			printCmdlMessage = false;
		} catch (ZipException e) {
			status.addStatus(Status.FATAL);
			log.fatal("Can't read the Zip file.", e);
		} catch (IOException e) {
			status.addStatus(Status.FATAL);
			log.fatal("Can't access the Zip file.", e);
		} catch (Throwable t) {
			status.addStatus(Status.FATAL);
			log.error(t.getClass().getName() + ": Import file failed", t);
		}

		log.info("Import finished with status " + status.getTotalStatus());

		if (printCmdlMessage) {
			System.out.println("Import finished with status " + status.getTotalStatus());
			System.out.println("See logfile(s) for more details.");
		}

		return status.getTotalStatus().toString();

	}

	public static void main(String[] args) {
		go(args);
		System.exit(status.getTotalStatus().ordinal());
	}

	private static class TTNFile {
		File file;
		Version version;
		Language language;
	}

}
