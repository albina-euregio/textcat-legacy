package ch.slf.lwp.bulletinedit.textcat.dbloader;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.AppStatus;

public interface CsvContentHandler {

	/**
	 * Receive notification of the beginning of a document.
	 */
	public abstract void startDocument();

	/**
	 * Receive notification of a new line
	 * 
	 * @param line
	 *            the line
	 */
	public abstract void processLine(String[] line);

	/**
	 * Receive notification of the end of a document.
	 */
	public abstract void endDocument();

	/**
	 * Reset the current status. Returns the old status. The returned AppStatus
	 * Object is not equals to the now used status.
	 * 
	 * @return the old status
	 */
	public abstract AppStatus resetStatus();

	/**
	 * Set the domain name
	 * 
	 * @param domainName
	 *            the domain name to set
	 */
	public abstract void setDomainName(DomainName domainName);
}