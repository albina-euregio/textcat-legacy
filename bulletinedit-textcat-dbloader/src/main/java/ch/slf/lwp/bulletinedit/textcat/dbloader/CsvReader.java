package ch.slf.lwp.bulletinedit.textcat.dbloader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import au.com.bytecode.opencsv.CSVReader;

public class CsvReader {

	private CsvContentHandler handler;

	public void parse(InputStream in) throws IOException {
		CSVReader reader = new CSVReader(new InputStreamReader(in));
		handler.startDocument();

		String[] nextLine;

		while ((nextLine = reader.readNext()) != null) {
			handler.processLine(nextLine);
		}
		
		reader.close();

		handler.endDocument();
	}

	public CsvContentHandler getHandler() {
		return handler;
	}

	public void setHandler(CsvContentHandler handler) {
		this.handler = handler;
	}
	
	
}
