package ch.slf.lwp.bulletinedit.textcat.dbloader.ttn;

import ch.slf.lwp.bulletinedit.textcat.Constants;
import ch.slf.lwp.bulletinedit.textcat.StringUtils;
import ch.slf.lwp.bulletinedit.textcat.TextCatException;
import ch.slf.lwp.bulletinedit.textcat.model.RegionEnum;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.StringUtil;
import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem;
import ch.slf.lwp.bulletinedit.textcat.model.util.SubPhraseOptionName;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;

/**
 * @author gw
 */
class RangeHandler extends ContentHandler {

    Logger log = LogManager.getLogger(this.getClass());

    private static final String REGION_PLACEHOLDER = "@@@@";

    private Map<String, TTNPhraseOption> ttnPhraseOptions = new HashMap<String, RangeHandler.TTNPhraseOption>();

    private HashSet<String> subOptionNames = new HashSet<>();

    void addZipEntry(ZipEntry entry) {

        String rs_CurlyName = StringUtil.removeFileExtension((new File(entry.getName())).getName());

        int itemPartNo = TTNUtils.getItemPartNoFromCurlyName(rs_CurlyName);
        String normalizedCurlyName = TTNUtils.getNormalizedCurlyName(rs_CurlyName);

        TTNPhraseOption ttnOption = null;

        if (ttnPhraseOptions.containsKey(normalizedCurlyName)) {
            ttnOption = ttnPhraseOptions.get(normalizedCurlyName);
        } else {
            ttnOption = new TTNPhraseOption();
            ttnOption.setNormalizedCurlyName(normalizedCurlyName);
            ttnPhraseOptions.put(normalizedCurlyName, ttnOption);
        }

        ttnOption.addItemPart(entry, itemPartNo);
    }

    Map<String, PhraseOption> preparePhraseOptions() throws Exception {

        Map<String, PhraseOption> options = new HashMap<String, PhraseOption>(ttnPhraseOptions.size());

        for (TTNPhraseOption ttnOption : ttnPhraseOptions.values()) {
            PhraseOption option = preparePhraseOption(ttnOption);
            options.put(ttnOption.getNormalizedCurlyName(), option);
        }

        // fix header if it is a sub phrase option and the header is not
        // surrounded by {}. See also bug #1001
        for (String s : subOptionNames) {
            PhraseOption o = options.get(s);

            if (o != null) {
                String convertedHeader = convertNormalizedCurlyName2Header(o.getHeader(), true);
                o.setHeader(convertedHeader);
            }
        }

        return options;
    }

    private PhraseOption preparePhraseOption(TTNPhraseOption ttnOption) throws IOException, TextCatException {

        PhraseOption option = textCatFactory.createPhraseOption(domainName);
        option.setName(ttnOption.getNormalizedCurlyName());

        //Clesius format 2018
        //String header = convertNormalizedCurlyName2Header(ttnOption.getNormalizedCurlyName(), false);
        //option.setHeader(header);

        option.setVersion(version);
        option.setBasisVersion(basisVersion);
        option.setLanguage(language);
        option.setDeleted(false);

        int itemNo = Constants.FIRST_ITEM_NO;

        if (ttnOption.getItemPartCount() == 1) {

            List<String> firstItemPartValues = fetchItemPartValues(ttnOption.getItemPart(1), ttnOption);

            for (String s : firstItemPartValues) {
                PhraseOptionItem item = getItem(option, itemNo);
                phrase2Item(item, Constants.FIRST_ITEM_PART_NO, s);

                itemNo++;
            }

        } else {

            List<String> firstItemPartValues = fetchItemPartValues(ttnOption.getItemPart(1), ttnOption);
            List<String> secondItemPartValues = fetchItemPartValues(ttnOption.getItemPart(2), ttnOption);

            if (firstItemPartValues.size() != secondItemPartValues.size()) {
                throw new TextCatException("Option item part size not equal! " + ttnOption.getNormalizedCurlyName());
            }

            for (int i = 0; i < firstItemPartValues.size(); i++) {
                PhraseOptionItem item = getItem(option, itemNo);
                phrase2Item(item, Constants.FIRST_ITEM_PART_NO, firstItemPartValues.get(i));
                phrase2Item(item, Constants.FIRST_ITEM_PART_NO + 1, secondItemPartValues.get(i));

                itemNo++;
            }

        }

        //Clesius format 2018
        option.setHeader(ttnOption.header);

        return option;
    }

    private PhraseOptionItem getItem(PhraseOption option, int itemNo) {
        PhraseOptionItem item = option.createPhraseOptionItem();
        item.setItemNo(itemNo);
        item.setVersion(version);
        item.setBasisVersion(basisVersion);
        return item;
    }

    private void phrase2Item(PhraseOptionItem item, int itemPartNo, String value) {
        Phrase phrase = item.createPhrase();
        phrase.setItemPartNo(itemPartNo);
        StringUtil.fillPhrase(value, phrase);

        // fetch sub phrase option names to fix header later. See also bug #1001
        List<SubPhraseOptionName> son = StringUtils.getSubPhraseOptionNames(phrase.getValue());

        for (SubPhraseOptionName n : son) {
            subOptionNames.add(n.getPhraseOptionName());
        }

        // correct char, because the database can't store it.
        if (phrase.getValue() != null && phrase.getValue().contains("’")) {
            String newValue = phrase.getValue().replace("’", "'");
            log.info("Apostroph-Problematik: replace " + phrase.getValue() + " -->" + newValue);
            phrase.setValue(newValue);
        }
    }

    /**
     * Parse a range file an returns the list of option item part values.
     *
     * @param itemEntry the item part zip entry
     * @return the value list
     * @throws IOException
     */
    private List<String> fetchItemPartValues(ZipEntry itemEntry, TTNPhraseOption ttnOption) throws IOException {

        List<String> itemPartValues = new ArrayList<String>();
        List<String> itemPartValuestmp = new ArrayList<String>();

        // charsetName
        String charsetName = getCharsetName(itemEntry);

        BufferedReader reader = new BufferedReader(new InputStreamReader(ttnZip.getInputStream(itemEntry), charsetName));
        String line = null;
        String region = null;

        while ((line = reader.readLine()) != null) {

            if (line.trim().isEmpty()) {
                continue;
            }

            //utf-16 BOM
            line=line.replace("\uFEFF","");

            LabelValue labelValue = new LabelValue().set(line);


            //get region
            if (labelValue.getLabel() == TTNLabel.Begin) {
                region = labelValue.getValue();
            }
            if (labelValue.getLabel() == TTNLabel.End) {
                region = null;
            }


            if (labelValue.getLabel() == TTNLabel.Line) {
                // add region
                if (region != null) {
                    //add region marker
                    String newLabel = composeLabelValueWithRegion(labelValue.getValue(), region);

                    /*// only for debug
                    int i = itemPartValuestmp.indexOf(labelValue.getValue());
                    int i2 = itemPartValuestmp.lastIndexOf(labelValue.getValue());
                    int y;
                    if (i2 > i)
                        y = 0;

                    //we have already the phrase, add other region
                    if (i >= 0)
                        itemPartValues.set(i, composeLabelValueWithRegion(itemPartValues.get(i), region));
                    else {*/
                        itemPartValuestmp.add(labelValue.getValue());
                        itemPartValues.add(newLabel);
                    //}
                } else {
                    itemPartValuestmp.add(labelValue.getValue());
                    itemPartValues.add(labelValue.getValue());
                }
            }
            if (labelValue.getLabel() == TTNLabel.RS_Header) {
                ttnOption.header = labelValue.getValue();
            }
        }

        return itemPartValues;
    }

    /**
     * Composes the label value containing the region description
     *
     * @param labelValue label value
     * @param region     region
     * @return description with region
     */
    private String composeLabelValueWithRegion(String labelValue, String region) {
        if (region == null) {
            return labelValue;
        }
        if (labelValue.indexOf(REGION_PLACEHOLDER) > -1)
            return labelValue + "," + regionNameToRegionId(region);
        else
            return labelValue + REGION_PLACEHOLDER + regionNameToRegionId(region);
    }

    /**
     * Converts the region name to its corresponding database id
     *
     * @param region name of the region
     * @return region id
     */
    private Integer regionNameToRegionId(String region) {

        Integer res = null;
        switch (region) {
            case "Switzerland":
                res = RegionEnum.SWITZERLAND.getRegionID();
                break;
            case "Tyrol":
                res = RegionEnum.TYROL.getRegionID();
                break;
            case "South Tyrol":
                res = RegionEnum.SOUTH_TYROL.getRegionID();
                break;
            case "Trentino":
                res = RegionEnum.TRENTINO.getRegionID();
                break;
        }
        return res;
    }

    private String convertNormalizedCurlyName2Header(String normalizedCurlyName, boolean force) {
        // if none '§' is inside the name does the name is not surrounded by
        // '{}' add them

        if (normalizedCurlyName.indexOf("§") == -1 || force) {

            if ((normalizedCurlyName.startsWith("{") && normalizedCurlyName.endsWith("}")) == false) {
                return "{" + normalizedCurlyName + "}";
            }
        }

        return normalizedCurlyName;
    }

    private static class TTNPhraseOption {
        private String normalizedCurlyName;
        private String header;
        private ZipEntry[] itemParts = new ZipEntry[2];

        void setNormalizedCurlyName(String rs_CurlyName) {
            this.normalizedCurlyName = rs_CurlyName;
        }

        String getNormalizedCurlyName() {
            return normalizedCurlyName;
        }

        void addItemPart(ZipEntry item, int itemPartNo) {
            itemParts[itemPartNo - 1] = item;
        }

        int getItemPartCount() {
            return itemParts[1] != null ? 2 : 1;
        }

        ZipEntry getItemPart(int itemPartNo) {
            return itemParts[itemPartNo - 1];
        }
    }

    public String getCharsetName(ZipEntry itemEntry) throws IOException {
        // test encoding
        InputStream bomIn = ttnZip.getInputStream(itemEntry);
        byte[] bom = new byte[4];

        int c = bomIn.read(bom);
        int total = c;

        while (total < 4) {
            c = bomIn.read(bom, total, 4 - total);
            total = total + c;
        }

        bomIn.close();

        byte b1 = (byte) 0xFF;
        byte b2 = (byte) 0xFE;

        if (bom[0] == b1 && bom[1] == b2) {
            return TTNZipFileHandler.FILE_ENCODING;
        } else {
            // return Charset.defaultCharset().name();
            // return "ISO-8859-1";
            return "UTF-8";
        }

    }

}
