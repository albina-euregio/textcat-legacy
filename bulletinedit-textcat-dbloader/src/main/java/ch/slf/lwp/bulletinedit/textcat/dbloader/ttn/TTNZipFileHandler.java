package ch.slf.lwp.bulletinedit.textcat.dbloader.ttn;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.ModelTextCat;
import ch.slf.lwp.bulletinedit.textcat.TextCatException;
import ch.slf.lwp.bulletinedit.textcat.TextCatFactory;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.AppStatus;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.AppStatus.Status;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

/**
 * 
 * @author gw
 * 
 */
public class TTNZipFileHandler {

	private static String FOLDER_NAME_RANGES = "Ranges";
	private static String FOLDER_NAME_SENTENCES = "Sentences";
	//public static String FILE_ENCODING = "UTF-16"; // "ISO-8859-1";
    public static String FILE_ENCODING = "UTF-8"; // "ISO-8859-1";

	private TextCatFactory textCatFactory;

	public void setTextCatFactory(TextCatFactory textCatFactory) {
		this.textCatFactory = textCatFactory;
	}

	private final Logger log = LogManager.getLogger(this.getClass());

	private AppStatus status = new AppStatus();

	private DomainName domainName;
	private Language language;
	private ZipFile ttnZip;
	private Version version;
	private Version basisVersion;

	private RangeHandler rangeHandler = new RangeHandler();
	private SentenceHandler sentenceHandler = new SentenceHandler();

	public void setDomainName(DomainName domainName) {
		this.domainName = domainName;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public void setTtnZip(ZipFile ttnZip) {
		this.ttnZip = ttnZip;
	}

	public void setVersion(Version version) {
		this.version = version;
	}

	public void setBasisVersion(Version basisVersion) {
		this.basisVersion = basisVersion;
	}

	public AppStatus getAppStatus() {
		return this.status;
	}

	private void initHandler(ContentHandler handler) {
		handler.setDomainName(domainName);
		handler.setLanguage(language);
		handler.setTtnZip(ttnZip);
		handler.setVersion(version);
		handler.setBasisVersion(basisVersion);
		handler.setTextCatFactory(textCatFactory);
	}

	public ModelTextCat readZipFile() {
		
		log.info("Process file: " + ttnZip.getName());

		initHandler(rangeHandler);
		initHandler(sentenceHandler);

		ModelTextCat textCat = new ModelTextCat();
		textCat.setDomainName(domainName);
		textCat.setLanguage(language);
		textCat.setVersion(version);
		textCat.setBasisVersion(basisVersion);

		Enumeration<? extends ZipEntry> entries = ttnZip.entries();

		while (entries.hasMoreElements()) {
			ZipEntry ttnEntry = entries.nextElement();

			if (ttnEntry.isDirectory() == false) {

				File parent = (new File(ttnEntry.getName())).getParentFile();

				if (FOLDER_NAME_RANGES.equals(parent.getName())) {
					rangeHandler.addZipEntry(ttnEntry);
				} else if (FOLDER_NAME_SENTENCES.equals(parent.getName())) {
					sentenceHandler.addZipEntry(ttnEntry);
				}

			}
		}

		Map<String, PhraseOption> phraseOptions = null;
		List<Sentence> sentences = null;

		// parse files, build modelsw
		try {
			phraseOptions = rangeHandler.preparePhraseOptions();
			sentenceHandler.setPhraseOptionMap(phraseOptions);
			sentences = sentenceHandler.createSentences();
		} catch (Exception e) {
			log.error("Parse text catalog failed.", e);
			status.addStatus(Status.ERROR);
		}

		if (status.getTotalStatus() == Status.OK) {

			try {
				textCat.init(sentences, new ArrayList<>(phraseOptions.values()));
			} catch (TextCatException e) {
				log.error("save sentences failed.", e);
				status.addStatus(Status.ERROR);
			}

		} else {
			log.info("Skip database import because of exitsting errors.");
		}
		
		log.info("Finished file " + ttnZip.getName() + " with status " + status.getTotalStatus());

		return textCat;
	}

	@Override
	public String toString() {
		return super.toString() + "\n\tdomain name: " + domainName + "; Used import language: " + language + "; Used version: " + version.toString() + "; Used basis version: " + basisVersion.toString() + "; Import file: " + ttnZip.getName();
	}

}
