package ch.slf.lwp.bulletinedit.textcat.dbloader;

import java.util.Arrays;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import ch.slf.lwp.bulletinedit.textcat.BoAdministration;
import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.StringUtils;
import ch.slf.lwp.bulletinedit.textcat.TextCatException;
import ch.slf.lwp.bulletinedit.textcat.TextCatFactory;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.AppStatus;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.StringUtil;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.AppStatus.Status;
import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

/**
 * The PhraseOptionHandler is responsible to parse a phrase option source file.
 * 
 * @author gw
 * 
 */
public class PhraseOptionHandler implements CsvContentHandler {

	private final static char OPTION_START = 'C';
	private final static char OPTION_STRUCTURE = 's';
	private final static char OPTION_REMARK = 'b';
	private final static char OPTION_HEADER = 'h';

	// some OPTION to ignore, just added for sentence compatibility
	private final static char SENTENCE_TOPIC = 't';
	private final static char SENTENCE_KEYWORD = 'k';

	// Clesius
	// NEW FORMAT 2018
	private final static int OPTION_CONTENT_COLUMN = 3;
	//private final static int OPTION_CONTENT_COLUMN = 5;
	private final static int OPTION_SUBHEADER_COLUMN = 6;

	private final static int PHRASE_PART_NO = StringUtil.char2digit('a');

	private final static int RGN_COLUMN = 6;

	private final Logger log = LogManager.getLogger(this.getClass());
	private AppStatus status = new AppStatus();

	// injected textcat factory
	private TextCatFactory textCatFactory;
	private BoAdministration boAdministration;

	private DomainName domainName = null;
	private PhraseOption currentOption;
	private String currentSubHeader = null;;

	private boolean skipCurrentOption = false;

	/**
	 * @param textCatFactory
	 *            the textCatFactory to set
	 */
	public void setTextCatFactory(TextCatFactory textCatFactory) {
		this.textCatFactory = textCatFactory;
	}

	/**
	 * @param boAdministration
	 *            the boAdministration to set
	 */
	public void setBoAdministration(BoAdministration boAdministration) {
		this.boAdministration = boAdministration;
	}

	@Override
	public void startDocument() {
	}

	@Override
	public void processLine(String[] line) {

		if (StringUtil.isEmptyLine(line)) {
			return; // nothing to do with a empty line
		}

		char key = line[0].charAt(0);

		if (skipCurrentOption) {
			if (OPTION_START != key) {
				return;
			}
		}

		try {
			switch (key) {
			case OPTION_START:
				processOptionStart(line);
				break;
			case OPTION_STRUCTURE:
				processOptionStructure(line);
				break;
			case OPTION_REMARK:
				processOptionRemark(line);
				break;
			case OPTION_HEADER:
				processOptionHeader(line);
				break;
			case SENTENCE_KEYWORD:
				// does not exists, but the file have ...
				break;
			case SENTENCE_TOPIC:
				// does not exists, but the file have ...
				break;
			default:
				// first field value must be a integer
				boolean process = true;
				
				try {
					Integer.parseInt(line[0]);
				} catch (NumberFormatException e) {
					process = false;
					log.warn("line does not start with a number:'" + Arrays.toString(line) + "' - line will be skiped");
					status.addStatus(Status.WARN);
				}
				
				if(process) {
					processOptionContent(line);
				}
				
				break;
			}
		} catch (Exception e) {
			status.addStatus(Status.ERROR);
			log.error("Error during parse line:'" + Arrays.toString(line) + "' - current phrase option will be skiped", e);
			skipCurrentOption = true;
		}
	}

	/**
	 * Handles the first line of an option
	 * 
	 * @param line
	 */
	private void processOptionStart(String[] line) {

		if (currentOption != null) {
			endPhraseOption();
		}

		currentOption = textCatFactory.createPhraseOption(domainName);
		currentSubHeader = null;
		skipCurrentOption = false;
	}

	/**
	 * Handle structure line. This is the line with the
	 * {@link #OPTION_STRUCTURE} char in the first column.
	 * 
	 * @param line
	 *            the line
	 * @throws TextCatException 
	 */
	private void processOptionStructure(String[] line) throws TextCatException {

		if (StringUtil.isEmptyString(line[1]) == false) {
			currentOption.setName(StringUtil.extractFromCurlyBrackets(line[1]));
		}

		if (StringUtil.isEmptyString(line[2]) == false) {
			currentOption.setLanguage(Language.valueOf(line[2].toLowerCase()));
		}

		if (StringUtil.isEmptyString(line[3]) == false) {
			currentOption.setVersion(Version.create(line[3]));
		}

		if (StringUtil.isEmptyString(line[4]) == false) {
			currentOption.setBasisVersion(Version.create(line[4]));
		}

		currentOption.setDeleted(false);
	}

	/**
	 * Handles the remark line. This is the line with the
	 * {@value #OPTION_REMARK} char in the first column.
	 * 
	 * @param line
	 *            the line
	 */
	private void processOptionRemark(String[] line) {

		StringBuilder remark = new StringBuilder();

		for (int i = 1; i < line.length; i++) {

			if (StringUtil.isEmptyString(line[i]) == false) {
				remark.append(line[i]);
			}
		}

		if (remark.length() > 0) {
			currentOption.setRemark(remark.toString());
		}
	}

	/**
	 * Handles the header line. This is the line with the {@link #OPTION_HEADER}
	 * char in the first column.
	 * 
	 * @param line
	 *            the line
	 */
	private void processOptionHeader(String[] line) {

		String header = line[OPTION_CONTENT_COLUMN];

		if (StringUtil.isEmptyString(header) == false) {
			currentOption.setHeader(header);
		}
	}

	/**
	 * Handles the content lines. This are the lines with a digit in the first
	 * column.
	 * 
	 * @param line
	 *            the line
	 */
	private void processOptionContent(String[] line) {

		PhraseOptionItem poi = currentOption.createPhraseOptionItem();
		poi.setDeleted(false);
		poi.setItemNo(Integer.parseInt(line[0]));

		String subHeaderField = StringUtil.normalizeString(line[OPTION_SUBHEADER_COLUMN]);
		if (currentSubHeader != null && subHeaderField == null) {
			subHeaderField = currentSubHeader;
		} else {
			currentSubHeader = subHeaderField;
		}

		poi.setVersion(currentOption.getVersion());
		poi.setBasisVersion(currentOption.getBasisVersion());

		Phrase phrase = poi.createPhrase();
		phrase.setIncorrect(false);
		phrase.setItemPartNo(PHRASE_PART_NO);
		StringUtil.fillPhrase(line[OPTION_CONTENT_COLUMN], phrase);
		phrase.setValue(StringUtils.expandShortSubPhraseOptionName(phrase.getValue(), currentOption.getName()));
	}

	@Override
	public void endDocument() {

		if (currentOption != null) {
			endPhraseOption();
		}
	}

	@Override
	public AppStatus resetStatus() {
		AppStatus s = status;
		status = new AppStatus();
		return s;
	}

	@Override
	public void setDomainName(DomainName domainName) {
		this.domainName = domainName;
	}

	/**
	 * Post work after phrase option is parsed.
	 */
	private void endPhraseOption() {
		if (skipCurrentOption == false) {

			try {

				if (log.isDebugEnabled()) {
					log.debug("Start saveing phrase option: " + currentOption.getName());
				}
// TODO change hole import way
//				boAdministration.insertOrUpdatePhraseOption(currentOption);

				if (log.isDebugEnabled()) {
					log.debug("Finshed saveing phrase option: " + currentOption.getName());
				}

			} catch (Exception e) {
				log.error("Insert or update failed. PhraseOptionNo: " + (currentOption != null ? currentOption.getName() : "UNKNOWN"), e);
				status.addStatus(Status.ERROR);
			}

		} else {
			status.addStatus(Status.WARN);
			StringBuilder msg = new StringBuilder("Skip phrase option.");

			if (currentOption != null) {
				msg.append(" PhraseOptionNo: " + currentOption.getName());
			}

			log.warn(msg.toString());
		}

		// reset
		currentOption = null;
		currentSubHeader = null;
		skipCurrentOption = false;
	}

}
