package ch.slf.lwp.bulletinedit.textcat.dbloader.ttn;

public enum TTNLabel {
	ST_CurlyName, ST_Header, ST_OutNo, PA_Pos, PA_PosGerman, RS_CurlyName,  RS_Header, Line, Begin, End;

	//ST_Header, ST_OutNo, ST_Reserve1, ST_Reserve2, ST_Reserve3, PA_Pos, PA_PosGerman, PA_TriggerRangeText, RS_CurlyName, RG_Offset, RG_Text, RG_Label_on_SatzkatalogSheet, RS_WorkSheetName, RS_Address, RS_Text, Line;
}
