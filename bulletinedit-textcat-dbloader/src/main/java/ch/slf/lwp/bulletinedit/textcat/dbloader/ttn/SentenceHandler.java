package ch.slf.lwp.bulletinedit.textcat.dbloader.ttn;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;

import ch.slf.lwp.bulletinedit.textcat.Constants;
import ch.slf.lwp.bulletinedit.textcat.TextCatException;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructure;

/**
 * @author gw
 */
class SentenceHandler extends ContentHandler {

	List<ZipEntry> ttnSentences = new ArrayList<ZipEntry>();
	Map<String, PhraseOption> phraseOptions = null;

	void addZipEntry(ZipEntry entry) {
		ttnSentences.add(entry);
	}

	void setPhraseOptionMap(Map<String, PhraseOption> phraseOptions) {
		this.phraseOptions = phraseOptions;
	}

	List<Sentence> createSentences() throws Exception {

		if (ttnSentences.size() > 0 && phraseOptions == null) {
			throw new IllegalStateException("Can not create sentences without phrase options");
		}

		List<Sentence> sentences = new ArrayList<Sentence>();

		for (ZipEntry ttnSentence : ttnSentences) {
			Sentence sentence = parseSentenceEntry(ttnSentence);
			sentences.add(sentence);
		}

		return sentences;
	}

	private Sentence parseSentenceEntry(ZipEntry ttnSentence) throws Exception {

		Sentence sentence = textCatFactory.createSentence(domainName);
		sentence.setBasisVersion(basisVersion);
		sentence.setVersion(version);
		sentence.setLanguage(language);
		sentence.setStructure(new SentenceStructure());
		sentence.setDeleted(false);

		BufferedReader reader = new BufferedReader(new InputStreamReader(ttnZip.getInputStream(ttnSentence), TTNZipFileHandler.FILE_ENCODING));
		String line = null;

		TTNSentenceModule currentModule = null;
		
		ArrayList<TTNSentenceModule> ttnModules = new ArrayList<>();

		while ((line = reader.readLine()) != null) {
			
			if(line.trim().isEmpty()) {
				continue;
			}

            //utf-16 BOM
            line=line.replace("\uFEFF","");

            LabelValue labelValue = new LabelValue().set(line);

			switch (labelValue.getLabel()) {

			case ST_CurlyName: //ST_Header
				sentence.setName(labelValue.getValue());
				break;
			case ST_Header:
				sentence.setHeader(labelValue.getValue());
				break;
			case PA_Pos:
				// the module start
				currentModule = new TTNSentenceModule();
				currentModule.pa_pos = Integer.parseInt(labelValue.getValue());
				break;
			case PA_PosGerman:
				currentModule.pa_posGerman = Integer.parseInt(labelValue.getValue());
				break;
			case RS_CurlyName:
				currentModule.rs_curlyName = labelValue.getValue();
				ttnModules.add(currentModule);
				break;
		/*	case RG_Label_on_SatzkatalogSheet:
				currentModule.rg_Label_on_SatzkatalogSheet = labelValue.getValue();
				// this is also the module end
				ttnModules.add(currentModule);
				break;
		*/
			default:
				break;
			}
		}
		
		correctTTNModules(ttnModules);
		
		for(int i = 0; i < ttnModules.size(); i++) {
			// verification
			if ( (i + 1) != ttnModules.get(i).pa_pos) {
				throw new TextCatException("Invalide Moduel order in sentence '" + sentence.getName() + "' . Expected position: " + ttnModules.get(i).pa_pos + "; actual position: " + (i+1) + ".");
			}
			
			handleTTNModule(ttnModules.get(i), sentence);
		}

		return sentence;
	}
	
	private void correctTTNModules(ArrayList<TTNSentenceModule> ttnModules) {
		
		// sort/split into first part modules, second part modules, new modules
		List<TTNSentenceModule> newModules = new ArrayList<>();
		List<TTNSentenceModule> firstPartModules = new ArrayList<>();
		List<TTNSentenceModule> secondPartModules = new ArrayList<>();
		
		int maxGermanPosNo = 0;
		
		for (TTNSentenceModule ttnmodule : ttnModules) {
			if(ttnmodule.pa_posGerman != 0  ) {
				// only a existing first part modules has a german pos number
				firstPartModules.add(ttnmodule);
				
				// remember max german pos no
				if(ttnmodule.pa_posGerman > maxGermanPosNo) {
					maxGermanPosNo = ttnmodule.pa_posGerman;
				}
				
			} else {
				// second or new
				if(TTNUtils.getItemPartNoFromCurlyName(ttnmodule.rs_curlyName) > Constants.FIRST_ITEM_PART_NO ) {
					// a existing or new second part module
					secondPartModules.add(ttnmodule);
				} else {
					// a new first part module
					newModules.add(ttnmodule);
				}
			}
		}
		
		// add a valid german pos number to the new modules and add them to the frist part modules
		for (TTNSentenceModule newModule : newModules) {
			maxGermanPosNo += 1;
			newModule.pa_posGerman = maxGermanPosNo;
			firstPartModules.add(newModule);
		}
		
		// correct the german pos number for second part modules
		for (TTNSentenceModule secondPartModule : secondPartModules) {
			String normalizedCurlyName = TTNUtils.getNormalizedCurlyName(secondPartModule.rs_curlyName);
			
			// search the according first part module
			for (TTNSentenceModule firstPartModule : firstPartModules) {
				if(normalizedCurlyName.equals(firstPartModule.rs_curlyName)) {
					secondPartModule.pa_posGerman = firstPartModule.pa_posGerman;
					// there is only one according module
					break;
				}
			}
		}
		
		
	}

	private void handleTTNModule(TTNSentenceModule currentModule, Sentence sentence) throws TextCatException {

		int moduleNo = currentModule.pa_posGerman;
		int itemPartNo = TTNUtils.getItemPartNoFromCurlyName(currentModule.rs_curlyName);
		String normalizedCurlyName = TTNUtils.getNormalizedCurlyName(currentModule.rs_curlyName);

		PhraseOption phraseOption = phraseOptions.get(normalizedCurlyName);
		
		if(phraseOption != null) {
	
			sentence.getStructure().add(moduleNo, itemPartNo);
			sentence.addSentenceModule(moduleNo, phraseOption);
	
			if (itemPartNo == Constants.FIRST_ITEM_PART_NO) {
				// some additional stuff for the phrase option
				// if it is not the first part, the field values are not defined
				//phraseOption.setHeader(currentModule.rg_Label_on_SatzkatalogSheet);
			}
		} else {
			throw new TextCatException("Missing phrase Option '" + normalizedCurlyName + "' for sentence '" + sentence.getName() + "'");
		}
	}

	private static class TTNSentenceModule {

		private int pa_pos;
		private int pa_posGerman;
		private String rs_curlyName;
		private String rg_Label_on_SatzkatalogSheet;

	}

}
