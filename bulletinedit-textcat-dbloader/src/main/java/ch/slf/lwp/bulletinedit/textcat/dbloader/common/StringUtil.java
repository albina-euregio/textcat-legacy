package ch.slf.lwp.bulletinedit.textcat.dbloader.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import ch.slf.lwp.bulletinedit.textcat.model.Phrase;

public class StringUtil {

	/**
	 * Returns <code>true</code> if the line is empty. A line is empty if the
	 * first field is empty, when the line is null or the line.length is 0.
	 * 
	 * @param line
	 *            the line to check
	 * @return <code>true</code> if the line is empty
	 */
	public static boolean isEmptyLine(String[] line) {
		return line == null || line.length == 0 || isEmptyString(line[0]);
	}

	/**
	 * Returns <code>true</code> if s is null otherwise s.isEmpty() will be
	 * returned.
	 * 
	 * @param s
	 *            the String to test
	 * @return <code>true</code> if it is an empty string or null.
	 */
	public static boolean isEmptyString(String s) {
		return s == null || s.isEmpty();
	}

	/**
	 * Normalize the given string. Returns <code>null</code> if the string is
	 * empty, the string self otherwise.
	 * 
	 * @param s
	 *            the string to normalize
	 * @return the normalized string
	 */
	public static String normalizeString(String s) {
		return isEmptyString(s) ? null : s;
	}

	/**
	 * Coding/Decoding string
	 */
	private final static String char2digit = "_abcdefghijklmnopqrstuvwxyz";

	/**
	 * maps a char to a digit.
	 * 
	 * @param s
	 *            the char to map.
	 * @return the mapped digit.
	 */
	public static int char2digit(char s) {
		return StringUtil.char2digit.indexOf(s);
	}

	/**
	 * maps a digit to a char
	 * 
	 * @param d
	 *            the digit to map
	 * @return the mapped char
	 */
	public static char digit2char(int d) {
		return StringUtil.char2digit.toCharArray()[d];
	}

	/**
	 * Patter to extract phrase parameters
	 */
	private final static Pattern phrasePattern = Pattern.compile("(?:\\((-{0,2})\\))?\\s*(.*?)\\s*(?:\\((-?)\\))?$");

	/**
	 * Parse the phrase string and set the phrase parameters. This method set
	 * following phrase parameters:
	 * <ul>
	 * <li>{@link Phrase#setPhrase(String)}</li>
	 * <li>{@link Phrase#setSpaceAfter(boolean)}</li>
	 * <li>{@link Phrase#setSpaceBefore(boolean)}</li>
	 * <li>{@link Phrase#setRemovePunctuationBefore(boolean)}</li>
	 * </ul>
	 * 
	 * @param s
	 *            the phrase string to parse.
	 * @param phrase
	 *            the phrase to fill.
	 */
	public static void fillPhrase(String s, Phrase phrase) {

		Matcher phraseMatcher = phrasePattern.matcher(s);
		phraseMatcher.matches();

		String beforeFlags = phraseMatcher.group(1);
		String value = phraseMatcher.group(2);
		String afterFlags = phraseMatcher.group(3);

		if (LogManager.getLogger(StringUtil.class).isDebugEnabled()) {
			LogManager.getLogger(StringUtil.class).debug("Phrase file value: '" + s + "' extracted values: value: '" + value + "', before flags: '" + beforeFlags + "', after flags: '" + afterFlags + "'");
		}

		phrase.setValue(value);
		phrase.setRemovePunctuationBefore(beforeFlags != null && beforeFlags.length() == 2);
		phrase.setSpaceAfter(!(afterFlags != null && afterFlags.length() == 1));
		phrase.setSpaceBefore(!(beforeFlags != null && beforeFlags.length() >= 1));
	}

	/**
	 * Parse a string enclosed by curly brackets and extract the inner string
	 * value. Returns null if the string format is not like '{text}'.
	 * 
	 * @param s
	 *            the string
	 * @return the extracted value
	 */
	public static String extractFromCurlyBrackets(String s) {

		String value = null;

		if (s != null && s.startsWith("{") && s.endsWith("}")) {
			value = s.substring(1, s.length() - 1);

			if (LogManager.getLogger(StringUtil.class).isDebugEnabled()) {
				LogManager.getLogger(StringUtil.class).debug("Extraced text from '" + s + "' is : '" + value + "'");
			}

		}

		return value;
	}

	/**
	 * Returns the file name without extension
	 * 
	 * @param fileName
	 *            the file name with extension.
	 * @return the file name without extension.
	 * @throws IllegalArgumentException if the file name is <code>null</code>
	 */
	public static String removeFileExtension(String fileName) {
		
		if(fileName == null) {
			throw new IllegalArgumentException("File name is 'null'");
		}

		if (fileName.lastIndexOf(".") > -1) {
			return fileName.substring(0, fileName.lastIndexOf("."));
		}

		return fileName;

	}

}
