package ch.slf.lwp.bulletinedit.textcat.dbloader.ttn;

import ch.slf.lwp.bulletinedit.textcat.Constants;

/**
 * Contains some special ttn help functions.
 * 
 * @author gw
 */
public class TTNUtils {
	
	private TTNUtils(){}
	
	public static String EMPTY_VALUE = "[Empty]";

	/**
	 * Returns the label enumeration for the given label string. If the label
	 * does not exists, <code>null</code> will be returnd
	 * 
	 * @param label
	 * @return
	 */
	public static TTNLabel getLabel(String label) {

		if (label == null) {
			return null;
		}

		label = label.trim();

		try {
			return TTNLabel.valueOf(label);
		} catch (Exception e) {
			// nothing to do
		}

		return null;
	}

	private static String SECOND_ITEM_PART_NO_SUFFIX = "_NO";

	/**
	 * Returns the unique phrase option curly name without the itemPartNo suffix
	 * <code>_NO"</code>
	 * 
	 * @param rs_CurlyName
	 * @return
	 */
	public static String getNormalizedCurlyName(String rs_CurlyName) {

		if (rs_CurlyName == null || rs_CurlyName.isEmpty()) {
			throw new IllegalArgumentException("Null or empty String are not allowed!");
		}

		if (rs_CurlyName.endsWith(SECOND_ITEM_PART_NO_SUFFIX)) {
			rs_CurlyName = rs_CurlyName.substring(0, rs_CurlyName.length() - 3);
		}

		return rs_CurlyName;
	}

	/**
	 * Returns the itemPartNo according to the curlyName value.
	 * 
	 * @param rs_CurlyName
	 *            the curly name
	 * @return the itemPartNo
	 */
	public static int getItemPartNoFromCurlyName(String rs_CurlyName) {
		if (rs_CurlyName == null || rs_CurlyName.isEmpty()) {
			throw new IllegalArgumentException("Null or empty String are not allowed!");
		}

		int itemPartNo = Constants.FIRST_ITEM_PART_NO;

		if (rs_CurlyName.endsWith(SECOND_ITEM_PART_NO_SUFFIX)) {
			itemPartNo++;
		}

		return itemPartNo;
	}

}
