package ch.slf.lwp.bulletinedit.textcat.dbloader.ttn;

import java.util.zip.ZipFile;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.TextCatFactory;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

abstract class ContentHandler {

	DomainName domainName;
	Language language;
	Version version = null;
	Version basisVersion = null;
	TextCatFactory textCatFactory;

	ZipFile ttnZip;

	void setDomainName(DomainName domainName) {
		this.domainName = domainName;
	}

	void setLanguage(Language language) {
		this.language = language;
	}

	void setTtnZip(ZipFile ttnZip) {
		this.ttnZip = ttnZip;
	}

	void setVersion(Version version) {
		this.version = version;
	}

	void setBasisVersion(Version basisVersion) {
		this.basisVersion = basisVersion;
	}

	void setTextCatFactory(TextCatFactory textCatFactory) {
		this.textCatFactory = textCatFactory;
	}

}
