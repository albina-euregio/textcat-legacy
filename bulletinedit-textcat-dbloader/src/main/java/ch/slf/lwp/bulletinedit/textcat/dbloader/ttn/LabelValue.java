package ch.slf.lwp.bulletinedit.textcat.dbloader.ttn;

public class LabelValue {

	private TTNLabel label;
	private String value;

	public TTNLabel getLabel() {
		return label;
	}

	public String getValue() {
		return value;
	}

	public LabelValue set(String labelValue) {
		
		int sepIdx = labelValue.indexOf(":");
		
		if(sepIdx == -1) {
			throw new IllegalArgumentException(labelValue + " is none label value string.");
		}
		
		String sLabel = labelValue.substring(0, sepIdx).trim();
		String sValue = labelValue.substring(sepIdx+1).trim();

		this.label = TTNUtils.getLabel(sLabel);

		if (sValue != null && (sValue.equals(TTNUtils.EMPTY_VALUE) == false)) {

			this.value = sValue;

		} else {
			this.value = "";
		}

		return this;
	}

}
