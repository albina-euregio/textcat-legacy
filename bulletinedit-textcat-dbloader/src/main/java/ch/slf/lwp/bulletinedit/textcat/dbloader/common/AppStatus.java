package ch.slf.lwp.bulletinedit.textcat.dbloader.common;

/**
 * Represent the program status.
 * 
 * @author gw
 * 
 */
public class AppStatus {

	/**
	 * Possible application statuses
	 */
	public enum Status {
		OK, WARN, ERROR, FATAL
	}

	private Status totalStatus = Status.OK;

	/**
	 * Add a new status. The total status is only changed if the new status is
	 * greater then the current total status. The status order is
	 * <code>OK &lt; WARN &lt; ERROR &lt; FATAL</code>
	 * 
	 * @param status
	 *            the status to add
	 */
	public void addStatus(Status status) {

		if (totalStatus.compareTo(status) < 0) {
			totalStatus = status;
		}
	}

	/**
	 * Returns the total status of the application.
	 * 
	 * @return the total status
	 */
	public Status getTotalStatus() {
		return totalStatus;
	}

	/**
	 * Merge the given application status. The new total status is the greater
	 * one of both total statuses.
	 * 
	 * @param appStatus
	 *            the application status to merge
	 */
	public void mergeAppStatus(AppStatus appStatus) {
		addStatus(appStatus.getTotalStatus());
	}
	
	public AppStatus clone()
	{
		AppStatus s = new AppStatus();
		s.totalStatus = getTotalStatus();
		return s;
	}

}
