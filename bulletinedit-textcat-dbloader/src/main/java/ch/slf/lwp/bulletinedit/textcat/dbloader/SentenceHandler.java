package ch.slf.lwp.bulletinedit.textcat.dbloader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import ch.slf.lwp.bulletinedit.textcat.BoAdministration;
import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.StringUtils;
import ch.slf.lwp.bulletinedit.textcat.TextCatException;
import ch.slf.lwp.bulletinedit.textcat.TextCatFactory;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.AppStatus;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.StringUtil;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.AppStatus.Status;
import ch.slf.lwp.bulletinedit.textcat.model.Phrase;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOption;
import ch.slf.lwp.bulletinedit.textcat.model.PhraseOptionItem;
import ch.slf.lwp.bulletinedit.textcat.model.Sentence;
import ch.slf.lwp.bulletinedit.textcat.model.util.SentenceStructure;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

/**
 * Parse the sentence file and save the created sentence.
 * 
 * @author weiss
 */
public class SentenceHandler implements CsvContentHandler {

	// regex to parse phrase option structure info
	private final static String optionRegex = "(\\d+?)([a-z]?)(?:\\{(.+?)\\})??";

	private final static char SENTENCE_START = 'C';
	private final static char SENTENCE_STRUCTURE = 's';
	private final static char SENTENCE_TOPIC = 't';
	private final static char SENTENCE_KEYWORD = 'k';
	private final static char SENTENCE_REMARK = 'b';
	private final static char SENTENCE_HEADER = 'h';

	private final static int SENTENCE_CONTENT_START_INDEX = 5;
	private final static int MAX_BLANK_COLUMNS = 10;

	// injected textcat factory
	private TextCatFactory textCatFactory;
	private BoAdministration boAdministration;

	// private DaoPhraseOption phraseOptionService;
	// private DaoSentence sentenceService;

	private Matcher optionMatcher = Pattern.compile(optionRegex).matcher("");

	private final Logger log = LogManager.getLogger(this.getClass());
	private AppStatus status = new AppStatus();

	private DomainName domainName;

	/*
	 * stuff about the current sentence
	 */

	/* the current sentence */
	private Sentence currentSentence = null;

	/*
	 * contains on entry for each phrase option. If the phrase option is a split
	 * phrase option, the parts are connected as double linked list.
	 */
	private List<OptionHelper> currentOptionHelpers = null;

	private boolean skipCurrentSentence = false;

	/**
	 * @param textCatFactory
	 *            the textCatFactory to set
	 */
	public void setTextCatFactory(TextCatFactory textCatFactory) {
		this.textCatFactory = textCatFactory;
	}

	/**
	 * @param boAdministration
	 *            the boAdministration to set
	 */
	public void setBoAdministration(BoAdministration boAdministration) {
		this.boAdministration = boAdministration;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.dbloader.CsvDocumentHandler#resetStatus
	 * ()
	 */
	@Override
	public AppStatus resetStatus() {
		AppStatus s = status;
		status = new AppStatus();
		return s;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.dbloader.CsvDocumentHandler#startDocument
	 * ()
	 */
	@Override
	public void startDocument() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.dbloader.CsvDocumentHandler#processLine
	 * (java.lang.String[])
	 */
	@Override
	public void processLine(String[] line) {

		if (StringUtil.isEmptyLine(line)) {
			return; // nothing to do with a empty line
		}

		char key = line[0].charAt(0);

		if (skipCurrentSentence) {
			if (SENTENCE_START != key) {
				return;
			}
		}

		try {
			switch (key) {
			case SENTENCE_START:
				processSentenceStart(line);
				break;
			case SENTENCE_STRUCTURE:
				processSentenceStructure(line);
				break;
			case SENTENCE_TOPIC:
				processSentenceTopic(line);
				break;
			case SENTENCE_KEYWORD:
				processSentenceKeyword(line);
				break;
			case SENTENCE_REMARK:
				processSentenceRemark(line);
				break;
			case SENTENCE_HEADER:
				processSentenceHeader(line);
				break;
			default:
				processSentenceContent(line);
				break;
			}
		} catch (Exception e) {
			status.addStatus(Status.ERROR);
			log.error("Error during parse line:'" + Arrays.toString(line) + "' - current sentence will be skiped", e);
			skipCurrentSentence = true;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.slf.lwp.bulletinedit.textcat.dbloader.CsvDocumentHandler#endDocument()
	 */
	@Override
	public void endDocument() {
		if (currentSentence != null) {
			endSentence();
		}
	}

	@Override
	public void setDomainName(DomainName domainName) {
		this.domainName = domainName;

	}

	private void processSentenceStart(String[] line) {
		if (currentSentence != null) {
			endSentence();
		}

		skipCurrentSentence = false;
		currentSentence = textCatFactory.createSentence(domainName);
		currentOptionHelpers = new ArrayList<OptionHelper>();
	}

	/**
	 * Handles the structure line There is no validation during the reading
	 * process!
	 * 
	 * @param line
	 *            the structure line
	 * @throws TextCatException
	 *             if an error occurred by fetching a multiple usable phrase
	 *             option.
	 */
	private void processSentenceStructure(String[] line) throws TextCatException {

		// first read meta data

		// sentence no
		if (StringUtil.isEmptyString(line[1]) == false) {
			currentSentence.setName(line[1]);
		}

		// language
		if (StringUtil.isEmptyString(line[2]) == false) {
			currentSentence.setLanguage(Language.valueOf(line[2].toLowerCase()));
		}

		// version
		if (StringUtil.isEmptyString(line[3]) == false) {
			currentSentence.setVersion(Version.create(line[3]) );
		}

		// basis version
		if (StringUtil.isEmptyString(line[4]) == false) {
			currentSentence.setBasisVersion((Version.create(line[4])));
		}

		// memory for existing phrase option position numbers (this is the digit
		// part)
		Collection<Integer> positionNumbers = new HashSet<Integer>();
		// the sentence structure string to extend
		SentenceStructure sentenceStructure = new SentenceStructure();
		// number of sequenced blank columns
		int blankColumns = 0;

		// read phrase option info and prepare structure
		for (int columnIndex = SENTENCE_CONTENT_START_INDEX; columnIndex < line.length; columnIndex++) {

			if (StringUtil.isEmptyString(line[columnIndex]) == false) {
				blankColumns = 0;

				// handle phraseOption info
				handlePhraseOptionInfo(line[columnIndex], columnIndex, sentenceStructure, positionNumbers);

			} else {
				blankColumns++;
				if (blankColumns > MAX_BLANK_COLUMNS) {
					break;
				}
			}

		}

		currentSentence.setStructure(sentenceStructure);

	}

	/**
	 * Handles the topic line.
	 * 
	 * @param line
	 *            the topic line
	 */
	private void processSentenceTopic(String[] line) {

		for (int i = 1; i < line.length; i++) {

			if (StringUtil.isEmptyString(line[i]) == false) {
				currentSentence.addTopic(line[i]);
			}
		}

	}

	/**
	 * Handles the keyword line.
	 * 
	 * @param line
	 *            the keyword line
	 */
	private void processSentenceKeyword(String[] line) {

		for (int i = 1; i < line.length; i++) {

			if (StringUtil.isEmptyString(line[i]) == false) {
				currentSentence.addKeyword(line[i]);
			}
		}
	}

	/**
	 * Handles the remark line.
	 * 
	 * @param line
	 *            the remark line
	 */
	private void processSentenceRemark(String[] line) {

		StringBuilder remark = new StringBuilder();

		for (int i = 1; i < line.length; i++) {

			if (StringUtil.isEmptyString(line[i]) == false) {
				remark.append(line[i]);
			}
		}

		if (remark.length() > 0) {
			currentSentence.setRemark(remark.toString());
		}
	}

	private void processSentenceHeader(String[] line) {

		for (OptionHelper oh : currentOptionHelpers) {

			if (!oh.ignorePhrases()) {

				if (oh.columnIndex < line.length) {

					if (StringUtil.isEmptyString(line[oh.columnIndex]) == false) {
						oh.getPhraseOption().setHeader(line[oh.columnIndex]);
					}
				}
			}
		}
	}

	private void processSentenceContent(String[] line) {

		for (OptionHelper oh : currentOptionHelpers) {

			if (oh.ignorePhrases() == false) {
				// in the first step all rows are parsed for all options.
				PhraseOptionItem poi = oh.getPhraseOption().createPhraseOptionItem();
				poi.setDeleted(false);
				poi.setItemNo(Integer.parseInt(line[0]));
				poi.setVersion(oh.getPhraseOption().getVersion());
				poi.setBasisVersion(oh.getPhraseOption().getBasisVersion());

				do {
					Phrase phrase = poi.createPhrase();
					phrase.setIncorrect(false);
					phrase.setItemPartNo(oh.itemPartNo);
					StringUtil.fillPhrase(line[oh.columnIndex], phrase);
					phrase.setValue(StringUtils.expandShortSubPhraseOptionName(phrase.getValue(), currentSentence.getName()));
					oh = oh.next;
				} while (oh != null);

			}

		}

	}

	private void endSentence() {

		if (skipCurrentSentence == false) {

			try {
				// trim phrase options to remove the last empty items
				for (OptionHelper oh : currentOptionHelpers) {
					oh.getPhraseOption().trim();
				}

				if (log.isDebugEnabled()) {
					log.debug("Start saveing sentence: " + currentSentence.getName());
				}

				// call insert or update sentence
// TODO CHANGE hole import way
//				boAdministration.insertOrUpdateSentence(currentSentence);

				if (log.isDebugEnabled()) {
					log.debug("Finshed saveing phrase option: " + currentSentence.getName());
				}

			} catch (Exception e) {
				log.error("Insert or update failed. Sentence No: " + (currentSentence != null ? currentSentence.getName() : "UNKNOWN"), e);
				status.addStatus(Status.ERROR);
			}

		} else {
			status.addStatus(Status.WARN);
			StringBuilder msg = new StringBuilder("Skip Sentence.");

			if (currentSentence != null) {
				msg.append(" SentenceNo: " + currentSentence.getName());
			}

			log.warn(msg.toString());
		}

		skipCurrentSentence = false;
		currentSentence = null;
		currentOptionHelpers = null;
	}

	/**
	 * Parse the sentence structure phrase option field.
	 * 
	 * @param s
	 *            the field content
	 * @return the extracted option info
	 */
	OptionHelper extractOptionInfo(String s) {
		optionMatcher.reset(s);
		optionMatcher.matches();

		OptionHelper oh = new OptionHelper();
		// at least, the position number must be exists! otherwise there is a
		// big mistake inside the file!
		oh.moduleNo = Integer.parseInt(StringUtil.normalizeString(optionMatcher.group(1)));
		String itemPartNo = StringUtil.normalizeString(optionMatcher.group(2));
		oh.itemPartNo = StringUtil.char2digit(itemPartNo != null ? itemPartNo.charAt(0) : 'a');
		oh.optionName = StringUtil.normalizeString(optionMatcher.group(3));

		return oh;
	}

	/**
	 * Handles the phrase option info inside the sentence structure line.
	 * 
	 * @param phraseOptionInfo
	 *            the phrase option string
	 * @param columnIndex
	 *            the current line column index
	 * @param structure
	 *            the sentence structure
	 * @param positionNumbers
	 *            memory for existing phrase option position no
	 * @throws TextCatException
	 */
	private void handlePhraseOptionInfo(String phraseOptionInfo, int columnIndex, SentenceStructure sentenceStructure, Collection<Integer> positionNumbers) throws TextCatException {

		// parse option info
		OptionHelper oh = extractOptionInfo(phraseOptionInfo);
		oh.columnIndex = columnIndex;
		
		sentenceStructure.add(oh.moduleNo, oh.itemPartNo);

		/*
		 * one structure no can be exists multiple times, because of the
		 * possible phrase option item parts. However, there is only one
		 * according phrase option object.
		 */
		if (positionNumbers.contains(oh.moduleNo) == false) {
			currentOptionHelpers.add(oh);
			positionNumbers.add(oh.moduleNo);

			if (oh.optionName != null) {
				// load existing multiple usage able phrase option
				PhraseOption phraseOption = boAdministration.getMultiplePhraseOption(oh.optionName, currentSentence.getLanguage(), null /*TODO add basis version*/, domainName);
				
				if(phraseOption == null) {
					log.error("Unable to fetch multiple usage able phrase option. key: " + oh.optionName + ";" + currentSentence.getLanguage() + ";" + domainName);
					throw new TextCatException("Unable to fetch multiple usage able phrase option." );
				}
				
				oh.phraseOption = phraseOption;
				
				currentSentence.addSentenceModule(oh.moduleNo, phraseOption);
			} else {
				// create individual phrase option
				PhraseOption phraseOption = currentSentence.createSentenceModule(oh.moduleNo);
				phraseOption.setBasisVersion(currentSentence.getBasisVersion());
				phraseOption.setDeleted(false);
				phraseOption.setLanguage(currentSentence.getLanguage());
				phraseOption.setName(StringUtils.createUniquePhraseOptionNo(currentSentence.getName(), oh.moduleNo));
				phraseOption.setRemark(null);
				phraseOption.setVersion(currentSentence.getVersion());

				oh.phraseOption = phraseOption;
			}
		} else {
			/*
			 * search all ready created phrase option an connect it to this
			 * option info. The number of phrase option is small, therefore, I
			 * think, a stupid brute force search is ok.
			 */
			for (OptionHelper h : currentOptionHelpers) {
				if (h.moduleNo == oh.moduleNo) {
					h.add(oh);
					// found - stop search
					break;
				}
			}
		}
	}

	/**
	 * PRIVATE helper class to create the phrase options. This class is used
	 * really tough inside the sentence handler, especially related to
	 * validation. So be care full if you change something.
	 */
	static class OptionHelper {

		private OptionHelper() {
		}

		/** double linked list for split phrase options **/
		private OptionHelper next;
		private OptionHelper previous;

		/** phrase structure number (position) inside the sentence **/
		int moduleNo;
		/** phrase option part **/
		int itemPartNo;
		/** phrase option unique name **/
		String optionName;
		/** the column index **/
		int columnIndex;
		/** the according created phrase option **/
		private PhraseOption phraseOption;

		/**
		 * Returns <code>true</code> if the containing phrases can be ignored.
		 * If the phrase option has a unique name, the phrase option can be used
		 * multiple times, therefore the phrase option is all ready exists and
		 * the given phrases can be ignored.
		 * 
		 * @return <code>true</code> if the containing phrases can be ignored
		 */
		boolean ignorePhrases() {
			return optionName != null;
		}

		void add(OptionHelper h) {
			if (this.next != null) {
				this.next.add(h);
			} else {
				this.next = h;
				h.previous = this;
			}
		}

		/**
		 * The first option helper has a phrase option at least!
		 * 
		 * @return the phrase option for this position
		 */
		PhraseOption getPhraseOption() {
			if (this.phraseOption == null) {
				phraseOption = this.previous.getPhraseOption();
			}

			return phraseOption;
		}
	}

}
