package ch.slf.lwp.bulletinedit.textcat.dbloader;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.AppStatus;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.AppStatus.Status;

/**
 * Import text catalog
 * 
 * @author gw
 */
public class Import {

	private enum HANDLER {
		SENTENCE, PHRASEOPTION
	}

	// spring application context
	private static ApplicationContext ctx;

	private static Logger log = LogManager.getLogger(Import.class);
	private static AppStatus status = new AppStatus();

	private static final DomainName DEFAULT_DOMAIN = DomainName.PRODUCTION;
	// options
	private static final String OPTION_FILE = "f";
	private static final String OPTION_FILE_LONG = "file";

	private static final String OPTION_DOMAIN = "d";
	private static final String OPTION_DOMAIN_LONG = "domain";

	private static final String OPTION_SENTENCE = "s";
	private static final String OPTION_SENTENCE_LONG = "sentence";

	private static final String OPTION_PHRASEOPTION = "o";
	private static final String OPTION_PHRASEOPTION_LONG = "phrase-option";

	private static final String OPTION_HELP = "h";
	private static final String OPTION_HELP_LONG = "help";

	/**
	 * Initialize the command line options.
	 * 
	 * @return the command line options
	 */
	private static Options initOptions() {
		Options options = new Options();

		Option file = new Option(OPTION_FILE, OPTION_FILE_LONG, true, "Import files listed in FILE");
		file.setRequired(true);
		file.setArgName("FILE");
		options.addOption(file);

		Option domain = new Option(OPTION_DOMAIN, OPTION_DOMAIN_LONG, true, "The domain name. If no name is given, '" + DEFAULT_DOMAIN + "' used as domain name.");
		domain.setRequired(false);
		domain.setArgName("DOMAIN");
		options.addOption(domain);

		OptionGroup type = new OptionGroup();
		type.setRequired(true);

		Option typeSentence = new Option(OPTION_SENTENCE, OPTION_SENTENCE_LONG, false, "Sentence import");
		typeSentence.setRequired(true);
		type.addOption(typeSentence);

		Option typePhraseOption = new Option(OPTION_PHRASEOPTION, OPTION_PHRASEOPTION_LONG, false, "Pulldown import");
		typePhraseOption.setRequired(true);
		type.addOption(typePhraseOption);

		options.addOptionGroup(type);

		Option help = new Option(OPTION_HELP, OPTION_HELP_LONG, false, "give this help list");
		help.setRequired(false);
		options.addOption(help);

		return options;
	}

	/**
	 * Print help to the standard out.
	 * 
	 * @param options
	 *            the command line options
	 */
	public static void printHelp(Options options) {
		HelpFormatter hf = new HelpFormatter();
		hf.setWidth(80);
		hf.printHelp("Import", "\nSentence Catalog database import for sentences and pulldowns.\nOption list:", options, "", true);
	}

	/**
	 * Parse the file with the files to import.
	 * 
	 * @param filename
	 *            the file
	 * @return the files to import
	 * @throws IOException
	 */
	public static List<File> parseFileList(String filename) throws IOException {
		List<File> files = new ArrayList<File>();

		File file = new File(filename);

		if (file.isFile()) {

			BufferedReader reader = null;

			try {

				reader = new BufferedReader(new FileReader(file));
				String line;
				int lineNo = 0;

				while ((line = reader.readLine()) != null) {
					lineNo++;
					if(!line.trim().isEmpty()){
						File f = new File(line.trim());
	
						if (f.isFile()) {
							files.add(f);
						} else {
							log.warn("(line: " + lineNo + ") import file " + f.getAbsolutePath() + " is no file and will be ignored");
							status.addStatus(Status.WARN);
						}
					} else {
						log.info("(line: " + lineNo + ") the line is empty and will be ignored");
					}
				}

			} finally {
				if (reader != null) {
					closeStream(reader);
				}
			}

		} else {
			log.error(filename + " is no file!");
			status.addStatus(Status.ERROR);
		}

		return files;
	}

	private static void importFiles(List<File> files, DomainName domainName, HANDLER h) {

		CsvReader reader = new CsvReader();
		CsvContentHandler handler = null;

		if (h == HANDLER.SENTENCE) {
			handler = ctx.getBean(SentenceHandler.class);
		} else if (h == HANDLER.PHRASEOPTION) {
			handler = ctx.getBean(PhraseOptionHandler.class);
		} else {
			log.fatal("Unknown import handler type!");
			status.addStatus(Status.FATAL);
			return;
		}

		handler.setDomainName(domainName);
		reader.setHandler(handler);

		for (File file : files) {

			InputStream in = null;

			try {

				if (log.isDebugEnabled()) {
					log.debug("Start reading file: " + file.getAbsolutePath());
				}

				in = new FileInputStream(file);
				reader.parse(in);
			} catch (FileNotFoundException e) {
				status.addStatus(Status.ERROR);
				log.error("File not found: " + file.getAbsolutePath(), e);
			} catch (IOException e) {
				status.addStatus(Status.ERROR);
				log.error("Read file failed: " + file.getAbsolutePath(), e);
			} catch (Throwable t) {
				status.addStatus(Status.FATAL);
				log.error(t.getClass().getName() + ": Parse file failed: " + file.getAbsolutePath(), t);
			} finally {
				closeStream(in);
			}

			status.mergeAppStatus(handler.resetStatus());
		}
	}

	/**
	 * Close the stream an ignore possible exceptions.
	 * 
	 * @param stream
	 *            the stream to close
	 */
	private static void closeStream(Closeable stream) {
		try {
			stream.close();
		} catch (IOException e) {
			// nothing to do ... write something to stderr
			System.err.println("Can't close: " + stream.getClass().getName());
			e.printStackTrace(System.err);
		}
	}

	/**
	 * Start the import
	 * 
	 * @param args
	 *            the command line
	 */
	public static void main(String[] args) {
		// for nice command line output
		boolean printCmdlMessage = true;

		// read command line
		Options options = initOptions();

		try {
			// setup application context
			ctx = new ClassPathXmlApplicationContext("classpath*:META-INF/*spring.xml");

			if (log.isDebugEnabled()) {
				log.debug("Spring initalized");
			}

			CommandLineParser parser = new GnuParser();

			// check if help is required
			// CommandLine cmdl = parser.parse(new
			// Options().addOption(options.getOption(OPTION_HELP)), args);
			//
			// if (cmdl.hasOption(OPTION_HELP)) {
			// printHelp(options);
			// System.exit(0);
			// }
			// parse command line
			CommandLine cmdl = parser.parse(options, args);

			List<File> files = parseFileList(cmdl.getOptionValue(OPTION_FILE));

			DomainName domainName = Import.DEFAULT_DOMAIN;

			if (cmdl.hasOption(OPTION_DOMAIN)) {
				domainName = DomainName.valueOf(cmdl.getOptionValue(OPTION_DOMAIN).toUpperCase());
			}

			// start import
			if (cmdl.hasOption(OPTION_SENTENCE)) {
				importFiles(files, domainName, HANDLER.SENTENCE);
			} else if (cmdl.hasOption(OPTION_PHRASEOPTION)) {
				importFiles(files, domainName, HANDLER.PHRASEOPTION);
			} else {
				/*
				 * this should be dead code, because, in this case, a
				 * ParserException is already thrown by parser.parse(). However
				 * nobody knows ..
				 */
				log.fatal("Unknown import type!");
				status.addStatus(Status.FATAL);
			}

		} catch (ParseException e) {
			status.addStatus(Status.ERROR);
			System.out.println(e.getMessage() + "\n\n");
			printHelp(options);

			// skip finished status text and log file reference
			printCmdlMessage = false;
		} catch (IOException e) {
			status.addStatus(Status.FATAL);
			log.fatal("Can't read file list file.", e);
		} catch (Throwable t) {
			status.addStatus(Status.FATAL);
			log.fatal(t.getMessage(), t);
		}

		log.info("Import finished with status " + status.getTotalStatus());

		if (printCmdlMessage) {
			System.out.println("Import finished with status " + status.getTotalStatus());
			System.out.println("See logfile(s) for more details.");
		}

		System.exit(status.getTotalStatus().ordinal());
	}

}
