/**
 * 
 */
package ch.slf.lwp.bulletinedit.textcat.dbloader.ttn;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipFile;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import ch.slf.lwp.bulletinedit.textcat.BoAdministration;
import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import ch.slf.lwp.bulletinedit.textcat.TextCatLanguages.Language;

import ch.slf.lwp.bulletinedit.textcat.ModelTextCat;
import ch.slf.lwp.bulletinedit.textcat.TextCatException;
import ch.slf.lwp.bulletinedit.textcat.TextCatFactory;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.AppStatus;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.AppStatus.Status;
import ch.slf.lwp.bulletinedit.textcat.model.util.Version;

/**
 * @author gerd
 * 
 */
public class TTNTextCatImport {

	private final Logger log = LogManager.getLogger(this.getClass());

	// injected
	private BoAdministration boAdministration;
	private TextCatFactory textCatFactory;

	private DomainName domainName;
	private Version basisVersion;
	private Version previousBasisVersion;
	private boolean replace;

	private AppStatus status = new AppStatus();

	private List<TTNZipFileHandler> zipFileHandler = new ArrayList<>();

	/**
	 * @param boAdministration the boAdministration to set
	 */
	public void setBoAdministration(BoAdministration boAdministration) {
		this.boAdministration = boAdministration;
	}

	/**
	 * @param textCatFactory the textCatFactory to set
	 */
	public void setTextCatFactory(TextCatFactory textCatFactory) {
		this.textCatFactory = textCatFactory;
	}

	/**
	 * @param domainName the domainName to set
	 */
	public void setDomainName(DomainName domainName) {
		this.domainName = domainName;
	}

	/**
	 * @param basisVersion the basisVersion to set
	 */
	public void setBasisVersion(Version basisVersion) {
		this.basisVersion = basisVersion;
	}

	/**
	 * @return the replace
	 */
	public boolean isReplace() {
		return replace;
	}

	/**
	 * @param replace the replace to set
	 */
	public void setReplace(boolean replace) {
		this.replace = replace;
	}

	/**
	 * @return the previousBaseVersion
	 */
	public Version getPreviousBasisVersion() {
		return previousBasisVersion;
	}

	/**
	 * @param previousBasisVersion the previousBaseVersion to set
	 */
	public void setPreviousBasisVersion(Version previousBasisVersion) {
		this.previousBasisVersion = previousBasisVersion;
	}

	public void addZipFile(File file, Version version, Language language) throws IOException {
		log.info("Create importer:");
		log.info("Used domain name: " + domainName);
		log.info("Used import language: " + language);
		log.info("Used version: " + version.toString());
		log.info("Used basis version: " + basisVersion.toString());
		log.info("Import file: " + file.getAbsolutePath());

		TTNZipFileHandler zipHandler = new TTNZipFileHandler();
		zipHandler.setTtnZip(new ZipFile(file, Charset.forName("UTF-8"))); //ZipFile needs to have content filenames in UTF-8
		zipHandler.setLanguage(language);
		zipHandler.setDomainName(domainName);
		zipHandler.setVersion(version);
		zipHandler.setTextCatFactory(textCatFactory);
		zipHandler.setBasisVersion(basisVersion);

		zipFileHandler.add(zipHandler);
	}

	public AppStatus runImport() {

		ArrayList<ModelTextCat> textCats = new ArrayList<>();

		for (TTNZipFileHandler ttnZipFileHandler : zipFileHandler) {
			ModelTextCat t = ttnZipFileHandler.readZipFile();
			status.mergeAppStatus(ttnZipFileHandler.getAppStatus());

			if (status.getTotalStatus() == Status.OK) {
				textCats.add(t);
			} else {
				log.error("Read Zip file: " + zipFileHandler.toString() + " failed");
				status.addStatus(Status.ERROR);
				break;
			}
		}

		//


		if (status.getTotalStatus() == Status.OK) {
			try {
				if (replace) {
					boAdministration.replaceTextCat(previousBasisVersion, textCats.toArray(new ModelTextCat[0]));
				} else {
					boAdministration.updateTextCat(previousBasisVersion, textCats.toArray(new ModelTextCat[0]));
				}
			} catch (TextCatException e) {

				log.error("Import TextCat failed", e);
				try {
					boAdministration.deleteDomain(this.basisVersion,DomainName.INTEGRATION_TEST);
				} catch (TextCatException e1) {
					log.error("Delete of integration_test data failed");
				}
				status.addStatus(Status.ERROR);
			}
		}
		if (status.getTotalStatus() != Status.OK) {
            //reload textcat
		}
		return status;
	}

}
