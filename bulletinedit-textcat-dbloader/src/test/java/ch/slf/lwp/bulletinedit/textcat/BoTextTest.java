package ch.slf.lwp.bulletinedit.textcat;

import ch.slf.lwp.bulletinedit.textcat.model.DomainName;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class BoTextTest {
    static class Bulletins extends ArrayList<Bulletin> {
    }

    static class Bulletin {
        public String tendencyCommentTextcat;
        public String avActivityHighlightsTextcat;
        public String avActivityCommentTextcat;
        public String snowpackStructureCommentTextcat;
    }

    @Test
    @Ignore("requires database")
    public void test2020() {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("classpath*:META-INF/*spring.xml");
        BoText boTextSvc = ctx.getBean(BoText.class);
        Stream.iterate(LocalDate.of(2020, 3, 1), d -> d.plusDays(1))
                .limit(100)
                .forEach(date -> testDate(boTextSvc, date));
    }

    private void testDate(BoText boTextSvc, LocalDate date) {
        try {
            URL url = new URL("https://avalanche.report/albina_files/" + date + "/avalanche_report.json");
            List<Bulletin> bulletins = new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .readValue(url, Bulletins.class);
            for (Bulletin bulletin : bulletins) {
                String text = boTextSvc.buildText(bulletin.avActivityCommentTextcat, TextCatLanguages.Language.de, DomainName.PRODUCTION);
                System.out.println(date + " " + text);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
