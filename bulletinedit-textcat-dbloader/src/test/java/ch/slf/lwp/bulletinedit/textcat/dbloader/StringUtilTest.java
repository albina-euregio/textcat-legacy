package ch.slf.lwp.bulletinedit.textcat.dbloader;

import junit.framework.TestCase;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.StringUtil;
import ch.slf.lwp.bulletinedit.textcat.model.Phrase;

public class StringUtilTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testIsEmptyLine() {

		// null
		assertEquals("Null as line", true, StringUtil.isEmptyLine(null));

		// zero length
		assertEquals("Zero length", true, StringUtil.isEmptyLine(new String[] {}));

		// none zero length, but the first field is null
		assertEquals("Null in first field", true, StringUtil.isEmptyLine(new String[] { null, "banane" }));

		// none zero length, but the first field is empty
		assertEquals("Empty string in first field", true, StringUtil.isEmptyLine(new String[] { "", "banane" }));

		// none zero length
		assertEquals("None empty line", false, StringUtil.isEmptyLine(new String[] { "banane", "banane" }));
	}

	public void testIsEmptyString() {

		// null
		assertEquals("String is null", true, StringUtil.isEmptyString(null));

		// zero length
		assertEquals("String length is 0", true, StringUtil.isEmptyString(""));

		// none zero length
		assertEquals("None empty string", false, StringUtil.isEmptyString("banane"));
	}

	public void testNormalizeString() {

		// null
		assertEquals("String is null", null, StringUtil.normalizeString(null));

		// zero length
		assertEquals("String length is 0", null, StringUtil.normalizeString(""));

		// none zero length
		assertEquals("None empty string", "banane", StringUtil.normalizeString("banane"));
	}

	public void testChar2digit2char() {

		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		int[] digits = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 };

		for (int i = 0; i < chars.length; i++) {
			assertEquals("Test char2digit: " + chars[i], digits[i], StringUtil.char2digit(chars[i]));
		}

		for (int i = 0; i < digits.length; i++) {
			assertEquals("Test digit2char: " + digits[i], chars[i], StringUtil.digit2char(digits[i]));
		}

	}

	public void testExtractFromCurlyBrackets() {

		assertEquals("NULL string", null, StringUtil.extractFromCurlyBrackets(null));
		assertEquals("Empty string", null, StringUtil.extractFromCurlyBrackets(""));
		assertEquals("Header", "banane", StringUtil.extractFromCurlyBrackets("{banane}"));
		assertEquals("Wrong 1", null, StringUtil.extractFromCurlyBrackets("banane}"));
		assertEquals("Wrong 2", null, StringUtil.extractFromCurlyBrackets("{banane"));
		assertEquals("Empty Header", "", StringUtil.extractFromCurlyBrackets("{}"));

	}

	public void testRemoveFileExtension() {
		assertEquals("Name mit Pfad", "/name/mit/pfad/test", StringUtil.removeFileExtension("/name/mit/pfad/test.txt"));
		assertEquals("Name ohne Pfad", "test", StringUtil.removeFileExtension("test.txt"));
		assertEquals("Name ohen Extension", "test", StringUtil.removeFileExtension("test"));

		// exeption
		Exception expected = null;
		try {
			StringUtil.removeFileExtension(null);
		} catch (Exception e) {
			expected = e;
		}

		assertEquals("Expected exception", expected.getClass(), IllegalArgumentException.class);

	}

	public void testFillPhrase() {

		Phrase phrase;
		String s;

		phrase = new MockPhrase();
		s = "(--)banane - apfel(-)";
		StringUtil.fillPhrase(s, phrase);
		assertEquals("Test1 - phrase", "banane - apfel", phrase.getValue());
		assertEquals("Test1 - space before", false, phrase.isSpaceBefore());
		assertEquals("Test1 - remove punctuation before", true, phrase.isRemovePunctuationBefore());
		assertEquals("Test1 - space after", false, phrase.isSpaceAfter());

		phrase = new MockPhrase();
		s = "(-)banane - apfel(-)";
		StringUtil.fillPhrase(s, phrase);
		assertEquals("Test2 - phrase", "banane - apfel", phrase.getValue());
		assertEquals("Test2 - space before", false, phrase.isSpaceBefore());
		assertEquals("Test2 - remove punctuation before", false, phrase.isRemovePunctuationBefore());
		assertEquals("Test2 - space after", false, phrase.isSpaceAfter());

		phrase = new MockPhrase();
		s = "banane - apfel";
		StringUtil.fillPhrase(s, phrase);
		assertEquals("Test3 - phrase", "banane - apfel", phrase.getValue());
		assertEquals("Test3 - space before", true, phrase.isSpaceBefore());
		assertEquals("Test3 - remove punctuation before", false, phrase.isRemovePunctuationBefore());
		assertEquals("Test3 - space after", true, phrase.isSpaceAfter());

		phrase = new MockPhrase();
		s = "(--) banane - apfel";
		StringUtil.fillPhrase(s, phrase);
		assertEquals("Test4 - phrase", "banane - apfel", phrase.getValue());
		assertEquals("Test4 - space before", false, phrase.isSpaceBefore());
		assertEquals("Test4 - remove punctuation before", true, phrase.isRemovePunctuationBefore());
		assertEquals("Test4 - space after", true, phrase.isSpaceAfter());

		phrase = new MockPhrase();
		s = "(-) banane - apfel (-)";
		StringUtil.fillPhrase(s, phrase);
		assertEquals("Test5 - phrase", "banane - apfel", phrase.getValue());
		assertEquals("Test5 - space before", false, phrase.isSpaceBefore());
		assertEquals("Test5 - remove punctuation before", false, phrase.isRemovePunctuationBefore());
		assertEquals("Test5 - space after", false, phrase.isSpaceAfter());

		phrase = new MockPhrase();
		s = "";
		StringUtil.fillPhrase(s, phrase);
		assertEquals("Test6 - phrase", "", phrase.getValue());
		assertEquals("Test6 - space before", true, phrase.isSpaceBefore());
		assertEquals("Test6 - remove punctuation before", false, phrase.isRemovePunctuationBefore());
		assertEquals("Test6 - space after", true, phrase.isSpaceAfter());

		phrase = new MockPhrase();
		s = " bannane apfel -(-)";
		StringUtil.fillPhrase(s, phrase);
		assertEquals("Test7 - phrase", "bannane apfel -", phrase.getValue());
		assertEquals("Test7 - space before", true, phrase.isSpaceBefore());
		assertEquals("Test7 - remove punctuation before", false, phrase.isRemovePunctuationBefore());
		assertEquals("Test7 - space after", false, phrase.isSpaceAfter());

		phrase = new MockPhrase();
		s = "banane - apfel ";
		StringUtil.fillPhrase(s, phrase);
		assertEquals("Test8 - phrase", "banane - apfel", phrase.getValue());
		assertEquals("Test8 - space before", true, phrase.isSpaceBefore());
		assertEquals("Test8 - remove punctuation before", false, phrase.isRemovePunctuationBefore());
		assertEquals("Test8 - space after", true, phrase.isSpaceAfter());

		phrase = new MockPhrase();
		s = "(-)() banane - apfel )(-)";
		StringUtil.fillPhrase(s, phrase);
		assertEquals("Test9 - phrase", "() banane - apfel )", phrase.getValue());
		assertEquals("Test9 - space before", false, phrase.isSpaceBefore());
		assertEquals("Test9 - remove punctuation before", false, phrase.isRemovePunctuationBefore());
		assertEquals("Test9 - space after", false, phrase.isSpaceAfter());

		phrase = new MockPhrase();
		s = "()banane - apfel()";
		StringUtil.fillPhrase(s, phrase);
		assertEquals("Test10 - phrase", "banane - apfel", phrase.getValue());
		assertEquals("Test10 - space before", true, phrase.isSpaceBefore());
		assertEquals("Test10 - remove punctuation before", false, phrase.isRemovePunctuationBefore());
		assertEquals("Test10 - space after", true, phrase.isSpaceAfter());
	}

	private static class MockPhrase implements Phrase {

		private boolean spaceBefore;
		private boolean spaceAfter;
		private boolean removePunctuationBefore;
		private String value;
		private String rgn;

		@Override
		public int getId() {
			// not used
			return 0;
		}


		@Override
		public String getRgn() {
			return rgn;
		}

		@Override
		public void setRgn(String rgn) {
			this.rgn = rgn;

		}
		@Override
		public String getValue() {
			return value;
		}

		@Override
		public void setValue(String value) {
			this.value = value;

		}

		@Override
		public int getItemPartNo() {
			// not used
			return 0;
		}

		@Override
		public void setItemPartNo(int itemPartNo) {
			// not used
		}

		@Override
		public boolean isRemovePunctuationBefore() {
			return this.removePunctuationBefore;
		}

		@Override
		public void setRemovePunctuationBefore(boolean removePunctuationBefore) {
			this.removePunctuationBefore = removePunctuationBefore;
		}

		@Override
		public boolean isSpaceAfter() {
			return this.spaceAfter;
		}

		@Override
		public void setSpaceAfter(boolean spaceAfter) {
			this.spaceAfter = spaceAfter;
		}

		@Override
		public boolean isSpaceBefore() {
			return spaceBefore;
		}

		@Override
		public void setSpaceBefore(boolean spaceBefore) {
			this.spaceBefore = spaceBefore;
		}

		@Override
		public boolean isIncorrect() {
			// not used
			return false;
		}

		@Override
		public void setIncorrect(boolean incorrect) {
			// not used
		}

	}

}
