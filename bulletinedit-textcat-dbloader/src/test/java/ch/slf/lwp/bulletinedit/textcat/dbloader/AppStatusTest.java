package ch.slf.lwp.bulletinedit.textcat.dbloader;

import ch.slf.lwp.bulletinedit.textcat.dbloader.common.AppStatus;
import ch.slf.lwp.bulletinedit.textcat.dbloader.common.AppStatus.Status;
import junit.framework.TestCase;

public class AppStatusTest extends TestCase {

	AppStatus status = null;

	protected void setUp() throws Exception {
		super.setUp();
		status = new AppStatus();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		status = null;
	}

	public void testAddStatus() {
		assertEquals("Init check", Status.OK, status.getTotalStatus());

		// ok -------------------------------

		// actual OK --> add OK
		status.addStatus(Status.OK);
		assertEquals("actual OK --> add OK ==> OK", Status.OK, status.getTotalStatus());

		// warn -----------------------------

		// actual OK --> add WARN ==> WARN
		status.addStatus(Status.WARN);
		assertEquals("actual OK --> add WARN ==> WARN", Status.WARN, status.getTotalStatus());

		// actual WARN --> add OK ==> WARN
		status.addStatus(Status.OK);
		assertEquals("actual WARN --> add OK ==> WARN", Status.WARN, status.getTotalStatus());

		// actual WARN --> add WARN ==> WARN
		status.addStatus(Status.WARN);
		assertEquals("actual WARN --> add WARN ==> WARN", Status.WARN, status.getTotalStatus());

		// error ----------------------------

		// actual WARN --> add ERROR ==> ERROR
		status.addStatus(Status.ERROR);
		assertEquals("actual WARN --> add ERROR ==> ERROR", Status.ERROR, status.getTotalStatus());

		// actual ERROR --> add OK ==> ERROR
		status.addStatus(Status.OK);
		assertEquals("actual ERROR --> add OK ==> ERROR", Status.ERROR, status.getTotalStatus());

		// actual ERROR --> add WARN ==> ERROR
		status.addStatus(Status.WARN);
		assertEquals("actual ERROR --> add WARN ==> ERROR", Status.ERROR, status.getTotalStatus());

		// actual ERROR --> add ERROR ==> ERROR
		status.addStatus(Status.ERROR);
		assertEquals("actual ERROR --> add ERROR ==> ERROR", Status.ERROR, status.getTotalStatus());

		// fatal

		// actual ERROR --> add FATAL ==> FATAL
		status.addStatus(Status.FATAL);
		assertEquals("actual ERROR --> add FATAL ==> FATAL", Status.FATAL, status.getTotalStatus());

		// actual FATAL --> add OK ==> FATAL
		status.addStatus(Status.OK);
		assertEquals("actual FATAL --> add OK ==> FATAL", Status.FATAL, status.getTotalStatus());

		// actual FATAL --> add WARN ==> FATAL
		status.addStatus(Status.WARN);
		assertEquals("actual FATAL --> add WARN ==> FATAL", Status.FATAL, status.getTotalStatus());

		// actual FATAL --> add ERROR ==> FATAL
		status.addStatus(Status.ERROR);
		assertEquals("actual FATAL --> add ERROR ==> FATAL", Status.FATAL, status.getTotalStatus());

		// actual FATAL --> add FATAL ==> FATAL
		status.addStatus(Status.FATAL);
		assertEquals("actual FATAL --> add FATAL ==> FATAL", Status.FATAL, status.getTotalStatus());
	}

	public void testMergeAppStatus() {
		assertEquals("Init check", Status.OK, status.getTotalStatus());

		AppStatus other = new AppStatus();
		other.addStatus(Status.ERROR);

		status.mergeAppStatus(other);
		assertEquals("Merged", Status.ERROR, status.getTotalStatus());
	}

}
