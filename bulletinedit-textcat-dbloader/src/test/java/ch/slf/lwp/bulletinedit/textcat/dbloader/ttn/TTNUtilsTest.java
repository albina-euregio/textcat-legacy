package ch.slf.lwp.bulletinedit.textcat.dbloader.ttn;

import ch.slf.lwp.bulletinedit.textcat.Constants;
import junit.framework.TestCase;

public class TTNUtilsTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testGetLabel() {
		assertNull("Unknown label", TTNUtils.getLabel("test"));
		assertNull("Label is null", TTNUtils.getLabel(null));
		assertEquals("Known label", TTNLabel.Line, TTNUtils.getLabel(TTNLabel.Line.toString()));
	}
	
	public void testGetNormalizedCurlyName() {
		// null & empty string => illigalArgument exception
		Exception actual = null;
		
		try {
			TTNUtils.getNormalizedCurlyName(null);
		} catch (Exception e) {
			assertEquals(IllegalArgumentException.class, e.getClass());
			actual = e;
		}
		
		assertNotNull("Exception expected", actual);
		actual = null;
		
		try {
			TTNUtils.getNormalizedCurlyName("");
		} catch (Exception e) {
			assertEquals(IllegalArgumentException.class, e.getClass());
			actual = e;
		}
		
		assertNotNull("Exception expected", actual);
		actual = null;
		
		
		assertEquals("with suffix", "Schneedecke01§wo", TTNUtils.getNormalizedCurlyName("Schneedecke01§wo_NO"));
		assertEquals("without suffix", "Schneedecke01§wo", TTNUtils.getNormalizedCurlyName("Schneedecke01§wo"));
	}
	
	public void testGetItemPartNoFromCurlyName() {
		// null & empty string => illigalArgument exception
		Exception actual = null;
		
		try {
			TTNUtils.getItemPartNoFromCurlyName(null);
		} catch (Exception e) {
			assertEquals(IllegalArgumentException.class, e.getClass());
			actual = e;
		}
		
		assertNotNull("Exception expected", actual);
		actual = null;
		
		try {
			TTNUtils.getItemPartNoFromCurlyName("");
		} catch (Exception e) {
			assertEquals(IllegalArgumentException.class, e.getClass());
			actual = e;
		}
		
		assertNotNull("Exception expected", actual);
		actual = null;
		
		// now the values
		assertEquals("second item part", Constants.FIRST_ITEM_PART_NO + 1, TTNUtils.getItemPartNoFromCurlyName("Schneedecke01§wo_NO"));
		assertEquals("first item part", Constants.FIRST_ITEM_PART_NO, TTNUtils.getItemPartNoFromCurlyName("Schneedecke01§wo"));
	}

}
