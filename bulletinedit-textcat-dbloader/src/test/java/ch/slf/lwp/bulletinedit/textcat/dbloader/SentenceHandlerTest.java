package ch.slf.lwp.bulletinedit.textcat.dbloader;

import junit.framework.TestCase;
import ch.slf.lwp.bulletinedit.textcat.dbloader.SentenceHandler.OptionHelper;

public class SentenceHandlerTest extends TestCase {

	SentenceHandler handler;

	protected void setUp() throws Exception {
		super.setUp();
		handler = new SentenceHandler();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		handler = null;
	}

	public void testExtractOptionInfo() {

		String s = "1a{way}";

		OptionHelper oi = handler.extractOptionInfo(s);
		assertEquals("Position", 1, oi.moduleNo);
		assertEquals("Part", 1, oi.itemPartNo);
		assertEquals("OptionNo", "way", oi.optionName);

		s = "1";
		oi = handler.extractOptionInfo(s);
		assertEquals("Position", 1, oi.moduleNo);
		assertEquals("Part", 1, oi.itemPartNo);
		assertEquals("OptionNo", null, oi.optionName);

		s = "1a";
		oi = handler.extractOptionInfo(s);
		assertEquals("Position", 1, oi.moduleNo);
		assertEquals("Part", 1, oi.itemPartNo);
		assertEquals("OptionNo", null, oi.optionName);

		s = "1{way}";
		oi = handler.extractOptionInfo(s);
		assertEquals("Position", 1, oi.moduleNo);
		assertEquals("Part", 1, oi.itemPartNo);
		assertEquals("OptionNo", "way", oi.optionName);

		s = "10b{way banane}";
		oi = handler.extractOptionInfo(s);
		assertEquals("Position", 10, oi.moduleNo);
		assertEquals("Part", 2, oi.itemPartNo);
		assertEquals("OptionNo", "way banane", oi.optionName);

		/*
		 * the next two test must stay in this order, to show if the method
		 * works after an exception
		 */

		try {
			s = "10ab{way banane}";
			oi = handler.extractOptionInfo(s);
			fail("Exception expected");
		} catch (Exception e) {
		}

		s = "10b{way banane}";
		oi = handler.extractOptionInfo(s);
		assertEquals("Position", 10, oi.moduleNo);
		assertEquals("Part", 2, oi.itemPartNo);
		assertEquals("OptionNo", "way banane", oi.optionName);
	}

}
